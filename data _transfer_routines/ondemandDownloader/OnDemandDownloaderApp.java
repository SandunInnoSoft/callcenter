package ondemandDownload;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;




public class OnDemandDownloaderApp
{
  public OnDemandDownloaderApp() {}
  
  public static void main(String[] args)
  {
    try
    {
      File file = new File("TransferConfig.xml");
      
      DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
      Document document = documentBuilder.parse(file);
      String hostIp = document.getElementsByTagName("HostPBX_IP").item(0).getTextContent();
      Integer port = Integer.valueOf(Integer.parseInt(document.getElementsByTagName("HostPBX_Port").item(0).getTextContent()));
      String userName = document.getElementsByTagName("UserNamePBX_SSH").item(0).getTextContent();
      String password = document.getElementsByTagName("PasswordPBX_SSH").item(0).getTextContent();
      String audioFilesDirectoryNAS = document.getElementsByTagName("NAS_AudioFiles_Directory_Path").item(0).getTextContent();
      String pbxAudioFilesDirectory = document.getElementsByTagName("PBX_AudioFiles_Directory").item(0).getTextContent();
      Long scanWindowInSeconds = Long.valueOf(Long.parseLong(document.getElementsByTagName("scanWindow").item(0).getTextContent()));
      
      Session session = null;
      Channel channel = null;
      try
      {
        JSch ssh = new JSch();
        session = ssh.getSession(userName, hostIp, port.intValue());
        session.setPassword(password);
        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.connect();
        channel = session.openChannel("sftp");
        channel.connect();
        

        callDate = args[0];
        callTime = args[1];
        callType = args[2];
        extension = args[3];
        externalNumber = args[4];
        

        sftp = (ChannelSftp)channel;
        sftp.cd(pbxAudioFilesDirectory + extension);
        URI uri = new URI(pbxAudioFilesDirectory + extension);
        audioFilesList = sftp.ls(uri.getPath());
        
        audioFileMatchFound = false;
        
        for (x = 0; x < audioFilesList.size();) {
          if (!audioFileMatchFound) {
            String audioFileName = ((ChannelSftp.LsEntry)audioFilesList.get(x)).getFilename();
            if (audioFileName.length() > 2) {
              String[] responseArray = findMatchingFile(scanWindowInSeconds.longValue(), audioFileName, callDate + " " + callTime, extension, externalNumber, callType);
              if (responseArray != null)
              {
                audioFileMatchFound = true;
                String matchingFileName = responseArray[0];
                byte[] buffer = new byte['Ѐ'];
                try
                {
                  BufferedInputStream bis = new BufferedInputStream(sftp.get(matchingFileName));
                  String replacedAudioFilesDirectoryNAS = audioFilesDirectoryNAS;
                  replacedAudioFilesDirectoryNAS = replacedAudioFilesDirectoryNAS.replace("DATE", callDate.replace("-", "_"));
                  replacedAudioFilesDirectoryNAS = replacedAudioFilesDirectoryNAS.replace("EXTENSION", extension);
                  replacedAudioFilesDirectoryNAS = replacedAudioFilesDirectoryNAS + "\\" + matchingFileName;
                  
                  File newFile = new File(replacedAudioFilesDirectoryNAS);
                  newFile.getParentFile().mkdirs();
                  OutputStream os = new FileOutputStream(newFile);
                  BufferedOutputStream bos = new BufferedOutputStream(os);
                  
                  try
                  {
                    int readCount;
                    while ((readCount = bis.read(buffer)) > 0)
                    {
                      bos.write(buffer, 0, readCount);
                    }
                    
                    System.out.println("Audio_file_downloaded");
                    







                    bis.close();
                    bos.close();
                  }
                  catch (Exception sftpe)
                  {
                    System.out.println("Expection called when writing file " + replacedAudioFilesDirectoryNAS);
                    System.out.println(sftpe.toString());
                    sftpe.printStackTrace();
                    


                    bis.close();
                    bos.close();
                  }
                  finally
                  {
                    bis.close();
                    bos.close();
                  }
                  x++;





















                }
                catch (SftpException fileException)
                {





















                  System.out.println("sftp Exeption thrown while checking at the next file - " + audioFileName);
                  fileException.printStackTrace();
                  if (id == 2)
                    System.out.println(audioFileName + " skipped");
                }
              }
            }
          }
        }
      } catch (JSchException e) {
        String callDate;
        String callTime;
        String callType;
        String extension;
        String externalNumber;
        ChannelSftp sftp;
        Vector<ChannelSftp.LsEntry> audioFilesList;
        boolean audioFileMatchFound;
        int x;
        System.out.println("Jsch expection thrown");
        e.printStackTrace();
      } finally {
        if (channel != null)
        {
          channel.disconnect();
        }
        if (session != null)
        {
          session.disconnect();
        }
      }
    }
    catch (Exception e) {
      System.out.println("Global exception thrown");
      e.printStackTrace();
    }
  }
  






  private static String[] findMatchingFile(long scanWindowInSeconds, String audioFileName, String cdrDateTime, String extension, String externalNumber, String callType)
  {
    String[] responseArray = new String[2];
    
    String[] audioFileNameSplitted = audioFileName.split("-");
    String dateArea = audioFileNameSplitted[0];
    String timeArea = audioFileNameSplitted[1];
    
    externalNumber = externalNumber.trim();
    extension = extension.trim();
    audioFileNameSplitted[2] = audioFileNameSplitted[2].trim();
    audioFileNameSplitted[3] = audioFileNameSplitted[3].trim();
    
    boolean extensionCallerMatch = false;
    
    if (callType.matches("incoming"))
    {






      if ((externalNumber.matches(audioFileNameSplitted[2])) && (extension.matches(audioFileNameSplitted[3]))) {
        extensionCallerMatch = true;





      }
      






    }
    else if ((extension.matches(audioFileNameSplitted[2])) && (externalNumber.matches(audioFileNameSplitted[3]))) {
      extensionCallerMatch = true;
    }
    







    if (extensionCallerMatch == true) {
      int year = Integer.parseInt(dateArea.substring(0, 4));
      int month = Integer.parseInt(dateArea.substring(4, 6));
      int date = Integer.parseInt(dateArea.substring(6, 8));
      int hour = Integer.parseInt(timeArea.substring(0, 2));
      int minute = Integer.parseInt(timeArea.substring(2, 4));
      int seconds = Integer.parseInt(timeArea.substring(4, 6));
      
      String[] cdrAudioFileNameSplitted = cdrDateTime.split(" ");
      String cdrDateArea = cdrAudioFileNameSplitted[0].replace("-", "");
      String cdrTimeArea = cdrAudioFileNameSplitted[1].replace(":", "");
      

      int cdrRecordYear = Integer.parseInt(cdrDateArea.substring(0, 4));
      int cdrRecordMonth = Integer.parseInt(cdrDateArea.substring(4, 6));
      int cdrRecordDate = Integer.parseInt(cdrDateArea.substring(6, 8));
      int cdrRecordHour = Integer.parseInt(cdrTimeArea.substring(0, 2));
      int cdrRecordMinute = Integer.parseInt(cdrTimeArea.substring(2, 4));
      int cdrRecordSeconds = Integer.parseInt(cdrTimeArea.substring(4, 6));
      
      try
      {
        LocalDateTime cdrRecordDateTime = LocalDateTime.of(cdrRecordYear, cdrRecordMonth, cdrRecordDate, cdrRecordHour, cdrRecordMinute, cdrRecordSeconds);
        LocalDateTime toTime = cdrRecordDateTime.plusSeconds(scanWindowInSeconds);
        LocalDateTime fromTime = cdrRecordDateTime.minusSeconds(scanWindowInSeconds);
        
        LocalDateTime audioFileDateTime = LocalDateTime.of(year, month, date, hour, minute, seconds);
        




        if ((audioFileDateTime.isAfter(fromTime)) && (audioFileDateTime.isBefore(toTime)))
        {

          responseArray[0] = audioFileName;
        }
        else {
          responseArray = null;
        }
      }
      catch (Exception e)
      {
        e = 
        






          e;System.out.println("Exception thrown at within time period check");System.out.println(e.toString());e.printStackTrace();responseArray = null;
      } finally {}
    } else {
      responseArray = null;
    }
    

    return responseArray;
  }
}
