package callcenteremail;

import javax.mail.*;
import javax.mail.internet.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;


public class Emailer {

	/**
	 * <b>Sends audio files missing notification to the provided list of recipients</b>
	 * 
	 * @param subject
	 * @param missedCallsList
	 * @param recipients
	 * @throws Exception
	 * 
	 * @author Sandun
	 * @since 2019-02-05
	 */
    public void sendEmail(String subject, ArrayList<Map> missedCallsList, ArrayList<String> recipients) throws Exception {
    	
    	if(recipients.size() > 0) {
    		// has recipients
	    	SMTPConfigurations smtpConfigurations = new SMTPConfigurations();
	    	
	        Properties props = new Properties();
	        props.put("mail.transport.protocol", "smtp");
	        props.put("mail.smtp.host", smtpConfigurations.getSmtpHost());
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.port", smtpConfigurations.getSmtpPort());
	        props.put("mail.smtp.starttls.enable", "true"); 
	        props.put("mail.smtp.ssl.trust", "*");
	        
	
	        Authenticator auth = new SMTPAuthenticator();
	        Session mailSession = Session.getDefaultInstance(props, auth);
	
	        Transport transport = mailSession.getTransport();
	        
	        String emailBody = buildEmailBody(missedCallsList);
	
	        MimeMessage message = new MimeMessage(mailSession);
	        subject = (subject == null) ? smtpConfigurations.getMissedFilesEmailSubject() : subject;
	        message.setSubject(subject, "text/plain");
	        message.setContent(emailBody, "text/html");
	        message.setFrom(new InternetAddress(smtpConfigurations.getFromAddress()));
	        
	        
	        for(int x = 0; x < recipients.size(); x++) {
	        	message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients.get(x)));
	        }
	        
	            transport.connect();
	            transport.sendMessage(message,
	                message.getRecipients(Message.RecipientType.TO));
	            
	            System.out.println("Email sent !");
	            transport.close();
        }else {
        	// has no recipents
        	System.out.println("No recipients available");
        }
    }
    
    
    /**
     * 
     * @param missedCallsList
     * @return String HTML table body of the email with data
     * 
     * @author Sandun
     * @since 2019-02-05
     */
    private static String buildEmailBody (ArrayList<Map> missedCallsList) {
    	ExtensionsUtil extensionsUtil = new ExtensionsUtil();
    	
    	String emailBodyTable = "";
    	
    	emailBodyTable += "<h2>List of audio file unavailable call records</h2>";
    	emailBodyTable += "<br><br>";
    	emailBodyTable += "<table width='600' style='border: 1px solid black; width: 100%' >";
		emailBodyTable += "<thead>";
		emailBodyTable += "<tr>";
		emailBodyTable += "<th style='border: 1px solid black' >#</th>";
		emailBodyTable += "<th style='border: 1px solid black' >Start</th>";
		emailBodyTable += "<th style='border: 1px solid black' >End</th>";
		emailBodyTable += "<th style='border: 1px solid black' >Direction</th>";
		emailBodyTable += "<th style='border: 1px solid black' >Source</th>";
		emailBodyTable += "<th style='border: 1px solid black' >Destination</th>";
		emailBodyTable += "<th style='border: 1px solid black' >Duration</th>";
//		emailBodyTable += "<th style='border: 1px solid black' >Ext Queue</th>";
//		emailBodyTable += "<th style='border: 1px solid black' >IVR Channel</th>";
		emailBodyTable += "</tr>";
		emailBodyTable += "</thead>";
		emailBodyTable += "<tbody>";
		for(int x = 0; x < missedCallsList.size() ; x++) {
			Map missingAudiorecordData = missedCallsList.get(x);
//			String extensionQueueName = "-";
//			if(missingAudiorecordData.get("callType").toString().matches("Incoming")) {
//				extensionQueueName = extensionsUtil.getQueueNameOfExtension(missingAudiorecordData.get("src").toString());
//			}else {
//				extensionQueueName = extensionsUtil.getQueueNameOfExtension(missingAudiorecordData.get("dst").toString());
//			}
//			
//			extensionQueueName = (extensionQueueName == null ? "-" : extensionQueueName);
//			
//			String callChannelName = extensionsUtil.getChannelName(missingAudiorecordData.get("channel").toString());
//			callChannelName = (callChannelName == null ? "-" : callChannelName);
			
			emailBodyTable += "<tr>";
			emailBodyTable += "<th style='border: 1px solid black' >"+(x+1)+"</th>";
			emailBodyTable += "<td style='border: 1px solid black' >"+missingAudiorecordData.get("start")+"</td>";
			emailBodyTable += "<td style='border: 1px solid black' >"+missingAudiorecordData.get("end")+"</td>";
			emailBodyTable += "<td style='border: 1px solid black' >"+missingAudiorecordData.get("callType")+"</td>";
			emailBodyTable += "<td style='border: 1px solid black' >"+missingAudiorecordData.get("src")+"</td>";
			emailBodyTable += "<td style='border: 1px solid black' >"+missingAudiorecordData.get("dst")+"</td>";
			emailBodyTable += "<td style='border: 1px solid black' >"+missingAudiorecordData.get("duration")+"</td>";
//			emailBodyTable += "<td style='border: 1px solid black' >"+extensionQueueName+"</td>";
//			emailBodyTable += "<td style='border: 1px solid black' >"+callChannelName+"</td>";
			emailBodyTable += "</tr>";
		}
		emailBodyTable += "</tbody>";
		emailBodyTable += "</table>";
    	
		emailBodyTable += "<br><br>";
		emailBodyTable += "<h3>This is an autogenerated email. Please do not reply</h3>";
    	return emailBodyTable;
    }
    

    private class SMTPAuthenticator extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
        	SMTPConfigurations smtpConfigurations = new SMTPConfigurations();
        	
	        String username = smtpConfigurations.getSmtpUsername();
	        String password = smtpConfigurations.getSmtpPassword();
	        return new PasswordAuthentication(username, password);
        }
    }
}