package callcenteremail;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

public class SMTPConfigurations {

	private String smtpHost;
	private String smtpPort;
	private String smtpUsername;
	private String smtpPassword;
	private String fromAddress;
	private String missedFilesEmailSubject;
	
	public SMTPConfigurations() {
		try {
			File smtpConfigFile = new File("SMTP_config.xml");
			
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document smtpConfigDocument = documentBuilder.parse(smtpConfigFile);
			
			this.smtpHost = smtpConfigDocument.getElementsByTagName("SMTP_HOST").item(0).getTextContent();
			this.smtpPort = smtpConfigDocument.getElementsByTagName("SMTP_PORT").item(0).getTextContent();
			this.smtpUsername = smtpConfigDocument.getElementsByTagName("SMTP_USERNAME").item(0).getTextContent();
			this.smtpPassword = smtpConfigDocument.getElementsByTagName("SMTP_PASSWORD").item(0).getTextContent();
			this.fromAddress = smtpConfigDocument.getElementsByTagName("FROM_ADDRESS").item(0).getTextContent();
			this.missedFilesEmailSubject = smtpConfigDocument.getElementsByTagName("MISSED_FILE_EMAIL_SUBJECT").item(0).getTextContent();
			
			smtpConfigFile = null;
			documentBuilderFactory = null;
			documentBuilder = null;
			smtpConfigDocument = null;
			
		}catch(Exception e) {
			System.out.println("4005 - Exception occurred at SMTP obj construction");
			System.out.println(e.toString());
		}
	}

	/**
	 * @return the fromAddress
	 */
	public String getFromAddress() {
		return fromAddress;
	}

	/**
	 * @return the smtpHost
	 */
	public String getSmtpHost() {
		return smtpHost;
	}

	/**
	 * @return the smtpPort
	 */
	public String getSmtpPort() {
		return smtpPort;
	}

	/**
	 * @return the smtpUsername
	 */
	public String getSmtpUsername() {
		return smtpUsername;
	}

	/**
	 * @return the smtpPassword
	 */
	public String getSmtpPassword() {
		return smtpPassword;
	}

	/**
	 * @return the missedFilesEmailSubject
	 */
	public String getMissedFilesEmailSubject() {
		return missedFilesEmailSubject;
	}

	
	
	
	
	
}
