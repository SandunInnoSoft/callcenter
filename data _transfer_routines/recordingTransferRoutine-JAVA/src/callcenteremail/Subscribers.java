package callcenteremail;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import callcenteraudiotransferroutine.TransferConfigurations;

/**
 * <b>keeps the subscribers information</b>
 * @author Sandun
 * @since 2019-02-01
 *
 */
public class Subscribers {
	

	private ArrayList<String> subscriberEmailAddresses = new ArrayList<String>();
	
	public Subscribers() {
        TransferConfigurations config = new TransferConfigurations();
        
        String ipAddress = config.getMainDbIpAddress();
        Integer port = config.getMainDbPort();
        String database = config.getMainDbName();
        String username = config.getMainDbUser();
        String password = config.getMainDbPassword();
        
        String connectionString = "jdbc:mysql://"+ipAddress+":"+port+"/"+database;
        
		try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(connectionString, username, password);
            
            Statement statement = con.createStatement();
            
            String query = "SELECT subsciber_name, email_address FROM missed_calls_email_subscribers WHERE status = 'active'";
            
            ResultSet result = statement.executeQuery(query);

            while(result.next() == true) {
            	subscriberEmailAddresses.add(result.getString("email_address"));
            }
                        
            con.close();
            statement = null;
            result = null;
            
		}catch(Exception e) {
			System.out.println("2050 - Exception occurred at creating email subscribers object ");
			System.out.println(e.toString());
			e.printStackTrace();
		}
	}

	/**
	 * @return the list of active subscriber email addresses
	 */
	public ArrayList<String> getSubscriberEmailAddresses() {
		return subscriberEmailAddresses;
	}

	
	

}
