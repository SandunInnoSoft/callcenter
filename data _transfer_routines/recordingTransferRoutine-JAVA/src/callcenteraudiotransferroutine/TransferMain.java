/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package callcenteraudiotransferroutine;


/**
 *<b>Main method class</b>
 *
 * @author Sandun
 * @since 2018-07-10
 * 
 */
public class TransferMain {
	
	/**
	 * <b>Main method<b/>
	 * 
	 * @param args
	 * 
	 * @author Sandun
	 * @since 2018-07-10
	 */
    public static void main(String[] args) {
        TransferThread audioTransfer = new TransferThread();
        audioTransfer.routinesSchedule();
    }
}
