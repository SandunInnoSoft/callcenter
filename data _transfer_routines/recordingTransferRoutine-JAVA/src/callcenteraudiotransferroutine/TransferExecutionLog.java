//package callcenteraudiotransferroutine;
//
//public class TransferExecutionLog {
//
//}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package callcenteraudiotransferroutine;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <b>This class is used to commit the log messages of the routine in logs DB</b>
 * 
 * @author Sandun
 * @since 2019-01-18
 */
public class TransferExecutionLog {
   
	/**
	 * <b>Inserts the audio transfer execution log information record in audioTransferRoutineLog table</b>
	 * 
	 * @param numberOfFilesCopied
	 * @param startedTime
	 * @param endedTime
	 * 
	 * @author Sandun
	 * @since 2019-01-18
	 */
    public void commitTransferExecutionLog(int numberOfFilesCopied, String startedTime, String endedTime, int numberOfMissedFiles){
        TransferConfigurations transferConfig = new TransferConfigurations();
        String ipAddress = transferConfig.getLog_mysql_CDR_DB_IP();
        String port = transferConfig.getLog_mysql_CDR_DB_Port();
        String database = transferConfig.getLog_mysql_CDR_DB_Name();
        String username = transferConfig.getLog_mysql_CDR_DB_Username();
        String password = transferConfig.getLog_mysql_CDR_DB_Password();
        
        String connectionString = "jdbc:mysql://"+ipAddress+":"+port+"/"+database;
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(connectionString, username, password);
            
            Statement statement = con.createStatement();
            int rs = statement.executeUpdate("INSERT INTO `audioTransferRoutineLog` (`copiedFilesCount`, `executionStartedTime`, `executionEndedTime`, `missedFilesCount`) VALUES ('"+numberOfFilesCopied+"', '"+startedTime+"', '"+endedTime+"', '"+numberOfMissedFiles+"')");
            
        }catch(Exception e){
            System.out.println("Exception occurred at writing transfer execution log");
            System.out.println(e.toString());
        }finally{
            transferConfig = null;
        }
    }
}
