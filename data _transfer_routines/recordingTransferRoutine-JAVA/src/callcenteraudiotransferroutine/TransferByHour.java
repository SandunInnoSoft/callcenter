
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package callcenteraudiotransferroutine;

import java.io.File;
//import java.io.BufferedReader;
//import java.io.InputStreamReader;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import java.util.Vector;
import java.net.URI;
//import java.text.SimpleDateFormat;
//import java.util.Date;
import java.time.LocalDateTime;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.BufferedWriter;
import java.io.FileWriter;


/**
 *
 * @author Sandun
 * @since 2018-07-04
 */
public class TransferByHour {
    
    private static boolean isCopying = false;
//    private int numberOfAudioFilesCopied = 0;

    /**
     * <b>Audio file transfer function</b>
     * @param args
     * 
     * @author Sandun
     * @since 2018-07-04
     */
    public static int initiateTransfer() {
    	
    	int numberOfAudioFilesCopied = 0;
    	
        try{
            File file = new File("TransferConfig.xml");

            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            String hostIp = document.getElementsByTagName("HostPBX_IP").item(0).getTextContent();
            Integer port = Integer.parseInt(document.getElementsByTagName("HostPBX_Port").item(0).getTextContent());
            String userName = document.getElementsByTagName("UserNamePBX_SSH").item(0).getTextContent();
            String password = document.getElementsByTagName("PasswordPBX_SSH").item(0).getTextContent();
            String audioFilesDirectoryNAS = document.getElementsByTagName("NAS_AudioFiles_Directory_Path").item(0).getTextContent();
            String eachHours = document.getElementsByTagName("EachHours").item(0).getTextContent();
            String pbxAudioFilesDirectory = document.getElementsByTagName("PBX_AudioFiles_Directory").item(0).getTextContent();
            
            Session session = null;
            Channel channel = null;
            
            try {
                JSch ssh = new JSch();
                session = ssh.getSession(userName, hostIp, port);
                session.setPassword(password);
                java.util.Properties config = new java.util.Properties(); 
                config.put("StrictHostKeyChecking", "no");
                session.setConfig(config);
                session.connect();
                channel = session.openChannel("sftp");
                channel.connect();
                System.out.println("Connected to "+hostIp);
                ChannelSftp sftp = (ChannelSftp) channel;
                sftp.cd(pbxAudioFilesDirectory);
                URI uri = new URI(pbxAudioFilesDirectory);
                Vector<LsEntry> extensionsFolderList = sftp.ls(uri.getPath());
                
                System.out.println("Number of extentions = "+extensionsFolderList.size());
                System.out.println("----------------------------------------------------");
                
                if(isCopying == false){
                    // another routine is not running, no copying takes place
                    isCopying = true;
                    TransferByHour.writeLog("Transfer routine started");
                    byte[] buffer = new byte[1024];                    

                    for(int x = 0; x < extensionsFolderList.size(); x++){                        
                        String extension = extensionsFolderList.get(x).getFilename();
                        if(extension.length() > 2){
                            Vector<LsEntry> audioFilesList = sftp.ls(uri.getPath()+"/"+extension);
                            System.out.println("Now in "+extension+" = Number of audio files"+audioFilesList.size());
                            System.out.println("--------------------------------------------------------------------");
                            sftp.cd(pbxAudioFilesDirectory+"/"+extension);
                            for(int y = 0; y < audioFilesList.size() ; y++){
                                String audioFileName = audioFilesList.get(y).getFilename();
                                if(audioFileName.length() > 2){
                                    String[] response = TransferByHour.isWithinTimePeriod(Integer.parseInt(eachHours), audioFileName);
                                    if(response != null){
                                        // can copy
                                        try{
                                            BufferedInputStream bis = new BufferedInputStream(sftp.get(audioFilesList.get(y).getFilename()));
                                            String replacedAudioFilesDirectoryNAS = audioFilesDirectoryNAS;
                                            replacedAudioFilesDirectoryNAS = replacedAudioFilesDirectoryNAS.replace("DATE", response[0]);
                                            replacedAudioFilesDirectoryNAS = replacedAudioFilesDirectoryNAS.replace("EXTENSION", extension);
                                            replacedAudioFilesDirectoryNAS = replacedAudioFilesDirectoryNAS+"\\"+audioFilesList.get(y).getFilename();
                                            System.out.println("File path to Save to - "+replacedAudioFilesDirectoryNAS);
                                            File newFile = new File(replacedAudioFilesDirectoryNAS);
                                            newFile.getParentFile().mkdirs();
                                            OutputStream os = new FileOutputStream(newFile);
                                            BufferedOutputStream bos = new BufferedOutputStream(os);

                                            try{
                                                System.out.println("Writing "+audioFileName);
                                                int readCount;
                                                while ((readCount = bis.read(buffer)) > 0) {
    
                                                    bos.write(buffer, 0, readCount);
                                                }
                                                
                                                System.out.println("Audio file written to "+replacedAudioFilesDirectoryNAS);
                                                TransferByHour.writeLog(audioFilesList.get(y).getFilename()+" Audio file copied to "+replacedAudioFilesDirectoryNAS);
                                                numberOfAudioFilesCopied++;

                                            }catch(Exception sftpe){
                                                System.out.println("Expection called when writing file "+replacedAudioFilesDirectoryNAS);
                                                TransferByHour.writeLog("Expection called when writing file "+replacedAudioFilesDirectoryNAS);
                                                System.out.println(sftpe.toString());
                                                sftpe.printStackTrace();
                                            }finally{
                                                System.out.println("Audio file "+audioFileName+" writing finished");
                                                System.out.println("************************************************");
                                                TransferByHour.writeLog("Audio file "+audioFileName+" writing finished");
                                                TransferByHour.writeLog("************************************************");
                                                bis.close();
                                                bos.close();
                                                continue;
                                            }

                                        }catch(SftpException fileException){
                                            System.out.println("sftp Exeption thrown while checking at the next file - "+audioFileName);
                                            TransferByHour.writeLog("sftp Exeption thrown while checking at the next file - "+audioFileName);

                                            fileException.printStackTrace();
                                            if(fileException.id == ChannelSftp.SSH_FX_NO_SUCH_FILE){
                                                System.out.println(audioFileName+" skipped");
                                                continue;
                                            }
//                                            continue;
                                        }                                        
                                    }else{
                                        System.out.println(audioFileName+" isWithinTimePeriod response is null");
                                        audioFileName = null;
                                    }
                                }else{
                                    System.out.println("Audio file name is . or ..");
                                    audioFileName = null;
                                }
                            }
                            System.out.println("Writing extension "+extension+" complete : loop index - "+x);

                            audioFilesList = null;
                        }else{
                            System.out.println("Extension is . or ..");
                            extension = null;
                        }

                        extension = null;
                    }
                    isCopying = false;
                }else{
                    System.out.println("Another instance is already running");
                }
                
                

            } catch (JSchException e) {
                System.out.println("Jsch expection thrown");
                e.printStackTrace();
            } 

            finally {
                if (channel != null) {
                    System.out.println("Channel disconnected");
                    channel.disconnect();
                }
                if (session != null) {
                    System.out.println("Session disconnected");
                    session.disconnect();
                }
                
                System.out.println("''''''''''''''''''''''''''''''''''''''''''''''''''''");
                System.out.println(numberOfAudioFilesCopied+" audio files copied to NAS");
                System.out.println("Last executed time - "+LocalDateTime.now());
                System.out.println("''''''''''''''''''''''''''''''''''''''''''''''''''''");
                TransferByHour.writeLog("''''''''''''''''''''''''''''''''''''''''''''''''''''");
                TransferByHour.writeLog(numberOfAudioFilesCopied+" audio files copied to NAS");
                TransferByHour.writeLog("''''''''''''''''''''''''''''''''''''''''''''''''''''");
                
            }
            
        }catch(Exception e){
            System.out.println("Exception thrown");
            System.out.println(e.toString());
            e.printStackTrace();
        }
        
        return numberOfAudioFilesCopied;
    }
        
    /**
     * <b><b/>
     * <p><p/>
     * 
     * @param int eachHours
     * @param String audioFileName
     * @return Boolean
     * 
     * @since 2018-07-05
     * @author Sandun
     * 
     * @modified Sandun 23-04-2019
     * @description just to temporaryly fetch all audio files from the PBX
     */
    private static String[] isWithinTimePeriod(int eachHours, String audioFileName){

            int totalHoursPeriodToScan = eachHours * 2;
            String[] responseArray  = new String[2];
            
            String[] audioFileNameSplitted = audioFileName.split("-");
            String dateArea = audioFileNameSplitted[0];
            String timeArea = audioFileNameSplitted[1];

            int year = Integer.parseInt(dateArea.substring(0, 4));
            int month = Integer.parseInt(dateArea.substring(4, 6));
            int date = Integer.parseInt(dateArea.substring(6, 8));
            int hour = Integer.parseInt(timeArea.substring(0, 2));
            int minute = Integer.parseInt(timeArea.substring(2, 4));
            int seconds = Integer.parseInt(timeArea.substring(4, 6));
            
            try{
                LocalDateTime todayDateAndTimeFormatted = LocalDateTime.now();
                LocalDateTime fromTime = LocalDateTime.now().minusHours(totalHoursPeriodToScan);

                LocalDateTime audioFileDateTime = LocalDateTime.of(year, month, date, hour, minute, seconds);

                if(audioFileDateTime.isAfter(fromTime) && audioFileDateTime.isBefore(todayDateAndTimeFormatted)){
                    // is within the time period
                    
                    String strMonth = (month >= 10 ? Integer.toString(month) : "0"+Integer.toString(month));
                    String strDate = (date >= 10 ? Integer.toString(date) : "0"+Integer.toString(date));
                    
                    responseArray[0] = year+"_"+strMonth+"_"+strDate;
                }else{
                    // not within the time period
                    responseArray = null;
                }


            }catch(Exception e){
                System.out.println("Exception thrown at within time period check");
                System.out.println(e.toString());
                e.printStackTrace();
                responseArray = null;
            }finally{
                System.out.println("Just compared audio file name - "+audioFileName);
            }


            return responseArray;
        
    }
    
    
    /**
     * 
     * @param logMessage 
     * 
     * @author Sandun
     * @since 2018-07-10
     */
    private static void writeLog(String logMessage){
        File logFile = new File("audioRoutineLog.log");
        
        if(logFile.exists() == true){
            try{
                BufferedWriter writer = new BufferedWriter(new FileWriter("audioRoutineLog.log"));
                String str = LocalDateTime.now()+" : "+logMessage;
                writer.append("\n");
                writer.append(str);
                writer.close();
            }catch(Exception e){
                System.out.println("Exception thrown at writing log file");
                e.printStackTrace();
            }
        }else{
            System.out.println("Log file not found");
        }
    }
    
}
