//package callcentercdrflagger;
//
//public class CdrFlagger {
//
//}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package callcentercdrflagger;

import java.sql.DriverManager;
import java.lang.System.Logger;
import java.sql.Connection;
import callcenteraudiotransferroutine.TransferConfigurations;
import callcenteremail.Emailer;
import callcenteremail.Subscribers;

import java.sql.ResultSet;
import java.sql.Statement;
//import java.sql.Time;
//import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Sandun
 * @since 2019-1-10
 */
public class CdrFlagger {
    
    private static boolean isSearching = false;
    
    private static int missedFilesCount = 0;
    
    public static int flagLastHourRecords(){
        
        TransferConfigurations config = new TransferConfigurations();
        
        String ipAddress = config.getReplica_mysql_CDR_DB_IP();
        Integer port = config.getReplica_mysql_CDR_DB_Port();
        String database = config.getReplica_mysql_CDR_DB_Name();
        String username = config.getReplica_mysql_CDR_DB_Username();
        String password = config.getReplica_mysql_CDR_DB_Password();
        
        String connectionString = "jdbc:mysql://"+ipAddress+":"+port+"/"+database;
        
        ArrayList<Map> missingAudioFileInfoArray = new ArrayList<Map>();
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection(connectionString, username, password);
            
            Statement statement = con.createStatement();
            
            Date currentDatetime = new Date();
            String dateFormatPattern = "yyyy-MM-dd HH:mm:ss";
            
            DateFormat dateFormat = new SimpleDateFormat(dateFormatPattern);
            String formattedCurrentDateTime = dateFormat.format(currentDatetime);
            
            Date hourBackDateTime = new Date(System.currentTimeMillis() - 3600*1000);
            String formattedHourBackDateTime = dateFormat.format(hourBackDateTime);
            
            String lastHourAnsweredCDRFetchQuery = "SELECT AcctId, channel, start, calltype, src, dst, channel, end, duration "
            		+ "FROM cdr "
            		+ "WHERE start BETWEEN '"+formattedHourBackDateTime+"' AND '"+formattedCurrentDateTime+"' "
//					+ "WHERE start BETWEEN '2019-02-14 00:00:00' AND '2019-02-14 23:59:59' "
            		+ "AND disposition = 8 "
            		+ "AND recordingfilename = '' "
            		+ "AND dst NOT LIKE '%IVR%' "
            		+ "AND dst NOT LIKE '%vm%' "
            		+ "AND dst NOT LIKE '%local%' "
            		+ "AND dst NOT LIKE '%direct%' "
            		+ "AND (LENGTH(dst) > 4 OR LENGTH(dst) = 4 )"
            		+ "AND dst != '' "
            		+ "AND calltype != '' "
            		+ "AND calltype != 'internal'";
            
            
            
            ResultSet rs = statement.executeQuery(lastHourAnsweredCDRFetchQuery);
            
            int acctId;
            String[] startTimeSplitted;
            String callStartedDate;
            String callStartedTime;
            String callDirection;
            String customerNumber;
            String extensionNumber;
            String[] channelSplitted;
            String[] callEndedTimeSplitted;
            String callEndedDate;
            String callEndedTime;
            
            AudioFileSearcher.count = 0;
            int scannedRecords = 0;
            
            boolean isFlaggable = false;
            
            while(rs.next()){
            	
            	callDirection = rs.getString("calltype");
            	
                if(callDirection.matches("incoming") == true){
                	// incoming call
                    customerNumber = rs.getString("src").trim();
                    extensionNumber = rs.getString("dst").trim();
                    
                    if(extensionNumber.trim().length() == 4) {
                    	// is an extension number
                    	isFlaggable = true;
                    }else {
                    	// not an extension
                    	isFlaggable = false;
                    }
                }else{
                	// outgoing call
                    channelSplitted = rs.getString("channel").split("-");
                    extensionNumber = channelSplitted[0].replace("SIP/", "").trim();
                    customerNumber = rs.getString("dst").trim();
                    if(customerNumber.length() == 9) {
                    	customerNumber = "0".concat(customerNumber);
                    }
                    isFlaggable = true;
                }
            	
            	if(isFlaggable == true) {
    	            scannedRecords++;

	                acctId = rs.getInt("AcctId");
	                startTimeSplitted = rs.getTimestamp("start").toString().split(" ");
	                callStartedDate = startTimeSplitted[0].replace(".0", "").trim();
	                callStartedTime = startTimeSplitted[1].replace(".0", "").trim();
	                callEndedTimeSplitted = rs.getTimestamp("end").toString().split(" ");
	                callEndedDate = callEndedTimeSplitted[0].replace(".0", "").trim();
	                callEndedTime = callEndedTimeSplitted[1].replace(".0", "").trim();
	                
	                
	                String matchingFilePath =  AudioFileSearcher.searchFiles(callStartedDate, callStartedTime, callDirection, extensionNumber, customerNumber, callEndedDate, callEndedTime, acctId);
	                
	                if(matchingFilePath != null) {
	                	// has matching files
	                	setAudiofileStateToCdr(con, acctId, matchingFilePath);
	                	System.out.println("File found - AcctId = "+acctId+" : "+matchingFilePath);
	                }else {
	                	// no matching files. Keep info for email
	                	System.out.println("Missed file - AcctId = "+acctId);
	                	
	                	Map missingAudioFileInfo = new HashMap();
	                	missingAudioFileInfo.put("start", rs.getTimestamp("start").toString());
	                	missingAudioFileInfo.put("end", rs.getTimestamp("end").toString());
	                	missingAudioFileInfo.put("duration", rs.getString("duration"));
	                	if(callDirection.matches("incoming") == true){
	                		// for incoming calls
	                		missingAudioFileInfo.put("src", rs.getString("src").trim());
	                		missingAudioFileInfo.put("dst", rs.getString("dst").trim());
	                		missingAudioFileInfo.put("callType", "Incoming");
	                	}else if(callDirection.matches("outgoing") == true){
	                		// for outgoing calls
	                		missingAudioFileInfo.put("src", extensionNumber);
	                		missingAudioFileInfo.put("dst", customerNumber);                
	                		missingAudioFileInfo.put("callType", "Outgoing");
	                	}else {
	                		// for internal calls
	                	}
	                	
	                	missingAudioFileInfoArray.add(missingAudioFileInfo);
	                	
	                }
            	}
                
            }
            
            System.out.println(missingAudioFileInfoArray.size()+" missing fies found out of total "+scannedRecords);
            
            if(missingAudioFileInfoArray.size() > 0) {
            	// has missing files
            	missedFilesCount = missingAudioFileInfoArray.size(); 
            	System.out.println("Calling emailer");
              sendMissingAudioNotificationEmail(missingAudioFileInfoArray);
            }else {
            	// no files found
            	missedFilesCount = 0;
            	System.out.println("None missed audio files found");
            }

            
            con.close();
            System.out.println(lastHourAnsweredCDRFetchQuery);
        }catch (Exception e){
        	System.out.println("2020 - Exception thrown at cdr flagging");
            System.out.println(e.toString());
            e.printStackTrace();
        }finally{
            config = null;
            System.out.println("Search and flagging ended");
        }
        
        return missedFilesCount;
    }
    
    private static boolean setAudiofileStateToCdr(Connection con, int acctId, String audioFileName){
        try {
            String updateQuery = "";
            if(audioFileName != null){
                // has audio file name
                updateQuery = "UPDATE cdr SET recordingfilename = '"+audioFileName+"' WHERE AcctId = "+acctId;
            }else{
                // no audio file
                updateQuery = "UPDATE cdr SET recordingfilename = null WHERE AcctId = "+acctId;
            }
            
            Statement statement = con.createStatement();
            int affectedRowCount = statement.executeUpdate(updateQuery);
            if(affectedRowCount > 0){
                return true;
            }else{
                return false;
            }
            
        } catch (Exception e) {
            System.out.println("2030 - Exception thrown at setting cdr file name");
            System.out.println(e.toString());
            return false;
        }
    }
    
    private static void sendMissingAudioNotificationEmail(ArrayList<Map> missingAudioFileInfoArray) {
    		System.out.println("Sending missed calls notification to subscribers");
    		
    		Subscribers subscribers = new Subscribers();
    		ArrayList<String> subscribersInfo = subscribers.getSubscriberEmailAddresses();
    		
    		
    		Emailer emailer = new Emailer();
    		try{
    			emailer.sendEmail(null, missingAudioFileInfoArray, subscribersInfo);
    		}catch(Exception e) {
    			System.out.println("3191 - Exception thrown at sending missing notifications email");
    			System.out.println(e.toString());
    			e.printStackTrace();
    		}
    }
}
