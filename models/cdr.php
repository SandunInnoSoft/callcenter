<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\Call_records;

/**
 * This model class is to exchange data with cdr table in the PBX db
 *
 * @author Sandun
 * @since 2017-06-30
 */
class cdr extends ActiveRecord {
    
    public $status;
    public $comment;

    public static function getDb() {
        return Yii::$app->db2;
    }

    /*
     * This function will get list of answered call by a specific agent. 
     * @author: Prabath
     * @since: 31/07/2017
     *     
     */


    public static function getAnsweredCallsListByUser($agent_ex) {
        $connection = Yii::$app->db2;

        $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where dst = $agent_ex  and calltype = 'incoming'  group by uniqueid order by start desc;");

        $answered_history = $command->queryAll();
        return $answered_history;
    }

    /*
     * return Answerd call details filter by date
     * @author: Supun
     * @since: 16/08/2017
     *     
     */

    public static function getAnsweredCallsListByDate($agent_ex, $fromDate, $toDate) {
        $connection = Yii::$app->db2;

        if ($agent_ex != null && $fromDate != null && $toDate != null) {
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where dst = $agent_ex  and calltype = 'incoming' and start between '$fromDate' and '$toDate 23:59:59' order by start desc;");
        } else if ($agent_ex != null && $fromDate == null && $toDate != null) {
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where dst = $agent_ex  and calltype = 'incoming' and start between '$toDate 00:00:00' and '$toDate 23:59:59' order by start desc;");
        } else if ($agent_ex != null && $fromDate != null && $toDate == null) {
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where dst = $agent_ex  and calltype = 'incoming' and start between '$fromDate' and '" . date('Y-m-d h:i:s') . "' order by start desc;");
        } else {
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where dst = $agent_ex  and calltype = 'incoming' order by start desc;");
        }

        //$command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where dst = $agent_ex  and calltype = 'incoming' and start between '$fromDate' and '$toDate 23:59:59' order by start desc;");

        $answered_history = $command->queryAll();
        return $answered_history;
    }

    /*
     * return Outgoing call details filter by user
     * @author: Supun
     * @since: 16/08/2017
     *     
     */

    public static function getOutgoingCallsListByUser($agent_ex) {
        $connection = Yii::$app->db2;
        if ($agent_ex != 0) { // If agent extention not for all users
//        $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where src = $agent_ex  and calltype = 'outgoing';");
//        SELECT src,dst,start,answer,end,duration FROM cdr where src = '807'  and calltype = 'outgoing' and MONTH(DATE_FORMAT(STR_TO_DATE(START, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d')) = MONTH(CURRENT_DATE())  group by uniqueid;
//SELECT src,dst,start,answer,end,duration FROM cdr where src = '$agent_ex'  and calltype = 'outgoing' and DATE_FORMAT(STR_TO_DATE(START, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d') = CURRENT_DATE()  group by uniqueid;
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/$agent_ex%'  and calltype = 'outgoing' and MONTH(DATE_FORMAT(STR_TO_DATE(START, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d')) = MONTH(CURRENT_DATE())  group by uniqueid order by AcctId desc;");
        } else {
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/%'  and  calltype = 'outgoing' and MONTH(DATE_FORMAT(STR_TO_DATE(START, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d')) = MONTH(CURRENT_DATE())  group by uniqueid order by AcctId desc;");
        }
//SELECT src,dst,start,answer,end,duration FROM cdr where src = '802'  and calltype = 'outgoing' and DATE_FORMAT(STR_TO_DATE(START, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d') = CURRENT_DATE()  group by uniqueid;
        $outgoing_history = $command->queryAll();
        return $outgoing_history;
    }

    public static function getOutgoingCallsCountByUser($agent_ex) {
        $connection = Yii::$app->db2;
        if ($agent_ex != 0) { // If agent extention not for all users
//        $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where src = $agent_ex  and calltype = 'outgoing';");
//        SELECT src,dst,start,answer,end,duration FROM cdr where src = '807'  and calltype = 'outgoing' and MONTH(DATE_FORMAT(STR_TO_DATE(START, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d')) = MONTH(CURRENT_DATE())  group by uniqueid;
//SELECT src,dst,start,answer,end,duration FROM cdr where src = '$agent_ex'  and calltype = 'outgoing' and DATE_FORMAT(STR_TO_DATE(START, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d') = CURRENT_DATE()  group by uniqueid;
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where channel LIKE 'SIP/$agent_ex%'  and calltype = 'outgoing' and MONTH(DATE_FORMAT(STR_TO_DATE(START, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d')) = MONTH(CURRENT_DATE())  group by uniqueid order by AcctId desc;");
        } else {
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where channel LIKE 'SIP/%'  and  calltype = 'outgoing' and MONTH(DATE_FORMAT(STR_TO_DATE(START, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d')) = MONTH(CURRENT_DATE())  group by uniqueid order by AcctId desc;");
        }
//SELECT src,dst,start,answer,end,duration FROM cdr where src = '802'  and calltype = 'outgoing' and DATE_FORMAT(STR_TO_DATE(START, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d') = CURRENT_DATE()  group by uniqueid;
        $outgoing_history = $command->queryAll();
        return count($outgoing_history);
    }

    /*
     * return Outgoing call details filter by date
     * @author: Supun
     * @since: 16/08/2017
     * 
     * @modified Sandun 2018-01-23
     * @param type $fromTime
     * @param type $toTime
     * @description Added 2 new parameters to support the time filtering facility
     * 
     */
    public static function getOutgoingCallsListByDate($agent_ex, $fromDate, $toDate, $fromTime, $toTime, $extensionsInQueue) {
        $connection = Yii::$app->db2;
        if ($agent_ex != 0) { // If agent extension not for all users
            if ($agent_ex != null && $fromDate != null && $toDate != null) {
                $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/$agent_ex%'  and calltype = 'outgoing' and start between '$fromDate $fromTime' and '$toDate $toTime' order by start desc;");
            } else if ($agent_ex != null && $fromDate == null && $toDate != null) {
                $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/$agent_ex%'  and calltype = 'outgoing' and start between '$toDate 00:00:00' and '$toDate 23:59:59' order by start desc;");
            } else if ($agent_ex != null && $fromDate != null && $toDate == null) {
                $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/$agent_ex%'  and calltype = 'outgoing' and start between '$fromDate' and '" . date('Y-m-d h:i:s') . "' order by start desc;");
            } else {
                $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/$agent_ex%'  and calltype = 'outgoing' order by start desc;");
            }
        } else {
            if ($agent_ex != null && $fromDate != null && $toDate != null) {
                if($extensionsInQueue == null){
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/%'  and  calltype = 'outgoing' and start between '$fromDate $fromTime' and '$toDate $toTime' order by start desc;");                    
                }else{
                    $extensionsForQuery = cdr::getSipChannelExtensionNumberListForSql($extensionsInQueue);
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where ($extensionsForQuery)  and  calltype = 'outgoing' and start between '$fromDate $fromTime' and '$toDate $toTime' order by start desc;");                    
                }
            } else if ($agent_ex != null && $fromDate == null && $toDate != null) {
                if($extensionsInQueue == null){
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/%'  and  calltype = 'outgoing' and start between '$toDate 00:00:00' and '$toDate 23:59:59' order by start desc;");                    
                }else{
                    $extensionsForQuery = cdr::getSipChannelExtensionNumberListForSql($extensionsInQueue);
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where ($extensionsForQuery)  and  calltype = 'outgoing' and start between '$toDate 00:00:00' and '$toDate 23:59:59' order by start desc;");                    
                }
            } else if ($agent_ex != null && $fromDate != null && $toDate == null) {
                if($extensionsInQueue == null){
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/%'  and  calltype = 'outgoing' and start between '$fromDate 00:00:00' and '" . date('Y-m-d h:i:s') . "' order by start desc;");                    
                }else{
                    $extensionsForQuery = cdr::getSipChannelExtensionNumberListForSql($extensionsInQueue);
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where ($extensionsForQuery)  and  calltype = 'outgoing' and start between '$fromDate 00:00:00' and '" . date('Y-m-d h:i:s') . "' order by start desc;");                    
                }
            } else {
                if($extensionsInQueue == null){
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/%' and MONTH(DATE_FORMAT(STR_TO_DATE(start, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d')) = MONTH(CURRENT_DATE())  and  calltype = 'outgoing' order by start desc;");
                }else{
                    $extensionsForQuery = cdr::getSipChannelExtensionNumberListForSql($extensionsInQueue);
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where ($extensionsForQuery) and MONTH(DATE_FORMAT(STR_TO_DATE(start, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d')) = MONTH(CURRENT_DATE()) and calltype = 'outgoing' order by start desc;");
                }
            }
        }
        //$command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where src = $agent_ex  and calltype = 'outgoing';");

        $outgoing_history = $command->queryAll();
        return $outgoing_history;
    }
    
    private static function getSipChannelExtensionNumberListForSql($extensionsInQueueArray){
        if(count($extensionsInQueueArray) > 0){
            $multipleLikeString = "";
            for($x = 0; $x < count($extensionsInQueueArray); $x++){
                $extensionNumber = $extensionsInQueueArray[$x];
                if($x == 0){
                    // first element
                    $multipleLikeString = "channel LIKE 'SIP/$extensionNumber%'";
                }else{
                    $multipleLikeString = $multipleLikeString." OR channel LIKE 'SIP/$extensionNumber%'";
                }
                
            }
            return $multipleLikeString;
        }else{
            return "channel LIKE 'SIP/%'";
        }
    }
    
    
    /**
     * <b>Search the outgoing calls records between the from and to dates and matches with the contact number</b>
     * <p>This function performs queries in the DB2 to get the outgoing calls occurred between the from and to dates
     * passed as paramaters and similar to the contact number passed as another parameter</p>
     * 
     * @param type $agent_ex
     * @param type $fromDate
     * @param type $toDate
     * @param type $contactNumber
     * @return type
     * 
     * @since 2017-11-15
     * @author Sandun
     * 
     * @modified Sandun 2018-1-23
     * @param type $fromTime
     * @param type $toTime
     */
    public static function getOutgoingCallsListByDateAndContactNumber($agent_ex, $fromDate, $toDate, $fromTime, $toTime, $contactNumber, $extensionsInQueue){
        $connection = Yii::$app->db2;
        if ($agent_ex != 0) { // If agent extension not for all users
            if ($agent_ex != null && $fromDate != null && $toDate != null) {
                $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/$agent_ex%'  and calltype = 'outgoing' and start between '$fromDate $fromTime' and '$toDate $toTime' and dst like '%$contactNumber%' order by start desc;");
            } else if ($agent_ex != null && $fromDate == null && $toDate != null) {
                $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/$agent_ex%'  and calltype = 'outgoing' and start between '$toDate 00:00:00' and '$toDate 23:59:59' and dst like '%$contactNumber%' order by start desc;");
            } else if ($agent_ex != null && $fromDate != null && $toDate == null) {
                $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/$agent_ex%'  and calltype = 'outgoing' and start between '$fromDate' and '" . date('Y-m-d h:i:s') . "' and dst like '%$contactNumber%' order by start desc;");
            } else {
                $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/$agent_ex%'  and calltype = 'outgoing' and dst like '%$contactNumber%';");
            }
        } else {
            if($extensionsInQueue == null){
                if ($agent_ex != null && $fromDate != null && $toDate != null) {
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/%'  and  calltype = 'outgoing' and start between '$fromDate $fromTime' and '$toDate $toTime' and dst like '%$contactNumber%' order by start desc;");
                } else if ($agent_ex != null && $fromDate == null && $toDate != null) {
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/%'  and  calltype = 'outgoing' and start between '$toDate 00:00:00' and '$toDate 23:59:59' and dst like '%$contactNumber%' order by start desc;");
                } else if ($agent_ex != null && $fromDate != null && $toDate == null) {
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/%'  and  calltype = 'outgoing' and start between '$fromDate' and '" . date('Y-m-d h:i:s') . "' and dst like '%$contactNumber%' order by start desc;");
                } else {
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where channel LIKE 'SIP/%'  and  calltype = 'outgoing' and dst like '%$contactNumber%';");
                }                
            }else{
                $extensionsForQuery = cdr::getSipChannelExtensionNumberListForSql($extensionsInQueue);
                if ($agent_ex != null && $fromDate != null && $toDate != null) {
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where ($extensionsForQuery) and  calltype = 'outgoing' and start between '$fromDate $fromTime' and '$toDate $toTime' and dst like '%$contactNumber%' order by start desc;");
                } else if ($agent_ex != null && $fromDate == null && $toDate != null) {
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where ($extensionsForQuery) and  calltype = 'outgoing' and start between '$toDate 00:00:00' and '$toDate 23:59:59' and dst like '%$contactNumber%' order by start desc;");
                } else if ($agent_ex != null && $fromDate != null && $toDate == null) {
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where ($extensionsForQuery) and  calltype = 'outgoing' and start between '$fromDate' and '" . date('Y-m-d h:i:s') . "' and dst like '%$contactNumber%' order by start desc;");
                } else {
                    $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration, channel FROM cdr where ($extensionsForQuery) and  calltype = 'outgoing' and dst like '%$contactNumber%';");
                }                
            }
        }
        //$command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where src = $agent_ex  and calltype = 'outgoing';");

        $outgoing_history = $command->queryAll();
        return $outgoing_history;
        
    }

    /*
     * This function will get list of answered call by a specific agent. 
     * @author: Prabath
     * @since: 31/07/2017
     *     
     */

    public static function getAnsweredCallsCount() {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT AcctId FROM cdr where calltype = 'incoming' group by uniqueid;");

        $outgoing_history = $command->queryAll();
        return count($outgoing_history);
    }

    public static function getAnsweredCallsCountBetweenDates($from, $to) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT AcctId FROM cdr where calltype = 'incoming' AND start between '$from' and '$to 23:59:59' group by uniqueid;");

        $outgoing_history = $command->queryAll();
        return count($outgoing_history);
    }

    public static function getHighestCdrID() {
//        $cdr = new cdr();
//        return $cdr->find()
//                        ->max('AcctId')
//                        ->all();
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT max(AcctId) FROM cdr;");

        $call_records = $command->queryAll();
        return $call_records[0]["max(AcctId)"];
    }

    public static function getNewCDRInfo($cdrId) {
//        $cdr = new cdr();
//        return $cdr->find()
//                        ->where("AcctId = $cdrId")
//                        ->one()
//                        ->all();
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT * FROM cdr where AcctId = $cdrId;");

        $call_records = $command->queryAll();
        return $call_records;
    }

    public static function getCdrInfoById($cdrId){
        $cdrInfo = cdr::find()
                    ->where("AcctId = $cdrId")
                    ->one();
        return $cdrInfo;
    }

    public static function getCallHistory() {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT src as caller,dst as reciever,start,end,duration FROM cdr order by start desc;");

        return $callHistory = $command->queryAll();
    }

    /**
     * <b>Get the longest call made by the given extension</b>
     * <p>This function returns the data associated with the longes call taken by the VOIp extension number passed as the paramater</p>
     * 
     * @param int $extensionNumber
     * @return array
     * @since 2017-07-25
     * @author Sandun
     */
    public static function getLongestCall($extensionNumber) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT * "
                . "FROM cdr  "
                . "where dst = '$extensionNumber' "
                . "order by duration desc "
                . "limit 1;");

        return $command->queryAll();
    }

    /**
     * <b>Get latest unique id for the number passed in parameters</b>
     * <p>This function the most recent unique id for the number passed in parameter</p>
     * 
     * @param int $callerNumber
     * @return array
     * @since 2017-08-02
     * @author Vikum
     */
    public static function getMostRecentUniqueId($callerNumber, $voipExtension) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT uniqueid "
                . "FROM cdr  "
                . "where (unix_timestamp() - 10 < unix_timestamp(end)) AND "
                . "`src` = '$callerNumber' AND "
                . "`dst` = '$voipExtension' "
                . "group by uniqueid");

        return $command->queryAll();
    }

    public static function getMostRecentCdrRecordDataWithUniqueId($callerNumber, $voipExtension) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT * "
                . "FROM cdr  "
                . "where (unix_timestamp() - 10 < unix_timestamp(end)) AND "
                . "`src` = '$callerNumber' AND "
                . "`dst` = '$voipExtension' "
                . "group by uniqueid");

        return $command->queryAll();
    }    
    
    public static function getMissedCalls($timestamp) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT uniqueid "
                . "FROM cdr  "
                . "where uniqueid > $timestamp "
                . "group by uniqueid");
        return $command->queryAll();
    }

    public static function getCallsFromSelectedList($data) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT src "
                . "FROM cdr  "
                . "where  uniqueid in (" . implode(',', $data) . ")"
                . "AND LENGTH(src) > 6 "
                . "group by src");
        return $command->queryAll();
    }

    /**
     * <b>Get cdr records of user</b>
     * <p>This function returns cdr records for a user number</p>
     * 
     * @author Vikum
     * @since 2017-07-21
     * 
     * @param number
     * @return array
     */
    public static function getRecordsByNumber($number) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT *, dst as comment, lastapp as status " // temporary fix to form the array for comments and status
                . "FROM cdr  "
                . "where  src = '$number' "
                . "group by uniqueid "
                . "order by AcctId desc");
        return $command->queryAll();
    }

    /**
     * <b>Get calls came today</b>
     * <p>This function all customer data of the current day</p>
     * 
     * @author Vikum
     * @since 2017-08-17
     * 
     * @param number
     * @return array
     */
    public static function getTodaysCalls($extensionsInQueue) {
        $connection = Yii::$app->db2;
        if($extensionsInQueue == NULL){
            $command = $connection->createCommand("select COUNT(DISTINCT uniqueid) as num "
                    . "FROM cdr  "
                    . "where STR_TO_DATE(start,'%Y-%m-%d') = CURDATE()");
        }else{
            $extensionsInQueueCsvString = implode(",", $extensionsInQueue);
             $command = $connection->createCommand("select COUNT(DISTINCT uniqueid) as num "
                    . "FROM cdr  "
                    . "where STR_TO_DATE(start,'%Y-%m-%d') = CURDATE() "
                     . "AND dst IN ($extensionsInQueueCsvString)");
        }
        return $command->queryAll();
    }

    /**
     * <b>Get Call duration sum of the records by unique id</b>
     * <p>This function return all cdr records of the given unique id</p>
     * 
     * @author Vikum
     * @since 2017-08-21
     * 
     * @param unique_id
     * @return array
     */
    public static function getRecordsDurationByUniqueId($uniqueId) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("select SUM(duration) as num "
                . "FROM cdr  "
                . "where uniqueid = $uniqueId");
        return $command->queryAll();
    }

    /**
     * <b>Returns cdr data matches with unique id and extension for incomming calls</b>
     * <p>This function returns a dataset array with all the call record information of the cdr record matches to the
     * unique id and the extension number passed as the paramater. If found returns the array else returns null</p>
     * 
     * @param string $uniqueId
     * @param int $extension
     * @return array / NULL
     * @since 2017-09-11
     * @author Sandun
     * 
     * @since 2017-09-26
     * @author Vikum
     * @version  to return latest row with the given parameters
     */
    public static function getIncommingCdrDataByUniqueIdAndExtension($uniqueId, $extension) {
        $cdr = cdr::find()
//                ->select('start, end')
                ->where("uniqueid = '$uniqueId'")
                ->andWhere("dst = '$extension'")
                ->orderBy("AcctId desc")
                ->one();
//                ->max('AcctId');

        if (count($cdr) > 0) {
            return $cdr;
        } else {
            return NULL;
        }
    }

    /**
     * <b>Returns cdr data matches with unique id and extension for outgoing calls</b>
     * <p>This function returns a dataset array with all the call record information of the cdr record matches to the
     * unique id and the extension number passed as the paramater. If found returns the array else returns null</p>
     * 
     * @param string $uniqueId
     * @param int $extension
     * @return array / NULL
     * @since 2017-09-19
     * @author Sandun
     */
    public static function getOutgoingCdrDataByUniqueIdAndExtension($uniqueId, $extension) {
        $cdr = cdr::find()
                ->where("uniqueid = '$uniqueId'")
                ->andWhere("src = '$extension'")
                ->one();

        if (count($cdr) > 0) {
            return $cdr;
        } else {
            return NULL;
        }
    }

    /**
     * <b>Return call records within the given dates and given extension</b>
     * <p>Agent extension, from date and end date passed through parameters. </p>
     * 
     * @param string $agent_ex
     * @param date $fromDate
     * @param date $toDate
     * @return array / NULL
     * @since 17-10-2017
     * @author Vikum
     */
//    public static function filterOutgoingCallsByDate($agent_ex, $fromDate, $toDate) {
//        
//    }

    /**
     * <b>Return call records within this month for the given extension</b>
     * <p>Agent extension passed through parameters. </p>
     * 
     * @param string $agent_ex

     * @return array / NULL
     * @since 17-10-2017
     * @author Vikum
     */
//    public static function filterOutgoingCallsByExtensionForMonth($agent_ex) {
//        $connection = Yii::$app->db2;
//        if ($agent_ex != 0) {
////            $cdr = cdr::find()
////                    ->innerJoin('call_center_user', 'call_center_user.voip_extension = cdr.src')
//        } else {
//            
//        }
//    }
//    public function getCall_center_user() {
////        return $this->hasMany(call_center_user::className(), ['user_id' => 'id']);
//        return $this->hasOne(call_center_user::className(), ['src' => 'voip_extension']);
//    }

    /**
     * <b>Returns total calls received</b>
     * <p>Returns the total number of calls received to the system</p>
     * @param type $from
     * @param type $to
     * @return int
     * 
     * @author Sandun
     * @since 2017-11-07
     * 
     * @updated 2017-11-09 Sandun
     * @description Modified the query to include str_to_date mysql functions
     */
    public static function getTotalCallsReceivedCountByDate($from, $to) {
//        $connection = Yii::$app->db2;
//        $command = $connection->createCommand("select count(DISTINCT uniqueid) as num "
//                . "FROM cdr  "
//                . "where start >= '$from 00:00:00' AND start <= '$to 23:59:59' AND calltype = 'incoming'");
//        $result = $command->queryAll();
//        return $result[0]['num'];
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("select COUNT(DISTINCT uniqueid) as num "
                . "FROM cdr  "
                . "where STR_TO_DATE(start,'%Y-%m-%d') >= STR_TO_DATE('$from','%Y-%m-%d') "
                . "AND STR_TO_DATE(start,'%Y-%m-%d') <= STR_TO_DATE('$to','%Y-%m-%d') "
                . "AND calltype = 'incoming';");
        $result = $command->queryAll(); 
        return $result[0]['num'];
    }

    public static function getMissedCallsBetweenDates($from, $to, $contactNumber) {
//        echo "From = $from : To = $to";
        $uniqueIds = Call_records::getAllUniqueIdsBetweenDates($from, $to, $contactNumber);
        if(count($uniqueIds) > 0) {
            $implodedUniqueIdArray = implode(",", $uniqueIds);
            if($contactNumber == NULL){
            $sql = "
                        SELECT distinct(uniqueid), src, start
                        FROM cdr 
                        WHERE uniqueid NOT IN ($implodedUniqueIdArray) 
                        AND (STR_TO_DATE(start,'%Y-%m-%d') > STR_TO_DATE('$from','%Y-%m-%d') OR STR_TO_DATE(start,'%Y-%m-%d') = STR_TO_DATE('$from','%Y-%m-%d'))
                        AND (STR_TO_DATE(start,'%Y-%m-%d') < STR_TO_DATE('$to','%Y-%m-%d') OR STR_TO_DATE(start,'%Y-%m-%d') = STR_TO_DATE('$to','%Y-%m-%d'))
                        AND calltype = 'incoming' 
                        group by uniqueid;
                    ";
            }else{
            $sql = "
                        SELECT distinct(uniqueid), src, start
                        FROM cdr 
                        WHERE uniqueid NOT IN ($implodedUniqueIdArray) 
                        AND (STR_TO_DATE(start,'%Y-%m-%d') > STR_TO_DATE('$from','%Y-%m-%d') OR STR_TO_DATE(start,'%Y-%m-%d') = STR_TO_DATE('$from','%Y-%m-%d'))
                        AND (STR_TO_DATE(start,'%Y-%m-%d') < STR_TO_DATE('$to','%Y-%m-%d') OR STR_TO_DATE(start,'%Y-%m-%d') = STR_TO_DATE('$to','%Y-%m-%d'))
                        AND calltype = 'incoming' 
                        AND src like '%$contactNumber%'
                        group by uniqueid;
                    ";            
            }
            $connection = Yii::$app->db2;
            $command = $connection->createCommand($sql);
            return $command->queryAll();
        }else{
            return NULL;
        }
    }
    



    /**
     * <b>Returns abandoned calls of an extension between a time period</b>
     * <p>This function returns the abandoned calls of an agent extension between a time period</p>
     * 
     * @param string $fromDate
     * @param string $toDate
     * @param int $agentExtension
     * @return array abandoned calls data / null
     * 
     * @since 2017-11-21
     * @author Sandun
     * 
     * @modified Sandun 2017-11-23
     * @param string $contactNumber
     * @param string $returnType "Count" / "Data"
     * @description
     * <p>Added 2 new paramaters pass the contact number and the return type. 
     * If the contact number is not null the query will filter records ti similar number to the passed in contact number.
     * If return type is "Count", returns the count of missed calls.
     * If return type is "Data", returns the full data set of missed calls.
     * Else returns only the 1st record</p>
     * 
     * @modified Sandun 2018-1-23
     * @param string $fromTime
     * @param string $toTime
     * 
     */
    public static function getAbandonedCallsOfAnAgentBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agentExtension, $contactNumber, $returnType, $extensionsInQueue){
        //select * from cdr where disposition = 4 and calltype = "incoming" and CHAR_LENGTH(src) > 6 and `start` like "2017-11-21%" and dst = '4020'
        if($agentExtension != 0){
            // single agent
            $abandonedCalls = cdr::find()
                            ->select("end, src")
                            ->where("disposition = 4")
                            ->andWhere("calltype = 'incoming'")
                            ->andWhere("CHAR_LENGTH(src) > 6")
                            ->andWhere(['between', 'end', "$fromDate $fromTime", "$toDate $toTime"])
                            ->andWhere("dst = '$agentExtension'");
                    if($contactNumber != NULL){
                        // has contact number
                        $abandonedCalls->andWhere(['like', 'src', $contactNumber]);
                    }
                    
                    if($extensionsInQueue != NULL){
                        $abandonedCalls->andWhere(['in', 'dst', $extensionsInQueue]);
                    }
                    
                    $abandonedCalls->orderBy("end DESC");
                    if($returnType == "Count"){
                        // return the count
                        return $abandonedCalls->count();
                    }else if ($returnType == "Data"){
                        // return data
                        return $abandonedCalls->all();
                    }else {
                        // return Only 1st one
                        return $abandonedCalls->one();
                    }
                            
        }else{
            // all agents
            return NULL;
        }
    }
    



    
    public static function getAllAnsweredCallsCountBetweenDates($fromDate, $toDate, $extensionsInQueue){
        $answeredCallsToday = cdr::find()
                            ->where("disposition = 8")
                            ->andWhere("calltype = 'incoming'")
                            ->andWhere("CHAR_LENGTH(src) > 6")
                            ->andWhere(['between', 'start', "$fromDate 00:00:00", "$toDate 23:59:59"])
                            ->andWhere(['not like', 'dst', 'IVR']);
        
                if($extensionsInQueue != NULL){
                    $answeredCallsToday->andWhere(['in', 'dst', $extensionsInQueue]);
                }
        
        $answeredCallsToday->groupBy("uniqueid");
        return $answeredCallsToday->count();
    }
    
    /**
     * 
     * @param type $fromDate
     * @param type $toDate
     * @param type $agentExtension
     * @param type $contactNumber
     * @param type $returnType
     * @param type $extensionsInQueue
     * @return type
     * 
     * @modified Sandun 2018-1-23
     * @param type $fromTime
     * @param type $toTime
     * @description Added 2 new parameters for the function to support with time wise data filter
     */
    public static function getAgentAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agentExtension, $contactNumber, $returnType, $extensionsInQueue, $selectedChannelsArray){
         $allAnsweredCalls = cdr::find()
                            ->where("disposition = 8")
                            ->andWhere("calltype = 'incoming'")
                            ->andWhere("CHAR_LENGTH(src) > 6")
                            ->andWhere(['between', 'start', "$fromDate $fromTime", "$toDate $toTime"]);
                            if($agentExtension != 0){
                                // single agent
                                $allAnsweredCalls->andWhere("dst = $agentExtension");
                            }else{
                                $allAnsweredCalls->andWhere(['not like', 'dst', 'IVR']);
                                $allAnsweredCalls->andWhere(['not like', 'dst', 'vm']);
                            }
                            
                            if($contactNumber != NULL){
                                // has contact number
                                $allAnsweredCalls->andWhere(['like', 'src', $contactNumber]);
                            }
                            
                            if($extensionsInQueue != NULL){
                                $allAnsweredCalls->andWhere(['in', 'dst', $extensionsInQueue]);
                            }

                            if($selectedChannelsArray != NULL){
                                // has channels to filter
                                $allAnsweredCalls->andWhere(['in', 'calleenum', $selectedChannelsArray]);
                            }
                           
                            // if($contactNumber == NULL){
                            //     $allAnsweredCalls->groupBy("uniqueid");
                            // }

                            $allAnsweredCalls->orderBy("answer DESC");
                           if($returnType == "Count"){
                               // return the count
                               return $allAnsweredCalls->count();
                           }else if ($returnType == "Data"){
                               // return data
                               return $allAnsweredCalls->all();
                           }else {
                               // return Only 1st one
                               return $allAnsweredCalls->one();
                           }                            
    }
    
    public static function getAgentAnsweredCallsCountBetweenDates($fromDate, $toDate, $agentExtension){
        return cdr::find()
                ->where("disposition = 8")
                ->andWhere("calltype = 'incoming'")
                ->andWhere("CHAR_LENGTH(src) > 6")
                ->andWhere(['between', 'start', "$fromDate 00:00:00", "$toDate 23:59:59"])
                ->andWhere("dst = $agentExtension")
                ->andWhere(['not like', 'dst', 'IVR'])
                ->groupBy("uniqueid")
                ->count();        
    }
    
    
    /**
     * <b>Gets the missed calls data between dates or from contact number<b/>
     * <p>This function returns the count or the dataset of missed calls recorded between the from and to date range<p/>
     * 
     * @param String $fromDate
     * @param String $toDate
     * @param String $contactNumber
     * @param String $returnType "Count" / "Data"
     * @return Array missed calls data / Int count
     * 
     * @author Sandun
     * @since 2017-11-23
     * 
     * @modified Sandun 2018-1-23
     * @param String $fromTime
     * @param String $toTime
     * @description Added 2 new parameters to input from and to time periods for the query to support with time filtering
     */
    public static function getAllMissedCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $contactNumber, $returnType, $extensionsInQueue){
        
        $allAnsweredCalls = cdr::find()
                            ->select("uniqueid")
                            ->where("disposition = 8")
                            ->andWhere("calltype = 'incoming'")
                            ->andWhere("CHAR_LENGTH(src) > 6")
                            ->andWhere(['between', 'start', "$fromDate $fromTime", "$toDate $toTime"])
                            ->andWhere(['not like', 'dst', 'IVR']);
                    if($extensionsInQueue != NULL){
                        $allAnsweredCalls->andWhere(['in', 'dst', $extensionsInQueue]);
                    }
                        $allAnsweredCalls->groupBy("uniqueid");
        
        $allMissedCalls = cdr::find()
                            ->select("end, src, duration")
                            ->where("disposition = 0")
                            ->andWhere("calltype = 'incoming'")
                            ->andWhere("CHAR_LENGTH(src) > 6")
                            ->andWhere(['between', 'start', "$fromDate $fromTime", "$toDate $toTime"])
                            ->andWhere(['not in', 'uniqueid', $allAnsweredCalls]);
                            if($contactNumber != NULL){
                                // has contact number
                                $allMissedCalls->andWhere(['like', 'src', $contactNumber]);
                            }
                            
                            if($extensionsInQueue != NULL){
                                $allMissedCalls->andWhere(['in', 'dst', $extensionsInQueue]);
                            }
        
                           if($contactNumber == NULL){
                                $allMissedCalls->groupBy("uniqueid");
                           }

                           $allMissedCalls->orderBy("end DESC");
                           if($returnType == "Count"){
                               // return the count
                               return $allMissedCalls->count();
                           }else if ($returnType == "Data"){
                               // return data
                               return $allMissedCalls->all();
                           }else {
                               // return Only 1st one
                               return $allMissedCalls->one();
                           }
                            
    }   
    
    /**
     * <b>Returns unique id by caller number and agent extension</b>
     * <p>This function returns the latest uniqueid inserted between last 5 seconds time for answered call of the agent extension and caller number</p>
     * 
     * @param type $callerNumber
     * @param type $agentExtension
     * @param type $endTimestampension and 
     * @return String / null
     * 
     * @author Sandun
     * @since 2017-11-28
     */
    public static function getUniqueIdByCallerNumberAgentExtensionAndEndTime($callerNumber, $agentExtension, $endTimestamp){
        //$endTimestamp = $endTimestamp - 5; // reduces 5 seconds from the call end time
        $callRecord = cdr::find()
                ->select("uniqueid")
                ->where("src = $callerNumber")
                ->andWhere("dst = $agentExtension")
                ->andWhere("calltype = 'incoming'")
                ->andWhere("CHAR_LENGTH(src) > 6")
                ->andWhere("disposition = 8")
                ->andWhere("unix_timestamp() - 5 < unix_timestamp(end)")
//                ->orWhere("unix_timestamp(end) = $endTimestamp")
                ->groupBy("uniqueid")
                ->orderBy("end DESC")
                ->one();
        if($callRecord){
            return $callRecord['uniqueid'];
        }else{
            return NULL;
        }
    }
    
    
    public static function todayCallWaitingAverageTime(){
        $callRecord = new cdr();
        $data = $callRecord->find()
                ->select('call_wait_duration')
                ->where('DATE(start) = CURDATE()')
                ->average('call_wait_duration');
        return $data;
    }
    
    public static function getAllAnsweredAndMissedCallsOfCaller($callerNumber){
//        return cdr::find()
// //                ->select("start, src, dst, duration, lastapp AS status, calltype AS comment")
//                ->select(["start" => "start", "disposition" => "disposition", "uniqueid" => "uniqueid","src" => "src", "dst" => "dst", "duration" => "duration", "status" => "lastapp", "comment" => "calltype"])
//                ->where("calltype = 'incoming'")
//                ->andWhere(['or', 'disposition = 8', 'disposition = 0'])
//                ->andWhere("src = $callerNumber")
//                ->groupBy("uniqueid")
//                ->orderBy("start DESC")
//                ->limit(1000)
//                ->all();
        
        $sql = "
                SELECT uniqueid, start, src, dst, disposition, duration, lastapp AS status, calltype AS comment
                FROM cdr
                WHERE calltype = 'incoming'
                AND (disposition = 8 OR disposition = 0)
                AND src = '$callerNumber'
                GROUP BY uniqueid
                ORDER BY start DESC
                LIMIT 1000                
                ;";
        
            $connection = Yii::$app->db2;
            $command = $connection->createCommand($sql);
            return $command->queryAll();
        
//                $allAnsweredCallsOfCaller = cdr::find()
//                            ->select("uniqueid, start, src, dst, disposition, duration")
//                            ->where("disposition = 8")
//                            ->andWhere("calltype = 'incoming'")
//                            ->andWhere("CHAR_LENGTH(src) > 6")
//                            ->andWhere("src = '$callerNumber'")
//                            ->orderBy("start DESC")
//                            ->groupBy("uniqueid")
//                            ->limit(1000);
//                
//                $allMissedCallsOfCaller = cdr::find()
//                            ->select("uniqueid, start, src, dst, disposition, duration")
//                            ->where("disposition = 0")
//                            ->andWhere("calltype = 'incoming'")
//                            ->andWhere("CHAR_LENGTH(src) > 6")
//                            ->andWhere("src = '$callerNumber'")
//                            ->andWhere(['not in', 'uniqueid', $allAnsweredCallsOfCaller])
//                            ->orderBy("start DESC")
//                            ->groupBy("uniqueid")
//                            ->limit(1000)
//                            ->all();
//        
////                   $allMissedCallsStartTime = array_column($allMissedCallsOfCaller, "start");
//                
//                $allMissedAndAnsweredCallsOfCaller = array_merge($allAnsweredCallsOfCaller, $allMissedCallsOfCaller);
//                return $allMissedCallsOfCaller;
        
    }
    
    public static function getAllCustomerCallRecordsWithinToday($extensionsInQueue){
        $allCustomerCallRecordsToday = cdr::find()
                                ->where(['between', 'start', date("Y-m-d 00:00:00"), date("Y-m-d 23:59:59")])
                                ->andWhere("CHAR_LENGTH(src) > 6");
                if($extensionsInQueue != NULL){
                    $allCustomerCallRecordsToday->andWhere(['in', 'dst', $extensionsInQueue]);
                }
        
        return $allCustomerCallRecordsToday->all();
    }
    
    public static function isThisUniqueIdAMissedCall($uniqueId){
        $callRecords = cdr::find()
                ->where("uniqueid = '$uniqueId'")
                ->all();
        $isMissed = TRUE;
        for($x = 0; $x < count($callRecords); $x++){
            if($callRecords[$x]['disposition'] == 8){
                $isMissed = FALSE;
            }
        }
        return $isMissed;
    }
    
    public static function getAnsweredExtensionFromUniqueid($uniqueid){
        return cdr::find()
                ->where("uniqueid = $uniqueid")
                ->andWhere("disposition = 8")
                ->one();
    }

    /**
     * <b></b>
     * <p></p>
     * 
     * @param type $dateFrom
     * @param type $dateTo
     * @param type $contactNumber
     * @param type $returnType
     * @param type $vmExtensionsArray
     * @return type
     * 
     * @since 2018-01-22
     * @author Sandun
     * 
     * @modified Sandun 2018-01-23 
     * @param type $fromTime
     * @param type $toTime
     */
    public static function getVoiceMailMessagesFromCustomers($dateFrom, $dateTo, $fromTime, $toTime, $contactNumber, $returnType, $vmExtensionsArray){
        $vmMessagesQuery = cdr::find()
                        ->select("src, dst, start, answer, end, duration")
                        ->where(['between', 'start', "$dateFrom $fromTime", "$dateTo $toTime"])
                        ->andWhere("calltype = 'incoming'");

                        if($contactNumber != NULL){
                            $vmMessagesQuery->andWhere(['like', 'src', $contactNumber]);
                        }

                        if($vmExtensionsArray != NULL){
                            $vmMessagesQuery->andWhere(['in', 'dst', $vmExtensionsArray]);
                        }else{
                            $vmMessagesQuery->andWhere(['like', 'dst', 'vm']);
                        }

                        $vmMessagesQuery->orderBy("end DESC");
                           if($returnType == "Count"){
                               // return the count
                               return $vmMessagesQuery->count();
                           }else if ($returnType == "Data"){
                               // return data
                               return $vmMessagesQuery->all();
                           }else {
                               // return Only 1st one
                               return $vmMessagesQuery->one();
                           }
    }

    public static function getIvrAnsweredCallsFromCustomers($dateFrom, $dateTo, $fromTime, $toTime, $contactNumber, $returnType, $ivrNumbersArray){
        $ivrNumbersQuery = cdr::find()
                        ->select("src, dst, start, answer, end")
                        ->where(['between', 'start', "$dateFrom $fromTime", "$dateTo $toTime"])
                        ->andWhere("calltype = 'incoming'");

                        if($contactNumber != NULL){
                            $ivrNumbersQuery->andWhere(['like', 'src', $contactNumber]);
                        }

                        $ivrNumbersQuery->andWhere(['in', 'dst', $ivrNumbersArray]);

                        $ivrNumbersQuery->orderBy("end DESC");
                        $ivrNumbersQuery->groupBy("uniqueid");

                           if($returnType == "Count"){
                               // return the count
                               return $ivrNumbersQuery->count();
                           }else if ($returnType == "Data"){
                               // return data
                               return $ivrNumbersQuery->all();
                           }else {
                               // return Only 1st one
                               return $ivrNumbersQuery->one();
                           }
    }

    public static function getVoipExtensionsByIncomingChannelNumberBetweenDates($channelNumbers, $dateFrom, $timeFrom, $dateTo, $timeTo){
        $voipExtensions = cdr::find()
            ->select("dst")
            ->Where(['between', 'start', "$dateFrom $timeFrom", "$dateTo $timeTo"])
            ->andWhere("calltype = 'incoming'")
            ->andWhere(["calleenum" => $channelNumbers])
            ->andWhere(["NOT LIKE", "dst", "IVR"])
            ->andWhere("CHAR_LENGTH(dst) > 3")
            ->distinct()
            ->all();

        $voiceExtensionsRemade = array();
        foreach($voipExtensions as $key){
            array_push($voiceExtensionsRemade, $key->dst);
        }

        return $voiceExtensionsRemade;
    }
    
    /**
     * <b>Finds destination null recorded call records</b>
     * <p></p>
     * 
     * @param type $dateFrom
     * @param type $dateTo
     * @return type
     * 
     * @since 2018-07-11
     * @author Sandun
     */
    public static function getDstNullRecordsBetweenDateRange($dateFrom, $dateTo){
//        $dstNullRecords = cdr::find()
//                ->where("dst", "")
//                ->where(["between", "start", "$dateFrom 00:00:00", "$dateTo 23:59:59"])
//                ->andWhere("disposition", 8)
//                ->all();
//        
//        return $dstNullRecords;
        
        $connection = Yii::$app->db2;
//
        $command = $connection->createCommand("SELECT * "
                . "FROM `cdr` "
                . "WHERE ((((disposition = 8) "
                . "AND (calltype = 'incoming')) "
                . "AND (CHAR_LENGTH(src) > 6)) "
                . "AND (`start` BETWEEN '$dateFrom 00:00:00' AND '$dateTo 23:59:59')) "
                . "AND (dst = '') "
                . "ORDER BY `start` DESC");

        $dstNullRecords = $command->queryAll();
//        $dstNullRecords = cdr::find()
//                            ->where("disposition = 8")
//                            ->andWhere("calltype = 'incoming'")
//                            ->andWhere("CHAR_LENGTH(src) > 6")
//                            ->andWhere(['between', 'start', "$dateFrom 00:00:00", "$dateTo 23:59:59"])
//                            ->andWhere("dst", "")
//                            ->orderBy("start DESC")
//                            ->all();
        return $dstNullRecords;
        
    }

    public static function getCdrdataByUniqueId($uniqueId){
        $cdrData = cdr::find()
                    ->where("uniqueid = $uniqueid")
                    ->all();

        return $cdrData;
    }

    public static function nextAnsweredExtensionOfuniqieId($uniqueId, $callEndedDatetime){
        $nextAnsweredExtensionData = cdr::find()
                                    ->where("uniqueid = $uniqueId")
                                    ->andWhere("disposition = 8")
                                    ->andWhere("start > '$callEndedDatetime'")
                                    ->orWhere("start = '$callEndedDatetime'")
                                    ->all();
        return $nextAnsweredExtensionData;
    }

    public static function previousAnsweredExtensionOfUniqueId($uniqueId, $callStartedDatetime){
        $previouslyAnsweredExtensionsData = cdr::find()
                                            ->where("uniqueid = $uniqueId")
                                            ->andWhere("disposition = 8")
                                            ->andWhere("end < '$callStartedDatetime'")
                                            ->orWhere("end = '$callStartedDatetime'")
                                            ->all();
        return $previouslyAnsweredExtensionsData;
    }

    public static function allAnsweredExtensionsInfoOfSameCall($uniqueId){
        $allAnsweredExtensionsInfo = cdr::find()
                                ->where("uniqueid = $uniqueId")
                                ->andWhere("disposition = 8")
                                ->orderBy("start asc")
                                ->all();
        return $allAnsweredExtensionsInfo;
    }

    /*
     * This function will get list of Missing audio file list of answered calls.. 
     * @author: Vinothan
     * @since: 29/01/2019
     * 
     * @param type $fromDate
     * @param type $toDate
     * @param type $fromTime
     * @param type $toTime     
     * @param type $agentExtension
     * @param type $contactNumber
     * @param type $returnType
     * @param type $extensionsInQueue
     */

    public static function getAgentAnsweredMissingAudioCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agentExtension, $contactNumber, $returnType, $extensionsInQueue, $selectedChannelsArray,$limit,$offset,$orderColumn,$orderDir,$asArray){
         $allAnsweredCalls = cdr::find()
                            ->where("disposition = 8")
                            ->andWhere("calltype = 'incoming'")
                            ->andWhere("CHAR_LENGTH(src) > 6")
                            ->andWhere(['between', 'start', "$fromDate $fromTime", "$toDate $toTime"])
                            ->andWhere(['or',['recordingfilename'=>NULL],['recordingfilename'=>""]]);
                            if($agentExtension != 0){
                                // single agent
                                $allAnsweredCalls->andWhere("dst = $agentExtension");
                            }else{
                                $allAnsweredCalls->andWhere(['not like', 'dst', 'IVR']);
                                $allAnsweredCalls->andWhere(['not like', 'dst', 'vm']);
                            }
                            
                            if($contactNumber != NULL){
                                // has contact number
                                $allAnsweredCalls->andWhere(['like', 'src', $contactNumber]);
                            }
                            
                            if($extensionsInQueue != NULL){
                                $allAnsweredCalls->andWhere(['in', 'dst', $extensionsInQueue]);
                            }else{
                                $allAnsweredCalls->andWhere("CHAR_LENGTH(dst) = 4");
                            }

                            if($selectedChannelsArray != NULL){
                                // has channels to filter
                                $allAnsweredCalls->andWhere(['in', 'calleenum', $selectedChannelsArray]);
                            }
                           
                            if($contactNumber == NULL){
                                $allAnsweredCalls->groupBy("uniqueid");
                            }

                            if ($orderColumn !=null) {
                                $allAnsweredCalls->orderBy("$orderColumn $orderDir");                                
                            }else{
                                $allAnsweredCalls->orderBy("answer DESC");                                
                            }

                           if($returnType == "Count"){
                               // return the count
                               return $allAnsweredCalls->count();
                           }else if ($returnType == "Data"){
                               // return data
                                $allAnsweredCalls->limit($limit);
                                $allAnsweredCalls->offset($offset);

                                if ($asArray != null) {
                                    $allAnsweredCalls->asArray();
                                }

                               return $allAnsweredCalls->all();
                           }else {
                               // return Only 1st one
                                if ($asArray != null) {
                                    $allAnsweredCalls->asArray();
                                }                            
                               return $allAnsweredCalls->one();
                           }                            
    }


    public static function getOutgoingMissingAudioCallsListByDate($agent_ex, $fromDate, $toDate, $fromTime, $toTime, $contactNumber,$extensionsInQueue,$returnType, $limit,$offset,$orderColumn,$orderDir,$asArray) {
        $allOutgoingCalls = cdr::find()
        ->select('src,dst,start,answer,end,duration,channel')
        ->andWhere("calltype = 'outgoing'")
        ->andWhere("disposition = '8'")
        ->andWhere(['or',['recordingfilename'=>NULL],['recordingfilename'=>""]]);
        if ($contactNumber != null) {
            $allOutgoingCalls->andWhere(['like','dst',"$contactNumber"]);
        }

        if ($agent_ex != 0) { // If agent extension not for all users
                $allOutgoingCalls->andWhere(['like', 'channel', "SIP/$agent_ex"]);

            if ($agent_ex != null && $fromDate != null && $toDate != null) {
                $allOutgoingCalls->andWhere(['between', 'start', "$fromDate $fromTime","$toDate $toTime"]);
            } else if ($agent_ex != null && $fromDate == null && $toDate != null) {
                $allOutgoingCalls->andWhere(['between', 'start', "$toDate 00:00:00","$toDate 23:59:59"]);
            } else if ($agent_ex != null && $fromDate != null && $toDate == null) {
                $allOutgoingCalls->andWhere(['between', 'start', "$fromDate",date('Y-m-d h:i:s')]);
            } else {
                echo "No further filters";
            }
        } else {
            if ($agent_ex != null && $fromDate != null && $toDate != null) {
                if($extensionsInQueue == null){
                    $allOutgoingCalls->andWhere(['like', 'channel', 'SIP/']);
                    $allOutgoingCalls->andWhere(['between', 'start', "$fromDate $fromTime","$toDate $toTime"]);
                }else{
                    $extensionsForQuery = cdr::getSipChannelExtensionNumberListForActivQuery($extensionsInQueue);
                    $allOutgoingCalls->andWhere('channel like '.$extensionsForQuery.'');
                    $allOutgoingCalls->andWhere(['between', 'start', "$fromDate $fromTime","$toDate $toTime"]);
                }
            } else if ($agent_ex != null && $fromDate == null && $toDate != null) {
                if($extensionsInQueue == null){
                    $allOutgoingCalls->andWhere(['like', 'channel', 'SIP/']);
                    $allOutgoingCalls->andWhere(['between', 'start', "$toDate 00:00:00","$toDate 23:59:59"]);
                }else{
                    $extensionsForQuery = cdr::getSipChannelExtensionNumberListForActivQuery($extensionsInQueue);
                    $allOutgoingCalls->andWhere('channel like '.$extensionsForQuery.'');
                    $allOutgoingCalls->andWhere(['between', 'start', "$toDate 00:00:00","$toDate 23:59:59"]);
                }
            } else if ($agent_ex != null && $fromDate != null && $toDate == null) {
                if($extensionsInQueue == null){
                    $allOutgoingCalls->andWhere(['like', 'channel', 'SIP/']);
                    $allOutgoingCalls->andWhere(['between', 'start', "$fromDate 00:00:00",date('Y-m-d h:i:s')]);
                }else{
                    $extensionsForQuery = cdr::getSipChannelExtensionNumberListForActivQuery($extensionsInQueue);
                    $allOutgoingCalls->andWhere('channel like '.$extensionsForQuery.'');
                    $allOutgoingCalls->andWhere(['between', 'start', "$fromDate 00:00:00",date('Y-m-d h:i:s')]);
                }
            } else {
                if($extensionsInQueue == null){
                    $allOutgoingCalls->andWhere(['like', 'channel', 'SIP/']);                    
                    $allOutgoingCalls->andWhere("MONTH(DATE_FORMAT(STR_TO_DATE(start, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d')) = MONTH(CURRENT_DATE())");
                }else{
                    $extensionsForQuery = cdr::getSipChannelExtensionNumberListForActivQuery($extensionsInQueue);
                    $allOutgoingCalls->andWhere('channel like '.$extensionsForQuery.'');
                    $allOutgoingCalls->andWhere("MONTH(DATE_FORMAT(STR_TO_DATE(start, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d')) = MONTH(CURRENT_DATE())");
                }
            }
        }


        if ($returnType == 'Data') {

        if ($orderColumn !=null) {
            $allOutgoingCalls->orderBy("$orderColumn $orderDir");                                
        }else{
            $allOutgoingCalls->orderBy("start DESC");                                
        }

        $allOutgoingCalls->limit($limit)
                         ->offset($offset);

        if ($asArray != null) {
            $allOutgoingCalls->asArray();
            return $allOutgoingCalls->all();
        }                                 

        }else{
            return $allOutgoingCalls->count();            
        }
        
    }

    private function getSipChannelExtensionNumberListForActivQuery($extensionsInQueueArray){
        if(count($extensionsInQueueArray) > 0){
            $multipleLikeString = "";
            for($x = 0; $x < count($extensionsInQueueArray); $x++){
                $extensionNumber = $extensionsInQueueArray[$x];
                if($x == 0){
                    // first element
                    $multipleLikeString = "'SIP/$extensionNumber%'";
                }else{
                    $multipleLikeString = $multipleLikeString." OR channel LIKE 'SIP/$extensionNumber%'";
                }
                
            }
            return $multipleLikeString;
        }else{
            return "SIP/%";
        }
    }


    /*
     * return Answered call details filter by date , created to filter data for pagination view
     * @author: Vinothan
     * @since: 06/02/2019
     * 
     * @param type $fromTime
     * @param type $toTime
     * 
     */
    public static function getAgentAnsweredCallsDataBetweenDatesForPagination($fromDate, $toDate, $fromTime, $toTime, $agentExtension, $contactNumber, $returnType, $extensionsInQueue, $selectedChannelsArray,$limit,$offset,$orderColumn,$orderDir,$asArray){
         $allAnsweredCalls = cdr::find()
                            ->where("disposition = 8")
                            ->andWhere("calltype = 'incoming'")
                            ->andWhere("CHAR_LENGTH(src) > 6")
                            ->andWhere(['between', 'start', "$fromDate $fromTime", "$toDate $toTime"]);
                            if($agentExtension != 0){
                                // single agent
                                $allAnsweredCalls->andWhere("dst = $agentExtension");
                            }else{
                                $allAnsweredCalls->andWhere(['not like', 'dst', 'IVR']);
                                $allAnsweredCalls->andWhere(['not like', 'dst', 'vm']);
                            }
                            
                            if($contactNumber != NULL){
                                // has contact number
                                $allAnsweredCalls->andWhere(['like', 'src', $contactNumber]);
                            }
                            
                            if($extensionsInQueue != NULL){
                                $allAnsweredCalls->andWhere(['in', 'dst', $extensionsInQueue]);
                            }

                            if($selectedChannelsArray != NULL){
                                // has channels to filter
                                $allAnsweredCalls->andWhere(['in', 'calleenum', $selectedChannelsArray]);
                            }
                           
                            if($contactNumber == NULL){
                                $allAnsweredCalls->groupBy("uniqueid");
                            }

                            if ($orderColumn !=null) {
                                $allAnsweredCalls->orderBy("$orderColumn $orderDir");                                
                            }else{
                                $allAnsweredCalls->orderBy("answer DESC");                                
                            }

                           if($returnType == "Count"){
                               // return the count
                               return $allAnsweredCalls->count();
                           }else if ($returnType == "Data"){
                               // return data
                                $allAnsweredCalls->limit($limit);
                                $allAnsweredCalls->offset($offset);

                                if ($asArray != null) {
                                    $allAnsweredCalls->asArray();
                                }

                               return $allAnsweredCalls->all();
                           }else {
                               // return Only 1st one
                                if ($asArray != null) {
                                    $allAnsweredCalls->asArray();
                                }                            
                               return $allAnsweredCalls->one();
                           }                            
    }


    /*
     * return Outgoing call details filter by date , created to filter data for pagination view
     * @author: Vinothan
     * @since: 06/02/2019
     * 
     * @param type $fromTime
     * @param type $toTime
     * @description Added 2 new parameters to support the time filtering facility
     * 
     */    


    public static function getOutgoingCallsListByDateForPaginationActiveQuery($agent_ex, $fromDate, $toDate, $fromTime, $toTime, $contactNumber,$extensionsInQueue,$returnType, $limit,$offset,$orderColumn,$orderDir,$asArray) {
        $allOutgoingCalls = cdr::find()
        ->where("calltype = 'outgoing'");
        if ($contactNumber != null) {
            $allOutgoingCalls->andWhere(['like','dst',"$contactNumber"]);
        }

        if ($agent_ex != 0) { // If agent extension not for all users
                $allOutgoingCalls->andWhere(['like', 'channel', "SIP/$agent_ex"]);

            if ($agent_ex != null && $fromDate != null && $toDate != null) {
                $allOutgoingCalls->andWhere(['between', 'start', "$fromDate $fromTime","$toDate $toTime"]);
            } else if ($agent_ex != null && $fromDate == null && $toDate != null) {
                $allOutgoingCalls->andWhere(['between', 'start', "$toDate 00:00:00","$toDate 23:59:59"]);
            } else if ($agent_ex != null && $fromDate != null && $toDate == null) {
                $allOutgoingCalls->andWhere(['between', 'start', "$fromDate",date('Y-m-d h:i:s')]);
            } else {
                echo "No further filters";
            }
        } else {
            if ($agent_ex != null && $fromDate != null && $toDate != null) {
                if($extensionsInQueue == null){
                    $allOutgoingCalls->andWhere(['like', 'channel', 'SIP/']);
                    $allOutgoingCalls->andWhere(['between', 'start', "$fromDate $fromTime","$toDate $toTime"]);
                }else{
                    $extensionsForQuery = cdr::getSipChannelExtensionNumberListForActivQuery($extensionsInQueue);
                    $allOutgoingCalls->andWhere('channel like '.$extensionsForQuery.'');
                    $allOutgoingCalls->andWhere(['between', 'start', "$fromDate $fromTime","$toDate $toTime"]);
                }
            } else if ($agent_ex != null && $fromDate == null && $toDate != null) {
                if($extensionsInQueue == null){
                    $allOutgoingCalls->andWhere(['like', 'channel', 'SIP/']);
                    $allOutgoingCalls->andWhere(['between', 'start', "$toDate 00:00:00","$toDate 23:59:59"]);
                }else{
                    $extensionsForQuery = cdr::getSipChannelExtensionNumberListForActivQuery($extensionsInQueue);
                    $allOutgoingCalls->andWhere('channel like '.$extensionsForQuery.'');
                    $allOutgoingCalls->andWhere(['between', 'start', "$toDate 00:00:00","$toDate 23:59:59"]);
                }
            } else if ($agent_ex != null && $fromDate != null && $toDate == null) {
                if($extensionsInQueue == null){
                    $allOutgoingCalls->andWhere(['like', 'channel', 'SIP/']);
                    $allOutgoingCalls->andWhere(['between', 'start', "$fromDate 00:00:00",date('Y-m-d h:i:s')]);
                }else{
                    $extensionsForQuery = cdr::getSipChannelExtensionNumberListForActivQuery($extensionsInQueue);
                    $allOutgoingCalls->andWhere('channel like '.$extensionsForQuery.'');
                    $allOutgoingCalls->andWhere(['between', 'start', "$fromDate 00:00:00",date('Y-m-d h:i:s')]);
                }
            } else {
                if($extensionsInQueue == null){
                    $allOutgoingCalls->andWhere(['like', 'channel', 'SIP/']);                    
                    $allOutgoingCalls->andWhere("MONTH(DATE_FORMAT(STR_TO_DATE(start, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d')) = MONTH(CURRENT_DATE())");
                }else{
                    $extensionsForQuery = cdr::getSipChannelExtensionNumberListForActivQuery($extensionsInQueue);
                    $allOutgoingCalls->andWhere('channel like '.$extensionsForQuery.'');
                    $allOutgoingCalls->andWhere("MONTH(DATE_FORMAT(STR_TO_DATE(start, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d')) = MONTH(CURRENT_DATE())");
                }
            }
        }


        if ($returnType == 'Data') {

        if ($orderColumn !=null) {
            $allOutgoingCalls->orderBy("$orderColumn $orderDir");                                
        }else{
            $allOutgoingCalls->orderBy("start DESC");                                
        }

        $allOutgoingCalls->limit($limit)
                         ->offset($offset);

        if ($asArray != null) {
            $allOutgoingCalls->asArray();
            return $allOutgoingCalls->all();
        }                                 

        }else{
            return $allOutgoingCalls->count();            
        }
        
    }


      /**
     * <b>Gets the missed calls data between dates or from contact number<b/>
     * <p>This function returns the count or the dataset of missed calls recorded between the from and to date range<p/>
     * @param String $fromDate
     * @param String $toDate
     * @param String $contactNumber
     * @return Array missed calls data / Int count
     * @author Vinothan
     * @since 06/02/2019
     * 
     */
    public function getAllMissedCallsDataBetweenDatesForPagination($fromDate, $toDate, $fromTime, $toTime, $contactNumber, $returnType, $extensionsInQueue,$limit,$offset,$orderColumn,$orderDir,$asArray){        
        $allAnsweredCalls = cdr::find()
                            ->select("uniqueid")
                            ->where("disposition = 8")
                            ->andWhere("calltype = 'incoming'")
                            ->andWhere("CHAR_LENGTH(src) > 6")
                            ->andWhere(['between', 'start', "$fromDate $fromTime", "$toDate $toTime"])
                            ->andWhere(['not like', 'dst', 'IVR']);
                    if($extensionsInQueue != NULL){
                        $allAnsweredCalls->andWhere(['in', 'dst', $extensionsInQueue]);
                    }
                        $allAnsweredCalls->groupBy("uniqueid");
        
        $allMissedCalls = cdr::find()
                            ->select("end, src,sum(duration) as dur")
                            ->where("disposition = 0")
                            ->andWhere("calltype = 'incoming'")
                            ->andWhere("CHAR_LENGTH(src) > 6")
                            ->andWhere(['between', 'start', "$fromDate $fromTime", "$toDate $toTime"])
                            ->andWhere(['not in', 'uniqueid', $allAnsweredCalls]);
                            if($contactNumber != NULL){
                                // has contact number
                                $allMissedCalls->andWhere(['like', 'src', $contactNumber]);
                            }
                            
                            if($extensionsInQueue != NULL){
                                $allMissedCalls->andWhere(['in', 'dst', $extensionsInQueue]);
                            }
        
                           // if($contactNumber == NULL){
                                $allMissedCalls->groupBy("uniqueid");
                           // }

                            if ($orderColumn !=null) {
                                $allMissedCalls->orderBy("$orderColumn $orderDir");                                
                            }else{
                                $allMissedCalls->orderBy("end DESC");                                
                            }

                           if($returnType == "Count"){
                               // return the count
                               return $allMissedCalls->count();
                           }else if ($returnType == "Data"){
                               // return data
                                $allMissedCalls->limit($limit)->offset($offset);

                                if ($asArray != null) {
                                    $allMissedCalls->asArray();
                                }                                                                     
                               return $allMissedCalls->all();
                           }else {
                               // return Only 1st one
                                if ($asArray != null) {
                                    $allMissedCalls->asArray();
                                }                            
                               return $allMissedCalls->one();
                           }                          

    }

      /**
     * <b>Gets the missed calls data between dates or from contact number<b/>
     * <p>This function returns the count of exact missed call duration by calculating minimum starttime and maximum endtime of a specific uniqueid<p/>
     * 
     * @param String $fromDate
     * @param String $toDate
     * @param String $contactNumber
     * @return Array missed calls data / Int count
     * 
     * @author Vinothan
     * @since 06/02/2019
     * 
     */


    public function getAllMissedCallsDataBetweenDatesForPaginationSql($fromDate,$toDate,$fromTime,$toTime,$contactNumber,$returnType, $extensionsInQueue,$per_page,$start){

        $connection = Yii::$app->db2;

        $query = "SELECT A.uniqueid as uniqueid, min(A.start) as start, max(A.end) as end,A.src as src, A.duration as duration,A.disposition as disposition, a.dst as dst 
                FROM   (SELECT uniqueid, 
                               start,
                               end, 
                               src, 
                               duration, 
                               disposition, 
                               dst 
                        FROM   cdr 
                        WHERE  ( ( disposition = 0 ) 
                                  OR ( ( disposition = 8 ) 
                                       AND ( dst LIKE ( '%IVR%' ) ) ) ) 
                               AND calltype = 'incoming' 
                               AND Char_length(src) > 6";
        if ($contactNumber != null) {
                       $query = $query. " AND src LIKE('%$contactNumber%')";             
                                }
        if ($extensionsInQueue != null) {
                       $extensionsForQuery = cdr::getSipChannelExtensionNumberListForSql($extensionsInQueue);
                        $query = $query." AND dst in($extensionsForQuery)";
                                }
                        $query = $query ." AND start BETWEEN '$fromDate $fromTime' AND '$toDate $toTime'
                                            order by end DESC
                                           ) AS A 
                                           left join (SELECT uniqueid 
                                                      FROM   cdr 
                                                      WHERE  ( ( disposition = 8 ) 
                                                               AND ( dst NOT LIKE ( '%IVR%' ) ) ) 
                                                             AND calltype = 'incoming' 
                                                             AND Char_length(src) > 6 
                                                             AND start BETWEEN '$fromDate $fromTime' AND '$toDate $toTime'
                                                      GROUP  BY uniqueid
                                                     ) AS B 
                                                  ON A.uniqueid = B.uniqueid 
                                    WHERE  B.uniqueid IS NULL
                                    group by A.uniqueid
                                    order by A.end DESC";

         if ($per_page!=null) {
                    $query  = $query." LIMIT $per_page OFFSET $start"; 
                }
        $command = $connection->createCommand($query);
        if ($returnType == "Count") {
            $allMissedCallssql = $command->queryAll();            
            return count($allMissedCallssql);
        }else{
            $allMissedCallssql = $command->queryAll();
            return $allMissedCallssql;
        }

    }


        /**
     * <b>Returns abandoned calls of an extension between a time period</b>
     * <p>This function returns the abandoned calls of an agent extension between a time period</p>
     * 
     *@modified vinothan 2018-09-20
     *<b> Added pagination filters </b>
     * @param string $perpage
     * @param string $start
     * 
     */
    public function getAbandonedCallsOfAnAgentBetweenDatesForPagination($fromDate, $toDate, $fromTime, $toTime, $agentExtension, $contactNumber, $returnType, $extensionsInQueue,$limit,$offset,$orderColumn,$orderDir){
        //select * from cdr where disposition = 4 and calltype = "incoming" and CHAR_LENGTH(src) > 6 and `start` like "2017-11-21%" and dst = '4020'
        if($agentExtension != 0){
            // single agent
            $abandonedCalls = cdr::find()
                            ->select("end, src")
                            ->where("disposition = 4")
                            ->andWhere("calltype = 'incoming'")
                            ->andWhere("CHAR_LENGTH(src) > 6")
                            ->andWhere(['between', 'start', "$fromDate $fromTime", "$toDate $toTime"])
                            ->andWhere("dst = '$agentExtension'");
                    if($contactNumber != NULL){
                        // has contact number
                        $abandonedCalls->andWhere(['like', 'src', $contactNumber]);
                    }
                    
                    if($extensionsInQueue != NULL){
                        $abandonedCalls->andWhere(['in', 'dst', $extensionsInQueue]);
                    }

                    if ($orderColumn !=null) {
                        $abandonedCalls->orderBy("$orderColumn $orderDir");                                
                    }else{
                        $abandonedCalls->orderBy("start DESC");                                
                    }
                    
                    if($returnType == "Count"){
                        // return the count
                        return $abandonedCalls->count();
                    }else if ($returnType == "Data"){
                        // return data
                        if ($limit != null) {
                            $abandonedCalls->limit($limit)->offset($offset);
                        }
                        return $abandonedCalls->asArray()->all();
                    }else {
                        // return Only 1st one
                        return $abandonedCalls->one();
                    }
                            
        }else{
            // all agents
            return NULL;
        }
    }


   /**
     * <b></b>
     * <p></p>
     * 
     * @param type $perpage
     * @param type $start
     * 
     * @since 2018-09-21
     * @author Vinothan
     * 
     */
    public function getVoiceMailMessagesFromCustomersForPagination($dateFrom, $dateTo, $fromTime, $toTime, $contactNumber, $returnType, $vmExtensionsArray,$limit,$offset,$orderColumn,$orderDir){
        $vmMessagesQuery = cdr::find()
                        ->select("src, dst, start, answer, end, duration")
                        ->where(['between', 'start', "$dateFrom $fromTime", "$dateTo $toTime"])
                        ->andWhere("calltype = 'incoming'");

                        if($contactNumber != NULL){
                            $vmMessagesQuery->andWhere(['like', 'src', $contactNumber]);
                        }

                        if($vmExtensionsArray != NULL){
                            $vmMessagesQuery->andWhere(['in', 'dst', $vmExtensionsArray]);
                        }else{
                            $vmMessagesQuery->andWhere(['like', 'dst', 'vm']);
                        }

                    if ($orderColumn !=null) {
                                $vmMessagesQuery->orderBy("$orderColumn $orderDir");                                
                            }else{
                                $vmMessagesQuery->orderBy("end DESC");                                
                            }                        

                           if($returnType == "Count"){
                               // return the count
                               return $vmMessagesQuery->count();
                           }else if ($returnType == "Data"){
                               // return data
                                    if ($limit != null) {
                                        $vmMessagesQuery->limit($limit)->offset($offset);
                                    }                            
                               return $vmMessagesQuery->all();
                           }else {
                               // return Only 1st one
                               return $vmMessagesQuery->one();
                           }
    }

    public function getIvrAnsweredCallsFromCustomersForPagination($dateFrom, $dateTo, $fromTime, $toTime, $contactNumber, $returnType, $ivrNumbersArray,$limit,$offset,$orderColumn,$orderDir){
        $ivrNumbersQuery = cdr::find()
                        ->select("src, dst, start, answer, end")
                        ->where(['between', 'start', "$dateFrom $fromTime", "$dateTo $toTime"])
                        ->andWhere("calltype = 'incoming'");

                        if($contactNumber != NULL){
                            $ivrNumbersQuery->andWhere(['like', 'src', $contactNumber]);
                        }

                        $ivrNumbersQuery->andWhere(['in', 'dst', $ivrNumbersArray]);

                        if ($orderColumn !=null) {
                                $ivrNumbersQuery->orderBy("$orderColumn $orderDir");                                
                            }else{
                                $ivrNumbersQuery->orderBy("end DESC");                                
                            }  

                        $ivrNumbersQuery->groupBy("uniqueid");

                           if($returnType == "Count"){
                               // return the count
                               return $ivrNumbersQuery->count();
                           }else if ($returnType == "Data"){
                               // return data
                                    if ($limit != null) {
                                        $ivrNumbersQuery->limit($limit)->offset($offset);
                                    }                            
                               return $ivrNumbersQuery->all();
                           }else {
                               // return Only 1st one
                               return $ivrNumbersQuery->one();
                           }
    }


    /*
     * This function will get the Recorded file name of a requested cdr file from CDR table 
     * @author: Vinothan
     * @since: 25/04/2018
     *     
     */
    public function findRecordingFilename($acctId){
        $result = cdr::find()
                    ->select("recordingfilename")
                    ->where("AcctId = '$acctId'");
                    return $result->asArray()->one();
    }


    /*
     * This function will update the blank Recorded file name with new find file name from Nas/Pbx 
     * @author: Vinothan
     * @since: 26/04/2018
     *     
     */
    public function updateRecordingFilename($acctId,$filename){
        $newCdrModel =cdr::findOne($acctId);
        $newCdrModel->recordingfilename = $filename;        
        if ($newCdrModel->save()) {
            return true;
        }else{
            return true;
        }
    }
    

    /*
     * This function will find Recorded file name of a requested cdr file from CDR table 
     * @author: Vinothan
     * @since: 21/06/2019
     *     
     */
    public function findRecordingFilenameExists($acctId){
        $result = cdr::find()
                    ->select("recordingfilename")
                    ->where("AcctId = '$acctId'");
        $output =  $result->asArray()->one();
        if ($output["recordingfilename"] != "") {
            return true;
        }else{
            return false;
        }
    }
   
    public function getIndividualAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $returnType, $agentExtension){      
        
        $allAnsweredCalls = cdr::find()
                            ->where("disposition = 8")
                            ->andWhere("calltype = 'incoming'")
                            ->andWhere("CHAR_LENGTH(src) > 6")
                            //->andWhere("start between '$fromDate $fromTime' and '$toDate $toTime'");
                            ->andWhere(['between', 'start', "$fromDate $fromTime", "$toDate $toTime"]);
                if($agentExtension != 0){
                    // single agent
                    $allAnsweredCalls->andWhere("dst = $agentExtension");
                }else{
                    $allAnsweredCalls->andWhere(['not like', 'dst', 'IVR']);
                    $allAnsweredCalls->andWhere(['not like', 'dst', 'vm']);
                } 

                if($returnType == "Count"){
                    // return the count
                    //echo $allAnsweredCalls->createCommand()->sql;
                    return $allAnsweredCalls->count();
                }
            }

    public function getIndividualMissedCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $returnType, $agentExtension){        
       
        $allAnsweredCalls = cdr::find()
                            ->select("uniqueid")
                            ->where("disposition = 8")
                            ->andWhere("calltype = 'incoming'")
                            ->andWhere("CHAR_LENGTH(src) > 6")
                            ->andWhere(['between', 'start', "$fromDate $fromTime", "$toDate $toTime"])
                            ->andWhere(['not like', 'dst', 'IVR']);

                        if($agentExtension != 0){
                            // single agent          
                            $allAnsweredCalls->andWhere("dst = $agentExtension");
                        }else{                                      
                            $allAnsweredCalls->andWhere(['not like', 'dst', 'IVR']);
                            $allAnsweredCalls->andWhere(['not like', 'dst', 'vm']);
                        } 
                        
                        $allAnsweredCalls->groupBy("uniqueid");

       $allMissedCalls = cdr::find()
                        ->select("end, src, duration")
                        ->where("disposition = 0")
                        ->andWhere("calltype = 'incoming'")
                        ->andWhere("CHAR_LENGTH(src) > 6")
                        ->andWhere(['between', 'start', "$fromDate $fromTime", "$toDate $toTime"])
                        ->andWhere(['not in', 'uniqueid', $allAnsweredCalls]);  
                        
                        $allMissedCalls->groupBy("uniqueid");

                    if($returnType == "Count"){
                        // return the count                      
                        return $allMissedCalls->count();
                    }        
    }

    public function getIndividualOutgoingCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $returnType, $agentExtension, $extensionsInQueue){
        
        $allOutgoingCalls = cdr::find()
        ->where("calltype = 'outgoing'");      

        if ($agentExtension != 0) { // If agent extension not for all users
                $allOutgoingCalls->andWhere(['like', 'channel', "SIP/$agentExtension"]);

            if ($agentExtension != null && $fromDate != null && $toDate != null) {
                $allOutgoingCalls->andWhere(['between', 'start', "$fromDate $fromTime","$toDate $toTime"]);
            } else if ($agentExtension != null && $fromDate == null && $toDate != null) {
                $allOutgoingCalls->andWhere(['between', 'start', "$toDate 00:00:00","$toDate 23:59:59"]);
            } else if ($agentExtension != null && $fromDate != null && $toDate == null) {
                $allOutgoingCalls->andWhere(['between', 'start', "$fromDate",date('Y-m-d h:i:s')]);
            } else {
                echo "No further filters";
            }
        } else {
            if ($agentExtension != null && $fromDate != null && $toDate != null) {
                if($extensionsInQueue == null){
                    $allOutgoingCalls->andWhere(['like', 'channel', 'SIP/']);
                    $allOutgoingCalls->andWhere(['between', 'start', "$fromDate $fromTime","$toDate $toTime"]);
                }
            } else if ($agentExtension != null && $fromDate == null && $toDate != null) {
                if($extensionsInQueue == null){
                    $allOutgoingCalls->andWhere(['like', 'channel', 'SIP/']);
                    $allOutgoingCalls->andWhere(['between', 'start', "$toDate 00:00:00","$toDate 23:59:59"]);
                }
            } else if ($agentExtension != null && $fromDate != null && $toDate == null) {
                if($extensionsInQueue == null){
                    $allOutgoingCalls->andWhere(['like', 'channel', 'SIP/']);
                    $allOutgoingCalls->andWhere(['between', 'start', "$fromDate 00:00:00",date('Y-m-d h:i:s')]);
                }
            } else {
                if($extensionsInQueue == null){
                    $allOutgoingCalls->andWhere(['like', 'channel', 'SIP/']);                    
                    $allOutgoingCalls->andWhere("MONTH(DATE_FORMAT(STR_TO_DATE(start, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d')) = MONTH(CURRENT_DATE())");
                }
            }
        }

        if ($returnType == 'Count') {
                                            
            return $allOutgoingCalls->count(); 
        }
    } 

}
