<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Description of help_requests
 *
 * @author Sandun
 * @since 2017-07-12
 */
class Cdr_records extends ActiveRecord {

    private static function getDb() {
        return Yii::$app->db2;
    }

    /**
     * <b>Get cdr records of user</b>
     * <p>This function returns cdr records for a user number</p>
     * 
     * @author Vikum
     * @since 2017-07-21
     * 
     * @param number
     * @return array
     */
    public static function getRecordsByNumber($number) {
        $query = $this->getDb()->find()
                ->where("src =" . $number)
                ->all();
        return $query;
    }

}
