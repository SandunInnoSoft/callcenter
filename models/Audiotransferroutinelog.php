<?php
/**
 * Created by PhpStorm.
 * User: Sandun
 * Date: 1/25/2019
 * Time: 5:45 PM
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Audiotransferroutinelog extends ActiveRecord
{
    public static function getDb() {
        return Yii::$app->db4;
    }

    public static function getRoutineLastExecutedTime(){
        return Audiotransferroutinelog::find()
                        ->max("executionEndedTime");
    }
}