<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This model class is to exchange data with the contact_list database table
 *
 * @author Vikum
 * @since 2017-07-23
 */
class Contact_list extends ActiveRecord {

    public static function saveContact($data) {

        $contactRecord = new Contact_list();
        $contactRecord->contact_number = $data['number'];
        $contactRecord->contact_name = $data['name'];
        $contactRecord->created_date = $data['created_date_time'];
        $contactRecord->insert();
        return $contactRecord->getPrimaryKey();
    }

    public static function loadAllContact() {
        $contactRecord = new Contact_list();
        return $contactRecord->find()
                        ->select('contact_number, contact_name, id')
                        ->orderBy(["contact_name" => SORT_ASC])
                        ->all();
    }

    public static function searchContact($val) {
        $contactRecords = new Contact_list();
        return $contactRecords->find()
                        ->select('contact_number, contact_name, id')
                        ->where(['like', 'contact_name', $val])
                        ->orderBy(["contact_name" => SORT_ASC])
                        ->all();
    }

    public static function deleteContact($id) {
        return Contact_list::deleteAll("id = $id");
    }

}
