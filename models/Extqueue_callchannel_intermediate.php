<?php


namespace app\models;
use yii;
use yii\db\ActiveRecord;
use app\models\Call_channel;

/**
 * This model class interacts with extqueue_callchannel_intermediate table in DB1
 *
 * @author Sandun
 * @since 2018-02-16
 */
class Extqueue_callchannel_intermediate extends ActiveRecord{
    
    /**
     * @since 2018-2-19
     * @author Sandun
     */
    public static function getChannelIdsofTheQueue($queueId){
        return Extqueue_callchannel_intermediate::find()
            ->where("extqueue_id = $queueId")
            ->all();
    }



    /**
     * <b></b>
     * <p></p>
     * 
     * @author Sandun
     * @since 2018-2-19
     * @return 1-many relation object
     */
    // public function getChannels(){
    //     return $this->hasMany(Call_channel::classname(), ['channel_id' => 'id']);
    // }
}
