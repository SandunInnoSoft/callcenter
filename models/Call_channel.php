<?php


namespace app\models;
use yii;
use yii\db\ActiveRecord;
use app\models\Extqueue_callchannel_intermediate;

/**
 * This model class interacts with Call_channel table in DB1
 *
 * @author Sandun
 * @since 2018-02-16
 */
class Call_channel extends ActiveRecord{
    
    /**
     * <b></b>
     * 
     * @author Sandun
     * @since 2018-2-19
     */
    public static function getChannelInfoFomId($channelId){
        return call_channel::find()
                ->where("id = $channelId")
                ->one();
    }

    // public function getQueueids(){
    //     return $this->hasOne(Extqueue_callchannel_intermediate::classname(), ['extqueue_id' => 'id']);
    // }
    
    public static function getChannelInfoFromChannelNumber($channelNumber){
        return call_channel::find()
                ->where("channel_number = $channelNumber")
                ->one();
    }
    
}
