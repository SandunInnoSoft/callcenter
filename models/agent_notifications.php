<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of agent_notifications
 *
 * @author Prabath
 * @since 25/07/2017
 * 
 */
class agent_notifications extends ActiveRecord {

    public static function getAgentNotifications($agent_id) {

        $agentNotif = new agent_notifications();
        $requestData = $agentNotif->find()
                ->where("agent_id = $agent_id")
                ->andWhere("notif_status = 'active'")
                ->all();
        if ($requestData) {
            $updater = new agent_notifications();
            $id = $requestData[0]['id'];
            $updater->updateAll(['notif_status' => 'inactive'], "id = $id");
            return $requestData;
        } else {
            return NULL;
        }
    }

    /**
     * <b>Inserts a new agent notification record</b>
     * <p>This function inserts a new agent notification record to the database table</p>
     * 
     * @author Sandun
     * @since 2017-08-04
     * @param array $notificationData
     * @return boolean
     */
    public static function insertAgentNotification($notificationData) {
        $agentNotif = new agent_notifications();
        $agentNotif->agent_id = $notificationData['to'];
        $agentNotif->supervisor_id = $notificationData['from'];
        $agentNotif->notif_type = $notificationData['notif_type'];
        $agentNotif->notif_status = $notificationData['status'];
        $agentNotif->notif_time = date("Y-m-d H:i:s");
        return $agentNotif->insert();
    }

}
