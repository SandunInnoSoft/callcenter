<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of call_center_user
 *
 * @since 14/8/2018
 * @author Vinothan
 */

namespace app\models;

use Yii;
//use yii\base\Model;
use yii\db\ActiveRecord;
use app\models\Roles;

class Allowed_exec_extqueues extends ActiveRecord {

    /**
     * <b>Delete and update queues of executive user</b>
     * <p>This function checks and deleted the allowed extensions of executive users and update the same</p>
     * 
     * @param int $executiveId
     * @return boolean
     * 
     * @since 2018-08-14
     * @author Vinothan
     */
    
    public function deleteAllowedQueuesOfExecutive($executiveId){        
        $query  = Allowed_exec_extqueues::deleteAll(['user_id' => $executiveId]);
        if($query){
            return TRUE;
        }else{
            return FALSE;
        }            
    }

    public function addNewAllowedQueuesOfExecutive($data){
        $allowed_exec_extqueues = new Allowed_exec_extqueues();

        $allowed_exec_extqueues->user_id = $data['user_id'];
        $allowed_exec_extqueues->allowed_ext_queue_id = $data['allowed_ext_queue_id'];
        $allowed_exec_extqueues->granted_by = $data['granted_by'];

        $allowed_exec_extqueues->attributes = $data;
        $query = $allowed_exec_extqueues->save();

        if($query){
            return TRUE;
        }else{
            return FALSE;
        }         
    }
}
