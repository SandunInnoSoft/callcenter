<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Description of call_queue
 *
 * @author Sandun
 * @since 2017 - 09 - 13
 */
class call_forwards extends ActiveRecord{
    
    
    public static function insertForwardedCall($cidnum, $exten, $ts, $time){
        $call = new call_forwards();
        $call->caller_number = $cidnum;
        $call->agent_extension = $exten;
        $call->time_stamp = $ts;
        $call->created_date = $time;
        $call->state = 1;
        return $call->insert();
    }
    
    
}// end of call_forwards model class
