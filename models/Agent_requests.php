<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Description of help_requests
 *
 * @author Sandun
 * @since 2017-07-12
 */
class Agent_requests extends ActiveRecord {

    /**
     * <b>Saves the break request</b>
     * <p>This function saves the initial agent request data in db</p>
     * 
     * @author Vikum
     * @since 2017-07-17
     * 
     * @param array()
     * @return bool
     */
    public static function saveRequest($breakData) {
        $breakRequest = new Agent_requests();
        $breakRequest->request_type = $breakData['request_type'];
        $breakRequest->requested_time = date("Y-m-d H:m:s");
        $breakRequest->request_status = $breakData['request_status'];
        $breakRequest->agent_id = $breakData['agent_id'];
        $breakRequest->created_date = date("Y-m-d H:i:s");
        $breakRequest->active = 1;
        $breakRequest->consumed = 0;
        return $breakRequest->insert();
    }

    public static function getDailyBreakTime($id) {
        $agentRequests = new Agent_requests();
        if ($id != 0) { // if agent id is not all
            $data = $agentRequests->find()
                    ->select('approved_time_period')
                    ->where("request_status = 'closed'") // At the moment 
//                ->orWhere("request_status = 'taken'")
                    ->andWhere('DATE(closed_time) = CURDATE()')
                    ->andWhere("agent_id = $id")
                    ->sum('TIMESTAMPDIFF(MINUTE,taken_time,closed_time)'); // This gets the difference between closed times and taken times of all closed breaks
//                ->sum('approved_time_period');
        } else { // If agent id all
            $data = $agentRequests->find()
                    ->select('approved_time_period')
                    ->where("request_status = 'closed'") // At the moment 
//                ->orWhere("request_status = 'taken'")
                    ->andWhere('DATE(closed_time) = CURDATE()')
//                ->andWhere("agent_id = $id")
                    ->sum('TIMESTAMPDIFF(MINUTE,taken_time,closed_time)'); // This gets the difference between closed times and taken times of all closed breaks
        }
        return $data;
//        return 1.3;
    }

    /**
     * 
     * @param type $id
     * @param type $dateFrom
     * @param type $dateTo
     * @param type $fromDate
     * @param type $todate
     * @return int
     */
    public static function getBreakTimeByDate($id, $dateFrom, $dateTo, $fromTime, $toTime) {
        if ($dateFrom != '' && $dateTo != '') {
            $agentRequests = new Agent_requests();
            if ($id != 0) { // If id not for all users
                $data = $agentRequests->find()
                        ->select('approved_time_period')
//                        ->where("DATE(closed_time) > STR_TO_DATE('$dateFrom', '%Y-%m-%d')") // At the moment 
                        ->where(['between', 'closed_time', $dateFrom." $fromTime", $dateTo." $toTime"])
//                ->orWhere("request_status = 'taken'")
//                    ->andWhere('DATE(closed_time) = CURDATE()')
//                        ->andWhere("DATE(closed_time) < STR_TO_DATE('$dateTo', '%Y-%m-%d')")
                        ->andWhere("request_status = 'closed'")
                        ->andWhere("agent_id = $id")
                        ->sum('TIMESTAMPDIFF(MINUTE,taken_time,closed_time)'); // This gets the difference between closed times and taken times of all closed breaks
//                ->sum('approved_time_period');
            } else {
                $data = $agentRequests->find()
                        ->select('approved_time_period')
                        ->where(['between', 'closed_time', $dateFrom." $fromTime", $dateTo." $toTime"])
//                        ->where("DATE(closed_time) > STR_TO_DATE('$dateFrom', '%Y-%m-%d')") // At the moment 
//                ->orWhere("request_status = 'taken'")
//                    ->andWhere('DATE(closed_time) = CURDATE()')
//                        ->andWhere("DATE(closed_time) < STR_TO_DATE('$dateTo', '%Y-%m-%d')")
                        ->andWhere("request_status = 'closed'")
//                        ->andWhere("agent_id = $id")
                        ->sum('TIMESTAMPDIFF(MINUTE,taken_time,closed_time)'); // This gets the difference between closed times and taken times of all closed breaks
            }
            return $data;
        } else {
            return 0;
        }

//        return 1.3;
    }

    public static function getThisMonthBreakTime($id) {
        $agentRequests = new Agent_requests();
        if ($id != 0) { // User id is not all
            $data = $agentRequests->find()
                    ->select('approved_time_period')
                    ->where("request_status = 'closed'") // At the moment 
//                ->orWhere("request_status = 'taken'")
                    ->andWhere('(MONTH(DATE(closed_time)) = MONTH(CURDATE()))')
                    ->andWhere("agent_id = $id")
                    ->sum('TIMESTAMPDIFF(MINUTE,taken_time,closed_time)'); // This gets the difference between closed times and taken times of all closed breaks
        } else {
            $data = $agentRequests->find()
                    ->select('approved_time_period')
                    ->where("request_status = 'closed'") // At the moment 
//                ->orWhere("request_status = 'taken'")
                    ->andWhere('(MONTH(DATE(closed_time)) = MONTH(CURDATE()))')
//                    ->andWhere("agent_id = $id")
                    ->sum('TIMESTAMPDIFF(MINUTE,taken_time,closed_time)'); // This gets the difference between closed times and taken times of all closed breaks
        }
//                ->sum('approved_time_period');
        return $data;
//        return 1.3;
    }

    /**
     * <b>Sets the consumed to 1</b>
     * <p>This function sets the consumed value of the help request identified by the passing parameter to 1 to indicates its consumed</p>
     * 
     * @author Sandun
     * @since 2017-07-13
     * 
     * @param int $helpRequestId
     * @return bool
     */
//    public static function markConsumed($helpRequestId){
//        $helpRequest = new Help_requests();
//        return $helpRequest->updateAll(['consumed' => 1], "id = $helpRequestId");
//    }
//    
    /**
     * <b>Gets one un-consumed agent requests</b>
     * <p>this function return one un-consumed agent requests from the DB table</p>
     * 
     * @return array
     * @author Vikum
     * @since 2017-07-17
     * 
     * @modified Sandun 2017-12-18
     * @description When a break request was made: for admin, shows all the agents break requests.
     * For supervisor / super user, shows the break requests made from the agents belongs to their extension queues
     * 
     */
    public static function getUnconsumedAgentRequest() {
        if(Yii::$app->session->get("user_role") != "1"){
            // not admin user
            $userIdsInQueue = call_center_user::getAgentIdsFromQueueId(Yii::$app->session->get("extQueueId"));
            
            $agentRequest = new Agent_requests();
            $requestData = $agentRequest->find()
                    ->where("request_status = 'pending'")
                    ->andWhere("consumed = 0")
                    ->andWhere(['in', 'agent_id', $userIdsInQueue])
                    ->all();
            if ($requestData) {
                $updater = new Agent_requests();
                $id = $requestData[0]['id'];
                $updater->updateAll(['consumed' => 1], "id = $id");
                return $requestData;
            } else {
                return NULL;
            }
        }else{
            // admin user
            $agentRequest = new Agent_requests();
            $requestData = $agentRequest->find()
                    ->where("request_status = 'pending'")
                    ->andWhere("consumed = 0")
                    ->all();
            if ($requestData) {
                $updater = new Agent_requests();
                $id = $requestData[0]['id'];
                $updater->updateAll(['consumed' => 1], "id = $id");
                return $requestData;
            } else {
                return NULL;
            }            
        }
        
    }

    /**
     * <b>Agent Request Response</b>
     * <p>Set response to agent request</p>
     * 
     * @return array
     * @author Vikum
     * @since 2017-07-19
     */
    public static function setAgentRequestResponse($data, $id) {

        $agentRequest = new Agent_requests();
        return $agentRequest->updateAll($data, "id = $id");
    }

    /**
     * <b>Get agentRequest by id</b>
     * <p>This function will return request type by id</p>
     * 
     * @return array
     * @author Vikum
     * @since 2017-07-19
     */
    public static function getAgentRequest($id) {
        $agentRequest = new Agent_requests();
        $request = Agent_requests::findOne($id);
        return $request['request_type'];
    }

    /**
     * <b>Get supervisor responses by id</b>
     * <p>This function will return record by id</p>
     * 
     * @return array
     * @author Vikum
     * @since 2017-07-20
     */
    public static function getSupervisorResponse($id) {
        $agentRequest = new Agent_requests();
        $request = $agentRequest->find()
                ->where("request_status = 'approved'")
                ->orWhere("request_status = 'denied'")
                ->andWhere("consumed = 0")
                ->andWhere("agent_id = " . $id)
                ->all();
        if ($request) {
            $updater = new Agent_requests();
            $id = $request[0]['id'];
            $updater->updateAll(['consumed' => 1], "id = $id");
            return $request;
        } else {
            return NULL;
        }
    }

    /**
     * <b>Get late arrived agents list</b>
     * 
     * @return array
     * @author Prabath
     * @since 2017-07-20
     * 
     * @modified Sandun 2017-12-19
     * @description returns only the agent information belongs to the same queue as the supervisor / super user.
     * For admin, returns all
     */
    public static function getLateBreakArrivals() {
        if(Yii::$app->session->get("user_role") != "1"){
            // not admin
            $queueUserIds = call_center_user::getAgentIdsFromQueueId(Yii::$app->session->get("extQueueId"));
            $agentRequest = new Agent_requests();
            $lateArrivals = $agentRequest->find()
                    ->select('agent_id')
                    ->where("request_status = 'closed'")
                    ->andWhere("consumed = 0")
                    ->andWhere(['in', 'agent_id', $queueUserIds])
                    ->all();

            if ($lateArrivals) {
                return $lateArrivals;
            } else {
                return NULL;
            }            
        }else{
            // is admin
            $agentRequest = new Agent_requests();
            $lateArrivals = $agentRequest->find()
                    ->select('agent_id')
                    ->where("request_status = 'closed'")
                    ->andWhere("consumed = 0")
                    ->all();

            if ($lateArrivals) {
                return $lateArrivals;
            } else {
                return NULL;
            }
        }
    }

    /**
     * <b>Update late arrival agent request record</b>
     * 
     * @return boolean
     * @author Prabath
     * @since 2017-07-21
     */
    public static function updateLateArrivalRecord($agent_id) {
        $updater = new Agent_requests();
        $updater->updateAll(['consumed' => 1], "request_status = 'closed' and consumed = 0 and agent_id=$agent_id");
        return $updater;
    }

    /**
     * <b>Get A taken break of an agent</b>
     * <p>this funcion gets a taken break of an agent passed as a parameter, if a taken break is found, this changes its status to closed and updates the closed time
     * If no taken break is found, returns NULL</p>
     * 
     * @param int $agentId
     * @return array / NULL
     * @since 2017-07-21
     */
    public static function getTakenBreaks($agentId) {
        $agentRequest = new Agent_requests();
        $request = $agentRequest->find()
                ->where("agent_id = $agentId")
                ->andWhere("request_status = 'taken'")
                ->andWhere("consumed = 1")
                ->one();

        if ($request) {
            $breakId = $request['id'];
            $updater = new Agent_requests();
            $updater->updateAll([
                'closed_time' => date("Y-m-d H:i:s"),
                'request_status' => 4 // Closed                
                    ], "id = $breakId");
            return $request;
        } else {
            return NULL;
        }
    }

    /**
     * <b>Sets the break as not consumed</b>
     * <p>This function sets the break passed as a parameter as not consumed. This is to identify ans agent who arrived late afater taking the given break</p>
     * 
     * @param int $breakId
     * @return Boolean
     * @author Sandun
     * @since 2017-07-24
     */
    public static function setBreakRequestConsumed($breakId) {
        $updater = new Agent_requests();
        return $updater->updateAll(['consumed' => 0], "id = $breakId");
    }

    /**
     * <b>Get Consumed Break requests</b>
     * <p>This function will return all consumed agent requests for the user</p>
     * 
     * @param $userId
     * @return array
     * @author Vikum
     * @since 2017-07-24
     */
    public static function getConsumedAgentRequests($userId) {

        $agentRequests = new Agent_requests();
        $data = $agentRequests->find()
                ->where("request_status = 'approved'")
                ->orWhere("request_status = 'pending'")
                ->andWhere("consumed = 1")
                ->andWhere("agent_id = $userId")
//                ->orWhere("request_status = 'taken'")
//                ->andWhere('(MONTH(DATE(closed_time)) = MONTH(CURDATE()))')
//                ->andWhere("agent_id = $id")
                ->all(); // This gets the difference between closed times and taken times of all closed breaks
//                ->sum('approved_time_period');
        return $data;
    }

    /**
     * <b>Get Consumed Break requests for supervisor</b>
     * <p>This function will return all consumed pending agent requests for supervisor</p>
     * 

     * @return array
     * @author Vikum
     * @since 2017-09-19
     * 
     * @modified Sandun 2017-12-19
     * @description filters the data to get only the agent requested related to queue of the supervisor / super user
     * For admin returns all
     */
    public static function getConsumedAgentRequestsBySupervisor() {
        $agentRequests = new Agent_requests();
        if(Yii::$app->session->get("user_role") != "1"){
            // not admin
            $queueUserIds = call_center_user::getAgentIdsFromQueueId(Yii::$app->session->get("extQueueId"));
            $data = $agentRequests->find()
                ->where("request_status = 'pending'")
                ->andWhere("consumed = 1")
                ->andWhere(['in', 'agent_id', $queueUserIds])
                ->all(); // This gets the difference between closed times and taken times of all closed breaks
            return $data;

        }else{
            // admin
            $data = $agentRequests->find()
                ->where("request_status = 'pending'")
                ->andWhere("consumed = 1")
                ->all(); // This gets the difference between closed times and taken times of all closed breaks
            return $data;

        }
    }
    
    /**
     * 
     * @param type $agentIdsInQueue
     * @return type
     * 
     * @since 2017-12-21
     * @author Sandun
     */
    public static function getDailyBreakTimesByQueue($agentIdsInQueue){
        $agentRequests = new Agent_requests();
                    $data = $agentRequests->find()
                    ->select('approved_time_period')
                    ->where("request_status = 'closed'") // At the moment 
//                ->orWhere("request_status = 'taken'")
                    ->andWhere('DATE(closed_time) = CURDATE()')
                    ->andWhere(['in', 'agent_id', $agentIdsInQueue])
                    ->sum('TIMESTAMPDIFF(MINUTE,taken_time,closed_time)'); // This gets the difference between closed times and taken times of all closed breaks
        return $data;
    }
    
    /**
     * 
     * @param type $agentIdsInQueue
     * @param type $dateFrom
     * @param type $dateTo
     * @return type
     * 
     * @since 2017-12-21
     * @author Sandun
     * 
     * @modified Sandun 2018-1-24
     * @param type $fromTime
     * @param type $toTime
     * 
     */
    public static function getBreakTimeByQueueBetweenDates($agentIdsInQueue, $dateFrom, $dateTo, $fromTime, $toTime){
        $agentRequests = new Agent_requests();
                $data = $agentRequests->find()
                        ->select('approved_time_period')
//                        ->where("DATE(closed_time) > STR_TO_DATE('$dateFrom', '%Y-%m-%d')") // At the moment 
//                        ->andWhere("DATE(closed_time) < STR_TO_DATE('$dateTo', '%Y-%m-%d')")
                        ->where(['between', 'closed_time', "$dateFrom $fromTime", "$dateTo $toTime"])
                        ->andWhere("request_status = 'closed'")
                        ->andWhere(['in', 'agent_id', $agentIdsInQueue])
                        ->sum('TIMESTAMPDIFF(MINUTE,taken_time,closed_time)'); // This gets the difference between closed times and taken times of all closed breaks
        return $data;
    }

}
