<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Description of help_requests
 *
 * @author Sandun
 * @since 2017-07-12
 */
class Miss_calls_email_log extends ActiveRecord {

    /**
     * <b>Save email log</b>
     * <p>This function saves the initial agent request data in db</p>
     * 
     * @author Vikum
     * @since 2017-08-04
     * 
     * @param array()
     * @return bool
     */
    public static function getUnconsumedAgentRequest($data) {
        $missCallsLog = new Miss_calls_email_log();
        $missCallsLog->numbers = $data;
        $missCallsLog->timestamp = microtime(true);
        $missCallsLog->active = 1;
        $missCallsLog->created_date = date("Y-m-d H:i:s");
        return $missCallsLog->insert();
    }

    /**
     * <b>select last timestamp of email log</b>
     * <p>This function Returns last recorded timestamp</p>
     * 
     * @author Vikum
     * @since 2017-08-08
     * 
     * @param array()
     * @return id
     */
    public static function getLastEmailTimestamp() {
        $missCallsLog = new Miss_calls_email_log();
        return $missCallsLog->find()
                        ->orderBy('id DESC')
                        ->limit(1)
                        ->one();
    }

}
