<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of call_center_user
 *
 * @since 27/6/2017
 * @author Prabath
 */

namespace app\models;

use Yii;
//use yii\base\Model;
use yii\db\ActiveRecord;
use app\models\Roles;

class call_center_user extends ActiveRecord {

    public static function getUserIdByPhoneNumber($phoneNumber) {
        $user = new call_center_user();
        $userId = $customer->find()
                ->select("id")
                ->where("contact_number = $phoneNumber")
                ->all();

        if ($userId) {
            return $userId[0]['id'];
        } else {
            return NULL;
        }
    }

    // This function takes user role as a parameter and return relevant results for te output
    /**
     * 
     * @param type $userRole
     * @return type
     * 
     * @modified Sandun 2017-12-20
     * @description filters the agent information to get only the agents data belongs to the same queue as the supervisor.
     * For admin returns all
     */
    public static function getUserData($userRole) {
        $user = new call_center_user();
        if ($userRole == 1) {
            return $user->find()
                            ->where("role_id = 2")
                            ->orWhere("role_id = 3")
                            ->orWhere("role_id = 4")
                            ->orWhere("role_id = 5")                            
                            ->andWhere("status = 'active'")
                            ->all();
        } else if ($userRole == 3 || $userRole == 5) {
            $queueAgentExtensions = Ext_queue_intermediate::getAllExtensionsOfTheQueue(Yii::$app->session->get("extQueueId"));
            return $user->find()
                            ->where("role_id = 2")
                            ->orWhere("role_id = 4")
                            ->andWhere("status = 'active'")
                            ->andWhere(['in', 'voip_extension', $queueAgentExtensions])
                            ->all();
        } else {
            return null;
        }
    }

    public static function getUser($userId) {
        $query = call_center_user::find();
        return $query->where("id = $userId")
                        ->all();
    }
    
    /**
     * <b>Get user information by user id in a 1D array</b>
     * 
     * @param type $userId
     * @return type
     * @since 2018-1-10
     * @author Sandun
     */
    public static function getUserInformationById($userId){
        return call_center_user::find()
                        ->where("id = $userId")
                        ->one();
    }

    public static function getAllAgents() {
        $agents = new call_center_user();
        return $agents->find()
                        ->select('*')
                        ->where("role_id = 2")
                        ->orWhere("role_id = 4")
                        ->andWhere("status = 'active'")
                        ->all();
    }

    /**
     * 
     * @return type
     * 
     * @modified Sandun 2017-12-19
     * @description filters agent data to get information of teh agents only belongs to the queue of the supervisor / super user
     * For admin returns all
     */
    public static function getAllAgentsExceptMe() {
        $myId = Yii::$app->session->get('user_id');
        $agents = new call_center_user();
        if(Yii::$app->session->get("user_role") != "1"){
            // not admin user
            $queueAgentExtensions = Ext_queue_intermediate::getAllExtensionsOfTheQueue(Yii::$app->session->get("extQueueId"));
            return $agents->find()
                ->select('*')
                ->where("role_id = 2")
                ->orWhere("role_id = 4")
                ->andWhere("status = 'active'")
                ->andWhere("id <> $myId")
                ->andWhere(['in', 'voip_extension', $queueAgentExtensions])
                ->all();
        }else{
            // admin user
            return $agents->find()
                            ->select('*')
                            ->where("role_id = 2")
                            ->orWhere("role_id = 4")
                            ->andWhere("status = 'active'")
                            ->andWhere("id <> $myId")
                            ->all();            
        }
    }

    /**
     * <b>Get the VOIP extension of an agent</b>
     * <p>This function returns the VOIP extension of the user identified by the ID passed as the parameter</p>
     * 
     * @param int $agentId
     * @return string agent VOIP extension
     * @author Sandun
     * @since 2017-07-25
     */
    public static function getUserVoipExtension($agentId) {
        $agent = new call_center_user();
        $agentVoip = $agent->find()
                ->select("voip_extension")
                ->where("id = $agentId")
                ->one();

        return $agentVoip['voip_extension'];
    }

    /**
     * <b>Set VOIP extension of an agent</b>
     * <p>This function is used to save the VOIP extension of the user passed as the parameter to the DB table</p>
     * 
     * @param int $agentId
     * @param int $voipExtension
     * @return boolean
     * @since 2017-07-27
     * @author Sandun
     */
    public static function setUserVoipExtension($agentId, $voipExtension) {
        return call_center_user::updateAll(['voip_extension' => $voipExtension], "id = $agentId");
    }

    /**
     * <b>Save Call center user</b>
     * <p>This function save call center user</p>
     * 
     * @param int $data
     * @param int $recordType
     * @return boolean
     * @since 2017-07-28
     * @author Vikum
     */
    public static function saveCallCenterUser($data, $recordType) {
        if ($recordType == 'save') {
            $user = new call_center_user();
        } else {
            $user = call_center_user::findOne($data['userId']);
        }
        if ($data['recordState'] == 1 || $data['recordState'] == 3) {
            $user->password = $data['hashedPassword'];
        }
        if ($data['recordState'] == 2 || $data['recordState'] == 3) {
            $user->user_profile_pic = $data['user_profile_pic'];
        }
        $user->name = $data['name'];
        $user->role_id = $data['role_id'];
        $user->fullname = $data['fullname'];
        $user->voip_extension = $data['extension'];
        $user->status = $data['status'];
        $user->created_date = $data['created_date'];
        $user->created_user_id = Yii::$app->session->get("user_id");
        if ($recordType == 'save') {
            if($user->insert()){
                    return $user->id;
            }else{
                return false;
            }            
        } else {
            return $user->update();
        }
    }

    /**
     * <b>Get agent data from VOIP extension</b>
     * <p>This function returns the agent information associated with the VOIP extension number passed as the parameter</p>
     * 
     * @param int $voipExtension
     * @return array agent data
     * 
     * @since 2017-08-03
     * @author Sandun
     */
    public static function getUserInfoFromVOIPExtension($voipExtension) {
        $agent = new call_center_user();
        return $agent->find()
                        ->where("voip_extension = '$voipExtension'")
                        ->one();
    }

    /**
     * <b>Get all assigned VOIP extension numbers</b>
     * <p>This function returns an array of all assigned VOIP numbers taken by the registered agents</p>
     * 
     * @return array taken VOIP extensions
     * @since 2017-08-04
     * @author Sandun
     */
    public static function getAllAssignedVOIPExtensions() {
        $voipExtensions = new call_center_user();
        $takenExtensions = $voipExtensions->find()
                ->select("voip_extension")
                ->where("status = 'active'")
                ->all();

        $takenExtensionsRemade = array();

        for ($x = 0; $x < count($takenExtensions); $x++) {
            $takenExtensionsRemade[$x] = $takenExtensions[$x]['voip_extension'];
        }

        return $takenExtensionsRemade;
    }

    public static function setUserStatusDeleted($userId) {
        return call_center_user::updateAll(['status' => 3], ['id' => $userId]);
    }

    /**
     * <b>Get supervisor email</b>
     * <p>This function returns Supervisor email. At the moment this will return admin user's email.</p>
     * 
     * @return $string
     * @since 2017-08-15
     * @author Vikum
     */
    public static function getSupervisorEmail() {
        $agent = new call_center_user();
        $data = $agent->find()
                ->where("name = 'admin'")
                ->one();
        return $data['user_email'];
    }

    public static function setUserRole($userId, $roleId) {
        return call_center_user::updateAll(['role_id' => $roleId], ['id' => $userId]);
    }

    public function getCall_records() {
        return $this->hasMany(Call_records::className(), ['user_id' => 'id']);
    }
    
    public function getExt_queue_intermediate() {
        return $this->hasOne(Ext_queue_intermediate::className(), ['voip_extension' => 'voip_extension']);
    }
    
    /**
     * 
     * @param type $from
     * @param type $to
     * @return int count
     * 
     * @since 2017-11-01
     * @author Sandun
     */
    public static function getRegisteredAgentsCountBetweenDates($from, $to){
        return call_center_user::find()
                ->where("created_date < $to")
                ->innerJoin("Ext_queue_intermediate", "Ext_queue_intermediate.voip_extension = call_center_user.voip_extension")
                ->with("Ext_queue_intermediate")
                ->count();
    }
    
    /**
     * <b>Returns all registered agent names and extensions</b>
     * @return array
     * 
     * @author Sandun
     * @since 2017-11-23
     */
    public static function getAllAgentNamesAndExtensionsInAnArray(){
        return call_center_user::find()
                ->select("voip_extension, fullname")
                ->all();
    }
    
    /**
     * <b>Get the agent webphone state</b>
     * <p>This function returns the webphone state of the agent matched with the agent id passed as the parameter</p>
     * @param int $agentId
     * @return int
     * 
     * @since 2017-12-08
     * @author Sandun
     */
    public static function getAgentWebphoneState($agentId){
        $agentWebphoneState = call_center_user::find()
                ->select("webphone")
                ->where("id = $agentId")
                ->one();
        
        return $agentWebphoneState['webphone'];
    }
    
    /**
     * <b>Sets the agent webphone state</b>
     * <p>This function sets the webphone state passed as a parameter the agent record that matches with the 
     * agent id passed as a parameter</p>
     * 
     * @param int $agentId
     * @param int $state "Enabled" = 1, "Disabled" = 2
     * @return boolean
     */
    public static function setAgentWebphoneState($agentId, $state){
        $agentData = call_center_user::findOne($agentId);
        $agentData->webphone = $state;
        return $agentData->update();
    }
    
    /**
     * <b>Returns all the supervisor user ids and names</b>
     * <p>This function returns all the registered supervisors ids and names in a 2D array based on a join with Roles class</p>
     * 
     * @return Array all the supervisors id and name
     * 
     * @author Sandun
     * @since 2017-12-13
     */
    public static function getAllSupervisorIdsAndNames(){
        $supervisorRole = Roles::find()
                ->where("name = 'Supervisor'")
                ->one();
        $allSupervisors = $supervisorRole->getCall_center_users()
                ->select("call_center_user.id, call_center_user.name")
                ->innerJoin('roles', 'roles.id = call_center_user.role_id')
                ->where("call_center_user.status = 'active'")
                ->with('roles')
                ->all();
        
        return $allSupervisors;
    }
    
    /**
     * <b>Returns One to One relation with Roles model class to be used with joins</b>
     * @return Active Query object
     * 
     * @author Sandun
     * @since 2017-12-13
     */
    public function getRoles() {
//        return $this->hasMany(call_center_user::className(), ['user_id' => 'id']);
        return $this->hasOne(Roles::className(), ['id' => 'role_id']);
    }
    
    
    /**
     * <b>Get the agent ids belong to the queue</b>
     * <p>This function returns the agent ids in a 1D array which belongs to the VOIP extensions assigned to the queue id passed as the parameter.</p>
     * 
     * @param type $queueId
     * @return array
     * 
     * @author Sandun
     * @since 2017-12-18
     */
    public static function getAgentIdsFromQueueId($queueId){
        $extensionsOfTheQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueue($queueId);
        
        $users = call_center_user::find()
                ->select("id")
                ->where(['in', 'voip_extension', $extensionsOfTheQueue])
                ->all();
        
        $userIdsArray = array();
        foreach($users as $key){
            array_push($userIdsArray, $key->id);
        }
        
        return $userIdsArray;
    }
    
    /**
     * <b>checks if the username is available or not</b>
     * <p>This function check if the passed in username string is already registered in the database.
     * Returns true if not, returns false if already registered.</p>
     * 
     * @param String $userName
     * @return boolean
     * 
     * @since 2018-1-3
     * @author Sandun
     */
    public static function isUsernameAvailable($userName){
        $userName = call_center_user::find()
                ->where("name = '$userName'")
                ->one();
        
        if(!$userName){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public static function getAgentIdsOfVoipExtensions($voipExtensions){
        $agentIds = call_center_user::find()
            ->select("id")
            ->where(["voip_extension" => $voipExtensions])
            ->all();
        $agentIdsRemade = array();
        foreach($agentIds as $key){
            array_push($agentIdsRemade, $key->id);
        }
        return $agentIdsRemade;
    }



        /**
     * <b>Get all available Agent Lists of selected queues</b>
     * <p>This function returns data of all agent information of selected queues</p>
     *
     * @param  $extenstionQueues(array) - Array of extension queues
     * @return list of agents
     * 
     * @since 2018-08-15
     * @author Vinothan
     */

    public function getAgentsOfSelectedQueues($extenstionQueues){
            $extensionQueueIds = implode(', ', array_map(function ($entry) {
                                return $entry['id'];}, $extenstionQueues));
            $agentList = call_center_user::find()
                    ->select(['call_center_user.id','fullname','call_center_user.voip_extension'])
                    ->from("call_center_user")
                    ->join("INNER JOIN",
                        "ext_queue_intermediate",
                        "ext_queue_intermediate.voip_extension = call_center_user.voip_extension")
                    ->where("ext_queue_intermediate.queueid in ($extensionQueueIds)")
                    ->groupby(['call_center_user.id','fullname','call_center_user.voip_extension'])
                    ->all();
            return $agentList;
    }



    /**
     * <b>Get all available Supervisors Lists selected queues</b>
     * <p>This function returns data of all Supervisors information of all  queues</p>
     *
     * @return information of users with queues
     * 
     * @since 2018-08-15
     * @author Vinothan
     */

    public function getAllSupervisorsWithQueues(){

            $SupervisorsQueueList = call_center_user::find()
                    ->select(['extension_queue.id','fullname','call_center_user.voip_extension','extension_queue.name','call_center_user.status'])
                    ->from("call_center_user")
                    ->join("INNER JOIN",
                        "extension_queue",
                        "extension_queue.supervisor_id = call_center_user.id")
                    ->where("extension_queue.status = 'Active'")
                    ->groupby(['extension_queue.id','fullname','call_center_user.voip_extension','extension_queue.name','call_center_user.status'])
                    ->all();
            return $SupervisorsQueueList;
    } 

    public function getSupervisorOfQueue($queueId){

            $SupervisorQueueData = call_center_user::find()
                    ->select(['extension_queue.id','fullname','call_center_user.voip_extension','extension_queue.name','call_center_user.status'])
                    ->from("call_center_user")
                    ->join("INNER JOIN",
                        "extension_queue",
                        "extension_queue.supervisor_id = call_center_user.id")
                    ->where("extension_queue.status = 'Active'")
                    ->andWhere("extension_queue.id = '$queueId'")
                    ->groupby(['extension_queue.id','fullname','call_center_user.voip_extension','extension_queue.name','call_center_user.status'])
                    ->all();
            return $SupervisorQueueData;
    }

    

}
