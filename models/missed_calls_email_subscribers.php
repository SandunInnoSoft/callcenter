<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * missed_calls_email_subscribers model refers missed_calls_email_subscribers table in db1
 *
 * @since 01/30/2019
 * @author Vinothan
 */

class missed_calls_email_subscribers extends ActiveRecord {

	public static function getAllEmailAddress() {
        $data = new missed_calls_email_subscribers();
            return $data->find()->where("status = 'active'")->all();
	}


	public function softDeleteEmail($id){
		$data = missed_calls_email_subscribers::findOne($id);
		$data->status = 'deleted';
		return $data->update();
	}


    public static function isEmailAvailable($emailaddress){
        $emailAddr = missed_calls_email_subscribers::find()
                ->where("email_address = '$emailaddress'")
                ->one();
        if(!$emailAddr){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public static function addNewEmail($name,$email){
    	$emailrecord = new missed_calls_email_subscribers();
    	$emailrecord->subsciber_name = $name;
    	$emailrecord->email_address = $email;
    	return $emailrecord->insert();
    }

    public function updateEmail($id,$name,$email){
        $emailrecord = missed_calls_email_subscribers::findOne($id);
        $emailrecord->subsciber_name = $name;
        $emailrecord->email_address = $email;
        return $emailrecord->save();        
    }

    public static function getAllEmailAddressById($id){
        $data = new missed_calls_email_subscribers();
            return $data->find()->where("id = $id")->all();
    }


}