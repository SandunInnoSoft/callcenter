<?php
namespace app\models;
use yii;
use yii\db\ActiveRecord;
use app\models\Voicemail_extension;
/**
 * This model class interacts extension_queue_intermediate table in DB1
 *
 * @author Sandun
 * @since 2017-12-12
 */
class Ext_queue_intermediate extends ActiveRecord{
    
    /**
     * <b>Returns all the queue assigned VOIP extensions</b>
     * <p></p>
     * @return array
     * 
     * @since 2017-12-12
     * @author Sandun
     */
    public static function getAllVoipExtensions(){
        return Ext_queue_intermediate::find()
                ->select("voip_extension")
                ->all();
    }
    
    public static function insertExtensionsOfNewQueue($queueId, $extensionsArray){
        foreach($extensionsArray as $key){
            if($key != NULL && $key != ""){
                $queueExtension = new Ext_queue_intermediate();
                $queueExtension->queueid = $queueId;
                $queueExtension->voip_extension = $key;
                $queueExtension->insert();
            }
        }
        
        return TRUE;
    }
    
    /**
     * <b>Returns all the assigned extensions of the queue</b>
     * <p>This function returns all the extensions belongs to the queue id passed as the parameter</p>
     * 
     * @param int $queueId
     * @return array extensions of the queue
     * 
     * @author Sandun
     * @since 2017-12-15
     */
    public static function getAllExtensionsOfTheQueue($queueId){
        $allExtensions = Ext_queue_intermediate::find()
                        ->select("voip_extension")
                        ->where("queueid = $queueId")
                        ->all();

        $allExtensionsRemade = array();

        foreach($allExtensions as $key){
            array_push($allExtensionsRemade, $key->voip_extension);
        }
        

        return $allExtensionsRemade;        
        
    }
    
    /**
     * <b>Returns extension queue id belongs to the agent extension</b>
     * <p>This function returns the extension queue id belongs to the agent extension matches with the extention passed as the parameter</p>
     * 
     * @param int $extension VOIP extension
     * @return int queue id / NULL
     * 
     * @since 2017-12-14
     * @author Sandun
     */
    public static function getExtensionQueueIdOfVOIPExtension($extension){
        $extensionDetails = Ext_queue_intermediate::find()
                ->select("queueid")
                ->where("voip_extension = $extension")
                ->one();
        
        if($extensionDetails){
            return $extensionDetails['queueid'];
        }else{
            return NULL;
        }
    }
    
    public function getCall_center_user() {
        return $this->hasOne(call_center_user::className(), ['voip_extension' => 'voip_extension']);
    }


    /**
     * <b>Returns all the assigned extensions of the queueidarray</b>
     * <p>This function returns all the extensions belongs to the queue id passed as the parameter</p>
     * 
     * @param array $queueId
     * @return array extensions of the queue
     * 
     * @author vinothan
     * @since 2018-08-17
     */
    public static function getAllExtensionsOfTheQueueArray($queueId){
        $extensionQueueIds = implode(', ', array_map(function ($entry) {
                            return $entry['id'];}, $queueId));        
        
        $allExtensions = Ext_queue_intermediate::find()
                        ->select("voip_extension")
                        ->where("queueid in ($extensionQueueIds)")
                        ->all();

        $allExtensionsRemade = array();

        foreach($allExtensions as $key){
            array_push($allExtensionsRemade, $key->voip_extension);
        }
        return $allExtensionsRemade;           
    }
/**
 * @author Nuwan.
 * @since 06/07/2019.
 * @description -get agent queue id.
 */
    public static function getAgentQueueId($agent_ex){
          
        $queueId = Ext_queue_intermediate::find()
        ->select("queueid")
        ->where("voip_extension = $agent_ex")
        ->one();

        if($queueId != null && $queueId != ""){            
            return $queueId;
        }       
    }
    /**
     * End.
     */

/**
 * @author Nuwan.
 * @since 06/07/2019.
 * @description -get all the extensions related to a specific queueid.
 */
    public static function getAllExtensionsCountOfTheQueue($queueId){
        $allExtensions = Ext_queue_intermediate::find()
        ->select("voip_extension")
        ->where("queueid = $queueId");                  
       
        return $allExtensions->count();   
    }
     /**
     * End.
     */    
}
