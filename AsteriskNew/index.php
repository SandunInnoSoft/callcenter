#!/usr/bin/php -q
<?php

set_time_limit(-1);

ini_set("default_socket_timeout", -1);

include('E:\xampp\htdocs\hnbGeneralCC\AsteriskNew\opt\asterisk\includes.php');
$db1Name = "hnbcallcenterdb";

$web = db_connect_web();

$saveCallRecordUrl = "http://localhost/hnbgeneralcc/web/index.php?r=asterisks/savecallrecord";
$hungUpCallRecordUrl = "http://localhost/hnbgeneralcc/web/index.php?r=asterisks/hungupcallrecord";
$socket = fsockopen("10.100.21.238","5038");
fputs($socket, "Action: Login\r\n");
fputs($socket, "Username: operator\r\n");
fputs($socket, "Secret: Password@123\r\n\r\n");

static $lastUpdatedTimestamp = 0;

//-----------------------------------------------------------------------------
// writeToLog("result of main loop socket start : ".fgets($socket));
$event = "";
while($ret = fgets($socket)){        
    if(substr($ret,0,6) == "Event:"){
        $e = explode(':', $ret);
        $event = trim($e[1]);
		
    }
	
//--------------------------------------DeviceStateChange ----------------------------------------------------	
	
    if($event == "DeviceStateChange"){
        $data = explode(':', $ret);
        if($data[0] == "Timestamp"){
            $ts = floor(trim($data[1]));
        }
        if($data[0] == "Device" && substr(trim($data[1]),0,3) == 'SIP'){
            $d = explode('/', trim($data[1]));
            $dev = trim($d[1]);
            $device = "";
            
            if(is_numeric($dev)){
                $device = $dev;
            }
        }

        if(!isset($device)){
        	$device = "";
        }
    
        if($data[0] == "State" && $device != ""){
            $state = trim($data[1]);
            
            if($state == "NOT_INUSE"){
                //Clear CID fields and update presence state
                $sql = "update callevents.web_presence set state='$state',cidnum = NULL, cidname = NULL, inorout = NULL, callstart = NULL where ext='$device'";
                mysqli_query($web,$sql);
            }else{
                //Update presence state
                $sql = "update callevents.web_presence set state='$state' where ext='$device'";
                mysqli_query($web,$sql);
                
            }
            
					
            $event = "";
            $device = "";
        }    
		
		
    }
	
//---------------------Call Queue ------------------------------------------------------

 if($event == "Newchannel"){
        $data = explode(':', $ret);
		$cidnum = "";

        if($data[0] == "ChannelState"){
            $cState = trim($data[1]);  
		}
   
		if($data[0] == "CallerIDNum"){
            $cidnum = trim($data[1]);
		}
    
	if($cidnum != "" && $cState == "0" && strlen($cidnum) > 4){ 
			$sql = "insert into callevents.call_queue (number) values ('$cidnum')";
            mysqli_query($web,$sql); 
	}

}

if($event == "Hangup"){
        $data = explode(':', $ret);
        $cidnamee = "";
        $cidnum = "";

			if($data[0] == "CallerIDNum"){
            	$cidnum = trim($data[1]);
			}
			if($data[0] == "CallerIDName"){
            	$cidnamee = trim($data[1]);
			}

		if($cidnum != $cidnamee){		

			$sql = "DELETE FROM callevents.call_queue where number='$cidnum'";  
				mysqli_query($web,$sql);
		}

	}

//-------------------------------------- DialBegin ----------------------------------------------------  
  
		if($event == "DialBegin"){
        $data = explode(':', $ret);
        
        if($data[0] == "Timestamp"){
            $ts = time();//floor(trim($data[1]));
        }
    
        if($data[0] == "Channel"){
            $c = explode('/',trim($data[1]));
            $c2 = explode('-', trim($c[1]));
            $channel = trim($c2[0]);
        }
    
        if($data[0] == "CallerIDNum"){
            $cidnum = trim($data[1]);
        }
    
        if($data[0] == "CallerIDName"){
            $cidname = trim($data[1]);
        }
		if($data[0] == "ChannelState"){
            $chnldesc = trim($data[1]);
        }
    
        if($data[0] == "DialString"){
            if(substr(trim($data[1]),0,3) == 'SIP' || is_numeric(trim($data[1]))){                
                if(is_numeric(trim($data[1]))){
                    $exten = trim($data[1]);
                }else{
                    $e = explode('/', trim($data[1]));
                    $exten = trim($e[1]);
                }
                
                //Update inbound presence call
                $sql = "update callevents.web_presence set cidnum = '$cidnum', cidname = '$cidname', inorout='I', callstart='$ts' where ext='$exten' and cidnum is null";
                mysqli_query($web,$sql);    
	
                $sql = "update callevents.web_presence set cidnum = '$exten', inorout='O', callstart='$ts' where ext='$channel' and cidnum is null";
                mysqli_query($web,$sql);  
                
				
				
				
				
//----------------------------------------Delete Queue Answered calls from the list-------------------------------------------------
				
				 
				 
				$sql = "DELETE FROM callevents.call_queue where number='$cidnum'";  
				mysqli_query($web,$sql);  
				
//-----------------------------------------------------------------------------------------------------------------------------------				
				
            }else{
                $e = explode('@', trim($data[1]));
                $dialed = trim($e[0]);
                
                if($channel != 'gateway'){                    
                    //Update outbound presence call
                    $sql = "update callevents.web_presence set cidnum = '$dialed', inorout='O', callstart='$ts' where ext='$channel'";
                    mysqli_query($web,$sql);    
					
				$sql = "DELETE FROM callevents.call_queue where number='$cidnum'";  
				mysqli_query($web,$sql); 
                
                }                
            }
			$numbersCount = strlen($cidnum);       				//Characters more than 7 
			if($cidnum != "" && $numbersCount > "6" && isset($exten) == TRUE){
				
				
			$time = date("Y-m-d H-i-s");
			$sql = "insert into callevents.call_forwards (caller_number,agent_extension,time_stamp,created_date,state) values ('$cidnum','$exten','$ts','$time','1')";
			mysqli_query($web,$sql); 
			}
			
            $event = "";
            $exten = "";
        }    
    }
//-------------------------------------- UnParkedCall ----------------------------------------------------   
 
    if($event == "UnParkedCall"){
        $data = explode(':', $ret);
        
        if($data[0] == "Timestamp"){
            $ts = floor(trim($data[1]));
        }
    
        if($data[0] == "RetrieverChannel"){
            $c = explode('/',trim($data[1]));
            $c2 = explode('-', trim($c[1]));
            $channel = trim($c2[0]);
        }
        
        if($data[0] == "ParkeeCallerIDNum"){
            $cidnum = trim($data[1]);
        }
        
        if($data[0] == "ParkeeCallerIDName"){
            $cidname = trim($data[1]);
        }        
    
        if($data[0] == "ParkingSpace"){
            $dialed = trim($data[1]);
            
            $pickup = "$cidnum ($dialed)";
            
            //Update outbound presence call
            $sql = "update callevents.web_presence set cidnum = '$pickup', cidname='$cidname', inorout='O', state='INUSE', callstart='$ts' where ext='$channel'";
            mysqli_query($web,$sql);    
            
//----------------------------------------Delete Queue Answered calls form the list----------------------------------------------------
				
				$sql = "DELETE FROM callevents.call_queue where number='$cidnum'";  
				mysqli_query($web,$sql);  
                
//-------------------------------------------------------------------------------------------------------------------------------------							
			
            $event = "";    
            $channel = "";        
        }    
    }
    $lastUpdate = updateTimestamp($web, $lastUpdatedTimestamp);
    if($lastUpdate != FALSE){
	    $lastUpdatedTimestamp = $lastUpdate;
    }
}

mysqli_close($web);





exit;    

/**
 * <b>Writes to loop log file</b>
 * <p>This function writes a log entry passed as a parameter with date time, the loopLog.txt file is located in the same directory as this index file resides</p>
 * 
 * @param String $logEntry
 * @return boolean
 * @author Sandun
 * @since 2017-09-26
 * 
 */
function writeToLog($logEntry){
    $myfile = fopen("loopLog.txt", "a") or die("Unable to open file!");
    fwrite($myfile, date("Y-m-d H:i:s"));
    fwrite($myfile, "\n");
    fwrite($myfile, $logEntry);
    fwrite($myfile, "\n");
    fwrite($myfile, "---------------------------------------------------------------------------");
    fwrite($myfile, "\n");
    fclose($myfile);
    return true;
}

/**
 * <b>Updates the timestamp field with current unix timestamp at every iteration of the endless loop</b>
 * @return type
 * @author Sandun
 * @since 2017-09-27
 *
 * @modified 2018-2-28 Sandun
 * @description gets the difference between last updated timestamp and the current timestamp.
 * If it is greater than 15 seconds, the current timestamp will be updated in the database and returned.
 * If not FALSE will be retured
 */
function updateTimestamp($web, $lastUpdatedTimestamp){
    $timestamp = time();
    $difference = $timestamp - $lastUpdatedTimestamp;
    if($difference > 15){
	    $sql = "UPDATE `loop_monitor` SET `timestamp`= '$timestamp' WHERE (`timestamp` IS NOT NULL) LIMIT 1";
	    mysqli_query($web, $sql);
	    $lastUpdatedTimestamp = $timestamp;
	    return $lastUpdatedTimestamp;
    }else{
    	return FALSE;
    }

}

/**
 * <b>Fetches the last updated timestamp of the loop</b>
 * @return type
 * @author Sandun
 * @since 2018-02-28
 */
function getTimestamp($web){
	$sql = "SELECT `timestamp` from `loop_monitor` LIMIT 1";
	$result = mysqli_query($web, $sql);
	$row = mysqli_fetch_assoc($result);
	return ($row["timestamp"] != NULL ? $row["timestamp"] : 0);
}


?>

