<?php
/*
 * 
 * @author Sandun
 * @since 2017-10-30
 */

?>
<div id="agentReportsDIV" class="container-fluid">
    <style>
        h3{
            text-align: center;
        }

        .tiles{
            text-align: center;
        }
    </style>

    <div class="row">
        <h3>Overall summery of <?= $agentInfo['agentName'] ?> performance from <?= $_GET['fromdate']?> to <?= $_GET['todate']?></h3>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-3 tiles">
            Total Calls Received <!-- BOTH : total calls received to call center-->
            <br>
            <?= $calls['totalCallsReceived'] ?>
        </div>
        <div class="col-xs-3 tiles">
            Total Calls Answered 
            <br>
            <?= $calls['totalCallsAnswered'] ?>
        </div>
        <?php 
            if($_GET['agent_id'] == 0){
                // All agents
                ?>
                    <div class="col-xs-3 tiles">
                        Total Calls Missed
                        <br>
                        <?= $calls['totalCallsMissed'] ?>
                    </div>
                <?php
            }else{
                // single agent
                ?>
                    <div class="col-xs-3 tiles">
                        Total Calls Abandoned
                        <br>
                        <?= $calls['totalCallsAbandoned'] ?>
                    </div>
                <?php
            }
        ?>
        <div class="col-xs-3 tiles">
            Total Outgoing Calls
            <br>
            <?= $calls['totalCallsOutgoing'] ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3 tiles">

        </div>
        <div class="col-xs-3 tiles">
            
            <br>
            
        </div>
        <div class="col-xs-3 tiles">
            
            <br>
            
        </div>
        <div class="col-xs-3 tiles">
            
            <br>
            
        </div>
    </div>
<!--    <hr>
    <div class="row">
        <div class="col-xs-3 tiles">
            Total Time worked
            <br>
            <? = $work['totalTimeWorked'] ?>
        </div>
        <div class="col-xs-3 tiles">
            Total time spent for breaks
            <br>
            <? = $work['totalBreakTime'] ?>
        </div>
        <div class="col-xs-3 tiles">
            Number of times turned ON DND
            <br>
            <? = $work['dndOnCount'] ?>
        </div>
        <div class="col-xs-3 tiles">
            Total number of agents registered
            <br>
            <? = $agentInfo['noOfAgents']?>
        </div>
    </div>-->
</div>