<?php

use yii\helpers\Url;
use yii\helpers\Html;

$data_breaksFullData = $breakFullData;
$data_breaksFullData = json_encode($data_breaksFullData);

$data_breaksDailyData = $breakDailyData;
$data_breaksDailyData = json_encode($data_breaksDailyData);

$dailyBreakPercentage = 0;
$dailyWorkPercentage = 0;

// Break and work times of the given period
$timePeriodBreakPercentage = 0;
$timePeriodWorkPercentage = 0;
$dailyBreakTime = '00 hours 00 minutes';
$dailyWorkTime = '00 hours 00 minutes';

$timePeriodBreakValue = '00 hours 00 minutes';
$timePeriodWorkValue = '00 hours 00 minutes';

if ($breakDailyData[0] != null && $breakDailyData[0] != 0) {
    $dailyBreakPercentage = ($breakDailyData[0] / ($breakDailyData[0] + $breakDailyData[1])) * 100;
    $dailyBreakTime = convertToHoursMins($breakDailyData[0], '%02d hours %02d minutes');
}
if ($breakDailyData[1] != null && $breakDailyData[1] != 0) {
    $dailyWorkPercentage = ($breakDailyData[1] / ($breakDailyData[0] + $breakDailyData[1])) * 100;
    $dailyWorkTime = convertToHoursMins($breakDailyData[1], '%02d hours %02d minutes');
}

if ($breakFullData[0] != null) {
    $timePeriodBreakPercentage = ($breakFullData[0] > 0 && $breakFullData[1] > 0 ? ($breakFullData[0] / ($breakFullData[0] + $breakFullData[1])) * 100 : 0);
    $timePeriodBreakValue = convertToHoursMins($breakFullData[0], '%02d hours %02d minutes');
}
if ($breakFullData[1] != null) {
    $timePeriodWorkPercentage = ($breakFullData[0] > 0 && $breakFullData[1] > 0 ? ($breakFullData[1] / ($breakFullData[0] + $breakFullData[1])) * 100 : 0);
    $timePeriodWorkValue = convertToHoursMins($breakFullData[1], '%02d hours %02d minutes');
}
$agentDailyAnsweredCalls = 0;
$agent_answered_percnt = 0;
$others_answered_percnt = 0;
$others_answered_count = $all_answered_calls_count - $agent_answered_calls_count;
if ($agent_answered_calls_count != 0) {
    $agent_answered_percnt = ($agent_answered_calls_count / $all_answered_calls_count) * 100;
}
if ($all_answered_calls_count != 0) {
    $others_answered_percnt = ($others_answered_count / $all_answered_calls_count) * 100;
}

// ======== Monthly calls data =============
$agentAnsweredCallsThisMonth = 0;
$agentAnsweredCallsPercentageThisMonth = 0;
$othersAnsweredCallsThisMonth = 0;
$otherAnsweredCallsPercentageThisMonth = 0;
if ($monthlyCallsData[0] != null && $monthlyCallsData[0] != 0) {
    $agentAnsweredCallsThisMonth = $monthlyCallsData[0];
    $agentAnsweredCallsPercentageThisMonth = (($monthlyCallsData[0] / $monthlyCallsData[1]) * 100);
}
if ($monthlyCallsData[1] != null && $monthlyCallsData[1] != 0) {
    $othersAnsweredCallsThisMonth = $monthlyCallsData[1] - $monthlyCallsData[0];
    $otherAnsweredCallsPercentageThisMonth = (($othersAnsweredCallsThisMonth / $monthlyCallsData[1]) * 100);
}
// ==========================================

$agent_answered_percnt = bcadd($agent_answered_percnt, '0', 2);
$others_answered_percnt = bcadd($others_answered_percnt, '0', 2);

function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}
?>

<script type="text/javascript" src="js/chartjs.js"></script>
<style>
    .panel-heading{
        font-size: 90%;
    }
    thead{
        font-weight: bold;
    }
    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }
</style>

<style>
    .answered-calls-history-table {
        width: 100%;
    }

    .outgoing-calls-history-table {
        width: 100%;
    }

    .abandoned-calls-history-table {
        width: 100%;
    }

    .call-history-thead, .call-history-tbody,.call-history-tbody tr,.call-history-tbody td,.call-history-thead th,.call-history-thead tr,.call-history-thead td {
        display: block;
    }

    .call-history-thead th {
        height: 30px;
    }

    .call-history-tbody {
        overflow-y: auto;
        height: 300px;
        font-size: 90%;
    }

    .call-history-tbody td, .call-history-thead th {
        float: left;
        width: 20%;
    }

    .abandoned-call-history-tbody td, .abandoned-call-history-thead th {
        float: left;
        width: 33.33%;
    }

    .call-history-thead tr:after, call-history-tbody tr:after {
        clear: both;
        content: ' ';
        display: block;
        visibility: hidden;
    }
</style>

<div class="container-fluid"  style="font-family: 'Lato' , sans-serif;"  id="agentReportsDIV">
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold;">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Break Reports</a>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px;">
                        <div class="panel-heading" style="height: 60px">
                            Daily break times comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Worked</td>
                                                <td>Breaks</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= round($dailyWorkPercentage, 2) ?>%</td>
                                                <td><?= round($dailyBreakPercentage, 2) ?>%</td>
                                            </tr>
                                            <tr>
                                                <td><?= $dailyWorkTime ?></td>
                                                <td><?= $dailyBreakTime ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="breakTimeDailyChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("breakTimeDailyChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'pie',
                            data: {
                                labels: ["Breaks", "Worked"],
                                datasets: [{
                                        //                            label: '# of Votes',
                                        data: <?= $data_breaksDailyData ?>,
                                        backgroundColor: [
                                            'rgba(244, 157, 0, 0.8)',
                                            'rgba(66, 139, 202, 0.8)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            Total break times comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Worked</td>
                                                <td>Breaks</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= round($timePeriodWorkPercentage, 2) . ' %' ?></td>
                                                <td><?= round($timePeriodBreakPercentage, 2) . ' %' ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= $timePeriodWorkValue ?></td>
                                                <td><?= $timePeriodBreakValue ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>                            
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="breakTimeFullChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("breakTimeFullChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'pie',
                            data: {
                                labels: ["Breaks", "Worked"],
                                datasets: [{
//                                        label: '# of Votes',
                                        data: <?= $data_breaksFullData ?>,
                                        backgroundColor: [
                                            'rgba(244, 157, 0, 0.8)',
                                            'rgba(66, 139, 202, 0.8)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>                
                </div>            
            </div>
        </div>
    </div><br>

    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold;">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Service Level Figures</a>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            Daily Answered Calls comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Others</td>
                                                <td>Agent</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= $others_answered_percnt . '%' ?></td>
                                                <td><?= $agent_answered_percnt . '%' ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= $others_answered_count . ' calls' ?></td>
                                                <td><?= $agent_answered_calls_count . ' calls' ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="serviceLevelDailyChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("serviceLevelDailyChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'pie',
                            data: {
                                labels: ["Agent Answered calls %", "Others Answered calls %"],
                                datasets: [{
                                        //                            label: '# of Votes',
                                        data: [<?= $agent_answered_calls_count ?>,<?= $others_answered_count ?>],
                                        backgroundColor: [
                                            'rgba(244, 157, 0, 0.8)',
                                            'rgba(66, 139, 202, 0.8)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            This Month Answered Calls comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Others</td>
                                                <td>Agent</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= round($otherAnsweredCallsPercentageThisMonth, 2) . '%' ?></td>
                                                <td><?= round($agentAnsweredCallsPercentageThisMonth, 2) . '%' ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= $othersAnsweredCallsThisMonth . ' calls' ?></td>
                                                <td><?= $agentAnsweredCallsThisMonth . ' calls' ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                                        
                </div>                            
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="serviceLevelFullChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("serviceLevelFullChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'pie',
                            data: {
                                labels: ["Agent Answered calls ", "Others Answered calls "],
                                datasets: [{
//                                        label: '# of Votes',
                                        data: [<?= $agentAnsweredCallsThisMonth ?>,<?= $othersAnsweredCallsThisMonth ?>],
                                        backgroundColor: [
                                            'rgba(244, 157, 0, 0.8)',
                                            'rgba(66, 139, 202, 0.8)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>            
            </div>
        </div>
    </div>
    <br>
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Answered Calls</a>
            <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($answered_calls) ?></a>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <?php if ($answered_calls) { ?>        
            <table class="table table-fixed table-striped answered-calls-history-table" style="border-radius: 5px">
                <thead class="call-history-thead">
                    <tr>
                        <th style="width:20%">
                            Caller Number
                        </th>
                        <th style="width:20%">
                            Answered Time
                        </th>
                        <th style="width:20%">
                            End Time
                        </th>
                        <th style="width:20%">
                            Duration
                        </th>
                        <th style="width:20%">
                            Call Playback
                        </th>
                    </tr>
                </thead>
                <tbody class="call-history-tbody">
                    <?php
                    for ($index = 0; $index < count($answered_calls); $index++) {

                        $seconds = $answered_calls[$index]['duration'];

                        $hours = floor($seconds / 3600);
                        $mins = floor($seconds / 60 % 60);
                        $secs = floor($seconds % 60);

                        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);

                        ?>
                        <tr>
                            <td style="width:20%"><?= $answered_calls[$index]['src'] ?></td>
                            <td style="width:20%"><?= $answered_calls[$index]['answer'] ?></td>
                            <td style="width:20%"><?= $answered_calls[$index]['end'] ?></td>
                            <td style="width:20%"><?= $timeFormat ?></td>
                            <td style="width:20%">
                                <a data-toggle="modal" data-target="#audioPlaybackModal" class="btn btn-md btn-success btn-block" onclick="playCallRecord(this, 'I', '<?= $answered_calls[$index]['voip'] ?>', '<?= $answered_calls[$index]['src'] ?>', '<?= str_replace(' ', '+', $answered_calls[$index]['start']) ?>','<?= str_replace(' ', '+', $answered_calls[$index]['end']) ?>')">Play</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
    <br>
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Outgoing Calls</a>
            <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($outgoing_calls) ?></a>
        </div>                    
    </div>

    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <?php if ($outgoing_calls) { ?>
            <table class="table table-fixed table-striped outgoing-calls-history-table" style="border-radius: 5px">
                <thead class="call-history-thead">
                    <tr>
                        <th style="width:20%">
                            Receiver
                        </th>
                        <th style="width:17%">
                            Start Time
                        </th>
                        <th style="width:17%">
                            Answered Time
                        </th>
                        <th style="width:17%">
                            End Time
                        </th>
                        <th style="width:10%">
                            Duration
                        </th>
                        <th style="width:18%">
                            Call Playback
                        </th>
                    </tr>
                </thead>
                <tbody class="call-history-tbody">
                    <?php
                    for ($index = 0; $index < count($outgoing_calls); $index++) {

                        $seconds = $outgoing_calls[$index]['duration'];

                        $hours = floor($seconds / 3600);
                        $mins = floor($seconds / 60 % 60);
                        $secs = floor($seconds % 60);

                        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                        ?>
                        <tr>
                            <td style="width:20%"><?= $outgoing_calls[$index]['dst'] ?></td>
                            <td style="width:17%"><?= $outgoing_calls[$index]['start'] ?></td>
                            <td style="width:17%"><?= $outgoing_calls[$index]['answer'] ?></td>
                            <td style="width:17%"><?= $outgoing_calls[$index]['end'] ?></td>
                            <td style="width:10%"><?= $timeFormat ?></td>
                            <td style="width:18%">
                                <?php
                                if($outgoing_calls[$index]['answer'] != null && $outgoing_calls[$index]['answer'] != ""){
                                ?>
                                    <a data-toggle="modal" data-target="#audioPlaybackModal" class="btn btn-md btn-success btn-block" onclick="playCallRecord(this, 'O', '<?= Yii::$app->session->get('voip') ?>', '<?= $outgoing_calls[$index]['dst'] ?>', '<?= str_replace(' ', '+', $outgoing_calls[$index]['start']) ?>','<?= str_replace(' ', '+', $outgoing_calls[$index]['end']) ?>')">Play</a>
                                <?php
                                }else{
                                ?>
                                    <a class="btn btn-md btn-success btn-block disabled">Play</a>     
                                <?php
                                }
                                ?>
                            </td>
                        </tr>
    <?php } ?>
                </tbody>
            </table>
<?php } ?>
    </div>

    <script>
                function playCallRecord(playAudioAnchor, direction, extension, customerNumber, datetime, callEndedDateTime) {
                    // $("#audioPlaybackModal").modal("toggle");
                    var row = playAudioAnchor.parentNode.parentNode;
                    var id = row.id;
                    
                    var audioFilePath = "<?= Url::to(['ftp/playaudiofile']) ?>" + "&datetime="+datetime+"&callType="+direction+"&ext="+extension+"&phoneNumber="+customerNumber+"&endedTime="+callEndedDateTime;
                    console.log("Playing audio file path = "+audioFilePath);
                    $(".audioPlayerClass").remove();
                    var newAudioElement = $("<audio id='audioPlayer' class='audioPlayerClass' controls></audio>").append("<source id='audioPlayerSource' src='" + audioFilePath + "' type='audio/wav'> ");
                    //                    $('#callRecordPlaybackSource').attr("src", audioFilePath);
                    $("#audioPlayerDiv").append(newAudioElement);
                    $(newAudioElement)[0].play();
                    //                    changeCallPlayBackAudioSRC(audioFilePath);
                    //                    $("#audioPlayerSource").attr("src", audioFilePath);
                    //                    $("#audioPlayer")[0].play();
                }
                function pauseAudio() {
    //                    alert($('#audioPlayer')[0].paused);
                    if ($('.audioPlayerClass')[0].paused == false) {
                        $('.audioPlayerClass')[0].pause();
                    }
                }

                $(document).ready(function(){
                    $('#audioPlaybackModal').on('hidden.bs.modal', function () {
                      pauseAudio();
                    });
                });

                function downloadCurrentlyPlayingAudioFile(){
                    var url = $("#audioPlayerSource").attr("src");
                    if(url != ""){
                        console.log("downloading audio file from "+url);
                        $("#downloaderIframe").attr("src", url);
                    }else{
                        console.log("No audio url to download");
                    }
                }

            
                
    </script>
    <!-- audio playback modal -->
        <div id="audioPlaybackModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align:center">Call Audio Player</h4>
                </div>
              <div class="modal-body">
                <!-- <p>Some text in the modal.</p> -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="audioPlayerDiv">
                                <audio id="audioPlayer" controls class="audioPlayerClass">
                                    <source id="audioPlayerSource" src="" type="audio/wav">
                                </audio>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-md btn-info btn-block" onclick="downloadCurrentlyPlayingAudioFile()">Download</a>
                        </div>
                    </div>
                </div>
              </div>
            </div>

          </div>
        </div>
    <!-- end of audio playback modal -->
    <!-- downloader iframe -->
    <iframe src="" id="downloaderIframe" style="display: none">
        
    </iframe>
    <!-- end of downloader iframe-->


    <br>
    <div class="row">
        <div class="col-xs-5" style="">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent Abandoned Calls</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($agent_abandoned_calls) ?></a>
                </div>
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
<?php if ($agent_abandoned_calls) { ?>        
                    <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Caller Number
                                </th>
                                <th>
                                    Abandoned Date
                                </th>
                                <th>
                                    Time
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
    <?php
    for ($index = 0; $index < count($agent_abandoned_calls); $index++) {

        $datetime = $agent_abandoned_calls[$index]['end'];
        $dateTimeExploded = explode(" ", $datetime);
        ?>
                                <tr>
                                    <td><?= $agent_abandoned_calls[$index]['src'] ?></td>
                                    <td><?= $dateTimeExploded[0] ?></td>
                                    <td><?= $dateTimeExploded[1] ?></td>
                                </tr>
    <?php } ?>
                        </tbody>
                    </table>
<?php } ?>

            </div>

        </div>

        <div class="col-xs-1"></div>
        <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent Login Details</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($login_records) ?></a>
                </div>    
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
<?php if ($login_records) { ?>        
                    <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Logged Date
                                </th>
                                <th>
                                    Logged Time
                                </th>
                                <th>
                                    Logout Time
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
    <?php
    for ($index = 0; $index < count($login_records); $index++) {

        $login_time_sig = $login_records[$index]['logged_in_time'];
        $login_date = date('Y-m-d', $login_time_sig);
        $login_time = date('H:i:s', $login_time_sig);

        $logged_time_sig = $login_records[$index]['time_signature'];
        $logged_date = date('Y-m-d', $logged_time_sig);
        $logged_time = date('H:i:s', $logged_time_sig);
        ?>
                                <tr>
                                    <td><?= $login_date ?></td>
                                    <td><?= $login_time ?></td>
                                    <td><?= $logged_time ?></td>
                                </tr>
    <?php } ?>
                        </tbody>
                    </table>
<?php } ?>
            </div>
        </div>
    </div>
</div>

