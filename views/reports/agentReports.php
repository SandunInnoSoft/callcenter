<?php

use yii\helpers\Url;
use yii\helpers\Html;
$ftpParams = include __DIR__ . '/../../config/pbx_audio_ftp_config.php';

if ($ftpParams['fileExtension'] == "wav") {
    $audioType = "audio/wav";
} else if ($ftpParams['fileExtension'] == "gsm") {
    $audioType = "audio/x-gsm";
}

?>



<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>-->
<script src="js/jquery-3.2.1.js"></script>
<script type="text/javascript" src="js/chartjs.js"></script>
<script src="js/moment-with-locales.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
<style>
    thead{
        font-weight: bold;
    }

    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }
</style>

<style>
    .answered-calls-history-table {
        width: 100%;
    }
    .outgoing-calls-history-table {
        width: 100%;
    }

    .abandoned-calls-history-table {
        width: 100%;
    }

    .call-history-thead, .call-history-tbody,.call-history-tbody tr,.call-history-tbody td,.call-history-thead th,.call-history-thead tr,.call-history-thead td {
        display: block;
    }

    .call-history-thead th {
        height: 30px;
    }

    .call-history-tbody {
        overflow-y: auto;
        height: 300px;
        font-size: 90%;
    }

    .call-history-tbody td, .call-history-thead th {
        float: left;
        width: 20%;
    }

    .abandoned-call-history-tbody td, .abandoned-call-history-thead th {
        float: left;
        width: 33.33%;
    }

    .call-history-thead tr:after, call-history-tbody tr:after {
        clear: both;
        content: ' ';
        display: block;
        visibility: hidden;
    }
</style>

<style>
    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }

    .channelLabel{
        margin-right: 10px;
    }
</style>



<div  style="font-family: 'Lato' , sans-serif;">

    <div class="col-xs-12" style="padding: 0%">
        <div class="row">
            <div class="col-xs-3">
                <label for="fromdate">Date from*</label>
                <!--<input class="form-control activateFields" type="date" id="fromdate">-->
                <div class="input-group date" id="fromdate">
                    <input type="text" class="form-control" id="dateFromValue"/>    <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                </div>
                <label for="fromtime">Time from*</label>
                <div class="form-group" id="fromtime">
                <select id="timeFromSelect" class="form-control">
                    <?php
                    for ($i=0; $i < 24; $i++) {
                        if($i < 10){
                            ?>
                            <option value="<?= '0'.$i.':00:00' ?>"><?= '0'.$i.':00' ?></option>
                            <?php
                        }else{
                            ?>
                            <option value="<?= $i.':00:00' ?>"><?= $i.':00' ?></option>
                            <?php
                        }
                    }
                    ?>
                    <option value="23:59:59">End of the day</option>
                </select>
                </div>
            </div>
            <div class="col-xs-3">
                <label for="todate">Date to*</label>
                <!--<input class="form-control activateFields" type="date" id="todate">-->
                <div class="input-group date" id="todate">
                    <input type="text" class="form-control" id="dateToValue"/>    <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                </div>
                <label for="fromtime">Time to*</label>
                <div class="form-group" id="totime">
                <select id="timeToSelect" class="form-control">
                    <?php
                    for ($i=0; $i < 24; $i++) {
                        if($i < 10){
                            ?>
                            <option value="<?= '0'.$i.':00:00' ?>"><?= '0'.$i.':00' ?></option>
                            <?php
                        }else{
                            ?>
                            <option value="<?= $i.':00:00' ?>"><?= $i.':00' ?></option>
                            <?php
                        }
                    }
                    ?>
                    <option value="23:59:59" selected>End of the day</option>
                </select>
                </div>
            </div>
            <div class="col-xs-3">
                <label for="contactNum">Contact</label>
                <input class="form-control activateFields" type="tel" id="contactNum">
            </div>
            <div class="col-xs-3" style="padding-top: 23px">
                <div class="row" style="padding-bottom: 1%">
                    <div class="col-xs-12">
                        <a class="btn btn-block btn-primary" id="btnSummeryView" onclick="filterRecordsSummery()">View Summery Report</a>
                    </div>
                </div>
                <div class="row" style="padding-top: 1%">
                    <div class="col-xs-12">
                       <!-- <a class="btn btn-block btn-primary" id="btnView" onclick="filterRecords()">View Full Report</a>  -->
                       <a class="btn btn-block btn-primary" id="btnView" onclick="filterRecordsTablle()">View Full Report</a>
                    </div>
                </div>
            </div>
            <input type="hidden" id="hiddenAgentId" />
            <input type="hidden" id="hiddenAgentExt" />
            <input type="hidden" id="hiddenAgentName" />
            <input type="hidden" id="hiddenQueueId" />
        </div>

        <ul class="nav nav-tabs" id="selectList" role="tablist">
        <li class="nav-item">
            <a class="nav-link" id="cdrList-tab" data-toggle="tab" href="#cdrList" role="tab" aria-controls="cdrList" aria-selected="false">Report Sections</a>
        </li>
    </ul>

    <div class="tab-content" id="selectListContent">
    <div class="tab-pane fade" id="cdrList" role="tabpanel" aria-labelledby="cdrList-tab">
        <br>
        <div class="col-md-12">
            <div class="well" id="channelsCdrListDiv">
                <span id="cdrCommonDiv" style="display: inline-block;">
                <label title="Answered-calls"><input type="checkbox" value="1" id="checkedCdrs">&nbsp Answered Calls &nbsp</label>
                <label title="Outgoing-calls"><input type="checkbox" value="2" id="checkedCdrs">&nbsp Outgoing Calls &nbsp</label>
                <label title="Missed-calls"><input type="checkbox" value="3" id="checkedCdrs">&nbsp Missed Calls &nbsp</label>
                </span>
                <span id="agentsOnlyDiv" style="display: inline-block;">
                <label title="Agent Abandoned Calls" id="agentAbandonedCdr"><input type="checkbox" value="4" id="checkedCdrs">&nbsp Abandoned Calls &nbsp</label>
                <label title="Agent Login Details" id="agentLoginCdr"><input type="checkbox" value="5" id="checkedCdrs">&nbsp Login Details &nbsp</label>
                <label title="Agent DND Records" id="agentDndCdr"><input type="checkbox" value="6" id="checkedCdrs">&nbsp DND Records &nbsp</label>
                </span>
            </div>
        </div>
    </div>
    </div>

        <span id="searchTimeSpan"></span>
        <br>
        <img class="img-responsive" src="images/loading.gif" id="loadingGifImg" style="display: none">
        <div class="row" id="agentReportsDiv" style="padding-left: 1%"></div>


        <div style="display: none;padding-left: 2%;padding-right: 2%;border: 1px solid #cdcdcd" id="resulttable">
            <div class="row" style="padding-top: 0%">
            <div style="background-color: #10297d;color: white;padding-top: 0%;text-align: center;font-size: 130%;" class="col-xs-2" id="agentNameTitle"></div>
        <div class="col-xs-8"></div>
        <div class="col-xs-2" style="text-align: right;background-color: #cdcdcd;font-size: 130%"><?= date('Y-m-d'); ?></div>
    </div>
    <br>

    <div id="answeredDataDiv" style="display: none">
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="">
            <a class="navbar-brand" href="#">Answered Calls</a>
             <div class="incoming-button" style="float:right;padding-right: 16px">

                        <a class="btn btn-block btn-primary" style="padding: 5px 44px;" id="answeredCalls-btn" onclick="createCsvFile('incoming')">CSV</a>
                    </div>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <br>
        <div style="margin: 10px" id="answeredDataTableDiv">
        </div>
        <br>
    </div>
    <br>
    </div>

    <div id="outgoingDataDiv" style="display: none">
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="">
            <a class="navbar-brand" href="#">Outgoing Calls </a>
             <div class="outgoing-button" style="float:right;padding-right: 16px">

                        <a class="btn btn-block btn-primary" style="padding: 5px 44px;" id="answeredCalls-btn" onclick="createCsvFile('outgoing')">CSV</a>
                    </div>
        </div>
    </div>

    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <br>
            <div style="margin: 10px" id="outgoingDataTableDiv">
            </div>
        <br>
    </div>
    <br>
    </div>


    <div id="missedDataDiv" style="display: none">
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="">
            <a class="navbar-brand" href="#">Missed Calls </a>
             <div class="missed-button" style="float:right;padding-right: 16px">

                            <a class="btn btn-block btn-primary" style="padding: 5px 44px;" id="answeredCalls-btn" onclick="createCsvFile('missed')">CSV</a>
                        </div>
        </div>
    </div>

    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <br>
        <div style="margin: 10px">
    <table id="missedcallDataTable" class="display compact" style="width:100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Time</th>
                <th>Caller Number</th>
            </tr>
        </thead>
    </table>
        </div>
        <br>
    </div>
    <br>
    </div>


<div id="ivrDataDiv" style="display: none">
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="">
            <a class="navbar-brand" href="#">IVR Received Calls </a>
             <div class="IVR-button" style="float:right;padding-right: 16px">

                            <a class="btn btn-block btn-primary" style="padding: 5px 44px;"  id="answeredCalls-btn" onclick="createCsvFile('ivrReceived')">CSV</a>
                        </div>
        </div>
    </div>

    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <br>
        <div style="margin: 10px">
    <table id="ivrDataTable" class="display compact" style="width:100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Time</th>
                <th>Caller Number</th>
            </tr>
        </thead>
    </table>
        </div>
        <br>
    </div>
    <br>
</div>


<div id="vmDataDiv" style="display: none">
        <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="">
            <a class="navbar-brand" href="#">Received Voice Mail Calls </a>
             <div class="received-button" style="float:right;padding-right: 16px">

                            <a class="btn btn-block btn-primary" style="padding: 5px 44px;" id="answeredCalls-btn" onclick="createCsvFile('voiceMail')">CSV</a>
                        </div>
        </div>
    </div>

    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <br>
        <div style="margin: 10px">
    <table id="vmDataTable" class="display compact" style="width:100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Time</th>
                <th>Caller Number</th>
            </tr>
        </thead>
    </table>
        </div>
        <br>
    </div>
    <br>
</div>


<div id="abandonedDataDiv" style="display: none">
        <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="">
            <a class="navbar-brand" href="#">Agent Abandoned Calls</a>
             <div class="abondond-button" style="float:right;padding-right: 16px">

                            <a class="btn btn-block btn-primary" style="padding: 5px 44px;" id="answeredCalls-btn" onclick="createCsvFile('abondondCalls')">CSV</a>
                        </div>
        </div>
    </div>

    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <br>
        <div style="margin: 10px">
    <table id="abandonedDataTable" class="display compact" style="width:100%">
        <thead>
            <tr>
                <th>Caller Number</th>
                <th>Abandoned Date</th>
                <th>Time</th>
            </tr>
        </thead>
    </table>
        </div>
        <br>
    </div>
    <br>
</div>

<div id="loginDataDiv" style="display: none">
        <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="">
            <a class="navbar-brand" href="#">Agent Login Details</a>
              <div class="logged-button" style="float:right;padding-right: 16px">

                            <a class="btn btn-block btn-primary" style="padding: 5px 44px;" id="answeredCalls-btn" onclick="createCsvFile('logged')">CSV</a>
                        </div>
        </div>
    </div>

    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <br>
        <div style="margin: 10px">
    <table id="loginDataTable" class="display compact" style="width:100%">
        <thead>
            <tr>
                <th>Logged Date</th>
                <th>Logged Time</th>
                <th>Logout Time</th>
            </tr>
        </thead>
    </table>
        </div>
        <br>
    </div>
    <br>
</div>


<div id="dndDataDiv" style="display: none">
        <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="">
            <a class="navbar-brand" href="#">Agent DND Records</a>
              <div class="DND-button" style="float:right;padding-right: 16px">

                            <a class="btn btn-block btn-primary" style="padding: 5px 44px;" id="answeredCalls-btn" onclick="createCsvFile('dnd')">CSV</a>
                        </div>
        </div>
    </div>

    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <br>
        <div style="margin: 10px">
    <table id="dndDataTable" class="display compact" style="width:100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Time</th>
                <th>Mode</th>
            </tr>
        </thead>
    </table>
        </div>
        <br>
    </div>
    <br>
</div>

</div>

</div>

    </div>
</div>

<script>

   /**
     *
     * @return type
     *
     * @author Nuwan
     * @since 26-02-2019
     */
//Get all the data to process the CSV file
    function createCsvFile(answered) {

        var incoming = answered;
        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var queueId = $('#hiddenQueueId').val();
        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        if (queueId == "") {

            var url = "<?= Url::to(['/reports/savefilecsv']) ?>";
        } else {

            var url = "<?= Url::to(['/reports/savefilecsvbyqueue']) ?>";
        }
        var selectedChannels = getSelectedChannelIds();

        window.open(
                url + "&fromDate=" + fromDate
                + "&toDate=" + toDate
                + "&fromTime=" + fromTime
                + "&toTime=" + toTime
                + "&queueId=" + queueId
                + "&incoming=" + incoming
                + "&agentEx=" + agentEx
                + "&agentId=" + agentId
                + "&selectedChannels=" + selectedChannels
               ,"_self");

    }


    $('#fromdate').datetimepicker({
//        format: 'DD/MM/YYYY'
        format: 'YYYY-MM-DD'
    });
    $('#todate').datetimepicker({
//        format: 'DD/MM/YYYY'
        format: 'YYYY-MM-DD'
    });

    $("#queueListGroup > a").click(function(){
        $("#channelsCdrListDiv > #noCdrsDiv").css("display","none");
        $("#channelsCdrListDiv > #cdrCommonDiv").css("display","inline-block");
        $("#channelsCdrListDiv > #agentsOnlyDiv").css("display","none");
        $("#channelsCdrListDiv > #allAgentsDiv").css("display","inline-block");
        $('#channelsCdrListDiv input:checked').removeAttr('checked');
    });

    $('#agentListGroup > a').not(document.getElementById("0")).click(function(){
        $("#channelsCdrListDiv > #cdrCommonDiv").css("display","inline-block");
        $("#channelsCdrListDiv > #agentsOnlyDiv").css("display","inline-block");
        $("#channelsCdrListDiv > #allAgentsDiv").css("display","none");
        $("#channelsCdrListDiv > #noCdrsDiv").css("display","none");
        $('#channelsCdrListDiv input:checked').removeAttr('checked');
    });

    $('#agentListGroup > #0').click(function(){
        $("#channelsCdrListDiv > #cdrCommonDiv").css("display","inline-block");
        $("#channelsCdrListDiv > #allAgentsDiv").css("display","inline-block");
        $("#channelsCdrListDiv > #agentsOnlyDiv").css("display","none");
        $("#channelsCdrListDiv > #noCdrsDiv").css("display","none");
        $('#channelsCdrListDiv input:checked').removeAttr('checked');
    });



    function callAgentReports(agent_id, agent_ex, agent_name, id) {
        //active view button
        <?php
            if(Yii::$app->session->get("user_role") == "1"){
                // user is admin
        ?>
        $("#channelsListDiv").empty();
        $("#channelsListDiv").append("<label>No channels available</label>");
        <?php
            }
        ?>
        $('#btnView').removeClass('disabled');
        $('#btnSummeryView').removeClass('disabled');
        $('#hiddenAgentExt').val(agent_ex);
        $('#hiddenAgentName').val(agent_name);
        $('#hiddenAgentId').val(agent_id);
        $('#hiddenQueueId').val("");
        var url = "<?= Url::to(['reports/performanceoverviewbyagent']) ?>";

        //Start : set agents panel css
        {
            var items = document.getElementById('agentListGroup').getElementsByTagName('a');
            for (var i = 0; i < items.length; i++) {
                items[i].style.backgroundColor = "white";
                items[i].style.color = "black";
            }
            document.getElementById(id).style.backgroundColor = "grey";
            document.getElementById(id).style.color = "white";

            $("#queueListGroup > a").css("backgroundColor", "white");
            $("#queueListGroup > a").css("color", "black");
        }
    }
    function filterRecords() {
        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();

        if(contactNumber != ""){
                var pattern = /[^0-9]/;
                var result = contactNumber.match(pattern);
                if(result){
                   swal(
                    'Oops!!!',
                    'Contact number is incorrect. Please remove non numeric characters',
                    'error'
                    );
                   return false;
                }
        }


        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
           var url = "<?= Url::to(['reports/performanceoverviewbydate']) ?>";
        }else{
           var url = "<?= Url::to(['reports/performanceoverviewbyqueue']) ?>";
        }

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();

        if (fromDateConverted < toDateConverted) {
            showLoadingScreen();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    fromdate: fromDate,
                    todate: toDate,
                    agent_id: agentId,
                    agentex: agentEx,
                    agentname: agentName,
                    contactNum: contactNumber,
                    queue_id : queueId,
                    fromTime : fromTime,
                    toTime : toTime,
                    channels: selectedChannels
                },
                success: function (data)
                {
                    hideLoadingScreen();
                    var result = $(data).find('#agentReportsDIV');
                    $("#agentReportsDiv").html(result);
                    showSearchTimeDuration(searchStartDate, new Date());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    hideLoadingScreen();
                    showErrorMessage();
                    console.log('filter records : ' + xhr.responseText + ', \n\
                        status: ' + xhr.status + ', \n\
                        thrownError: ' + thrownError);
//                    alert(xhr.responseText);
//                    alert(xhr.status);
//                    alert(thrownError);
//                    alert(xhr.responseText);

                }
            });
        } else {
            swal(
                    'Oops!!!',
                    'Invalid dates, please select a valid date range.',
                    'error'
                    );
        }


    }

    /**
     *
     * @returns {undefined}
     *
     * @since 2017-10-30
     * @author Sandun
     *
     */
    function filterRecordsSummery(){
        $("#resulttable").css("display","none");
        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();

        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
            var url = "<?= Url::to(['reports/summery']) ?>";
        }else{
            var url = "<?= Url::to(['reports/summerybyqueue']) ?>";
        }

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

//        alert(fromDate + " +++ " + toDate);
        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();

        if (fromDateConverted < toDateConverted) {
            showLoadingScreen();
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    fromdate: fromDate,
                    todate: toDate,
                    agent_id: agentId,
                    agentex: agentEx,
                    agentname: agentName,
                    contactNum: contactNumber,
                    queue_id : queueId,
                    fromTime : fromTime,
                    toTime : toTime,
                    channels: selectedChannels
                },
                success: function (data)
                {
//                    alert(data);
                    hideLoadingScreen();
                    var result = $(data).find('#agentReportsDIV');
                    $("#agentReportsDiv").html(result);
                    showSearchTimeDuration(searchStartDate, new Date());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    hideLoadingScreen();
                    showErrorMessage();
//                    alert(xhr.responseText);
                    console.log('filter records summery : ' + xhr.responseText + ', \n\
                        status: ' + xhr.status + ', \n\
                        thrownError: ' + thrownError);
                }
            });
        } else {
            swal(
                    'Oops!!!',
                    'Invalid dates, please select a valid date range.',
                    'error'
                    );
        }

    }

    function showLoadingScreen(){
        $("#agentReportsDiv").empty();
        $("#loadingGifImg").css("display", "inline");
        $("#searchTimeSpan").html(""); // clears the search time diplay span
        return true;
    }

    function hideLoadingScreen(){
        $("#loadingGifImg").css("display", "none");
        return true;
    }

    function showErrorMessage(){
        $("#agentReportsDiv").empty();
        $("#agentReportsDiv").append("ERROR Occured. Please try again!");
    }


    function callQueueReports(queue_name, id) {
        //active view button
        $('#btnView').removeClass('disabled');
        $('#btnSummeryView').removeClass('disabled');
        $('#hiddenAgentExt').val(0);
        $('#hiddenAgentName').val(queue_name);
        $('#hiddenAgentId').val(0);
        $('#hiddenQueueId').val(id);

        //Start : set agents panel css
        {
            var items = document.getElementById('queueListGroup').getElementsByTagName('a');
            for (var i = 0; i < items.length; i++) {
                items[i].style.backgroundColor = "white";
                items[i].style.color = "black";
            }
            document.getElementById(id).style.backgroundColor = "grey";
            document.getElementById(id).style.color = "white";

            $("#agentListGroup > a").css("backgroundColor", "white");
            $("#agentListGroup > a").css("color", "black");
        }

        loadChannelsInfoOfTheQueue(id);

    }



    function loadChannelsInfoOfTheQueue(extQueueId){
        if(extQueueId){
            $("#channelsListDiv").empty();
            $("#channelsListDiv").append("<label>Loading channels...</label>");
            $.ajax({
                type : "GET",
                url : "<?= Url::to(['reports/findchannelsforqueue'])?>",
                data : {
                    queueId : extQueueId
                },
                success : function(data){
                    $("#channelsListDiv").empty();
                    if(data != "0"){
                        var queueChannelsObj = JSON.parse(data);
                        for(var x = 0; x < queueChannelsObj.length; x++){
                            var channelTickBox = $("<input>");
                            $(channelTickBox).attr("type", "checkbox");
                            $(channelTickBox).val(queueChannelsObj[x]['id']);
                            $(channelTickBox).addClass("channelCheckBoxesClass");
                            $(channelTickBox).attr("id", "channel"+queueChannelsObj[x]['channel_number']);

                            var channelName = " "+queueChannelsObj[x]['channel_name']+" ";

                            var channelNameLabel = $("<label></label>");
                            $(channelNameLabel).attr("data-toggle", "tooltip");
                            $(channelNameLabel).attr("data-placement", "left");
                            $(channelNameLabel).addClass("channelLabel");
                            $(channelNameLabel).attr("title", queueChannelsObj[x]['channel_description']);

                            $(channelNameLabel).append(channelTickBox);
                            $(channelNameLabel).append(channelName);


                            $("#channelsListDiv").append(channelNameLabel);
                        }
                    }else{
                        $("#channelsListDiv").append("<label>No channels available</label>");
                    }
                },
                error : function(error){
                    console.log("load channels error = "+error.responseText);
                    $("#channelsListDiv").empty();
                    $("#channelsListDiv").append("<label style='color:red'>Error loading channels</label>");
                }
            });
        }else{
            $("#channelsListDiv").empty();
            $("#channelsListDiv").append("<label style='color:red'>Queue ID is incorrect. Please select the queue again</label>");
        }
    }

    function loadQueueDataForSupervisor(element){
        $('#btnView').removeClass('disabled');
        $('#btnSummeryView').removeClass('disabled');
        $('#hiddenAgentExt').val(0);
        $('#hiddenAgentName').val("");
        $('#hiddenAgentId').val(0);
        $('#hiddenQueueId').val("");
        var url = "<?= Url::to(['reports/performanceoverviewbyqueue']) ?>";
        var items = document.getElementById('agentListGroup').getElementsByTagName('a');
            for (var i = 0; i < items.length; i++) {
                items[i].style.backgroundColor = "white";
                items[i].style.color = "black";
            }
            element.style.backgroundColor = "grey";
            element.style.color = "white";

            $("#queueListGroup > a").css("backgroundColor", "white");
            $("#queueListGroup > a").css("color", "black");

    }

    function getSelectedChannelIds(){
        if($(".channelCheckBoxesClass:checked").length > 0){
            // channels selected
            var selectedChannelsCsvString = "";
            $(".channelCheckBoxesClass:checked").each(function(){
                var channelNumber = $(this).attr('id');
                channelNumber = channelNumber.replace("channel", "");
                selectedChannelsCsvString = selectedChannelsCsvString+channelNumber+",";
            });
            return selectedChannelsCsvString;
        }else{
            // no channels selected
            return "0";
        }
    }

    function showSearchTimeDuration (start, end){
        var diff = end - start;
        $("#searchTimeSpan").html("Search time = "+ (diff/1000)+" seconds");
        return true;
    }

     function getSelectedCdrTypes() {
        var selectedCdrCsvString = "";
        $("#checkedCdrs:checked").each(function (index) {
            selectedCdrCsvString = selectedCdrCsvString.concat(this.value, ",");
        });
        return selectedCdrCsvString;
    }

</script>




    <script>

function filterRecordsTablle(){
        $("#resulttable").css("display","none");
        $("#answeredDataDiv").css("display","none");
        $("#outgoingDataDiv").css("display","none");
        $("#missedDataDiv").css("display","none");
        $("#ivrDataDiv").css("display","none");
        $("#vmDataDiv").css("display","none");
        $("#abandonedDataDiv").css("display","none");
        $("#loginDataDiv").css("display","none");
        $("#dndDataDiv").css("display","none");
        $("#agentReportsDIV").html("");
        $("#searchTimeSpan").html("");

        var searchStartDate = new Date();
        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");
        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);
        var agentEx = $('#hiddenAgentExt').val();
        var checkedCdrs = getSelectedCdrTypes();

        if (checkedCdrs.length == 0) {
            if (agentEx == 0) {
                checkedCdrs = "1,2,3,7,8";
            }else{
                checkedCdrs = "1,2,3,4,5,6";
            }
        }

        if (fromDateConverted < toDateConverted) {

            if ( agentEx == 0 ) {
                $('#agentNameTitle').html("All Agents");
            }else{
                $('#agentNameTitle').html($('#hiddenAgentName').val() + ":" + $('#hiddenAgentExt').val());
            }

            $("#resulttable").css("display","block");

            if(checkedCdrs.indexOf("1") != -1){
                $("#answeredDataDiv").css("display","block");
                getAnsweredCallsData();
            }
            if(checkedCdrs.indexOf("2") != -1){
                $("#outgoingDataDiv").css("display","block");
                getOutgoingCallsData();
            }
            if(checkedCdrs.indexOf("3") != -1){
                $("#missedDataDiv").css("display","block");
                getMissedCallsData();
            }
            if (agentEx == 0) {
                if(checkedCdrs.indexOf("7") != -1){
                    $("#ivrDataDiv").css("display","block");
                        getIvrCallsData();
                }
                if(checkedCdrs.indexOf("8") != -1){
                    $("#vmDataDiv").css("display","block");
                        getVmData();
                }
            }else{
                if(checkedCdrs.indexOf("4") != -1){
                    $("#abandonedDataDiv").css("display","block");
                    getAbandonedData();
                }

                if(checkedCdrs.indexOf("5") != -1){
                    $("#loginDataDiv").css("display","block");
                    getLoginDetailsData();

                }

                if(checkedCdrs.indexOf("6") != -1){
                    $("#dndDataDiv").css("display","block");
                    getDndData();
                }

            }

        }else{
            swal(
                'Oops!!!',
                'Invalid dates, please select a valid date range.',
                'error'
                );
        }
    }


        function getAnsweredCallsData(){

        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();

            $("#answeredDataTable").remove();
            $("#tableWrapperAnswered").remove();

            var newtableWrapper = $('<div></div>').attr('id','tableWrapperAnswered');

            var newTable = $('<table></table').attr('class','display compact');
            newTable.attr('id','answeredDataTable');
            newTable.css('width','100%');

            var newHead = $('<thead></thead>').attr('id','tablehead');
            var tr = $('<tr></tr>');

            var th1 = $('<th></th>').html('Caller Number');
            tr.append(th1);

            var th2 = $('<th></th>').html('Answered Time');
            tr.append(th2);

        if (agentEx == 0) {
            var th3 = $('<th></th>').html('Agent');
            tr.append(th3);
            }

            var th4 = $('<th></th>').html('End Time');
            tr.append(th4);

            var th5 = $('<th></th>').html('Duration');
            tr.append(th5);

            var th6 = $('<th></th>').html('PlayBack');
            tr.append(th6);

            newHead.append(tr);
            newTable.append(newHead);
            newtableWrapper.append(newTable);

            $("#answeredDataTableDiv").append(newtableWrapper);

        if(contactNumber != ""){
                var pattern = /[^0-9]/;
                var result = contactNumber.match(pattern);
                if(result){
                   swal(
                    'Oops!!!',
                    'Contact number is incorrect. Please remove non numeric characters',
                    'error'
                    );
                   return false;
                }
        }

        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
           var outputType = "byDate";
        }else{
           var outputType = "byQueue";
        }
        var url = "<?= Url::to(['datareports/performanceoverviewbydate']) ?>";


        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();
            $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { console.log("Datatables Error :"+ message)};
            $('#answeredDataTable').DataTable( {
                "processing": true,
                "serverSide": true,
                "destroy": true,
                "searching": false,
                "order": [ 3, 'desc' ],
                "iDisplayLength": 100,
                "ajax":{
                    type: "POST",
                    url: url,
                    data: {
                        fromdate: fromDate,
                        todate: toDate,
                        agent_id: agentId,
                        agentex: agentEx,
                        agentname: agentName,
                        contactNum: contactNumber,
                        queue_id : queueId,
                        fromTime : fromTime,
                        toTime : toTime,
                        channels: selectedChannels,
                        tableType : "Answered",
                        outputType:outputType
                    }
                }
            });
    }



        function getOutgoingCallsData(){

        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();


            $("#outgoingDataTable").remove();
            $("#tableWrapperOutgoing").remove();

            var newtableWrapper = $('<div></div>').attr('id','tableWrapperOutgoing');

            var newTable = $('<table></table').attr('class','display compact');
            newTable.attr('id','outgoingDataTable');
            newTable.css('width','100%');

            var newHead = $('<thead></thead>').attr('id','tablehead');
            var tr = $('<tr></tr>');

            var th1 = $('<th></th>').html('Receiver');
            tr.append(th1);

        if (agentEx == 0) {
            var th3 = $('<th></th>').html('Agent');
            tr.append(th3);
            }

            var th2 = $('<th></th>').html('Start Time');
            tr.append(th2);

            var th7 = $('<th></th>').html('Answered Time');
            tr.append(th7);

            var th4 = $('<th></th>').html('End Time');
            tr.append(th4);

            var th5 = $('<th></th>').html('Duration');
            tr.append(th5);

            var th6 = $('<th></th>').html('PlayBack');
            tr.append(th6);

            newHead.append(tr);
            newTable.append(newHead);
            newtableWrapper.append(newTable);

            $("#outgoingDataTableDiv").append(newtableWrapper);


        if(contactNumber != ""){
                var pattern = /[^0-9]/;
                var result = contactNumber.match(pattern);
                if(result){
                   swal(
                    'Oops!!!',
                    'Contact number is incorrect. Please remove non numeric characters',
                    'error'
                    );
                   return false;
                }
        }


        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
           var outputType = "byDate";
        }else{
           var outputType = "byQueue";
        }
        var url = "<?= Url::to(['datareports/performanceoverviewbydate']) ?>";

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();
            $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { console.log("Datatables Error :"+ message)};
            $('#outgoingDataTable').DataTable( {
                "processing": true,
                "serverSide": true,
                "destroy": true,
                "searching": false,
                "order": [ 4, 'desc' ],
                "iDisplayLength": 100,
                "ajax":{
                    type: "POST",
                    url: url,
                    data: {
                        fromdate: fromDate,
                        todate: toDate,
                        agent_id: agentId,
                        agentex: agentEx,
                        agentname: agentName,
                        contactNum: contactNumber,
                        queue_id : queueId,
                        fromTime : fromTime,
                        toTime : toTime,
                        channels: selectedChannels,
                        tableType : "Outgoing",
                        outputType:outputType
                    }
                }
            });
    }



        function getMissedCallsData(){

        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();

        if(contactNumber != ""){
                var pattern = /[^0-9]/;
                var result = contactNumber.match(pattern);
                if(result){
                   swal(
                    'Oops!!!',
                    'Contact number is incorrect. Please remove non numeric characters',
                    'error'
                    );
                   return false;
                }
        }


        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
           var outputType = "byDate";
        }else{
           var outputType = "byQueue";
        }
        var url = "<?= Url::to(['datareports/performanceoverviewbydate']) ?>";

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();
            $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { console.log("Datatables Error :"+ message)};
            $('#missedcallDataTable').DataTable( {
                "processing": true,
                "serverSide": true,
                "destroy": true,
                "searching": false,
                "order": [ 0, 'asc' ],
                "iDisplayLength": 100,
                "ajax":{
                    type: "POST",
                    url: url,
                    data: {
                        fromdate: fromDate,
                        todate: toDate,
                        agent_id: agentId,
                        agentex: agentEx,
                        agentname: agentName,
                        contactNum: contactNumber,
                        queue_id : queueId,
                        fromTime : fromTime,
                        toTime : toTime,
                        channels: selectedChannels,
                        tableType : "Missed",
                        outputType:outputType
                    }
                }
            });
    }


        function getIvrCallsData(){

        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();

        if(contactNumber != ""){
                var pattern = /[^0-9]/;
                var result = contactNumber.match(pattern);
                if(result){
                   swal(
                    'Oops!!!',
                    'Contact number is incorrect. Please remove non numeric characters',
                    'error'
                    );
                   return false;
                }
        }


        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
           var outputType = "byDate";
        }else{
           var outputType = "byQueue";
        }
        var url = "<?= Url::to(['datareports/performanceoverviewbydate']) ?>";

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();
            $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { console.log("Datatables Error :"+ message)};
            $('#ivrDataTable').DataTable( {
                "processing": true,
                "serverSide": true,
                "destroy": true,
                "searching": false,
                "order": [ 0, 'desc'],
                "ajax":{
                    type: "POST",
                    url: url,
                    data: {
                        fromdate: fromDate,
                        todate: toDate,
                        agent_id: agentId,
                        agentex: agentEx,
                        agentname: agentName,
                        contactNum: contactNumber,
                        queue_id : queueId,
                        fromTime : fromTime,
                        toTime : toTime,
                        channels: selectedChannels,
                        tableType : "IVR",
                        outputType:outputType
                    }
                }
            });
    }


        function getVmData(){

        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();

        if(contactNumber != ""){
                var pattern = /[^0-9]/;
                var result = contactNumber.match(pattern);
                if(result){
                   swal(
                    'Oops!!!',
                    'Contact number is incorrect. Please remove non numeric characters',
                    'error'
                    );
                   return false;
                }
        }


        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
           var outputType = "byDate";
        }else{
           var outputType = "byQueue";
        }
        var url = "<?= Url::to(['datareports/performanceoverviewbydate']) ?>";

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();
            $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { console.log("Datatables Error :"+ message)};
            $('#vmDataTable').DataTable({
                "processing": true,
                "serverSide": true,
                "destroy": true,
                "searching": false,
                "order": [ 0, 'desc'],
                "ajax":{
                    type: "POST",
                    url: url,
                    data: {
                        fromdate: fromDate,
                        todate: toDate,
                        agent_id: agentId,
                        agentex: agentEx,
                        agentname: agentName,
                        contactNum: contactNumber,
                        queue_id : queueId,
                        fromTime : fromTime,
                        toTime : toTime,
                        channels: selectedChannels,
                        tableType : "VM",
                        outputType:outputType
                    }
                }
            });
        }

        function getAbandonedData(){

        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();

        if(contactNumber != ""){
                var pattern = /[^0-9]/;
                var result = contactNumber.match(pattern);
                if(result){
                   swal(
                    'Oops!!!',
                    'Contact number is incorrect. Please remove non numeric characters',
                    'error'
                    );
                   return false;
                }
        }


        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
           var outputType = "byDate";
        }else{
           var outputType = "byQueue";
        }
        var url = "<?= Url::to(['datareports/performanceoverviewbydate']) ?>";

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();
            $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { console.log("Datatables Error :"+ message)};
            $('#abandonedDataTable').DataTable({
                "processing": true,
                "serverSide": true,
                "destroy": true,
                "searching": false,
                "order": [ 0, 'desc'],
                "ajax":{
                    type: "POST",
                    url: url,
                    data: {
                        fromdate: fromDate,
                        todate: toDate,
                        agent_id: agentId,
                        agentex: agentEx,
                        agentname: agentName,
                        contactNum: contactNumber,
                        queue_id : queueId,
                        fromTime : fromTime,
                        toTime : toTime,
                        channels: selectedChannels,
                        tableType : "Abandoned",
                        outputType:outputType
                    }
                }
            });
        }


        function getLoginDetailsData(){

        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();

        if(contactNumber != ""){
                var pattern = /[^0-9]/;
                var result = contactNumber.match(pattern);
                if(result){
                   swal(
                    'Oops!!!',
                    'Contact number is incorrect. Please remove non numeric characters',
                    'error'
                    );
                   return false;
                }
        }


        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
           var outputType = "byDate";
        }else{
           var outputType = "byQueue";
        }
        var url = "<?= Url::to(['datareports/performanceoverviewbydate']) ?>";

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");
        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();
            $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { console.log("Datatables Error :"+ message)};
            $('#loginDataTable').DataTable({
                "processing": true,
                "serverSide": true,
                "destroy": true,
                "searching": false,
                "order": [ 0, 'desc'],
                "ajax":{
                    type: "POST",
                    url: url,
                    data: {
                        fromdate: fromDate,
                        todate: toDate,
                        agent_id: agentId,
                        agentex: agentEx,
                        agentname: agentName,
                        contactNum: contactNumber,
                        queue_id : queueId,
                        fromTime : fromTime,
                        toTime : toTime,
                        channels: selectedChannels,
                        tableType : "Login",
                        outputType:outputType
                    }
                }
            });
        }


        function getDndData(){

        var searchStartDate = new Date();
        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();

        if(contactNumber != ""){
                var pattern = /[^0-9]/;
                var result = contactNumber.match(pattern);
                if(result){
                   swal(
                    'Oops!!!',
                    'Contact number is incorrect. Please remove non numeric characters',
                    'error'
                    );
                   return false;
                }
        }

        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
           var outputType = "byDate";
        }else{
           var outputType = "byQueue";
        }
        var url = "<?= Url::to(['datareports/performanceoverviewbydate']) ?>";

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();
            $.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { console.log("Datatables Error :"+ message)};
            $('#dndDataTable').DataTable({
                "processing": true,
                "serverSide": true,
                "destroy": true,
                "searching": false,
                "order": [ 0, 'desc' ],
                "ajax":{
                    type: "POST",
                    url: url,
                    data: {
                        fromdate: fromDate,
                        todate: toDate,
                        agent_id: agentId,
                        agentex: agentEx,
                        agentname: agentName,
                        contactNum: contactNumber,
                        queue_id : queueId,
                        fromTime : fromTime,
                        toTime : toTime,
                        channels: selectedChannels,
                        tableType : "DND",
                        outputType:outputType
                    }
                }
            });
        }

    </script>
    <script>

                var audioType = "<?= $audioType ?>";

                function playCallRecord(data) {
                    // var row = playAudioAnchor.parentNode.parentNode;
                    var inputFields = data.split(',');
                    var playAudioAnchor = inputFields[0];
                    var direction = inputFields[1];
                    var extension = inputFields[2];
                    var customerNumber = inputFields[3];
                    var datetime = inputFields[4];
                    var callEndedDateTime = inputFields[5];


                    var id = playAudioAnchor;

                    var audioFilePath = "<?= Url::to(['ftp/playaudiofile']) ?>" + "&datetime="+datetime+"&callType="+direction+"&ext="+extension+"&phoneNumber="+customerNumber+"&endedTime="+callEndedDateTime+"&acctId="+id;
                    console.log("Playing audio file path = "+audioFilePath);
                    $(".audioPlayerClass").remove();
                    var newAudioElement = $("<audio id='audioPlayer' class='audioPlayerClass' controls></audio>").append("<source id='audioPlayerSource' src='" + audioFilePath + "' type='audio/wav'> ");

                    $("#audioPlayerDiv").append(newAudioElement);
                    $(newAudioElement)[0].play();

                    createShowTransfersButton(direction, extension, customerNumber, datetime, id);
                }

                function pauseAudio() {
    //                    alert($('#audioPlayer')[0].paused);
                    if ($('.audioPlayerClass')[0].paused == false) {
                        $('.audioPlayerClass')[0].pause();
                    }
                }


                $(document).ready(function(){
                     $('#audioPlaybackModal').on('hidden.bs.modal', function () {
                        $("#transfersTable").css("display", "none");
                        $("#transferDataLoadingGifImg").css("display", "none");
                        pauseAudio();
                    });

                    $("#cdrList-tab").click();

                    callAgentReports(<?= $agents[0]['id'] ?>,<?= $agents[0]['voip_extension'] ?>, '<?= $agents[0]['fullname'] ?>', this.id);
                });


                function downloadCurrentlyPlayingAudioFile(){
                    var url = $("#audioPlayerSource").attr("src");
                    if(url != ""){
                        console.log("downloading audio file from "+url);
                        $("#downloaderIframe").attr("src", url);
                    }else{
                        console.log("No audio url to download");
                    }
                }

                function getTransferCallsInfo(direction, extension, customerNumber, datetime, acctId){
                    showTransferCallsLoadingScreen();
                    $.ajax({
                        url : "<?= Url::to(['ftp/get_call_tranfer_information']) ?>",
                        type : "GET",
                        data: {
                            datetime : datetime,
                            callType : direction,
                            ext: extension,
                            phoneNumber: customerNumber,
                            acctId : acctId
                        },
                        success: function(str){
                            hideTransferCallsLoadingScreen();
                            console.log("get transfer calls info success = "+str);
                            var jsonDecodedTransferDataSet = JSON.parse(str);
                            generateTransferCallsTableData(jsonDecodedTransferDataSet);
                        },
                        error: function(err){
                            hideTransferCallsLoadingScreen();
                            console.log("get transfer calls info error - "+err.responseText);
                        }
                    });
                }

                function showTransferCallsLoadingScreen(){
                    $("#transferDataLoadingGifImg").css("display", "inline");
                    $("#transfersTable").css("display", "none");
                    $("#transfersTable > tbody").empty();
                    hideNoNextTransfersAvailableMessage();
                    return true;
                }

                function hideTransferCallsLoadingScreen(){
                    $("#transferDataLoadingGifImg").css("display", "none");
                    return true;
                }

                function showNoNextTransfersAvailableMessage(){
                    $("#noTransfersAvailableMsgDiv").css("display", "inline");
                    return true;
                }

                function hideNoNextTransfersAvailableMessage(){
                    $("#noTransfersAvailableMsgDiv").css("display", "none");
                    return true;
                }

                function generateTransferCallsTableData(jsonDecodedTransferData){
                    if(jsonDecodedTransferData['nextTransferedCallsCount'] > 0){
                        // has transfered calls
                        hideNoNextTransfersAvailableMessage();
                        $("#transfersTable").css("display", "initial");
                        $("#transfersTable").css("width", "100%");
                        populateTransferTable(jsonDecodedTransferData['nextTransferedCalls']);
                    }else{
                        showNoNextTransfersAvailableMessage();
                    }
                }

                function loadCallTransfersTable(direction, extension, customerNumber, datetime, id){
                    $("#showCallTransfersAnchor").remove();
                    getTransferCallsInfo(direction, extension, customerNumber, datetime, id);
                    return true;
                }

                function createShowTransfersButton(direction, extension, customerNumber, datetime, id){
                    $(".showTransfersBtnClass").remove();
                    var anchorBtn = $("<a></a>");
                    $(anchorBtn).addClass("btn btn-primary btn-block showTransfersBtnClass");
                    $(anchorBtn).attr("id", "showCallTransfersAnchor");
                    var onclickUrl = "loadCallTransfersTable('"+direction+"', "+extension+", "+customerNumber+", '"+datetime+"', "+id+")";
                    $(anchorBtn).attr("onclick", onclickUrl);
                    $(anchorBtn).html("Show call transfers");

                    $("#transfersTable").parent().prepend(anchorBtn);
                }

                function populateTransferTable(transferTableDataArray){
                    for (var x = 0; x < transferTableDataArray.length; x++) {
                        var newRow = $("<tr></tr>");

                        var index = $("<b></b>").html(x + 1);
                        var indexCell = $("<td></td>").html(index);
                        var callerNumberCell = $("<td></td>").html(transferTableDataArray[x]['callerNumber']);
                        var answeredTimeCell = $("<td></td>").html(transferTableDataArray[x]['answeredTime']);
                        var agentNameCell = $("<td></td>").html(transferTableDataArray[x]['agentName']+" : "+transferTableDataArray[x]['dst']);
                        var endTimeCell = $("<td></td>").html(transferTableDataArray[x]['endTime']);
                        var durationCell = $("<td></td>").html(transferTableDataArray[x]['duration']);

                        /* alert("playTransferCallAudio('I', "+transferTableDataArray[x]['dst']+", "+transferTableDataArray[x]['callerNumber']+", "+transferTableDataArray[x]['startedTime']+")"); */
                        var startTime = transferTableDataArray[x]['startedTime'];
                        var startTimereplaced = startTime.replace(" ", "+");
                        var playButton = $("<a></a>").addClass("btn btn-block btn-primary").attr("href", "#").html("Play");
                        $(playButton).attr("onclick", "playTransferCallAudio('I', '"+transferTableDataArray[x]['dst']+"', '"+transferTableDataArray[x]['callerNumber']+"', '"+transferTableDataArray[x]['startedTime']+"')");
                        var playButtonCell = $("<td></td>").html(playButton);

                        $(newRow).append(indexCell);
                        $(newRow).append(callerNumberCell);
                        $(newRow).append(answeredTimeCell);
                        $(newRow).append(agentNameCell);
                        $(newRow).append(endTimeCell);
                        $(newRow).append(durationCell);
                        $(newRow).append(playButtonCell);

                        $("#transfersTable > tbody").append(newRow);

                    }
                }

                function playTransferCallAudio(direction, extension, customerNumber, datetime){
                    var audioFilePath = "<?= Url::to(['ftp/playaudiofile']) ?>" + "&datetime="+datetime+"&callType="+direction+"&ext="+extension+"&phoneNumber="+customerNumber;
                    console.log("Playing audio file path = "+audioFilePath);
                    $(".audioPlayerClass").remove();
                    var newAudioElement = $("<audio id='audioPlayer' class='audioPlayerClass' controls></audio>").append("<source id='audioPlayerSource' src='" + audioFilePath + "' type='audio/wav'> ");
                    $("#audioPlayerDiv").append(newAudioElement);
                    $(newAudioElement)[0].play();

                }


    </script>
    <style>
        .audioPlayerClass{
            width: 100% !important;
        }
    </style>
    <!-- audio playback modal -->
        <div id="audioPlaybackModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align:center">Call Audio Player</h4>
                </div>
              <div class="modal-body">
                <!-- <p>Some text in the modal.</p> -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="audioPlayerDiv">
                                <audio id="audioPlayer" controls class="audioPlayerClass">
                                    <source id="audioPlayerSource" src="" type="audio/wav">
                                </audio>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-md btn-info btn-block" onclick="downloadCurrentlyPlayingAudioFile()">Download</a>
                        </div>
                    </div>
                    <!-- Transfers section -->
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title" style="text-align:center" id = "transfersH4Id">Call Transfers</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <table id="transfersTable" class="table table-fixed table-striped" style="display: none">
                                <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            Caller Number
                                        </th>
                                        <th>
                                            Answered Time
                                        </th>
                                        <th>
                                            Agent
                                        </th>
                                        <th>
                                            End Time
                                        </th>
                                        <th>
                                            Duration
                                        </th>
                                        <th>
                                            Call Playback
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- End of Transfers section -->
                    <!-- Loading screen section -->
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <img class="img-responsive" src="images/loading.gif" id="transferDataLoadingGifImg" style="display: none">
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                    <div class="row" id="noTransfersAvailableMsgDiv" style="display: none">
                        <div class="col-md-12">
                            <div class="alert alert-warning" style="text-align: center">
                                <strong>No transfers available</strong>
                            </div>
                        </div>
                    </div>
                    <!-- End of Loading screen section -->
                </div>
              </div>
            </div>

          </div>
        </div>
    <!-- end of audio playback modal -->
    <!-- downloader iframe -->
    <iframe src="" id="downloaderIframe" style="display: none">

    </iframe>
    <!-- end of downloader iframe-->