<?php

use yii\helpers\Url;
use yii\helpers\Html;

$data_arr = $dataSet;
$data_arr_jsn = json_encode($data_arr);
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.js"></script>

<div>
    <div class="col-xs-6">
        <canvas id="myBarChart" height="300px"></canvas>
        <script>
            var ctx = document.getElementById("myBarChart").getContext('2d');
            var myBarChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                    datasets: [{
                            label: '# of Votes',
                            data: <?= $data_arr_jsn ?>,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ]
//                            borderColor: [
//                                'rgba(255,99,132,1)',
//                                'rgba(54, 162, 235, 1)',
//                                'rgba(255, 206, 86, 1)',
//                                'rgba(75, 192, 192, 1)',
//                                'rgba(153, 102, 255, 1)',
//                                'rgba(255, 159, 64, 1)'
//                            ],
//                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                    }
                }
            });
        </script>
    </div>
    <div class="col-xs-6">
        <canvas id="myPieChart" height="300px"></canvas>
        <script>
            var ctx = document.getElementById("myPieChart").getContext('2d');
            var myPieChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                    datasets: [{
                            label: '# of Votes',
                            data: <?= $data_arr_jsn ?>,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ]
                        }]
                },
                options: {
                
                }
            });
        </script>

    </div>
</div>
