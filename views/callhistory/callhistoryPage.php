<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<style>
    .call-history-table {
        width: 100%;
    }

    .call-history-thead, .call-history-tbody,.call-history-tbody tr,.call-history-tbody td,.call-history-thead th,.call-history-thead tr,.call-history-thead td {
        display: block;
    }

    .call-history-thead th {
        height: 30px;
    }

    .call-history-tbody {
        overflow-y: auto;
        height: 500px;
        font-size: 90%;
    }

    .call-history-tbody td, .call-history-thead th {
        float: left;
        width: 20%;
    }

    .call-history-thead tr:after, call-history-tbody tr:after {
        clear: both;
        content: ' ';
        display: block;
        visibility: hidden;
    }                
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3">
            <label for="filterDropdown">Filter by agent</label>
            <select name="filterDropdown" class="form-control">
                <option>Showing all Agents</option>
            </select>
        </div>
        <div class="col-xs-3">
            <label for="filterDropdown">Filter by Customer</label>
            <select name="filterDropdown" class="form-control">
                <option>Showing all Customers</option>
            </select>
        </div>
        <div class="col-xs-6" align="right"><a>Clear all filters</a></div>
    </div><br>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-fixed table-striped call-history-table" style="border-radius: 5px">
                <thead class="call-history-thead">
                    <tr>
                        <th>
                            Customer
                        </th>
                        <th>
                            Agent
                        </th>
                        <th>
                            Start
                        </th>
                        <th>
                            End
                        </th>
                        <th>
                            Duration
                        </th>
                    </tr>
                </thead>
                <tbody class="call-history-tbody">
                    <?php
                    $json_callHistory = json_decode($callHistory, TRUE);

                    for ($index = 0; $index < count($json_callHistory); $index++) {

                        $seconds = $json_callHistory[$index]['duration'];
                        
                        $hours = floor($seconds / 3600);
                        $mins = floor($seconds / 60 % 60);
                        $secs = floor($seconds % 60);
                        
                        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                                                
                        ?>
                        <tr>
                            <td><i></i><?= $json_callHistory[$index]['caller'] ?></td>
                            <td><?= $json_callHistory[$index]['reciever'] ?></td>
                            <td><?= $json_callHistory[$index]['start'] ?></td>
                            <td><?= $json_callHistory[$index]['end'] ?></td>
                            <td><?= $timeFormat ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
