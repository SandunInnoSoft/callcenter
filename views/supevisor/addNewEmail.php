<?php
use yii\helpers\Url;
?>


<?php 

$data = 0;
if (isset($emailInfo)) {
    $data = $emailInfo;
}


?>
<body>
    <div class = "container-fluid">
        <div class = "row">
            <div class = "col-md-12">
                <a href = "<?= Url::to(['supevisor/manageemail']); ?>" class = "btn btn-info btn-sm" type = "button">< Back</a>
                <h3 class = "text-primary text-center">
                    <?php if ($data != 0) { ?>
                        <b> Update Email </b>
                    <?php }else{ ?>
                        <b> Add New Email </b>
                    <?php } ?>
                </h3>
                <!-- <form action="#" role = "form" id = "idNewEmailForm"> -->
                    <div class = "form-group">

                        <label>
                            Subscriber Name<span style="color: tomato">*</span>
                        </label>
                        <input type = "text" class = "form-control" id = "subname" name = "subname" <?php if ($data !=0) { ?> value = "<?=$data[0]['subsciber_name']?>" <?php } ?> />
                        <br>
                        <label>
                            Email Address<span style="color: tomato">*</span>
                        </label>
                        <input type = "text" class = "form-control" id = "emailaddress" name = "emailaddress" <?php if ($data !=0) { ?> value = "<?=$data[0]['email_address']?>" <?php } ?>/>                            
                        <span id="emailValidationMessage"></span>
                    </div>


                    <a id = "btnSaveExtension" class = "btn btn-success btn-md disabled" onclick="validateForm()">
                        <?php if ($data !=0) { ?> Update <?php }else{ ?> Save <?php } ?>
                    </a>
                    <a class = "btn btn-info" onclick="clearForm()">
                        Clear
                    </a>

                    <!-- </form> -->

                </div>
            </div>
            <br>
            <div class="row" id="notficationDiv">
            </div>
        </div>
    </body>
    <script>


        $("document").ready(function(){
            <?php if ($data == 0 ) { ?>
                clearForm();
            <?php }else{ ?>
                $("#subname").val("<?=$data[0]['subsciber_name']?>");
                $("#emailaddress").val("<?=$data[0]['email_address']?>");
            <?php } ?>    
        });

        var isEmailExists = true;

        $("#emailaddress").keypress(function(event) {
           pressEnter(event);
       });

        function pressEnter(event) {
            if (event.keyCode == 13) {
                $("#btnSaveExtension").click();
            }
        }

        function showSaveSuccessMessage(){
           $("#notficationDiv").empty();
           var successMsgDiv = $("<div></div>");
           $(successMsgDiv).addClass("alert alert-success");
           $(successMsgDiv).append("<strong> Email Saved Successfully!</strong>");
           $("#notficationDiv").append(successMsgDiv);
       }

       function showSaveFailedMessage(){
           $("#notficationDiv").empty();
           var failMsgDiv = $("<div></div>");
           $(failMsgDiv).addClass("alert alert-danger");
           $(failMsgDiv).append("<strong> Email Saving Failed!</strong> Please try saving again");
           $("#notficationDiv").append(failMsgDiv);
       }


        function showUpdateSuccessMessage(){
           $("#notficationDiv").empty();
           var successMsgDiv = $("<div></div>");
           $(successMsgDiv).addClass("alert alert-success");
           $(successMsgDiv).append("<strong> Email Updated Successfully!</strong>");
           $("#notficationDiv").append(successMsgDiv);
       }

       function showUpdateFailedMessage(){
           $("#notficationDiv").empty();
           var failMsgDiv = $("<div></div>");
           $(failMsgDiv).addClass("alert alert-danger");
           $(failMsgDiv).append("<strong> Email Updating Failed!</strong> Please try saving again");
           $("#notficationDiv").append(failMsgDiv);
       }


       function addNewEmailInfo(){
        var subname = $("#subname").val();
        var emailaddress = $("#emailaddress").val();

        $.ajax({
            url: "<?= Url::to(['supevisor/addnewemailajax'])?>",
            type: 'POST',
            data: {subname : subname,emailaddress:emailaddress},
            success: function (data, textStatus, jqXHR) {
                if(data == "1"){
                   showSaveSuccessMessage();
                   clearForm();
               }else{
                  showSaveFailedMessage();
              }
          },
          error: function (jqXHR, textStatus, errorThrown) {
           showSaveFailedMessage();
       }
   });

    }


    function updateEmailInfo(){
        var id = "<?=$data[0]['id']?>";
        var subname = $("#subname").val();
        var emailaddress = $("#emailaddress").val();

        console.log(id);
        console.log(subname);
        console.log(emailaddress);

        $.ajax({
            url: "<?= Url::to(['supevisor/updateemailajax'])?>",
            type: 'POST',
            data: {id:id,subname : subname,emailaddress:emailaddress},
            success: function (data, textStatus, jqXHR) {
                if(data == "1"){
                   showUpdateSuccessMessage();
                   setTimeout(function(){ location.reload(); }, 1000);                   
               }else{
                  showUpdateFailedMessage();
              }
          },
          error: function (jqXHR, textStatus, errorThrown) {
           showUpdateFailedMessage();
       }
   });

    }


    function validateForm(){
        var validationSuccess = true;
        if($("#emailaddress").val() == ""){
                // Queue name is empty
                if(validationSuccess == true){
                    validationSuccess = false;
                    swal({
                        title: 'Oops!',
                        text: 'Email Address is empty!',
                        type: "warning"
                    });
                }
            }

            if($("#subname").val() == ""){
                // Queue name is empty
                if(validationSuccess == true){
                    validationSuccess = false;
                    swal({
                        title: 'Oops!',
                        text: 'Subscriber Name is empty!',
                        type: "warning"
                    });
                }
            }

            
            if(validationSuccess == true){
            	if(isEmailExists == false){

                    <?php if ($data == 0 ) { ?>
                        addNewEmailInfo();
                    <?php }else{ ?>
                        updateEmailInfo();
                    <?php } ?>   


                }else{
                 swal({
                    title: 'Oops!',
                    text: 'Email Address already exists!',
                    type: "warning"
                });
             }
         }

     }          


     function clearForm(){
        $("#subname").val("");
        $("#emailaddress").val("");
        $("#emailValidationMessage").html("");
    }

    $("#emailaddress").keyup(function(){
        var typingText = $(this).val();
        var validatedEmail = validateEmail(typingText);
        var valform = true;
        var currentEmail = null;
            <?php if ($data != 0 ) { ?>
                currentEmail ="<?=$data[0]['email_address']?>";
            <?php } ?>


        if (!validatedEmail) {
            $("#emailValidationMessage").html("Email Address Is Not Valid");
            $("#emailValidationMessage").css("color", "red");  
            $("#btnSaveExtension").addClass("disabled");
            valform = false
        }

        if(typingText != ""  && valform){
                // typing text is not empty
                // $("#emailValidationMessage").html("Checking..");
                // $("#emailValidationMessage").css("color", "black");  
                $("#btnSaveExtension").addClass("disabled");

                $.ajax({
                    url: "<?= Url::to(['supevisor/checkemailavailability'])?>",
                    data: {typingText : typingText},
                    type: 'GET',
                    success: function (data, textStatus, jqXHR) {
                        if(data == "1"){
                            $("#emailValidationMessage").html("");
                            $("#emailValidationMessage").css("color", "green");
                            $("#btnSaveExtension").removeClass("disabled");
                            isEmailExists = false;
                        }else{
                            if (currentEmail == typingText) {
                                $("#emailValidationMessage").html("");
                                $("#emailValidationMessage").css("color", "red");  
                                $("#btnSaveExtension").removeClass("disabled");   
                                isEmailExists = false;                             
                            }else{
                                $("#emailValidationMessage").html("Email Address Already Exists");
                                $("#emailValidationMessage").css("color", "red");  
                                $("#btnSaveExtension").addClass("disabled");
                                isEmailExists = true;
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("extension validation error : "+jqXHR.responseText);
                        isEmailExists = true;
                    }
                });    
            }else{
                // typing text is empty
                $("#emailValidationMessage").html("");
                $("#emailValidationMessage").css("color", "red");  
                $("#btnSaveExtension").addClass("disabled");
            }
        });


    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }        


</script>