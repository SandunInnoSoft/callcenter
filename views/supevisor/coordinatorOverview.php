<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

use yii\helpers\Url;
use yii\helpers\Html;

$sessionDnd = (Yii::$app->session->get("dnd") == TRUE ? 1 : 0);
$userRole = Yii::$app->session->get("user_role");
?>
<html>
    <head>
        <meta charset="UTF-8">

        <script>
            $("title").html("Coordinator Overview");</script>        
        <!--<script src="softphone/webphone_api.js"></script>--> 

        <style>
            .fontSizeClass{
                font-size: 1em;
                margin-bottom: 0.5%;
            }

            body{
                background-color: #ededed;     
                /*font-size: 10px;*/
            }
            .panel-heading{
                background-color: #10297d !important;
                color: white !important;
            }
        </style>
        <style>
            @import url(css/font1.css);
            @import url(css/font-awesome.min.css);

            .span4
            {
                width: 35px;
                float: left;
                margin: 0 4px 8px 8px;
            }

            .phone
            {
                /*padding-top: 15px;*/
                /*padding-bottom: 10px;*/
                background: #fff;
                border: 1px solid #333;
                border-radius: 5px;
            }
            .tel
            {
                font-family: 'Lato' , sans-serif;   
                font-weight: bold;
                font-size: 20px;
                margin-bottom: 10px;
                margin-top: 10px;
                border: 1px solid #9e9e9e;
                border-radius: 5px;
            }
            .num-pad
            {
                padding-left: 10px;
            }

            .num
            {
                border: 1px solid #9e9e9e;
                -webkit-border-radius: 999px;
                border-radius: 999px;
                -moz-border-radius: 999px;
                height: 35px;
                background-color: #fff;
                color: #333;
                cursor: pointer;
            }
            .num:hover
            {
                background-color: #10297d;
                color: #faa61a;
                transition-property: background-color .2s linear 0s;
                -moz-transition: background-color .2s linear 0s;
                -webkit-transition: background-color .2s linear 0s;
                -o-transition: background-color .2s linear 0s;
            }
            .txt
            {
                font-size: 15px;
                font-weight: bold;
                text-align: center;
                margin-top: 2px;
                font-family: 'Lato' , sans-serif;
                line-height: 30px;
                color: #333;
            }
            .txt:hover{
                color: #faa61a;                
            }
            .small
            {
                font-size: 15px;
            }
            .btn
            {
                font-family: 'Lato' , sans-serif;        
                margin-top: 8px;
                font-weight: bold;
                -webkit-transition: .1s ease-in background-color;
                -webkit-font-smoothing: antialiased;
                letter-spacing: 1px;
            }
            .btn:hover
            {
                transition-property: background-color .2s linear 0s;
                -moz-transition: background-color .2s linear 0s;
                -webkit-transition: background-color .2s linear 0s;
                -o-transition: background-color .2s linear 0s;
            }
            .spanicons
            {
                width: 48px;
                float: left;
                text-align: center;
                margin-top: 10px;
                color: #000;
                font-size: 20px;
                cursor: pointer;
            }
            .spanicons:hover
            {
                color: #faa61a;
                transition-property: color .2s linear 0s;
                -moz-transition: color .2s linear 0s;
                -webkit-transition: color .2s linear 0s;
                -o-transition: color .2s linear 0s;
            }
            .keypad:hover{
                color: #faa61a;
                transition-property: color .2s linear 0s;
                -moz-transition: color .2s linear 0s;
                -webkit-transition: color .2s linear 0s;
                -o-transition: color .2s linear 0s;                
            }
            .badge{
                background-color: rgba(119, 119, 119, 0);
            }
            .list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus {
                background-color: #10297d;
                border-color: #10297d;
            }            

            .headertitles{
                font-size: 80%;
            }
            .onlineTime{
                font-size: 10px;
            }
            
            .onlineCalls{
                height: 50px;
            }

        </style>
        <script>


            $(document).ready(function () {
                if (onloadAgentRequests.length > 0) {
                    setAgentRequestsOnload();
                }
                $('.num').click(function () {
                    var num = $(this);
                    var text = $.trim(num.find('.txt').clone().children().remove().end().text());
                    var telNumber = $('#telNumber');
                    $(telNumber).val(telNumber.val() + text);
                });
            });</script>

        <style>
            /* CSS used here will be applied after bootstrap.css */
            .badge-notify{
                background:red;
                position:relative;
                top: -10px;
                left: -15px;
            }            
        </style>
        <script>
            // for webphone functionalities

            var serverAddress = "<?= $webphoneParams['serverAddress'] ?>";
            var username = "<?= $webphoneParams['username'] ?>";
            var password = "<?= $webphoneParams['password'] ?>";
            var onloadAgentRequests = new Array();
<?php
if (count($agentRequests) > 0) {
    $i = 0;
    foreach ($agentRequests as $key) {
        ?>
                    var data = ["<?= $key['type'] ?>", "<?= $key['id'] ?>", "<?= $key['name'] ?>"];
                    onloadAgentRequests.push(data);
        <?php
        $i++;
    }
}
?>
        </script>

        <script>

            // This is to change the title to get the user's attention

            $("title").html("Agent caller profile"); // change the header title
            var timeoutTitleFlash;
            /**
             * <b>This function shows flashing suns with rays at both sides of the title to get user's attention.</b>
             * 
             * @returns {undefined}
             * @author Sandun
             * @since 2017-07-14
             */
//    (function () {
            function showFlashingTitle() {
                var n = 0;
                var t = "Incomming call";
//        var f = function () {

//            timeoutTitleFlash = setTimeout(f, 500); // every 500ms
                timeoutTitleFlash = setInterval(function () {
                    n++;
                    switch (n) {
                        case 3:
                            n = 1; // no break, so continue to next label
                        case 1:
                            document.title = '☏ ☏ ' + t + ' ☎ ☎';
                            break;
                        default:
                            document.title = '☎ ☎ ' + t + ' ☏ ☏';
                    }
                }, 500);
            }

//        f(); // start the animation
//    })();

            /**
             * <b>This function clears the timer of the flashing title and </b>
             * 
             * @returns {undefined}
             * @author Sandun
             * @since 2017-07-14
             */
            function resetPageHeaderTitle() {
                clearTimeout(timeoutTitleFlash);
                $("title").html("Agent caller profile"); // change the header title
            }
        </script>

        <script>
            var contactNamesArray = new Array(); // This array hold all contact names in the contact list
            // for DND mode functions
            var webphoneRegistered = false; // This is to identify the webphone associated with the page is registered with the PBX or not
            var isDNDOn = "<?= $sessionDnd ?>";
            isDNDOn = (isDNDOn == "1" ? true : false);
            var userRole = "supervisor";
            var interval; // This keeps the JS interval object which runs on every 1 second since the page loaded until the webphone is registered 
            $(document).ready(function () {
                var value = '<?php echo $contacts ?>';
                addNewContactToList(value);
//        $("[name='dndSwitch']").bootstrapSwitch();
               interval = setInterval(function () {
                   if (webphoneRegistered == true) { //&& isDNDOn == "0"
                       turnDndOn();
                       clearWebphoneIntervalTimeout();
                   }

                   // if (webphoneRegistered == true) {
                   //     clearWebphoneIntervalTimeout();
                   // }
               }, 1000);
            });
//    $(function () {
//        $("#dndSwitch").bootstrapSwitch();
//    });

            /**
             * <p>This is to clear the JS interval used to turn on the DND mode when the webphone got registered with the PBX</p>
             * @returns {undefined}
             * @since 2017-07-24
             * @author Sandun
             */
            function clearWebphoneIntervalTimeout() {
                clearTimeout(interval);
            }

            /**
             * <p>this turns on the DND mode ON by dialing a specific number to the PBX and changes the appearance of the DND click button in the UI</p>
             * @param {type} dial_code
             * @returns {undefined}
             * @since 2017-07-24
             * @author Sandun
             */
            function turnDndOn() {
//                var number = "*74";
               document.getElementById('webphoneframe').contentWindow.unregisterWebphone();
                // document.getElementById('webphoneframe').contentWindow.setPhoneDndOn();
                $("#dndButtonAnchor").removeClass("btn-danger");
                $("#dndButtonAnchor").addClass("btn-success");
                $("#dndButtonAnchor").html("Set DND OFF");
                setSessionDNDState(1);
                isDNDOn == true;  
            }

            /**
             * <p>this turns on the DND mode OFF by dialing a specific number to the PBX and changes the appearance of the DND click button in the UI</p>
             * @returns {undefined}
             * @since 2017-07-24
             * @author Sandun
             */
            function turnDndOff() {
//                var number = "*074";
//                document.getElementById('webphoneframe').contentWindow.registerWebphone();
                // document.getElementById('webphoneframe').contentWindow.setPhoneOnline();
                document.getElementById('webphoneframe').contentWindow.registerWebphone();

                $("#dndButtonAnchor").removeClass("btn-success");
                $("#dndButtonAnchor").addClass("btn-danger");
                $("#dndButtonAnchor").html("Set DND ON");
                setSessionDNDState(0);
                isDNDOn == false;
            }
            
//            function activateOrDeactivateAgentMode(){
//                 $("#agentModeInputHidden").click();
//                 
//                if($("#agentModeInputHidden").prop('checked') == true){
//                    // should show agent mode
//
//                }else{
//                    // should hide agent mode
//
//                }
//            }

            $(document).ready(function () {
                $("#dndSwitch").change(function (event) {

                    if ($("#dndSwitch").prop('checked') == true) {
                        // DND is set to ON
                        turnDndOn();
                    } else {
                        // DND is set to OFF
                        turnDndOff();
                    }
                });
                $("#dndButtonAnchor").click(function (event) {
                    $("#dndSwitch").click();
                });
                
                $("#agentModeInputCheckboxHidden").change(function (event) {

                    if ($("#agentModeInputCheckboxHidden").prop('checked') == true) {
                        // should show agent mode
                        loadAgentView(); // This loads up the agent view at the bottom
                        turnDndOff();
                    } else {
                       // should hide agent mode
                        hideAgentView(); // clears the agent view at the bottom
                        turnDndOn();
                    }
                });
                
                
                $("#agentModeAnchor").click(function(event){
                    $("#agentModeInputCheckboxHidden").click();
                });
            });
            function setSessionDNDState(state) {
                $.ajax({
                    url: "<?= Url::to(['user/setdndsessionstate']) ?>",
                    type: 'GET',
                    data: {state: state},
                    success: function (data, textStatus, jqXHR) {
//                alert(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                alert(jqXHR.responseText);
                    }
                });
            }
        </script>

        <script>
            // for queued calls and ongoing calls
            var noQueuedCallsMessage = "<li class='list-group-item' id='noQueuedCallsMessage' style='border: none'>No Queued Calls</li>";
            var noOngoingCallsMessage = "<li class='list-group-item' id='noOngoingCallsMessage' style='border: none'>No Ongoing Calls</li>";
            var noQueuedCallsMessageDisplay = false;
            var noOngoingCallsMessageDisplay = false;
//            if (typeof (EventSource) !== "undefined") {
//                var source = new EventSource("<? = Url::to(['events/getcalls']) ?>");
//                source.onmessage = function (event) {
//
//                };
//            } else {
//                document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
//            }

            function getCallsDataAJAX() {
                $.ajax({
                    url: "<?= Url::to(['events/getcalls']) ?>",
                    type: 'POST',
                    success: function (data, textStatus, jqXHR) {
                        console.log("get calls success timestamp " + Date.now() + " : " + number);
                        number++;
                        if (data != "data:-1") {
//                            alert("New call received");
                            console.log("get calls data fetch success timestamp " + Date.now() + " : " + number);
                            listQueuedAndOngoingCalls(data);
                        } else {
                            // no queued or ongoing calls
                            clearQueuedCallsAndOngoingCalls();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("get calls fail timestamp " + Date.now() + " : " + number);
                        console.log("get calls fail timestamp " + jqXHR.responseText);
                        number++;
                    }
                });
            }

            function clearQueuedCallsAndOngoingCalls() {
                $("#queuedCallsList").empty();
                $("#queuedCallsCount").html(0);
                $("#ongoingCallsList").empty();
                $("#ongoingCallsCount").html(0);
                $("#queuedCallsList").append(noQueuedCallsMessage);
                $("#ongoingCallsList").append(noOngoingCallsMessage);
                noQueuedCallsMessageDisplay = true;
                noOngoingCallsMessageDisplay = true;
                clearCustomerData();
                clearOngoingCallAgentData();
                return true;
            }

            function listQueuedAndOngoingCalls(data) {
                console.log("Data for queued and ongoing calls = " + data + " : " + number);
                number++;
//                if (data !== 'no') {
                var callDataArray = $.parseJSON(data);
                var queuedCalls = callDataArray['queuedCalls'];
                var ongoingCalls = callDataArray['ongoingCalls'];
//                        alert("Queued = "+queuedCalls.length);
                if (queuedCalls.length > 0) {
                    // have queued calls
                    $("#queuedCallsList").empty();
                    noQueuedCallsMessageDisplay = false;
                    for (var x = 0; x < queuedCalls.length; x++) {
                        var queuedCall = "<li class='list-group-item' id='" + queuedCalls[x]['id'] + "'>" + queuedCalls[x]['number'] + "</li>";
                        $("#queuedCallsList").append(queuedCall);
                    }
                    $("#queuedCallsCount").html(queuedCalls.length);
                } else {
                    // no queued calls
//                        if (noQueuedCallsMessageDisplay == false) {
                    $("#queuedCallsList").empty();
                    $("#queuedCallsList").append(noQueuedCallsMessage);
                    $("#queuedCallsCount").html(0);
                    noQueuedCallsMessageDisplay = true;
//                        }
                }

//                        alert("Ongoing = " + ongoingCalls.length);
                if (ongoingCalls.length > 0) {
                    // have outgoing calls
                    $("#ongoingCallsList").empty();
                    noOngoingCallsMessageDisplay = false;
                    for (var y = 0; y < ongoingCalls.length; y++) {

                        var ongoingCall = "<li class='list-group-item onlineCalls' id='" + ongoingCalls[y]['ext'] + "'><a onclick='setCallRecordData(this)' id='" + ongoingCalls[y]['ext'] + "' href='#'>" + ongoingCalls[y]['cidnum'] + "</a><br><span class='callDuration'></span><span id='callDurationSeconds' style='display: none'>" + ongoingCalls[y]['duration'] + "</span></li>";
                        $("#ongoingCallsList").append(ongoingCall);

                    }
                    $("#ongoingCallsCount").html(ongoingCalls.length);

                } else {
                    // no outgoing calls
//                        if (noOngoingCallsMessageDisplay == false) {
                    $("#ongoingCallsList").empty();
                    $("#ongoingCallsList").append(noOngoingCallsMessage);
                    $("#ongoingCallsCount").html(0);
                    clearCustomerData();
                    clearOngoingCallAgentData();
                    noOngoingCallsMessageDisplay = true;
//                        }
                }


//                }
            }

        </script>
        <script>
            // Common Interval to invoke mandatory AJAX functions
            $(document).ready(function () {

                setInterval(function () {
                    getOnlineUsersAJAX();

                }, 3000);

                setInterval(function () {
                    getHelpRequestsAJAX()();

                }, 2000);

                setInterval(function () {
                    getCallsDataAJAX()();

                }, 2000);
            });
        </script>

        <script>
            // for online agents
            var number = 0;
            var isWebphoneDisabled = false;
//            if (typeof (EventSource) !== "undefined") {
//                var source = new EventSource("<? = Url::to(['events/liveagents']) ?>");
//                source.onmessage = function (event) {
// 
//                };
//            } else {
//                document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
//            }
//            
//            

            function getOnlineUsersAJAX() {

                $.ajax({
                    url: "<?= Url::to(['events/liveagents']) ?>",
                    type: 'POST',
                    success: function (data, textStatus, jqXHR) {
                        console.log("online agents success timestamp " + Date.now() + " : " + number);
                        number++;
                        showOnlineAgents(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("online agents fail timestamp " + Date.now() + " : " + number);
                        console.log("online agents fail timestamp " + jqXHR.responseText);
                        number++;
                    }
                });
            }

            function showOnlineAgents(data) {
                console.log('ShowOnlineAgents(): ' + data);
                var events_data_array = $.parseJSON(data);
                var agents = events_data_array[0];
                var lateArrivalIds = events_data_array[1];
                var myExtensionRingingInformation = events_data_array[3];

                if (data !== 'no') {
                    $('#agentListGroup').empty();
                    var onlineAgentsCount = 0;
                    for (var index = 0; index < agents.length; index++) {

                        var agentName = (agents[index][2].length > 5 ? agents[index][2] + ".." : agents[index][2]);
                        var agentLoggedInTime = agents[index][5];
                        var agentExtension = agents[index][4];
                        var isagentBusy = agents[index][6];
                        if (lateArrivalIds.indexOf(agents[index][0]) != -1) {
                            var html1 = "<div  id='" + agents[index][0] + "' onclick='ignoreLateArrival(this.id)' style='background-color:#ff0000' class='list-group-item'><span class='badge'>";
                        } else {
                            var html1 = "<div class='list-group-item'><span class='badge'>";
                        }



                        if (agents[index][3] == 1 && isagentBusy == 0) {
                            // agent is online and available
                            var html2 = "<a style='cursor:pointer;text-decoration: none;' onclick='initiatetransfer(" + agentExtension + ")' class='glyphicon glyphicon-transfer' title='Transfer Call'>&nbsp</a><a style='cursor:pointer;text-decoration: none;' onclick='initiateConference(" + agentExtension + ")' class='glyphicon glyphicon-refresh' title='Conference Call'>&nbsp</a><a style='color:green;cursor:default;text-decoration: none;' class='fa fa-circle'></a></span>" + agentName + "<br><b class='onlineTime'>" + agentLoggedInTime + "</b>";
                            onlineAgentsCount++;
                        } else if (agents[index][3] == 1 && isagentBusy == 1) {
                            // agent is online but busy
                            var html2 = "<a style='color:red;cursor:default;text-decoration: none;' class='fa fa-circle'></a></span>" + agentName + "<br><b class='onlineTime'>" + agentLoggedInTime + "</b>";
                            onlineAgentsCount++;

                        } else if (agents[index][3] == 0) {
                            var html2 = "<a style='color:yellow;cursor:default;text-decoration: none;' class='fa fa-circle'></a></span>" + agentName + "";
                        }
                        var html3 = "</div>";
                        var agent = html1 + html2 + html3;
                        $('#agentListGroup').append(agent);
                        $("#onlineAgentsCounter").html(onlineAgentsCount);
                    }
                    
                    if(myExtensionRingingInformation['webphoneDisabled'] == 'Enabled' && isWebphoneDisabled == true){
                        // Enable the webphone
                        enableWebphone();
                        
                    }else if(myExtensionRingingInformation['webphoneDisabled'] == 'Disabled' && isWebphoneDisabled == false){
                        // disable the webphone
                        disableWebphone();
                        
                    }
                    
                }
            }

        </script>
        <script>
            function disableWebphone(){
                  $("#webphoneSectionDiv").css("display", "none");
                  $("#webphoneframe").attr("src", "");
                  $("#webphoneDisabledMessageDiv").css("display", "block");
                  isWebphoneDisabled = true;
                  console.log("hard phone support : disabled webphone");
            }   

            function enableWebphone(){
                  $("#webphoneDisabledMessageDiv").css("display", "none");
                  $("#webphoneSectionDiv").css("display", "block");
                  $("#webphoneframe").attr("src", "softphone/softphone.html");
                  isWebphoneDisabled = false;
                  console.log("hard phone support : enabled webphone");
            }

            <?php 
                if($webphoneParams['phoneState'] == 'Enabled'){
                    // webphone is enabled
                ?>
                     $(document).ready(function(){
                         enableWebphone();
                     });               
                <?php
                }else{
                    // webphone is disabled
                ?>
                    $(document).ready(function(){
                        disableWebphone();
                    });
                <?php
                }
            ?>      


</script>   

    </head>
    <body style="font-family: 'Lato' , sans-serif;">
        <div class="row form-group">
            <div class="col-xs-12" style="padding-right: 0px">
                <a href="#" class="btn btn-primary"><span class="glyphicon glyphicon-dashboard"></span> <?=Yii::$app->session->get('full_name')?> : <?=Yii::$app->session->get('voip')?></a>                
                <a onclick="window.open('<?= Url::to(['reports/supervisorreports']) ?>', '_blank');" class="btn btn-danger"><span class="glyphicon glyphicon-stats"></span> Statistics</a>
                <?php if (Yii::$app->session->get('user_role') != '1') { ?>
                <a onclick="window.open('<?= Url::to(['supevisor/manage']) ?>', '_blank')" class="btn btn-primary btn-success"><span class="glyphicon glyphicon-user"></span> Manage Users</a>
            <?php }else{ ?>
                <span class="dropdown">
                  <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Manage Resources
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="margin-top: 7%">
                    <li><a href="<?= Url::to(['supevisor/manage']); ?>"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;Manage Users</a></li>
                    <li><a href="<?= Url::to(['reports/searchmissingaudio']); ?>"><span class="glyphicon glyphicon-phone-alt"></span>&nbsp;&nbsp;Search Missing CDRs </a></li>
                    <li><a href="<?= Url::to(['supevisor/manageemail']); ?>"><span class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;Email Contacts </a></li>
                </ul>
            </span>      
            <?php } ?>
                <!--                <button class="btn btn-warning">
                                    <span class="glyphicon glyphicon-envelope"></span>
                                </button>
                                <span class="badge badge-notify">3</span>
                                <button class="btn btn-info">
                                    <span class="glyphicon glyphicon-comment"></span>
                                </button>
                                <span class="badge badge-notify">5</span>
                                <input style="width: 300px;font-size: 110%;" class="input-lg" id="customerPhoneNumberSearchInput" name="customerPhoneNumberSearchInput" placeholder="Search customer by phone number">-->

            </div>
        </div>
        <div class="row" style="font-family: 'Lato' , sans-serif;font-weight: bolder">
            <div class="col-xs-2" style="">
                <!-- Queued calls panel -->
                <div class="panel-group">
                    <div class="panel panel-default" style="border: 1px solid #10297d">
                        <div class="panel-heading"  style="height: 40px">
                            <!--<h4 class="panel-title">-->
                            <div class="row">
                                <div class="col-xs-8 headertitles" style="">Queued Calls</div>
                                <div class="col-xs-4" style="text-align: left"><span class="btn btn-xs btn-warning" style="margin-top: 0%" id="queuedCallsCount">0</span></div>
                            </div>
                            <!--</h4>-->
                        </div>
                        <div id="collapse1" class="" style="height: 500px;overflow-y: scroll">
                            <ul class="list-group" id="queuedCallsList">

                            </ul>
                        </div>                        
                    </div>
                </div>
                <!-- End of queued calls panel -->  
            </div>
            <div class="col-xs-2" style="padding-left: 0%">
                <div class="panel-group">
                    <!-- Ongoing calls panel -->
                    <div class="panel panel-default" style="border: 1px solid #10297d">
                        <div class="panel-heading"  style="height: 40px">
                            <!--<h4 class="panel-title">-->
                            <div class="row">
                                <div class="col-xs-8 headertitles" style="">Ongoing Calls</div>
                                <div class="col-xs-4" style="text-align: left"><span class="btn btn-xs btn-warning" style="margin-top: 0%" id="ongoingCallsCount">0</span></div>
                            </div>
                            <!--</h4>-->
                        </div>
                        <div id="collapse1" class="" style="height: 278px;overflow-y: scroll">
                            <ul class="list-group" id="ongoingCallsList">
                                <!--<? php for ($i = 0; $i < count($liveCalls); $i++) { ?>-->
<!--                                    <li class="list-group-item onlineCalls" id="<? = $liveCalls[$i]['id'] ?>"><a id="<? = $liveCalls[$i]['id'] ?>" href="#"><? = $liveCalls[$i]['customer_name'] ?></a>
                                        <br>
                                        <span class="callDuration"></span>
                                        <span id="callDurationSeconds" style="display: none">60</span>
                                    </li>-->
                                <!--<? php } ?>-->
                            </ul>
                        </div>                        
                    </div>
                    <!-- End of ongoing calls panel -->
                    <!-- Online agents panel -->
                    <div class="panel panel-default" style="border: 1px solid #10297d">
                        <div class="panel-heading"  style="height: 40px">
                            <h4 class="panel-title">
                                <div class="row">
                                    <div class="col-xs-4 headertitles">Agents</div>
                                    <div class="col-xs-8" style="text-align: right"><span id="onlineAgentsCounter" class="btn btn-xs btn-warning" style="margin-top: 0%">0</span></div>
                                </div>
                            </h4>
                        </div>
                        <div id="collapse1" class="" style="height: 180px;overflow-y: scroll">
                            <ul id="agentListGroup" class="list-group" style="">

                            </ul>
                        </div>                        
                    </div>
                    <!-- End of online agents panel -->
                </div>
            </div>
            <div class="col-xs-5" style="padding-left: 0px">
                <!-- Ongoing call description panel -->
                <div class="panel profile-panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                    <div class="panel-heading" style="height: 40px">
                        <div class="col-md-9">
                            <h3 id="idOngoingCallCustomerName" class="panel-title"></h3>                            
                        </div>
                        <div class="col-md-3" align="right">
                            <img style="height: 40px;padding-bottom: 15px" src="hnb_images/ongoing-call.png">                                                    
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <!--                            <div class="col-md-3 col-lg-3 " align="center">
                                                            <div class="row">
                                                                <img alt="User Pic" src="images/dummy_profile.jpg" id="userProfilePic" class="img-circle img-responsive">                                     
                                                            </div><br>
                                                        </div>-->
                            <div class=" col-md-12 col-lg-12" id="ongoingCallInformation">
                                <div class="row">
                                    <div class="col-xs-6">
                                        Customer NIC
                                    </div>    
                                    <div class="col-xs-6">
                                        <span id="idOngoingCallCustomerNIC"></span>
                                    </div>    
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-6">
                                        Home Address
                                    </div>    
                                    <div class="col-xs-6">
                                        <span id="idOngoingCallCustomerAddress"></span>
                                    </div>    
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-6">
                                        Phone Number
                                    </div>    
                                    <div class="col-xs-6">
                                        <span id="idOngoingCallCustomerContact"></span>
                                    </div>    
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-6">
                                        Policy Number
                                    </div>    
                                    <div class="col-xs-6">
                                        <span id="idOngoingCallCustomerPolicyNo"></span>
                                    </div>    
                                </div>
                                <hr>
                            </div>
                        </div>
                        <style>
                            #ongoingCallInformation > hr{
                                margin-top: 3%;
                                margin-bottom: 3%;
                            } 
                        </style>
                        <div class="row">                            
                            <div class="col-xs-3" align="middle">
                                <span class="glyphicon glyphicon-headphones" style="font-size: 150%;font-weight: bold">Agent</span>
                                <br>
                                <span id="ongoingCallAgentName"></span>                                                                            
                            </div>
                            <div class="col-xs-3">
                                <a href="#" class="btn btn-warning btn-block" onclick="call_dial_code('*90')">Listen</a>                                        
                            </div>
                            <div class="col-xs-3">
                                <a href="#" class="btn btn-info btn-block" onclick="call_dial_code('*91')">Whisper</a>
                            </div>
                            <div class="col-xs-3">
                                <a href="#" class="btn btn-primary btn-block" onclick="call_dial_code('*92')">Join</a>
                            </div>                            
                            <input type="hidden" id="agent_extension" value="" />
                            <!--<input type="hidden" id="agent_extensionBackup" value="" />-->
                            <input type="hidden" id="agent_id" value="" />
                        </div>
                    </div>
                </div>
                <!-- End of ongoing call description panel -->
                <div class="row" style="padding-top: 0px">
                    <!-- Agent requests panel -->
                    <div class="col-xs-4" style="padding-right: 0px;">
                        <div style="border: 1px solid #10297d;border-radius: 5px">
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="#" class="list-group-item active">
                                        <div class="row">
                                            <div class="col-xs-9 headertitles">Agent Requests</div>
                                            <div class="col-xs-3 col-xs-pull-1" style="text-align: left;"><span id="agentRequestsCounter" class="btn btn-xs btn-warning" style="margin-top: 0%">0</span></div>
                                        </div>
                                    </a>    
                                    <div id="agentRequestsDisplayDiv" class="list-group" style="height: 155px;overflow-y: scroll;font-size: 90%; background-color: white">
                                        <div class='list-group-item' style='border: #fff' id='noAgentRequestsDiv'><span style='font-weight: bold'>No agent break requests</span></div>
                                    </div>
                                </div>
                            </div>
                        </div><br>                
                    </div>
                    <!-- End of agent requests panel -->
                    <!-- Help requests panel -->
                    <div class="col-xs-4" style="padding-right: 0px">
                        <div style="border: 1px solid #10297d;border-radius: 5px">
                            <div class="row">
                                <div class="col-xs-12">
                                    <a href="#" class="list-group-item active">
                                        <div class="row">
                                            <div class="col-xs-9 headertitles">Help Requests</div>
                                            <div class="col-xs-3 col-xs-pull-1" style="text-align: left;"><span id="helpRequestsCounter" class="btn btn-xs btn-warning" style="margin-top: 0%">0</span></div>
                                        </div>
                                    </a>                            
                                    <div id="helpRequestsDisplayDiv" class="list-group" style="height: 155px;overflow-y: scroll;font-size: 90%; background-color: white">
                                        <div class="list-group-item" style="border: #fff" id="noHelpRequestsDiv">
                                            <span style="font-weight: bold">No help requests</span>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                    <!-- End of help requests panel -->
                    <!-- Contacts panel -->
                    <div class="col-xs-4">
                        <div style="border: 1px solid #10297d;border-radius: 5px">
                            <div class="row">
                                <div class="col-xs-12" style="height: 225px;">
                                    <a href="#" class="list-group-item active" style="height:35px;">
                                        <div class="row">
                                            <div class="col-xs-4 headertitles">Contacts</div>
                                            <div class="col-xs-8" style="text-align: right"><span class="btn btn-xs btn-success" id="btnAddNewContact"  style="margin-top: 0%; margin-top: -8px;">Add</span></div>
                                        </div>
                                    </a>                            

                                    <div id="contactsDisplayDiv" class="list-group" style="height: 155px;overflow-y: scroll;font-size: 90%; background-color: white; margin-bottom:2px">
                                        <div class="list-group-item" style="border: #fff" id="noContactsDiv">
                                            <span style="font-weight: bold">No Contacts</span>
                                        </div>

                                    </div>
                                    <input type="text" id="idContactTxt" style="width:130px; margin:2px" placeholder="Contact Name ..." onkeypress="searchContact(event)">
                                </div>
                            </div>
                        </div>  
                    </div>
                    <!-- End of contacts panel -->
                </div>
            </div>
            <div id="webphoneDisabledMessageDiv" class="col-xs-3"   style="padding-left: 0%;padding-right: 0%; font-size: 2em; display: none">
                <!-- webphone disabled message -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="alert alert-danger">
                            <strong>Your softphone has been disabled by your Administrator!</strong> 
                            <!--<p>Please connect a hard phone with extension set to <? =Yii::$app->session->get('voip')?> to continue your work.</p>-->
                        </div>
                        <div class="alert alert-info">
                            <strong>Please use a hard phone with extension set to <?=Yii::$app->session->get('voip')?> to continue your work. Make sure to put DND ON</strong> 
                        </div>
                    </div>
                </div>
                <!-- End of webphone disabled message -->
            </div>
            <!-- softphone and DND button section -->
            <div id="webphoneSectionDiv" class="col-xs-3" style="padding-left: 0%;padding-right: 0%; display: block">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="phone" style="">
                            <a class="btn btn-block btn-primary" style="margin-top: 0%" onclick="document.getElementById('webphoneframe').src = 'softphone/softphone.html'">Refresh Phone</a>
                            <iframe frameborder="0" width="291" height="460" src="softphone/softphone.html" name="wphone" id="webphoneframe"></iframe>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <!--<input style="display: none" type="checkbox" name="dndSwitch" data-toggle="toggle" id="dndSwitch">-->
                        <!--<a id="dndButtonAnchor" class="btn btn-block btn-danger">Set DND ON</a>-->
                        <a id="agentModeAnchor" class="btn btn-block btn-info">Activate Agent Mode</a>
                        <input style="display: none" type="checkbox" name="agentModeInputCheckboxHidden" data-toggle="toggle" id="agentModeInputCheckboxHidden">
                        <?php
                        if (Yii::$app->session->get('dnd') == TRUE) {
                            // DND is ON
                            ?>
                            <input style="display: none" type="checkbox" name="dndSwitch" data-toggle="toggle" id="dndSwitch" checked="">
                            <a id="dndButtonAnchor" class="btn btn-block btn-success">Set DND OFF</a>
                            <?php
                        } else {
                            // DND is OFF
                            ?>
                            <input style="display: none" type="checkbox" name="dndSwitch" data-toggle="toggle" id="dndSwitch">
                            <a id="dndButtonAnchor" class="btn btn-block btn-danger">Set DND ON</a>
                            <?php
                        }
                        ?>

                    </div>
                </div>
            </div>
            <!-- End of softphone and DND button section -->
        </div>
        <script>
            $("#btnAddNewContact").click(function () {
                $('#idContactNumber').val('');
                $('#idContactName').val('');
                $('#addCordModal').modal({
                    show: 'true'
                });
            });
            function loadAllContacts() {

                $.ajax({
                    url: "<?= Url::to(['supevisor/refreshcontacts']) ?>",
                    type: 'POST',
                    // data: {contact_number: contactNumber, contact_name:contactName },
                    success: function (data, textStatus, jqXHR) {
                        addNewContactToList(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Error" + jqXHR.rex);
                    }
                });
            }

            function addNewContactToList(data) {
                console.log('+++ --- addNewContactToList(data) called');
                var numbers = $.parseJSON(data);
                $('#contactsDisplayDiv').empty();
                for (var index = 0; index < numbers.length; index++) {
                    var temp = [numbers[index]['name'], numbers[index]['number']];
                    contactNamesArray.push(temp);
                    var html1 = "<div class='list-group-item' id='" + numbers[index]['number'] + "'><span class='badge' style='float: initial;'>";
                    // var agentName = (agents[index][1].length > 4 ? agents[index][1] + ".." : agents[index][1]);
                    var agentName = numbers[index]['name'];
                    // var agentLoggedInTime = agents[index][5];
                    // if (agents[index][3] == 1) {
                    var html2 = "<a style='cursor:pointer;text-decoration: none;' onclick='initiateContactCall(" + numbers[index]['number'] + ")' class='glyphicon glyphicon-earphone' title='Initiate Call'>&nbsp</a><a style='cursor:pointer;text-decoration: none;' onclick='initiateContacttransfer(" + numbers[index]['number'] + ")' class='glyphicon glyphicon-transfer' title='Transfer Call'>&nbsp</a><a style='cursor:pointer; text-decoration: none;' onclick='initiateContactConference(" + numbers[index]['number'] + ")' class='glyphicon glyphicon-refresh' title='Conference Call'>&nbsp</a><a style='cursor:pointer;text-decoration: none; color: red;' onclick='deleteContact(" + numbers[index]['id'] + ")' class='glyphicon glyphicon-remove' title='Delete Contact'>&nbsp</a></span>" + agentName + "";
                    // } else if (agents[index][3] == 0) {
                    // var html2 = "<i class='fa fa-circle' aria-hidden='true' style='color:yellow'></i></span>" + agentName + "";
                    // }

                    var html3 = "</div>";

                    var contactEntry = html1 + html2 + html3;
                    $('#contactsDisplayDiv').append(contactEntry);

                }
            }
            function searchContact(event) {
                console.log('+++ --- searchContact() Called');
                if (event.keyCode == 13) {
                    console.log('+++ --- Enter press captured');
                    var value = $('#idContactTxt').val();
                    searchContacts(value);
                }

            }

            function searchContacts(val) {
                console.log('+++ --- searchContacts() Called');

                $.ajax({
                    url: "<?= Url::to(['supevisor/searchcontact']) ?>",
                    type: 'POST',
                    data: {q: val},
                    success: function (data, textStatus, jqXHR) {
                        console.log('+++ --- ajax success called');
                        addNewContactToList(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert("Error" + jqXHR.rex);
                        console.log('+++ --- ajax error block called with ' + jqXHR.rex);
                    }
                });


            }
            function deleteContact(id) {
                swal({
                    title: 'Are you sure?',
                    text: "Do you want to remove this contact?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: 'grey',
                    showLoaderOnConfirm: true,
                    confirmButtonText: 'Yes, delete it!'
                }).then(function () {

                    $.ajax({
                        url: "<?= Url::to(['supevisor/delete_contact']) ?>",
                        type: 'POST',
                        data: {id: id},
                        success: function (data, textStatus, jqXHR) {
                            loadAllContacts();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("Error" + jqXHR.responseText);
                        }
                    });
                });

            }


        </script>
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- Add new Contact block Start  -->

        <!-- Agent mode section -->
        <div class="row">
            <div class="col-xs-12">
                <div id="agentDashboardDiv">

                </div>
            </div>
        </div>
        <!-- end of Agent mode section -->
        <!-- Model block -->
        <div id="addCordModal" class="modal fade" role="dialog">
            <div class="modal-dialog">        
                <div class="modal-content">
                    <div class="modal-header">
                        <h3<u><b>Add new Contact</b></u></h3>
                    </div>
                    <div class="modal-body form-group" style="">
                        <input type="number" class="form-control" placeholder="Number" id="idContactNumber"> <br>
                        <input type="text" class="form-control" placeholder="Full name" id="idContactName"> <br>                                                          
                    </div>
                    <div class="modal-footer">                
                        <!-- <button id="saveContactButton" type="button" class="btn btn-success">Save</button> -->
                        <a id="saveContactButton" href="#" class="btn btn-default">Save</a>
                        <button   class="btn btn-default" id="closeBtn" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div> 
        <script type="text/javascript">
            $("#saveContactButton").click(function () {
                var contactNumber = $('#idContactNumber').val();
                var contactName = $('#idContactName').val();
                if (contactNumber != '' && contactName != '') {

                    $.ajax({
                        url: "<?= Url::to(['supevisor/savecontact']) ?>",
                        type: 'POST',
                        data: {contact_number: contactNumber, contact_name: contactName},
                        success: function (data, textStatus, jqXHR) {
                            $('#addCordModal').modal('toggle');
                            loadAllContacts();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("Error" + jqXHR.rex);
                        }
                    });

                }
            });
        </script> 
        <!-- Model block end                  -->
        <script>
            // supervisor agent mode functions
            function loadAgentView() {
                $("#agentModeAnchor").removeClass("btn-info");
                $("#agentModeAnchor").addClass("btn-warning");
                $("#agentModeAnchor").html("Activate Supervisor Mode");
                $('html,body').animate({
                    scrollTop: $("#agentDashboardDiv").offset().top},
                        'slow');
                $("#agentDashboardDiv").load("<?= Url::to(['agent/callerinformation']) ?>", "agentmode=true");
            }

            function hideAgentView() {
                $("#agentModeAnchor").removeClass("btn-warning");
                $("#agentModeAnchor").addClass("btn-info");
                $("#agentModeAnchor").html("Activate Agent Mode");
                $("#agentDashboardDiv").empty();
            }
        </script>
        <script>

            function setCallRecordData(anchor) {
                var answeringExtension = anchor.id;
                var customerCallingNumber = anchor.innerHTML;
                console.log("Answering Extension will be " + answeringExtension + " and Customer Number is: " + customerCallingNumber);
                $("#agent_extension").val(answeringExtension);
//                $("#agent_extensionBackup").val(answeringExtension);                
                getAgentInfoAJAX(answeringExtension);
                getCustomerInfo(customerCallingNumber);
            }

            function getAgentInfoAJAX(agentVoipExt) {
                $.ajax({
                    type: "POST",
                    url: "<?= Url::to(['user/getagentinfofromvoipext']) ?>",
                    data: {extension: agentVoipExt},
                    success: function (data) {
//                        alert(data);
//                        clearOngoingCallAgentData();
                        if (data != 0) {
                            var agentDataArray = $.parseJSON(data);
//                            alert(agentDataArray['id']);
                            $("#agent_id").val(agentDataArray['id']);
                            $("#ongoingCallAgentName").html(agentDataArray['fullname']);
                        } else {
                            // alert("No user for extension " + agentVoipExt);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                        alert(jqXHR.responseText);
                    }
                });
            }


            function getCustomerInfo(number) {
//                number = '771980774'; // for testing only
                $.ajax({
                    url: "<?= Url::to(['agent/getcustomerinformation']) ?>",
                    type: 'POST',
                    data: {number: number},
                    success: function (data, textStatus, jqXHR) {
//                    alert(data);
                        var jsonDecodedResponse = JSON.parse(data);
                        clearCustomerData();
                        if (jsonDecodedResponse['customer_policies'] != 0) {
                            // has customer data
                            $("#idOngoingCallCustomerName").text(jsonDecodedResponse['customer_insuered_name']);
                            $("#idOngoingCallCustomerNIC").text(jsonDecodedResponse['customer_NIC']);
                            $("#idOngoingCallCustomerAddress").text(jsonDecodedResponse['customer_address']);
                            $("#idOngoingCallCustomerContact").text(jsonDecodedResponse['customer_contact']);
                            $("#idOngoingCallCustomerIsVIP").text(jsonDecodedResponse['customer_isVIP']);
                            $("#idOngoingCallCustomerPolicyNo").text(jsonDecodedResponse['customer_policyNumber']);

                        } else {
                            // no customer data
                            $("#idOngoingCallCustomerName").text("No Customer Data");
                        }

//                jsonDecodedResponse['callRecords'][0]['']
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
//                    $("#ongoingCallImg").css("display", "block");
                        $("#loadingCallDataIndicator").css("display", "none");
                        $("#callDataStatusLabel").css("display", "block");
                        setTimeout(function () {
                            $("#callDataStatusLabel").css("display", "none");
                        }, 3000);
                    }

                });
            }

            function clearCustomerData() {
                $("#idOngoingCallCustomerName").text("");
                $("#idOngoingCallCustomerNIC").text("");
                $("#idOngoingCallCustomerAddress").text("");
                $("#idOngoingCallCustomerIsVIP").text("");
                $("#idOngoingCallCustomerPolicyNo").text("");
                $("#idOngoingCallCustomerContact").text("");
            }

            function clearOngoingCallAgentData() {
                $("#ongoingCallAgentName").text("");
                $("#agent_extension").val("");
//                $("#agent_extensionBackup").val("");
                $("#agent_id").val("");
            }


//            $(document).ready(function () {


//                $('.onlineCalls').click(function () {
//                    alert($(this).html());
//                var customerPhoneNumber = $(this).html();
//                var url = "<? = Url::to(['supevisor/getuser']) ?>";
//                $.ajax({
//                    type: "POST",
//                    url: url,
//                    data: {q: customerPhoneNumber},
//                    success: function (data)
//                    {
//                        var customerData = JSON.parse(data);
//                        $('#idCustomerName').text(customerData['customer_name']);
//                        $('#idCustomerEmail').text(customerData['email']);
//                        $('#idCustomerContact').text(customerData['contact']);
//                        $('#idCustomerAddress').text(customerData['address']);
//                        $('#idCustomerDOB').text(customerData['dob']);
//                        $('#idCustomerGender').text(customerData['gender']);
//                        if (customerData['gender'] == "male") {
//                            $('#userProfilePic').prop("src", "images/dummy_profile_male.jpg");
//                        } else {
//                            $('#userProfilePic').prop("src", "images/dummy_profile_female.jpg");
//                        }
//                        $('.profile-panel').show();
//                    },
//                    error: function (xhr, ajaxOptions, thrownError) {
////                        alert(xhr.responseText);
////                        alert(xhr.status);
////                        alert(thrownError);
////                        alert(xhr.responseText);
//                    }
//                });
//                });
//            });
        </script>

        <script>
            /**
             * Function triggers when listen,whisper,join button clicked.
             */
            function  call_dial_code(dial_code) {

                var agent_ext = document.getElementById('agent_extension').value; //get the value of hidden input firld agent_extension.
//                alert('Came here' + $("#agent_extensionBackup").val());
//                var agent_id = document.getElementById('agent_Id').value;//get the value of hidden input firld agent_id.
                var agent_id = document.getElementById('agent_id').value;
                console.log('Dial code function, the agent extension = ' + agent_ext + ' and dial code = ' + dial_code);
//                alert(agent_ext + ' : ' + agent_id);

                if (dial_code == '*91') {
                    callAjaxForWhisperOrJoin(1, agent_id);
                }
                if (dial_code == '*92') {
                    callAjaxForWhisperOrJoin(2, agent_id);
                }
                agent_ext = dial_code + agent_ext;
                document.getElementById('webphoneframe').contentWindow.click_btn_call(agent_ext);
                //webphoneframe is an iframe.That iframe contains softphone.html. This will call a javascript function click_btn_call in softphone.html                
            }
        </script>

        <script>
            function callAjaxForWhisperOrJoin(dial_type, agent_id) {
                var dial_type = dial_type;
                var agent_id = agent_id;
                var url = "<?= Url::to(['supevisor/notifyagent']) ?>";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {dial_type: dial_type, agent_id: agent_id},
                    success: function (data)
                    {
//                        alert(data);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
//                        alert(xhr.responseText);
//                        alert(xhr.status);
//                        alert(thrownError);
//                        alert(xhr.responseText);
                    }
                });
            }
        </script>
        <script>
            // for help requests functionality
//            if (typeof (EventSource) !== "undefined") {
//                var source = new EventSource("<? = Url::to(['events/requests']) ?>");
//                source.onmessage = function (event) {
//
//
//                };
//            } else {
//                document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
//            }


            function getHelpRequestsAJAX() {
                $.ajax({
                    url: "<?= Url::to(['events/requests']) ?>",
                    type: 'POST',
                    success: function (data, textStatus, jqXHR) {
                        console.log("help requests success timestamp " + Date.now() + " : " + number);
                        number++;
                        if (data != "data:-1") {
                            console.log("help requests data fetch timestamp " + Date.now() + " : " + number);
                            getHelpRequests(data);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("help requests fail timestamp " + Date.now() + " : " + number);
                        console.log("help requests fail timestamp " + jqXHR.responseText);
                        number++;
                    }
                });
            }

//            function clearHelpAndAgentRequests() {
//                $("#helpRequestsDisplayDiv").html("");
//                $("#agentRequestsDisplayDiv").html("");
//                $("#agentRequestsDisplayDiv").html("");
//            }


            function getHelpRequests(data) {
                var eventData = data;
                var arr = eventData.split(',');
                if (arr[0] != -1) {
                    if (arr[0] != '0')
                        showNewHelpRequest(arr[0], arr[1], arr[2]);
                    if (arr[3] != '0')
                        showNewAgentRequest(arr[3], arr[4], arr[5]);
                } else {

                }
            }
            // Sync break requests here
            function setAgentRequestsOnload() {
//                alert();
                for (var x = 0; x < onloadAgentRequests.length; x++) {

                    showNewAgentRequest(onloadAgentRequests[x][1], onloadAgentRequests[x][2], onloadAgentRequests[x][0]);

                }
            }



            function showNoHelpRequestsMessage() {
                var noHelpMessageDiv = "<div class='list-group-item' style='border: #fff' id='noHelpRequestsDiv'><span style='font-weight: bold'>No help requests</span> </div>";
                $("#helpRequestsDisplayDiv").html("");
                $("#helpRequestsDisplayDiv").append(noHelpMessageDiv);
                $("#helpRequestsCounter").html(0);
            }

            function showNewHelpRequest(id, agentName, dateTime) {
//                var newHelpRequestDiv = "<div class='list-group-item helpRequests'><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: blueviolet'>Need a help.</span> "+dateTime+"</div>";
                var newHelpRequestDiv = "<div class='list-group-item helpRequests' id='h" + agentName + "'><span  style='font-weight: bold'>" + agentName + "</span><span class='badge'> <a style='color: red' href='#' id='" + agentName + "' onclick='removeAgentHelpNotification(this.id)' class='glyphicon glyphicon-remove'></a></span><br><span style='color: blueviolet'>Need a help.</span> <br> <span style='font-size: 80%; color: #10297d'>" + dateTime + "</span></div>";
                if ($(".helpRequests").length > 0) {
                    $("#helpRequestsDisplayDiv").prepend(newHelpRequestDiv);
                    incrementHelpRequestsCount();
                } else {
                    $("#helpRequestsDisplayDiv").html("");
                    $("#helpRequestsDisplayDiv").prepend(newHelpRequestDiv);
                    incrementHelpRequestsCount();
                }
            }
            function showNewAgentRequest(id, agentName, request) {

                var newAgentRequestDiv = '';
                if (request == 'lunch') {

                    newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><a style='color: green' id='" + id + "'  class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,0)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: blueviolet'>Lunch break</span> </div>";
//                    console.log(newAgentRequestDiv);
                } else if (request == 'short') {
                    newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><a style='color: green'  id='" + id + "' class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,0)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: orange'>Short break</span> </div>";
                } else if (request == 'sick') {
                    newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><input type='number' style='width:45px;color: black;' id='T" + id + "'  maxlength='1'><a style='color: green'  id='" + id + "' class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,1)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: #42b0f4'>Sick break</span> </div>";
                } else if (request == 'other') {
                    newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><a style='color: green'  id='" + id + "'   class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,0)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: #000cc'>Other break</span> </div>";
                } else if (request == 'meeting') {
                    newAgentRequestDiv = "<div class='list-group-item agentRequests' id='e" + id + "'><span class='badge'><input type='number' style='width:45px;color: black;' id='T" + id + "'  maxlength='1'><a style='color: green'  id='" + id + "'   class='glyphicon glyphicon-ok acceptAgentRequest' onclick='acceptClick(this.id,1)' href='#'></a>&nbsp;&nbsp;&nbsp;<a style='color: red' href='#' id='" + id + "' onclick='deniedClick(this.id)' class='glyphicon glyphicon-remove'></a></span><span  style='font-weight: bold'>" + agentName + "</span><br><span style='color: #f44180'>Meeting / training break</span> </div>";
                }

                if ($(".agentRequests").length > 0) {

                    $("#agentRequestsDisplayDiv").prepend(newAgentRequestDiv);
                    incrementAgentRequestsCount();

                } else {

                    $("#agentRequestsDisplayDiv").html("");
                    $("#agentRequestsDisplayDiv").prepend(newAgentRequestDiv);
                    incrementAgentRequestsCount();
                }
            }
            function showNoAgentRequestsMessage() {
                var noAgentMessageDiv = "<div class='list-group-item' style='border: #fff' id='noAgentRequestsDiv'><span style='font - weight: bold'>No agent break requests</span></div>";
                $("#agentRequestsDisplayDiv").html("");
                $("#agentRequestsDisplayDiv").append(noAgentMessageDiv);
                $("#agentRequestsCounter").html(0);
            }

            function decrementHelpRequestsCount() {
                var helpRequestsCount = $("#helpRequestsCounter").html();
                helpRequestsCount--;
                $("#helpRequestsCounter").html(helpRequestsCount);
                if (helpRequestsCount == 0) {
                    showNoHelpRequestsMessage();
                }
            }

            function incrementHelpRequestsCount() {
                var helpRequestsCount = $("#helpRequestsCounter").html();
                helpRequestsCount++;
                $("#helpRequestsCounter").html(helpRequestsCount);
            }

            function incrementAgentRequestsCount() {
                var requestsCount = $("#agentRequestsCounter").html();
                requestsCount++;
                $("#agentRequestsCounter").html(requestsCount);
            }

            function decrementAgentRequestsCount() {
                var requestsCount = $("#agentRequestsCounter").html();
                requestsCount--;
                $("#agentRequestsCounter").html(requestsCount);
                if (requestsCount == 0) {
                    showNoAgentRequestsMessage();
                }
            }

// This function will be called when supervisor accept user request
            function acceptClick(elementId, isSick) {
                agentRequestResponse(elementId, 'approved', isSick);
            }
// This function will be called when supervisor denied user request
            function deniedClick(elementId) {
                agentRequestResponse(elementId, 'denied', '0');
//                removeAgentRequestNotification(elementId);
//                alert(elementId);

            }
            function removeAgentRequestNotification(elementId) {
                var id = '#e' + elementId;
                $(id).remove();
                decrementAgentRequestsCount();
                if ($(".agentRequests").length == 0) {
                    showNoAgentRequestsMessage();
                }
            }
            function removeAgentHelpNotification(elementId) {
                var id = '#h' + elementId;
                $(id).remove();
                decrementHelpRequestsCount();
                if ($(".helpRequests").length == 0) {
                    showNoHelpRequestsMessage();
                }

            }
            function agentRequestResponse(id, response, isSick) {
//                var id = $(this).attr('id');
                var execute = '1';
                var timeSlot = 0;
                var approvedTime = '#T' + id;
                if (isSick == '1') {
                    $(approvedTime).val();
                    timeSlot = $(approvedTime).val();
                    if (timeSlot == '')
                        execute = '0';
                }
                var url = "<?= Url::to(['supevisor/agentresponse']) ?>";
                if (execute == '1') {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {q: id, res: response, time: timeSlot},
                        success: function (data)
                        {
                            removeAgentRequestNotification(id);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
//                            alert(xhr.responseText);
//                            alert(xhr.status);
//                            alert(thrownError);
//                            alert(xhr.responseText);
                        }
                    });
                }
            }
        </script>
        <script>
// conference call functionality
            function initiateConference(otherPhoneNumber) {
                // contentWindow.addToConference
                // inviteForConferenceAJAX(otherPhoneNumber);

//                 if(otherPhoneNumber.length > 6)
//                 otherPhoneNumber="0" + otherPhoneNumber;
                 document.getElementById('webphoneframe').contentWindow.addToConference(otherPhoneNumber);


            }
            function initiateContactConference(otherPhoneNumber) {
                // contentWindow.addToConference
                // inviteForConferenceAJAX(otherPhoneNumber);

                // if(otherPhoneNumber.length > 6)
                otherPhoneNumber = "0" + otherPhoneNumber;
                document.getElementById('webphoneframe').contentWindow.addToConference(otherPhoneNumber);


            }

            function removeFromConference(otherPhoneNumber) {
                document.getElementById('webphoneframe').contentWindow.removeConference(otherPhoneNumber);
            }
            function initiateContactCall(otherPhoneNumber) {
                // alert(otherPhoneNumber);
                otherPhoneNumber = "0" + otherPhoneNumber;
                document.getElementById('webphoneframe').contentWindow.removeConference(otherPhoneNumber);
            }

            function initiateContacttransfer(number) {
                number = "0" + number;
                document.getElementById('webphoneframe').contentWindow.transfer(number);
            }

            // transfer call functionality
            function initiatetransfer(number) {
                document.getElementById('webphoneframe').contentWindow.transfer(number);
            }

            function ignoreLateArrival(id) {
                var url = "<?= Url::to(['agent/updateagentrequest']) ?>";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {agent_id: id},
                    success: function (data)
                    {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
//                        alert(xhr.responseText);
//                        alert(xhr.status);
//                        alert(thrownError);
//                        alert(xhr.responseText);
                    }
                });
            }


        </script>
        <script>
            // for the countdown functionalities
            $(document).ready(function () {
                setInterval(function () {
                    $(".callDuration").each(function () {
                        var time = parseInt($(this).siblings("#callDurationSeconds").html());
                        time++;
                        $(this).html(secondsTimeSpanToHMS(time));
                        $(this).siblings("#callDurationSeconds").html(time);
                    });
                }, 1000);
            });
            function secondsTimeSpanToHMS(s) {
                var h = Math.floor(s / 3600); //Get whole hours
                s -= h * 3600;
                var m = Math.floor(s / 60); //Get remaining minutes
                s -= m * 60;
                return h + ":" + (m < 10 ? '0' + m : m) + ":" + (s < 10 ? '0' + s : s); //zero padding on minutes and seconds
            }
        </script>

    </body>
</html>
