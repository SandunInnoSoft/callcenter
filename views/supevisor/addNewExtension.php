<?php
use yii\helpers\Url;
?>

<body>
    <div class = "container-fluid">
            <div class = "row">
                <div class = "col-md-12">
                <a href = "<?= Url::to(['supevisor/manage']); ?>" class = "btn btn-info btn-sm" type = "button">< Back</a>
                    <h3 class = "text-primary text-center">
                        <b> Add New Extension </b>
                    </h3>
                    <!-- <form action="#" role = "form" id = "idNewExtensionForm"> -->
                        <div class = "form-group">

                            <label>
                                New VOIP Extension Number<span style="color: tomato">*</span>
                            </label>
                            <input type = "number" class = "form-control" id = "extensionNumberInput" name = "extensionNumberInput" />
                            <span id="extensionValidationMessage"></span>
                        </div>


                        <a id = "btnSaveExtension" class = "btn btn-success btn-md disabled" onclick="validateForm()">
                            Save
                        </a>
                        <a class = "btn btn-info" onclick="clearForm()">
                            Clear
                        </a>

                    <!-- </form> -->

                </div>
            </div>
            <br>
            <div class="row" id="extInsertNotifDiv">
            </div>
    </div>
</body>
<script>
		var isExtensionExists = true;

		$("#extensionNumberInput").keypress(function(event) {
			pressEnter(event);
		});

		function pressEnter(event) {
            if (event.keyCode == 13) {
                $("#btnSaveExtension").click();
            }
        }

		function showSaveSuccessMessage(extension){
			$("#extInsertNotifDiv").empty();
			var successMsgDiv = $("<div></div>");
			$(successMsgDiv).addClass("alert alert-success");
			$(successMsgDiv).append("<strong> "+ extension +" Extension Saved Successfully!</strong>");
			$("#extInsertNotifDiv").append(successMsgDiv);
		}

		function showSaveFailedMessage(extension){
			$("#extInsertNotifDiv").empty();
			var failMsgDiv = $("<div></div>");
			$(failMsgDiv).addClass("alert alert-danger");
			$(failMsgDiv).append("<strong> "+ extension +" Extension Saving Failed!</strong> Please try saving again");
			$("#extInsertNotifDiv").append(failMsgDiv);
		}

        function addNewExtensionAjax(){
            var extensionNumber = $("#extensionNumberInput").val();
            
            $.ajax({
                url: "<?= Url::to(['supevisor/addnewextensionajax'])?>",
                type: 'POST',
                data: {extension : extensionNumber},
                success: function (data, textStatus, jqXHR) {
                    if(data == "1"){
                        // swal({
                        //     title: 'Added!',
                        //     text: 'New extension added successfully',
                        //     type: "success"
                        // });
            			showSaveSuccessMessage(extensionNumber);
                        clearForm();
                }else{
                    // swal({
                    //     title: 'Some problem!',
                    //     text: 'An error occured, Please re submit your new extension!',
                    //     type: "danger"
                    // });
            		showSaveFailedMessage(extensionNumber);
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // alert(jqXHR.responseText);
                    // swal({
                    //     title: 'Some problem!',
                    //     text: 'An error occured, Please re submit your new extension!',
                    //     type: "danger"
                    // });
					showSaveFailedMessage(extensionNumber);
                }
            });
            
        }
        
        
        function validateForm(){
            var validationSuccess = true;
            if($("#extensionNumberInput").val() == ""){
                // Queue name is empty
                if(validationSuccess == true){
                    validationSuccess = false;
                        swal({
                            title: 'Oops!',
                            text: 'Extension Number is empty!',
                            type: "warning"
                        });
                }
            }

            
            if(validationSuccess == true){
            	if(isExtensionExists == false){
                // successfully validated
                	addNewExtensionAjax();
            	}else{
					swal({
                        title: 'Oops!',
                        text: 'Extension Number already exists!',
                        type: "warning"
                    });
            	}
            }
            
            function clearForm(){
                $("#extensionNumberInput").val("");
            }
        }
        
        $("#extensionNumberInput").keyup(function(){
            var typingText = $(this).val();
            if(typingText != "" && typingText.length > 2){
                // typing text is not empty
                $("#extensionValidationMessage").html("Checking..");
                $("#extensionValidationMessage").css("color", "black");  
                $("#btnSaveExtension").addClass("disabled");

                $.ajax({
                    url: "<?= Url::to(['admin/checkextensionavailability'])?>",
                    data: {typingExtension : typingText},
                    type: 'GET',
                    success: function (data, textStatus, jqXHR) {
                        if(data == "1"){
                            // extension is available
                            $("#extensionValidationMessage").html("Extension is available");
                            $("#extensionValidationMessage").css("color", "green");
                            $("#btnSaveExtension").removeClass("disabled");
                            isExtensionExists = false;
                        }else{
                            // extension is not available
                            $("#extensionValidationMessage").html("Extension is not available");
                            $("#extensionValidationMessage").css("color", "red");  
                            $("#btnSaveExtension").addClass("disabled");
                            isExtensionExists = true;
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("extension validation error : "+jqXHR.responseText);
                    	isExtensionExists = true;
                    }
                });    
            }else{
                // typing text is empty
                $("#extensionValidationMessage").html("");
                $("#extensionValidationMessage").css("color", "red");  
                $("#btnSaveExtension").addClass("disabled");
            }
        });
        

</script>