<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

use yii\helpers\Url;
$params = require(__DIR__ . '../../../config/view_parameters.php');

?>
<style>
    .navbar-inverse{
        display: none;
    }

    .navbar-fixed-top{
        display: none;
    }
    .wrlogo{
        height: 100px;
    }
    .login-body{
        background-color: #ffffff;
        border: 3px solid #fbad18;
        border-radius: 5px;
        padding-top: 4%;padding-bottom: 5%;
    }
    body{
        background-color: rgb(29, 76, 161);
    }
</style>
<script>
    $(function () {

        $(".dropdown-menu li a").click(function () {

            $(".user-role-dropdown-btn:first-child").text($(this).text());
            $(".user-role-dropdown-btn:first-child").val($(this).text());
        });

    });
</script>

<div class="container login-body" style="">
    <div class="row" style="">
        <div class="col-xs-5"></div>
        <div class="col-xs-2"><img class="img-responsive" src="<?=$params['login_image_url'];?>"></div>
        <div class="col-xs-5"></div>
    </div><br>
    <div class="row" style="">
        <div class="col-xs-3"></div>
        <div class="col-xs-6" style="font-size: 110%; text-align: center">Logging in to <span style="color: #00a;font-weight: bold"><?=$params['logging_into_text'];?></span> call center software.</div>
        <div class="col-xs-3"></div>
    </div><br>
    <div class="row" style="">
        <div class="col-xs-4"></div>
        <div class="col-xs-4">

            <form id="loginForm" name="loginForm" >
                <fieldset>                    
                    <div class="form-group">
                        <!--<span class="notificationSpan" style="display: none;color: red;"><span class="noifyText">Invalid username or password</span></span>-->
                        <div style=" display: none; width: 100%" class="alert alert-danger" id="invalidCredentialsAlertDiv">
                            <strong>Invalid username or password!</strong> Please check
                        </div>
                        <div style="display: none; width: 100%" class="alert alert-warning" id="missingCredentialsAlertDiv">
                            <strong>Please insert your username, password and select your role!</strong>
                        </div>
                        <div style="display: none; width: 100%" class="alert alert-info" id="queueNotAssignedAlertDiv">
                            <strong>You have not yet assigned to a queue. Please contact your administrator!</strong>
                        </div>
                    </div>
                    <div class="dropdown form-group">
                        <button style="text-align: left" class="user-role-dropdown-btn btn btn-block btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select User Role
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" style="width: 100%">
                            <li><a href="#" onclick="$('#hiddenSelectedUserRoleInput').val(1)">Supervisor</a></li>
                            <li><a href="#" onclick="$('#hiddenSelectedUserRoleInput').val(2)">Super User</a></li>
                            <li><a href="#" onclick="$('#hiddenSelectedUserRoleInput').val(3)">Agent</a></li>
                            <li><a href="#" onclick="$('#hiddenSelectedUserRoleInput').val(4)">Executive</a></li>

                        </ul>
                        <input style="display: none" id="hiddenSelectedUserRoleInput" name="hiddenSelectedUserRoleInput" value="0">
                    </div>                    
<!--                    <div class="form-group">
                        <label style="font-size: 100%;" for="user_name">VOIP Extension</label>                        
                        <input class="form-control loginFields" title="You can enter your VOIP Extention and login." name="voipExtensionInput" type="number" id="voipExtensionInput" autofocus style="display: none">
                        <select id="selectVoipExtension" name="selectVoipExtension" class="form-control" title="Select your VOIP extension" onchange="$('#voipExtensionInput').val($(this).val())">
                            <option value="">Select your VOIP extension</option>
                            <option value="800">800</option>
                            <option value="801">801</option>
                            <option value="802">802</option>
                            <option value="803">803</option>
                            <option value="804">804</option>
                            <option value="805">805</option>
                            <option value="806">806</option>
                            <option value="807">807</option>
                            <option value="808">808</option>
                            <option value="809">809</option>
                            <option value="810">810</option>
                        </select>
                    </div>-->
                    <div class="form-group">
                        <label style="font-size: 110%;" for="user_name">Username</label>
                        <input class="form-control loginFields" name="user_name" type="text" id="user_name" autofocus>
                    </div>
                    <div class="form-group">
                        <label style="font-size: 110%;" for="password">Password</label>
                        <input class="form-control loginFields" name="password" type="password" id="id_password" onkeypress="pressEnter(event)">
                    </div>
                    <div class="form-group"><a class="btn btn-block btn-primary loginbutton" style="border: 1px solid black" name="btnSubmit" id="btnSubmit" onclick="triggerEvent(event);">Log in</a></div>
                </fieldset>
                <br>
                <div class="alert alert-warning" style="display: none" id="warningMessageDiv">
                    <strong id="warningMessageHead"></strong> <p id="warningMessageBody"></p>
                </div>
                <div class="alert alert-danger" style="display: none" id="failMessageDiv">
                    <strong id="failMessageHead"></strong> <p id="failMessageBody"></p>
                </div>
            </form>

            <script>
                function pressEnter(event) {
                    if (event.keyCode == 13) {
                        triggerEvent(event);
                    }
                }// end of pressEnter function 

            </script>
            <script>
                function triggerEvent(e) {
                    $("#missingCredentialsAlertDiv").css("display", "none");
                    $("#invalidCredentialsAlertDiv").css("display", "none");

                    var url = "<?= Url::to(['user/verifytext']) ?>"; // the script where you handle the form input.
                    if ($('#user_name').val() != '' && $('#id_password').val() != '' && $('#hiddenSelectedUserRoleInput').val() != 0)
                    {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: $("#loginForm").serialize(), // serializes the form's elements.
                            success: function (data)
                            {
                                $(".notificationSpan").fadeOut();
                                if (data == "1") {
                                    $('#btnSubmit').css("background-color", "#00b300");
                                    $('#btnSubmit').css("color", "black");

                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                } else if(data == "2"){
                                    $("#missingCredentialsAlertDiv").css("display", "none");
                                    $("#invalidCredentialsAlertDiv").css("display", "none");
                                    $("#queueNotAssignedAlertDiv").css("display", "block");
                                }else {
//                                    $(".notificationSpan").css("display", "inline");//.fadeOut(5000);
                                    $("#missingCredentialsAlertDiv").css("display", "none");
                                    $("#queueNotAssignedAlertDiv").css("display", "none");
                                    $("#invalidCredentialsAlertDiv").css("display", "block");
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {

                            }

                        });
                    } else if ($('#user_name').val() != '' && $('#id_password').val() != '' && $('#hiddenSelectedUserRoleInput').val() == 0) {
                        $("#invalidCredentialsAlertDiv").css("display", "none");
                        $("#missingCredentialsAlertDiv > strong").html("Please select your role!");
                        $("#missingCredentialsAlertDiv").css("display", "block");
                    } else {
//                        $(".notificationSpan").css("display", "inline");
                        $("#invalidCredentialsAlertDiv").css("display", "none");
                        $("#missingCredentialsAlertDiv > strong").html("Please insert your username, password and select your role!");
                        $("#missingCredentialsAlertDiv").css("display", "block");
                    }
                    e.preventDefault(); // avoid to execute the actual submit of the form.
                }


            </script>
        </div>
        <div class="col-xs-12" style="font-size: 110%; text-align: center">Go to <a href="#" onclick="window.open('<?= $params['ccDashboradUrlLoginPage'] ?>', '_blank');"><span style="color: #16a900;font-weight: bold"><?=$params['ccDashboradName'];?></span></a></div>
        <div class="col-xs-4 col-lg-4"></div>
    </div>
</div>