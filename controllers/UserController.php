<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginController
 *
 * @since 2017-06-26
 * @author Prabath
 * 
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\call_center_user;
use app\models\customer;
use yii\helpers\Url;
use yii\helpers\FileHelper;
use app\models\Agent_requests;
use app\models\Logged_in_users;
use app\models\Dnd_records;
use app\models\Extension_queue;
use app\models\Ext_queue_intermediate;
use DateTime;
use app\models\Deleted_call_center_user;

class UserController extends Controller {

    public function actionLogin_view() {
        $session = Yii::$app->session;
        if (!$session->has('user_id')) {
            return $this->render('userLogin');
        } else {
            $user_role = $session->get('user_role');
            if ($user_role == '1' || $user_role == '3') {
                $this->redirect('index.php?r=supevisor/overview');
            } else if ($user_role == '2' || $user_role == '4') {
                $this->redirect('index.php?r=agent/callerinformation');
            }else if ($user_role == '5') {
                $this->redirect('index.php?r=reports/supervisorreports');
            }
        }
    }

    public function actionVerifytext() {
        $users = call_center_user::find();

        Yii::$app->request->post();
        $post = Yii::$app->request->post('loginForm');

        $user_name = $_POST['user_name'];
        $selectedUserRole = $_POST['hiddenSelectedUserRoleInput'];
        $password = $_POST['password'];
        $hashedPassword = md5($password);

        //check username.if exists get password.
        $dbuser = $users
                ->where("name = '$user_name'") //   
                ->andWhere("status = 'active'")
                ->all();

        if ($dbuser) {
            $dbpassword = $dbuser[0]['password'];

            if ($dbpassword == $hashedPassword) {
                // user logged in successfully   

                $user_role = $dbuser[0]['role_id'];
                
                if($user_role == '3' && Extension_queue::isSupervisorAssignedToAnExtensionQueue($dbuser[0]['id']) == FALSE){
                    return 2; // the user is a supervisor but has no extension queue assigned to him, returns 2
                }
                
                
                $session = Yii::$app->session;
                $session->set('user_id', $dbuser[0]['id']);
                $session->set('user_name', $dbuser[0]['name']);
                $session->set('full_name', $dbuser[0]['fullname']);
                $session->set('user_role', $dbuser[0]['role_id']);
                $session->set('access_time', time());
                $session->set('voip', $dbuser[0]['voip_extension']);

                Logged_in_users::setOldRecordInactive($dbuser[0]['id']);
                Logged_in_users::insertNewRecordForLogin($dbuser[0]['id'], $_SERVER['REMOTE_ADDR']);


                if ($user_role == '1' || $user_role == '3') {
                    // logged in user is a admin or supervisor
                    $session->set('dnd', true); // This means default DND is ON
                    
                    
                    if($user_role == '3'){
                        // user is a supervisor
                        $session->set('extQueueId', Extension_queue::getExtensionQueueIdOfTheSupervisor($dbuser[0]['id'])); // This is to keep the extension queue id belongs to the supervisor
                    }
                    
                    
                    if ($selectedUserRole == 1) {
                        // user selected to show the supervisor dashboard
                        $this->redirect('index.php?r=supevisor/overview');
                    }else if ($selectedUserRole == 4) {
                        $this->redirect('index.php?r=reports/supervisorreports');                        
                    }else{
                        // show the supervisor the agent dashbord
                        $this->redirect('index.php?r=agent/callerinformation');
                    }
                } else if ($user_role == '2') {
                    // logged in user is an agent
                    $session->set('dnd', false); // This means default DND is OFF
                    $session->set('extQueueId', Ext_queue_intermediate::getExtensionQueueIdOfVOIPExtension($dbuser[0]['voip_extension'])); // This is to keep the extension queue id belongs to the agent

                    if ($this->checkIfagentislate($dbuser[0]['id']) == TRUE) {
                        // agent is late
                        $this->redirect('index.php?r=agent/callerinformation&late');
                    } else {
                        // agent is not late
                        $this->redirect('index.php?r=agent/callerinformation');
                    }
                } else if ($user_role == '4') {
                    // logged in user is a super agent
                    $session->set('dnd', false); // This means default DND is OFF
                    $session->set('extQueueId', Ext_queue_intermediate::getExtensionQueueIdOfVOIPExtension($dbuser[0]['voip_extension'])); // This is to keep the extension queue id belongs to the agent

//                    $this->redirect("index.php?r=superuser/dashboard");
                    $this->redirect('index.php?r=agent/callerinformation');
                } else if ($user_role == '5') {
                    // logged in user is a executive    
                    $session->set('dnd', true);
                    if ($selectedUserRole == 1) {
                        $hadSupervisorAccess = Extension_queue::find()->where(['supervisor_id'=> Yii::$app->session->get('user_id')])->one();
                        if ($hadSupervisorAccess != null) {
                            $session->set('extQueueId', Extension_queue::getExtensionQueueIdOfTheSupervisor($dbuser[0]['id'])); // This is to keep the extension queue id belongs to the supervisor
                            $this->redirect('index.php?r=supevisor/overview');                            
                        }else{
                            $this->redirect('index.php?r=reports/supervisorreports');                            
                        }
                    }

                    if ($selectedUserRole == 4) {
                        $this->redirect('index.php?r=reports/supervisorreports');
                    }

                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * 
     * @return type
     * 
     * @modified Sandun 2017-12-19
     * @description removes the extQueueId element from the session when logs out
     */
    public function actionLogout(){
        $session = Yii::$app->session;
        $session->remove('user_id');
        $session->remove('user_name');
        $session->remove('user_role');
        $session->remove('voip');
        $session->remove('dnd');
        $session->remove('extQueueId');
        return $this->render('userLogin');
    }

    private function checkIfagentislate($agentId) {
        $takenBreak = Agent_requests::getTakenBreaks($agentId);
        if ($takenBreak) {
            //has a taken break
            $breakTakenTime = $takenBreak['taken_time'];
            $breakDuration = $takenBreak['approved_time_period'];
            $breakOverTime = date("Y-m-d H:i:s", strtotime("+$breakDuration minutes", strtotime($breakTakenTime)));
            $reLoggedTime = date("Y-m-d H:i:s");

            if (strtotime($reLoggedTime) > strtotime($breakOverTime)) {
                // the agent is late
                Agent_requests::setBreakRequestConsumed($takenBreak['id']);
                return TRUE;
            } else {
                // agent is not late
                return FALSE;
            }
        } else {
            // dont have a taken break
            return FALSE;
        }
    }

    /**
     * <b>Set DND status in the session</b>
     * <p>This function updates the session state for the DND mode of the agent</p>
     * 
     * @author Sandun
     * @since 2017-08-01
     * @return boolean
     * 
     * @modified Sandun 2017-09-25
     * @description Added a session check, if there is no user id set to the active session, user will be redirected to the login page
     */
    public function actionSetdndsessionstate() {
        if (!Yii::$app->session->has('user_id')) {
            $this->redirect('index.php?r=user/login_view');
        }

        $state = $_GET['state'];
        if ($state == 1) {
            // DND is ON
            $this->setDNDStateinDB("On");
            Yii::$app->session->set('dnd', true);
            echo 1;
        } else {
            // DND is OFF
            $this->setDNDStateinDB("Off");
            Yii::$app->session->set('dnd', false);
            echo 0;
        }
    }

    /**
     * 
     * 
     * @modified Sandun 2017-09-25
     * @description Added a session check, if there is no user id set to the active session, user will be redirected to the login page
     */
    public function actionGetagentinfofromvoipext() {
        if (!Yii::$app->session->has('user_id')) {
            $this->redirect('index.php?r=user/login_view');
        }
        $extensionNumber = $_POST['extension'];
        $agentInformation = call_center_user::getUserInfoFromVOIPExtension($extensionNumber);
        if (count($agentInformation) > 0) {
            $agentInformationJson = array();
            $agentInformationJson['id'] = $agentInformation['id'];
            $agentInformationJson['name'] = $agentInformation['name'];
            $agentInformationJson['fullname'] = $agentInformation['fullname'];
            $agentInformationJson['user_profile_pic'] = $agentInformation['user_profile_pic'];
            $agentInformationJson['contact_number'] = $agentInformation['contact_number'];
            echo json_encode($agentInformationJson);
        } else {
            echo 0;
        }
    }

    /**
     * <b></b>
     * <p></p>
     * 
     * @since 2017-08-13
     * @author Sandun
     * @param String $state
     * @return String
     * 
     * @modified Sandun 2017-09-25
     * @description Added a session check, if there is no user id set to the active session, user will be redirected to the login page
     */
    private function setDNDStateinDB($state) {
        if (!Yii::$app->session->has('user_id')) {
            $this->redirect('index.php?r=user/login_view');
        }
        return Dnd_records::insertNewDNDRecord($state);
    }

    /**
     * 
     * @param type $userId
     * @return type
     * 
     * @modified Sandun 2017-09-25
     * @description Added a session check, if there is no user id set to the active session, user will be redirected to the login page
     */
    private function getLatestDndStatusFromDB($userId) {
        if (!Yii::$app->session->has('user_id')) {
            $this->redirect('index.php?r=user/login_view');
        }
        $dndState = Dnd_records::getLatestDNDStateOfUser($userId);
        return $dndState['dnd_mode'];
    }

    /**
     * 
     * @modified Sandun 2017-09-25
     * @description Added a session check, if there is no user id set to the active session, user will be redirected to the login page
     * @modified Sandun 2018-01-10
     * @description Changed the function used to delete a user, now it invokes a function which moves the to be deleted user data to an archival table and delete the user record from the users table
     */
    public function actionDeleteuser() {
        if (!Yii::$app->session->has('user_id')) {
            $this->redirect('index.php?r=user/login_view');
        }
        $agentId = Yii::$app->request->get("agentId");
        if($agentId != NULL){
            if ($this->deleteUserRecord($agentId) == TRUE) {
                echo 1;
            } else {
                echo 0;
            }
        }else{
            echo 0;
        }
    }
    
    
    /**
     * <b>Deletes the call center user record and move the user data to a deleted user table</b>
     * 
     * @param type $agentId
     * @return boolean
     * @since 2018-1-10
     * @author Sandun
     */
    private function deleteUserRecord($agentId){
        $userInformation = call_center_user::getUserInformationById($agentId);
        $movedToArchive = Deleted_call_center_user::insertDeletedUserRecord($userInformation);
        $deleted = call_center_user::deleteAll("id = $agentId");
        if($movedToArchive == TRUE && $deleted == TRUE){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    private function writeToLogOutLog($locationdata){
        $logEntry = "\n";
        $logEntry .= "session data = ".json_encode(Yii::$app->session);
        $logEntry .= "location data = ".$locationdata;
        $logEntry .= "";
        $logEntry .= "\n";
        $myfile = fopen("logoutLog.txt", "a") or die("Unable to open file!");
        fwrite($myfile, date("Y-m-d H:i:s"));
        fwrite($myfile, "\n");
        fwrite($myfile, $logEntry);
        fwrite($myfile, "\n");
        fwrite($myfile, "---------------------------------------------------------------------------");
        fwrite($myfile, "\n");
        fclose($myfile);
        return true;
    }

}
