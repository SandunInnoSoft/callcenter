<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use app\models\Customer;
use app\models\Help_requests;
use app\models\Agent_requests;
use app\models\cdr;
use app\models\Logged_in_users;
use app\models\Call_records;
use app\models\agent_notifications;
use app\models\web_presence;
use app\models\call_answer_times;
use app\models\Ext_queue_intermediate;
use app\models\Queue_ivr_number;

/**
 * This controller is to access dashboard data from hnb databases
 *
 * @author Vikumn
 * @since 2017-08-17
 */
class DashboardController extends Controller {

    /**
     *
     * @author Vikum
     * @since 2017-08-17
     * 
     * @modified Sandun 2018-2-27
     * @description prints a json result set of the current calls and agent statuses counts (summery of call center current agents and current calls)
     */
    public function actionDashboard_records() {
        $queueId = Yii::$app->request->post("queueId");
        $extensionsInQueue = ($queueId == NULL || $queueId == '0' ? NULL : Ext_queue_intermediate::getAllExtensionsOfTheQueue($queueId));

        $extensionSummary = $this->getAllAgentSummary($extensionsInQueue);
        
        $states = array(
            'NOT_INUSE' => 0,
            'UNAVAILABLE' => 0,
            'INUSE' => 0,
            'RINGING' => 0,
            'ONHOLD' => 0,
            'NONE' => 0
        );
        foreach ($extensionSummary as $state) {
            if ($state['state'] == "NOT_INUSE") {
                $states['NOT_INUSE'] = $state['num'];
            } else if ($state['state'] == "UNAVAILABLE") {
                $states['UNAVAILABLE'] = $state['num'];
            } else if ($state['state'] == "INUSE") {
                $states['INUSE'] = $state['num'];
            } else if ($state['state'] == "RINGING") {
                $states['RINGING'] = $state['num'];
            } else if ($state['state'] == "ONHOLD"){
                $states['ONHOLD'] = $state['num'];
            } else {
                $states['NONE'] = $state['num'];
            }
        }
        $data = array(
            'agentSummary' => $states
        );

        return json_encode($data);
    }

    /**
     * <b>Print today call center performance statistics</b>
     * <p>This function prints the today's call center performance statistics values in JSON format</p>
     * 
     * @author Sandun
     * @since 2018-02-27
     * 
     */
    public function actionGetdashboardstats(){
        $queueId = Yii::$app->request->post("queueId");
        $extensionsInQueue = ($queueId == '0' || $queueId == NULL ? NULL : Ext_queue_intermediate::getAllExtensionsOfTheQueue($queueId));
//        $allCalls = $this->getAllCallsToday($extensionsInQueue);
        $allAnswered = $this->getAllAnsweredCallsToday($extensionsInQueue);
        $missedCalls = $this->getAllMissedCallsToday($extensionsInQueue);
        $averageWaitingTime= $this->getAllCallWaitingAverage($extensionsInQueue);
        $maxWaitingTime= $this->getAllCallWaitingMax($extensionsInQueue);
        $serviceLevelCalls= $this->getAllServiceLevelledCalls($extensionsInQueue);
        $allOutgoing= $this->getAllOutgoingCallsToday($extensionsInQueue);
        $ivrReceivedCalls= $this->getAllIvrReceivedCallsToday($queueId);

        $data = array(
//            'allCalls' => $allCalls,
            'answeredCalls' => $allAnswered,
            'outgoingCalls' => $allOutgoing,
            'missedCalls' => $missedCalls,
            'waiting'=>round($averageWaitingTime, 2),
            'maxWaiting'=>round($maxWaitingTime, 2),
            'serviceLevelCalls'=>$serviceLevelCalls,
            'ivrReceivedCalls'=>$ivrReceivedCalls
        );
        return json_encode($data);
    }

    private function getAllCallsToday($extensionsInQueue) {
        $data = cdr::getTodaysCalls($extensionsInQueue);
        return $data[0]['num'];
    }
    
    private function getAllMissedCallsToday($extensionsInQueue){
//        return cdr::getMissedCallsCountBetweenDates(date("Y-m-d"), date("Y-m-d"));
        return cdr::getAllMissedCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), '00:00:00', '23:59:59', NULL, "Count", $extensionsInQueue);
        
    }

    private function getAllAnsweredCallsToday($extensionsInQueue) {
//        $data = Call_records::getAllCallsToday();
        // $data = cdr::getAllAnsweredCallsCountBetweenDates(date("Y-m-d"), date("Y-m-d"), $extensionsInQueue);
        $data = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), '00:00:00', "23:59:59", 0, NULL, 'Count', $extensionsInQueue, NULL);

        return $data;
    }

    private function getAllOutgoingCallsToday($extensionsInQueue) {
        $data = cdr::getOutgoingCallsListByDateForPaginationActiveQuery("0", date("Y-m-d"), date("Y-m-d"), '00:00:00', "23:59:59",NULL, $extensionsInQueue,'Count',NULL,NULL,NULL,NULL,NULL);
        return $data;
    }

    private function getAllIvrReceivedCallsToday($queueId){
        $ivrNumbers = ($queueId == '0' || $queueId == NULL ? "0" : Queue_ivr_number::getIvrNumbersOfQueue($queueId));
        $data = cdr::getIvrAnsweredCallsFromCustomersForPagination(date("Y-m-d"), date("Y-m-d"), '00:00:00', "23:59:59",NULL,"Count",$ivrNumbers,NULL,NULL,NULL,NULL);
        return $data;        
    }       
        

    private function getAllAgentSummary($extensionsInQueue) {
        $data = web_presence::getAllSummary($extensionsInQueue);
        return $data;
    }
    private function getAllCallWaitingAverage($extensionsInQueue) {
        $todayWaitingTimes = $this->getAllWaitingTimesOfToday($extensionsInQueue);
        $totalWaitingTime = array_sum($todayWaitingTimes);
        $averageWaitingTime = ($todayWaitingTimes == 0 || count($todayWaitingTimes) == 0 ? 0 : $totalWaitingTime / count($todayWaitingTimes) );
        return $averageWaitingTime;
    }
    private function getAllCallWaitingMax($extensionsInQueue) {
        $todayWaitingTimes = $this->getAllWaitingTimesOfToday($extensionsInQueue);
        return (isset($todayWaitingTimes[0]) ? $todayWaitingTimes[0] : 0);
    }
    private function getAllServiceLevelledCalls($extensionsInQueue) {
        $waitingTimesCountComesUnderServiceLevel = $this->getServiceLevelCallsCountOfToday($extensionsInQueue);
        return $waitingTimesCountComesUnderServiceLevel;
    }
    
    
    private function getAllWaitingTimesOfToday($extensionsInQueue){
        $allCustomerCallRecordsOfToday = cdr::getAllCustomerCallRecordsWithinToday($extensionsInQueue);
        $waitingTimes = array();
        foreach ($allCustomerCallRecordsOfToday as $key){
            if($key->disposition == 8){
                // answered call
                if(strpos($key->dst, "IVR") === TRUE){
                    // IVR answered call
                    $startTimestamp = strtotime($key->start);
                    $endTimestamp = strtotime($key->end);
                    $waitingTime = $endTimestamp - $startTimestamp;
                    array_push($waitingTimes, $waitingTime);
                }else{
                    // Agent answered call
                    $answeredTime = call_answer_times::getAnswerTimeBetweenStartAndEnd($key->start, $key->end, $key->src, $key->dst);
                    if($answeredTime){
                        // found the answered time
                        $startTimestamp = strtotime($key->start);
                        $answeredTimestamp = strtotime($answeredTime);
                        $waitingTime = $answeredTimestamp - $startTimestamp;
                        array_push($waitingTimes, $waitingTime);
                    }else{
                        // did not found the answered time, uses the cdr answer time
                        $startTimestamp = strtotime($key->start);
                        $answeredTimestamp = strtotime($key->answer);
                        $waitingTime = $answeredTimestamp - $startTimestamp;
                        array_push($waitingTimes, $waitingTime);                    
                    }
                }
            }else{
                // missed or abandoned call
                $startTimestamp = strtotime($key->start);
                $endTimestamp = strtotime($key->end);
                $waitingTime = $endTimestamp - $startTimestamp;
                array_push($waitingTimes, $waitingTime);
            }
        }
        rsort($waitingTimes);
        return $waitingTimes;
    }
    
    private function getServiceLevelCallsCountOfToday($extensionsInQueue){
        // $todayAnsweredCalls = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), 0, NULL, "Data", $extensionsInQueue);
        $todayAnsweredCalls = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), "00:00:00", "23:59:59", 0, NULL, "Data", $extensionsInQueue, NULL);
        $serviceLevelCallsCount = 0;
        foreach ($todayAnsweredCalls as $key){
            if(strpos($key->dst, "IVR") === FALSE){
                // agent answered
                    $answeredTime = call_answer_times::getAnswerTimeBetweenStartAndEnd($key->start, $key->end, $key->src, $key->dst);
                    if($answeredTime){
                        // found the answered time
                        $startTimestamp = strtotime($key->start);
                        $answeredTimestamp = strtotime($answeredTime);
                        $waitingTime = $answeredTimestamp - $startTimestamp;
                        if($waitingTime < 3600){
                            $serviceLevelCallsCount++;
                        }
                    }else{
                        // did not found the answered time, uses the cdr answer time
                        $startTimestamp = strtotime($key->start);
                        $answeredTimestamp = strtotime($key->answer);
                        $waitingTime = $answeredTimestamp - $startTimestamp;
                        if($waitingTime < 3600){
                            $serviceLevelCallsCount++;
                        }                 
                    }                
            }
        }
        return $serviceLevelCallsCount;
    }
    
    
    public function actionTestwaiting(){
        print_r($this->getAllWaitingTimesOfToday());
    }
    
    public function actionTestservicelevel(){
        echo $this->getServiceLevelCallsCountOfToday();
    }
}
