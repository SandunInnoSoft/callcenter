<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use app\models\Customer;
use app\models\Help_requests;
use app\models\Agent_requests;
use app\models\cdr;
use app\models\Logged_in_users;
use app\models\abandon_calls;
use app\models\call_center_user;
use app\models\Dnd_records;
use app\models\Call_records;
use app\models\Extension_queue;
use app\models\Ext_queue_intermediate;
use app\models\Voicemail_extension;
use app\models\Queue_ivr_number;
use app\models\Call_channel;
use app\models\Extqueue_callchannel_intermediate;
use app\models\Audiotransferroutinelog;
use app\models\Ftp;

use yii\helpers\Json;

/**
 *
 * @since 31/01/2019
 * @author Vinothan
 */
class DatareportsController extends Controller {

    
    private function getExtensionFromCdrChannel($channel) {
        preg_match("'SIP/(.*?)-'si", $channel, $match);
        if ($match) {
            if ($match[1] != '' && strlen($match[1]) < 5) {
                $userName = call_center_user::getUserInfoFromVOIPExtension($match[1]);
                return $userName['fullname'];
//                return implode(" ",$userName);
//                return $match[1];
            } else {
                return $match[1];
            }
        } else {
            return 0;
        }
    }

    private function getExtensionNumberFromOutgoingCdrChannel($channel){
        preg_match("'SIP/(.*?)-'si", $channel, $match);
        if ($match) {
            if ($match[1] != '' && strlen($match[1]) < 5) {
                return $match[1];
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }


    private function convertToHoursMins($time, $format = '%02d:%02d') {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }    


    private function getAllMissedCallsBetweenDates($from, $to, $fromTime, $toTime,$returnType, $contactNumber, $extensionsInQueue,$limit,$offset,$orderColumn,$orderDi,$draw) {
        $result = array();
        $missedCalls = array();

        if($draw == 1){
            $missedCalls = cdr::getAllMissedCallsDataBetweenDatesForPagination($from, $to, $fromTime, $toTime, $contactNumber, $returnType, $extensionsInQueue,null,null,null,null,1);         
        }else{
            $missedCalls = cdr::getAllMissedCallsDataBetweenDatesForPagination($from, $to, $fromTime, $toTime, $contactNumber, $returnType, $extensionsInQueue,$limit,$offset,$orderColumn,$orderDi,1); 
        }

        if ($missedCalls != NULL && count($missedCalls) > 0) {
            // have missed calls
            for ($i = 0; $i < count($missedCalls); $i++) {
                if ($missedCalls[$i]['end'] != "" || $missedCalls[$i]['end'] != null) {
                    $callDateExplode = explode(" ", $missedCalls[$i]['end']);
                    $missedCallInfo = array(
                        "date" => $callDateExplode[0],
                        "time" => $callDateExplode[1],
                        "caller_num" => $missedCalls[$i]['src']
                    );

                    array_push($result, $missedCallInfo);
                }
            }

            return $result;
        } else {
            // no missed calls
            return null;
        }
    }

    /**
     * @author Nuwan.
     * @since 06/07/2019.
     * @description get all the data to calculate the service level.
     */

    public function actionPerformservicelevel(){       
        
        if (Yii::$app->session->has('user_id') == false) {
            // has no active session
            $this->redirect('index.php?r=user/login_view');
        }

        if (Yii::$app->request->post('outputType') == "byDate") {
            $agent_id = $_POST['agent_id'];
            $agent_ex = $_POST['agentex'];
            $agent_name = $_POST['agentname'];
            $searchContact = ($_POST['contactNum'] == "" || $_POST['contactNum'] == NULL ? NULL : $this->removeFirstZeroFilterWhitespaceIfExistsInContact($_POST['contactNum']));
            $fromDate = trim($_POST['fromdate'], " ");
            $toDate = trim($_POST['todate'], " ");

            $fromTime = (Yii::$app->request->post("fromTime") != NULL ? Yii::$app->request->post("fromTime") : "00:00:00");
            $toTime = (Yii::$app->request->post("toTime") != NULL ? Yii::$app->request->post("toTime") : "23:59:59");

            $selectedChannelsArray = (Yii::$app->request->post("channels") != "0" ? explode(",", Yii::$app->request->post("channels")) : NULL);

            $extensionsInQueue = (Yii::$app->session->get("user_role") == '1' || Yii::$app->session->get("user_role") == '5' ? NULL : Ext_queue_intermediate::getAllExtensionsOfTheQueue(Yii::$app->session->get("extQueueId")));
            $agentIdsInQueue = (Yii::$app->session->get("user_role") == '1' || Yii::$app->session->get("user_role") == '5' ? NULL : call_center_user::getAgentIdsFromQueueId(Yii::$app->session->get("extQueueId")));

            if(Yii::$app->session->get('user_role') == '1' || Yii::$app->session->get('user_role') == '3'){                 
                
                $allAnswered = $this->getAllAnsweredCallsCount(null,$fromDate,$toDate,$fromTime,$toTime,$agent_ex);
                $missedCalls = $this->getAllMissedCallsCount(null,$fromDate,$toDate,$fromTime,$toTime,$agent_ex);
                $allOutgoing= $this->getAllOutgoingCallsCount(null,$fromDate,$toDate,$fromTime,$toTime,$agent_ex);  

                $data = array(
                    'allAnsweredCalls' => $allAnswered,
                    'allMissedCalls' => $missedCalls,
                    'allOutgoing' => $allOutgoing
                );
                return json_encode($data);
            }         

        }else if(Yii::$app->request->post('outputType') == "byQueue"){
           
            if(Yii::$app->session->get('user_role') == "1"){                             
                // user is admin
                $agent_id = (Yii::$app->request->post("agent_id") == NULL || Yii::$app->request->post("agent_id") == "" ? 0 : Yii::$app->request->post("agent_id"));
                $agent_ex = (Yii::$app->request->post("agentex") == NULL || Yii::$app->request->post("agentex") == "" ? 0 : Yii::$app->request->post("agentex"));
                $agent_name = $_POST['agentname'];
                $searchContact = (Yii::$app->request->post("contactNum") == NULL || Yii::$app->request->post("contactNum") == "" ? NULL : $this->removeFirstZeroFilterWhitespaceIfExistsInContact(Yii::$app->request->post("contactNum")));
                $queue_id = (Yii::$app->request->post("queue_id") == "" ? NULL : Yii::$app->request->post("queue_id"));                
                if(Yii::$app->request->post("fromdate") == NULL || Yii::$app->request->post("fromdate") == ""){
                    $fromDate = date("Y-m-01");
                }else{
                    $fromDate = trim($_POST['fromdate'], " ");                    
                }

                if(Yii::$app->request->post("todate") == NULL || Yii::$app->request->post("todate") == ""){
                    $toDate = date("Y-m-d");
                }else{
                    $toDate = trim($_POST['todate'], " ");                    
                }

                $fromTime = (Yii::$app->request->post("fromTime") != NULL ? Yii::$app->request->post("fromTime") : "00:00:00");
                $toTime = (Yii::$app->request->post("toTime") != NULL ? Yii::$app->request->post("toTime") : "23:59:59");                
                 
                $extensionsInQueueNew = ($queue_id == '0' || $queue_id == NULL ? NULL : Ext_queue_intermediate::getAllExtensionsOfTheQueue($queue_id));
                $allAnswered = $this->getAllAnsweredCallsCount($extensionsInQueueNew,$fromDate,$toDate,$fromTime,$toTime,null);
                $missedCalls = $this->getAllMissedCallsCount($extensionsInQueueNew,$fromDate,$toDate,$fromTime,$toTime,null);
                $allOutgoing= $this->getAllOutgoingCallsCount($extensionsInQueueNew,$fromDate,$toDate,$fromTime,$toTime,null);  
                            
                $data = array(
                    'allAnsweredCalls' => $allAnswered,
                    'allMissedCalls' => $missedCalls,
                    'allOutgoing' => $allOutgoing
                );               
                return json_encode($data);
            }        
        }
    }

    public function getAllAnsweredCallsCount($extensionsInQueueNew,$fromDate,$toDate,$fromTime,$toTime,$agent_ex){
      if($agent_ex ==null){
        $data = cdr::getAgentAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, 0, NULL, 'Count', $extensionsInQueueNew, NULL);
      }elseif ($agent_ex != null) {
         $data = cdr::getIndividualAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, "Count", $agent_ex);
      }
      return $data; 
  }    
    public function getAllMissedCallsCount($extensionsInQueueNew,$fromDate,$toDate,$fromTime,$toTime,$agent_ex){
        if($agent_ex ==null){            
            $data = cdr::getAllMissedCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, NULL, "Count", $extensionsInQueueNew);
            return $data; 

          }elseif ($agent_ex != null) {
            $dataAgent = cdr::getIndividualMissedCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, "Count", $agent_ex);

            if($agent_ex !=0){
                $agentQueueId = Ext_queue_intermediate::getAgentQueueId($agent_ex);

                foreach ($agentQueueId as $key) {
                    if($key != null){
                      $agentQueueId = $key;                   
                    }                            
                } 
                $extensionsInQueue = ($agentQueueId == '0' || $agentQueueId == NULL ? NULL : Ext_queue_intermediate::getAllExtensionsOfTheQueue($agentQueueId));
                $dataNew = cdr::getAllMissedCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, NULL, "Count", $extensionsInQueue);
                $totalNoOfExtensions = Ext_queue_intermediate::getAllExtensionsCountOfTheQueue($agentQueueId); 
                $data = ($dataNew/ $totalNoOfExtensions); 
                return $data;                      
            } else{
                return $dataAgent; 
            }                     
          } 
                 
    }
    
    public function getAllOutgoingCallsCount($extensionsInQueueNew,$fromDate,$toDate,$fromTime,$toTime,$agent_ex){
        if($agent_ex ==null){
            $data = cdr::getOutgoingCallsListByDateForPaginationActiveQuery("0", $fromDate, $toDate, $fromTime, $toTime,NULL, $extensionsInQueueNew,'Count',NULL,NULL,NULL,NULL,NULL);
          }elseif ($agent_ex != null) {
             $data = cdr::getIndividualOutgoingCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, "Count", $agent_ex, $extensionsInQueueNew);
          }
          return $data; 
    }
    /**
     * End
     */
  
  public function actionPerformanceoverviewbydate() {
        if (Yii::$app->session->has('user_id') == false) {
            // has no active session
            $this->redirect('index.php?r=user/login_view');
        }

        $params = $_REQUEST;

        if (Yii::$app->request->post('tableType') == "Answered") {
            $columns = array( 0 =>'src',1 =>'answer', 2 => 'dst',3 => 'end',4 => 'duration', 5 => 'end');
        }else if(Yii::$app->request->post('tableType') == "Outgoing"){
            $columns = array( 0 =>'dst',1 =>'channel', 2 => 'start',3 => 'answer',4 => 'end',5 => 'duration', 6 => 'end');
        }else if(Yii::$app->request->post('tableType') == "Missed"){
            $columns = array( 0 =>'end',1 =>'end', 2 => 'src');
        }else if(Yii::$app->request->post('tableType') == "IVR"){
            $columns = array( 0 =>'end',1 =>'end', 2 => 'src');
        }else if(Yii::$app->request->post('tableType') == "VM"){
            $columns = array( 0 =>'end',1 =>'end', 2 => 'src');
        }else if(Yii::$app->request->post('tableType') == "Abandoned"){
            $columns = array( 0 =>'end',1 =>'end', 2 => 'src');
        }else if(Yii::$app->request->post('tableType') == "Login"){
            $columns = array( 0 =>'logged_in_time',1 =>'time_signature',2 =>'time_signature');
        }else if(Yii::$app->request->post('tableType') == "DND"){
            $columns = array( 0 =>'timestamp',1 =>'timestamp', 2 => 'dnd_mode');
        }

        if (Yii::$app->request->post('outputType') == "byDate") {
            $agent_id = $_POST['agent_id'];
            $agent_ex = $_POST['agentex'];
            $agent_name = $_POST['agentname'];
            $searchContact = ($_POST['contactNum'] == "" || $_POST['contactNum'] == NULL ? NULL : $this->removeFirstZeroFilterWhitespaceIfExistsInContact($_POST['contactNum']));
            $fromDate = trim($_POST['fromdate'], " ");
            $toDate = trim($_POST['todate'], " ");

            $fromTime = (Yii::$app->request->post("fromTime") != NULL ? Yii::$app->request->post("fromTime") : "00:00:00");
            $toTime = (Yii::$app->request->post("toTime") != NULL ? Yii::$app->request->post("toTime") : "23:59:59");

            $selectedChannelsArray = (Yii::$app->request->post("channels") != "0" ? explode(",", Yii::$app->request->post("channels")) : NULL);

            $extensionsInQueue = (Yii::$app->session->get("user_role") == '1' || Yii::$app->session->get("user_role") == '5' ? NULL : Ext_queue_intermediate::getAllExtensionsOfTheQueue(Yii::$app->session->get("extQueueId")));
            $agentIdsInQueue = (Yii::$app->session->get("user_role") == '1' || Yii::$app->session->get("user_role") == '5' ? NULL : call_center_user::getAgentIdsFromQueueId(Yii::$app->session->get("extQueueId")));

            if (Yii::$app->session->get('user_role') == '5') {
                //user is executive 
                $queuesIds = Extension_queue::getAllAvailableQueuesIdOfSelectedExecutive(Yii::$app->session->get('user_id'));
                $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueueArray($queuesIds);
                $agentIdsInQueue = call_center_user::getAgentsOfSelectedQueues($queuesIds);
            }


            if ($extensionsInQueue == NULL && $agent_id != 0) {
                // individual agent listing,
                $queueId = Ext_queue_intermediate::getExtensionQueueIdOfVOIPExtension($agent_ex);
                $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueue($queueId);
                $agentIdsInQueue = call_center_user::getAgentIdsFromQueueId($queueId);
            }

        }else if(Yii::$app->request->post('outputType') == "byQueue"){

            if(Yii::$app->session->get('user_role') == "1"){
                // user is admin
                $agent_id = (Yii::$app->request->post("agent_id") == NULL || Yii::$app->request->post("agent_id") == "" ? 0 : Yii::$app->request->post("agent_id"));
                $agent_ex = (Yii::$app->request->post("agentex") == NULL || Yii::$app->request->post("agentex") == "" ? 0 : Yii::$app->request->post("agentex"));
                $agent_name = $_POST['agentname'];
                $searchContact = (Yii::$app->request->post("contactNum") == NULL || Yii::$app->request->post("contactNum") == "" ? NULL : $this->removeFirstZeroFilterWhitespaceIfExistsInContact(Yii::$app->request->post("contactNum")));
                $queue_id = (Yii::$app->request->post("queue_id") == "" ? NULL : Yii::$app->request->post("queue_id"));
                if(Yii::$app->request->post("fromdate") == NULL || Yii::$app->request->post("fromdate") == ""){
                    $fromDate = date("Y-m-01");
                }else{
                    $fromDate = trim($_POST['fromdate'], " ");
                }

                if(Yii::$app->request->post("todate") == NULL || Yii::$app->request->post("todate") == ""){
                    $toDate = date("Y-m-d");
                }else{
                    $toDate = trim($_POST['todate'], " ");
                }
            }else if(Yii::$app->session->get('user_role') == "3"){
                // user is supervisor
                $agent_id = 0;
                $agent_ex = 0;
                $agent_name = "";
                $searchContact = NULL;
                $queue_id = Yii::$app->session->get('extQueueId');
                $fromDate = date("Y-m-01");
                $toDate = date("Y-m-d");
            } else if(Yii::$app->session->get('user_role') == "5"){
                // user is executive
                $agent_id = (Yii::$app->request->post("agent_id") == NULL || Yii::$app->request->post("agent_id") == "" ? 0 : Yii::$app->request->post("agent_id"));
                $agent_ex = (Yii::$app->request->post("agentex") == NULL || Yii::$app->request->post("agentex") == "" ? 0 : Yii::$app->request->post("agentex"));
                $agent_name = $_POST['agentname'];
                $searchContact = (Yii::$app->request->post("contactNum") == NULL || Yii::$app->request->post("contactNum") == "" ? NULL : $this->removeFirstZeroFilterWhitespaceIfExistsInContact(Yii::$app->request->post("contactNum")));
                $queue_id = (Yii::$app->request->post("queue_id") == "" ? NULL : Yii::$app->request->post("queue_id"));
                if (Yii::$app->request->post("fromdate") == NULL || Yii::$app->request->post("fromdate") == "") {
                    $fromDate = date("Y-m-01");
                } else {
                    $fromDate = trim($_POST['fromdate'], " ");
                }

                if (Yii::$app->request->post("todate") == NULL || Yii::$app->request->post("todate") == "") {
                    $toDate = date("Y-m-d");
                } else {
                    $toDate = trim($_POST['todate'], " ");
                }
            }
            
            $fromTime = (Yii::$app->request->post("fromTime") != NULL ? Yii::$app->request->post("fromTime") : "00:00:00");
            $toTime = (Yii::$app->request->post("toTime") != NULL ? Yii::$app->request->post("toTime") : "23:59:59");

            $selectedChannelsArray = (Yii::$app->request->post("channels") != "0" ? explode(",", Yii::$app->request->post("channels")) : NULL);

            $agentIdsInQueue = call_center_user::getAgentIdsFromQueueId($queue_id);
            $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueue($queue_id);
        }




        if ($_POST['tableType'] == "Answered") {

            $answered_calls_set = cdr::getAgentAnsweredCallsDataBetweenDatesForPagination($fromDate, $toDate, $fromTime, $toTime, $agent_ex, $searchContact,"Data",$extensionsInQueue, $selectedChannelsArray,$params['length'],$params['start'],
                $columns[$params['order'][0]['column']],$params['order'][0]['dir'],1);

            $countAnsweredData = cdr::getAgentAnsweredCallsDataBetweenDatesForPagination($fromDate, $toDate, $fromTime, $toTime, $agent_ex, $searchContact, "Count",$extensionsInQueue, $selectedChannelsArray,null,null,null,null,1);            

            $data = $this->getAnsweredCallsForDatatableJson($agent_ex,$answered_calls_set,$countAnsweredData,$agent_name);
            echo $data;

        }else if($_POST['tableType'] == "Outgoing"){

            $outgoing_calls = cdr::getOutgoingCallsListByDateForPaginationActiveQuery($agent_ex, $fromDate, $toDate, $fromTime, $toTime,$searchContact,$extensionsInQueue,"Data",$params['length'],$params['start'],$columns[$params['order'][0]['column']],$params['order'][0]['dir'],1);  
            $countOutgoingCallsData = cdr::getOutgoingCallsListByDateForPaginationActiveQuery($agent_ex, $fromDate, $toDate, $fromTime, $toTime,$searchContact,$extensionsInQueue,"Count",null,null,null,null,null);

            $data = $this->getOutgoingCallsForDatatableJson($agent_ex,$outgoing_calls,$countOutgoingCallsData,$agent_name);
            echo $data;
            

        }else if($_POST['tableType'] == "Missed"){

            $missedCalls = $this->getAllMissedCallsBetweenDates($fromDate, $toDate, $fromTime, $toTime,"Data", $searchContact,$extensionsInQueue,$params['length'],$params['start'],$columns[$params['order'][0]['column']],$params['order'][0]['dir'],0);            
        
            $countOfMissedCalls = cdr::getAllMissedCallsDataBetweenDatesForPagination($fromDate, $toDate, $fromTime, $toTime,$searchContact,"Count", $extensionsInQueue,null,null,null,null,null);

            $data = $this->getMissedCallsForDatatableJson($missedCalls,$countOfMissedCalls);
            echo $data;                

                
        }else if($_POST['tableType'] == "IVR"){

        $ivrCallsData = null;
        $countOfivrCallsData = 0;
        if ($agent_ex == 0) {

            $queueId = (Yii::$app->session->get('user_role') == '1' ? NULL : Yii::$app->session->get('extQueueId'));

            $ivrNumbers = Queue_ivr_number::getIvrNumbersOfQueue($queueId);
            if ($ivrNumbers != false) {
                $ivrCallsData = cdr::getIvrAnsweredCallsFromCustomersForPagination($fromDate, $toDate, $fromTime, $toTime, $searchContact, "Data", $ivrNumbers,$params['length'],$params['start'],$columns[$params['order'][0]['column']],$params['order'][0]['dir']);
                $countOfivrCallsData = cdr::getIvrAnsweredCallsFromCustomersForPagination($fromDate, $toDate, $fromTime, $toTime, $searchContact, "Count", $ivrNumbers,null,null,null,null);
            }
        }            

            $data = $this->getIVRCallsForDatatableJson($ivrCallsData,$countOfivrCallsData);
            echo $data;


        }else if($_POST['tableType'] == "VM"){

        $voiceMailsData = null;
        $countOfVoiceMailsData = 0;
        if ($agent_ex == 0) {
            // all agents, show voice mail numbers and ivr numbers calls
            $queueId = (Yii::$app->session->get('user_role') == '1' ? NULL : Yii::$app->session->get('extQueueId'));
            $voiceMailExtensions = Voicemail_extension::getVoicemailNumbersOfQueue($queueId);
            
            if ($voiceMailExtensions != false) {
                $voiceMailsData = cdr::getVoiceMailMessagesFromCustomersForPagination($fromDate, $toDate, $fromTime, $toTime, $searchContact, "Data", $voiceMailExtensions,$params['length'],$params['start'],$columns[$params['order'][0]['column']],$params['order'][0]['dir']);
                $countOfVoiceMailsData = cdr::getVoiceMailMessagesFromCustomersForPagination($fromDate, $toDate, $fromTime, $toTime, $searchContact, "Count", $voiceMailExtensions,null,null,null,null);                
                }

            }

            $data = $this->getVMCallsForDatatableJson($voiceMailsData,$countOfVoiceMailsData);
            echo $data;


        }else if($_POST['tableType'] == "Abandoned"){

                $agent_abandoned_calls = cdr::getAbandonedCallsOfAnAgentBetweenDatesForPagination($fromDate, $toDate, $fromTime, $toTime, $agent_ex, $searchContact, "Data", $extensionsInQueue,$params['length'],$params['start'],$columns[$params['order'][0]['column']],$params['order'][0]['dir']);          

                $countOfAgent_abandoned_calls = cdr::getAbandonedCallsOfAnAgentBetweenDatesForPagination($fromDate, $toDate, $fromTime, $toTime, $agent_ex, $searchContact, "Count", $extensionsInQueue,null,null,null,null);                    

            $data = $this->getAbandonedCallsForDatatableJson($agent_abandoned_calls,$countOfAgent_abandoned_calls);
            echo $data;

        }else if($_POST['tableType'] == "Login"){

                $logged_in_records = Logged_in_users::getLoginRecordsByDateWithPagination($agent_id, $fromDate, $toDate, $fromTime, $toTime, $agentIdsInQueue,"Data",$params['length'],$params['start'],$columns[$params['order'][0]['column']],$params['order'][0]['dir']);

                $countOflogged_in_records = Logged_in_users::getLoginRecordsByDateWithPagination($agent_id, $fromDate, $toDate, $fromTime, $toTime, $agentIdsInQueue,"Count",null,null,null,null);

            $data = $this->getLoginCallsForDatatableJson($logged_in_records,$countOflogged_in_records);
            echo $data;


        }else if($_POST['tableType'] == "DND"){                

                    $agent_dnd_records = Dnd_records::getDndRecordsByDateWithPagination($agent_id, $fromDate, $toDate, $fromTime, $toTime, $agentIdsInQueue,"Data",$params['length'],$params['start'],$columns[$params['order'][0]['column']],$params['order'][0]['dir']);

                    $countOfAgent_dnd_records = Dnd_records::getDndRecordsByDateWithPagination($agent_id, $fromDate, $toDate, $fromTime, $toTime, $agentIdsInQueue,"Count",null,null,null,null);                

            $data = $this->getDNDCallsForDatatableJson($agent_dnd_records,$countOfAgent_dnd_records);
            echo $data;

        }else{
                $result =  [
                      "draw"=> $_POST["draw"],
                      "recordsTotal"=> 0,
                      "recordsFiltered"=> 0,
                      "data"=> array()                         
                     ];
                echo json_encode($result);
        }
        

    }
    
    

    private function checkAudioFile($dateTime, $callType, $extensionNumber, $callerNumber, $callEndedTime){

        $routineLastExecutedTimeUnix = strtotime(Audiotransferroutinelog::getRoutineLastExecutedTime());

        $callEndedTimeUnix = strtotime($callEndedTime);
        if($callEndedTimeUnix <= $routineLastExecutedTimeUnix){
            return Ftp::checkIfAudioFileExists($dateTime, $callType, $extensionNumber, $callerNumber, $callEndedTime);
        }else{
            return "Recent";
        }
    }


    private function checkAudioFileAvilibility($acctId, $callEndedTime){

        $routineLastExecutedTimeUnix = strtotime(Audiotransferroutinelog::getRoutineLastExecutedTime());

        $callEndedTimeUnix = strtotime($callEndedTime);
        if($callEndedTimeUnix <= $routineLastExecutedTimeUnix){
            $result =  cdr::findRecordingFilenameExists($acctId);
            if ($result) {
                return "Found";
            }else{
                return "Not Found";
            }
        }else{
            return "Recent";
        }
    }    

    private function removeFirstZeroFilterWhitespaceIfExistsInContact($contactNumber){
        if($contactNumber[0] == "0"){
            // first number is zero
            $contactNumber =  substr($contactNumber, 1);
        }

        return preg_replace('/\s+/', '', $contactNumber);
    }




    private function getAnsweredCallsForDatatableJson($agent_ex,$answered_calls_set,$countAnsweredData,$agent_name){
         $answered_calls = array();
        if ($agent_ex == 0) {
            $agentNames = call_center_user::getAllAgentNamesAndExtensionsInAnArray();
        } else {
            $agentNames = NULL;
        }

        for ($i = 0; $i < count($answered_calls_set); $i++) {

            $agentName = NULL;
            if ($agentNames != NULL && count($agentNames) > 0) {
                // has registered agents and loading all agent reports
                foreach ($agentNames as $key) {
                    if ($key->voip_extension == $answered_calls_set[$i]['dst']) {
                        // match found
                        $agentName = $key->fullname;
                        break;
                    }
                }
            }

            $temp = array(
                'id' => $answered_calls_set[$i]['AcctId'],
                'start' => $answered_calls_set[$i]['start'],
                'src' => $answered_calls_set[$i]['src'],
                'answer' => $answered_calls_set[$i]['answer'],
                'duration' => $answered_calls_set[$i]['duration'],
                'name' => ($agentName != NULL ? $agentName : $answered_calls_set[$i]['dst']),
//            date_diff($date1, $date2),
                'end' => $answered_calls_set[$i]['end'],
                'voip' => $answered_calls_set[$i]['dst']
            );
            array_push($answered_calls, $temp);
        }

                $answeredCallsFinal = array();
                for ($i=0; $i < count($answered_calls); $i++) { 

                        if ($agent_ex == 0) {
                            $outputName = $answered_calls[$i]['name'];
                        }else{
                            $outputName = $agent_name;
                        }
                    
                    $fileExists = $this->checkAudioFile($answered_calls[$i]['start'], 'I', $answered_calls[$i]['voip'], $answered_calls[$i]['src'], $answered_calls[$i]['end']);
                    if($fileExists != "Found" && $fileExists != "Recent"){
                        $fileNameExistsInDb = $this->checkAudioFileAvilibility($answered_calls[$i]['id'],$answered_calls[$i]['end']);
                            if ($fileNameExistsInDb != "Found" && $fileNameExistsInDb != "Recent") {
                                $style = "style='color:red'";
                            }else{
                                $style = "style='color:black'";                            
                            }
                    }else{
                        $style = "style='color:black'";

                    }                     

                    $parameters = $answered_calls[$i]['id'].",I,".$answered_calls[$i]['voip'].",".$answered_calls[$i]['src'].",".str_replace(' ','+',$answered_calls[$i]['start']).",".str_replace(' ','+',$answered_calls[$i]['end']);

                    if ($agent_ex == 0) {
                        $temp = array(
                             "<span $style>".$answered_calls[$i]['src']."</span>",
                             "<span $style>".$answered_calls[$i]['answer']."</span>",
                             "<span $style>".($outputName != $answered_calls[$i]['voip'] ? $outputName.':'.$answered_calls[$i]['voip'] : $outputName)."</span>",
                             "<span $style>".$answered_calls[$i]['end']."</span>",
                             "<span $style>".$answered_calls[$i]['duration']." Sec"."</span>",
                             "<a title='$fileExists' data-toggle='modal' data-target='#audioPlaybackModal' class='btn btn-sm btn-success btn-block' onclick='playCallRecord(\"$parameters\")'>PLAY</a>"
                        );
                        array_push($answeredCallsFinal, $temp);
                    }else{
                        $temp = array(
                             "<span $style>".$answered_calls[$i]['src']."</span>",
                             "<span $style>".$answered_calls[$i]['answer']."</span>",
                             "<span $style>".$answered_calls[$i]['end']."</span>",
                             "<span $style>".$answered_calls[$i]['duration']." Sec"."</span>",
                             "<a title='$fileExists' data-toggle='modal' data-target='#audioPlaybackModal' class='btn btn-sm btn-success btn-block' onclick='playCallRecord(\"$parameters\")'>PLAY</a>"
                        );
                        array_push($answeredCallsFinal, $temp);                        
                    }

                }            
                
                $result =  [
                      "draw"=> $_POST["draw"],
                      "recordsTotal"=> $countAnsweredData,
                      "recordsFiltered"=> $countAnsweredData,
                      "data"=> $answeredCallsFinal                         
                     ];

                return json_encode($result); 
    }

    private function getOutgoingCallsForDatatableJson($agent_ex,$outgoing_calls,$countOutgoingCallsData,$agent_name){

        if ($agent_ex == 0) {
                for ($i = 0; $i < count($outgoing_calls); $i++) {
                    $name = $this->getExtensionFromCdrChannel($outgoing_calls[$i]['channel']);
                    if ($name) {
                        $outgoing_calls[$i]['name'] = $name;
                    } else {
                        $outgoing_calls[$i]['name'] = '';
                    }
                    $outgoing_calls[$i]['voip'] = $this->getExtensionNumberFromOutgoingCdrChannel($outgoing_calls[$i]['channel']);
                }
            } else {
                for ($i = 0; $i < count($outgoing_calls); $i++) {
                    $outgoing_calls[$i]['voip'] = $agent_ex;
                }
            }            

                $outgoing_calls_array = array();

                for ($i=0; $i < count($outgoing_calls) ; $i++) {

                        if ($agent_ex == 0) {
                            $outputName = $outgoing_calls[$i]['name'];
                        }else{
                            $outputName = $agent_name;
                        }


                        if($outgoing_calls[$i]['answer'] != NULL && $outgoing_calls[$i]['answer'] != ''){       

                            $fileExists = $this->checkAudioFile($outgoing_calls[$i]['start'], 'O', $outgoing_calls[$i]['voip'], $outgoing_calls[$i]['dst'], $outgoing_calls[$i]['end']);                                           
                            if($fileExists != "Found" && $fileExists != "Recent"){
                                $fileNameExistsInDb = $this->checkAudioFileAvilibility($outgoing_calls[$i]['AcctId'],$outgoing_calls[$i]['end']);
                                if ($fileNameExistsInDb != "Found" && $fileNameExistsInDb != "Recent") {
                                    $style = "style='color:red'";
                                }else{
                                    $style = "style='color:black'"; 
                                }
                            }else{
                                $style = "style='color:black'";
                            }
                        }else{
                            $fileExists = "No File";
                            $style = "style='color:black'";
                        }

                        // if($outgoing_calls[$i]['answer'] != NULL && $outgoing_calls[$i]['answer'] != ''){
                            if($outgoing_calls[$i]['disposition'] == 8){
                            $classOFBtn = "class = 'btn btn-sm btn-success btn-block'";
                        }else{
                            $classOFBtn = "class = 'btn btn-sm btn-success btn-block disabled'";
                        }


                    $parameters = $outgoing_calls[$i]['AcctId'].",O,".$outgoing_calls[$i]['voip'].",".$outgoing_calls[$i]['dst'].",".str_replace(' ','+',$outgoing_calls[$i]['start']).",".str_replace(' ','+',$outgoing_calls[$i]['end']);

                    if ($agent_ex == 0) {
                        $temp=array(
                            "<span $style>".$outgoing_calls[$i]['dst']."</span>",
                            "<span $style>".($outputName != $outgoing_calls[$i]['voip'] ? $outputName.':'.$outgoing_calls[$i]['voip'] : $outputName)."</span>",
                            "<span $style>".$outgoing_calls[$i]['start']."</span>",
                            "<span $style>".$outgoing_calls[$i]['answer']."</span>",
                            "<span $style>".$outgoing_calls[$i]['end']."</span>",
                            "<span $style>".$outgoing_calls[$i]['duration']." Sec"."</span>",
                            "<a title='$fileExists' data-toggle='modal' data-target='#audioPlaybackModal' $classOFBtn' onclick='playCallRecord(\"$parameters\")'>PLAY</a>"
                        );
                        array_push($outgoing_calls_array, $temp);
                    }else{
                        $temp=array(
                            "<span $style>".$outgoing_calls[$i]['dst']."</span>",
                            "<span $style>".$outgoing_calls[$i]['start']."</span>",
                            "<span $style>".$outgoing_calls[$i]['answer']."</span>",
                            "<span $style>".$outgoing_calls[$i]['end']."</span>",
                            "<span $style>".$outgoing_calls[$i]['duration']." Sec"."</span>",
                            "<a title='$fileExists' data-toggle='modal' data-target='#audioPlaybackModal' $classOFBtn' onclick='playCallRecord(\"$parameters\")'>PLAY</a>"
                        );
                        array_push($outgoing_calls_array, $temp);                        
                    }
                }                

                $result =  [
                      "draw"=> $_POST["draw"],
                      "recordsTotal"=> $countOutgoingCallsData,
                      "recordsFiltered"=> $countOutgoingCallsData,
                      "data"=> $outgoing_calls_array                         
                     ];
                return json_encode($result);

    }


    private function getMissedCallsForDatatableJson($missedCalls,$countOfMissedCalls){

                $missedCalls_array = array();

                for ($i=0; $i < count($missedCalls) ; $i++) { 
                    $temp=array(
                        $missedCalls[$i]['date'],
                        $missedCalls[$i]['time'],
                        $missedCalls[$i]['caller_num'],
                    );
                    array_push($missedCalls_array, $temp);
                }


                $result =  [
                      "draw"=> $_POST["draw"],
                      "recordsTotal"=> $countOfMissedCalls,
                      "recordsFiltered"=> $countOfMissedCalls,
                      "data"=> $missedCalls_array                         
                     ];
                return json_encode($result);
    }


    private function getIVRCallsForDatatableJson($ivrCallsData,$countOfivrCallsData){

                $ivrCallsData_array = array();

                for ($i=0; $i < count($ivrCallsData) ; $i++) { 
                    $temp=array(
                        $ivrCallsData[$i]['date'],
                        $ivrCallsData[$i]['time'],
                        $ivrCallsData[$i]['caller_num'],
                    );
                    array_push($ivrCallsData_array, $temp);
                }

                $result =  [
                      "draw"=> $_POST["draw"],
                      "recordsTotal"=> $countOfivrCallsData,
                      "recordsFiltered"=> $countOfivrCallsData,
                      "data"=> $ivrCallsData_array                         
                     ];
                return json_encode($result);
    }


    private function getVMCallsForDatatableJson($voiceMailsData,$countOfVoiceMailsData){
                $voiceMailsData_array = array();

                for ($i=0; $i < count($voiceMailsData) ; $i++) { 
                    $temp=array(
                        $voiceMailsData[$i]['date'],
                        $voiceMailsData[$i]['time'],
                        $voiceMailsData[$i]['caller_num'],
                    );
                    array_push($voiceMailsData_array, $temp);
                }

                $result =  [
                      "draw"=> $_POST["draw"],
                      "recordsTotal"=> $countOfVoiceMailsData,
                      "recordsFiltered"=> $countOfVoiceMailsData,
                      "data"=> $voiceMailsData_array                         
                     ];
                return json_encode($result);
    }


    private function getAbandonedCallsForDatatableJson($agent_abandoned_calls,$countOfAgent_abandoned_calls){

                $agent_abandoned_calls_array = array();

                for ($index = 0; $index < count($agent_abandoned_calls); $index++) {

                    $datetime = $agent_abandoned_calls[$index]['end'];
                    $dateTimeExploded = explode(" ", $datetime);
                    $temp=array(
                        $agent_abandoned_calls[$index]['src'],
                        $dateTimeExploded[0],
                        $dateTimeExploded[1]
                    );
                    array_push($agent_abandoned_calls_array, $temp);
                    }


                $result =  [
                      "draw"=> $_POST["draw"],
                      "recordsTotal"=> $countOfAgent_abandoned_calls,
                      "recordsFiltered"=> $countOfAgent_abandoned_calls,
                      "data"=> $agent_abandoned_calls_array                         
                     ];

                return json_encode($result);
    }


    private function getLoginCallsForDatatableJson($logged_in_records,$countOflogged_in_records){

               $logged_in_records_array = array();

                for ($i=0; $i < count($logged_in_records) ; $i++) { 

                    $login_time_sig = $logged_in_records[$i]['logged_in_time'];
                    $login_date = date('Y-m-d', $login_time_sig);
                    $login_time = date('H:i:s', $login_time_sig);

                    $logged_time_sig = $logged_in_records[$i]['time_signature'];
                    $logged_date = date('Y-m-d', $logged_time_sig);
                    $logged_time = date('H:i:s', $logged_time_sig);

                    $temp=array(
                         $login_date,
                         $login_time,
                         $logged_time
                    );

                    array_push($logged_in_records_array, $temp);
                }

                $result =  [
                      "draw"=> $_POST["draw"],
                      "recordsTotal"=> $countOflogged_in_records,
                      "recordsFiltered"=> $countOflogged_in_records,
                      "data"=> $logged_in_records_array                         
                     ];
                return json_encode($result);

    }


    private function getDNDCallsForDatatableJson($agent_dnd_records,$countOfAgent_dnd_records){

                $agent_dnd_records_array = array();

                for ($i=0; $i < count($agent_dnd_records) ; $i++) { 
                                $date_time = $agent_dnd_records[$i]['timestamp'];
                                $explods = explode(" ", $date_time);                
                    $temp=array(
                                    $explods[0],
                                    $explods[1],
                                    $agent_dnd_records[$i]['dnd_mode']
                    );
                    array_push($agent_dnd_records_array, $temp);
                }

                $result =  [
                      "draw"=> $_POST["draw"],
                      "recordsTotal"=> $countOfAgent_dnd_records,
                      "recordsFiltered"=> $countOfAgent_dnd_records,
                      "data"=> $agent_dnd_records_array                         
                     ];

                return json_encode($result);
    }


}