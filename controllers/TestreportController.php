<?php

/**
 * TestReportController for integrate and test reporting tools for the application.
 *
 * @since 2017-06-26
 * @author Prabath
 * 
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\FileHelper;

class TestreportController extends Controller {
    
    public function actionReport() {
        
        $data = [10,40,30,10,5,60];
        return $this->render('reportPage',['dataSet'=> $data]);
    }
    
}
