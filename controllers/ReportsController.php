<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use app\models\Customer;
use app\models\Help_requests;
use app\models\Agent_requests;
use app\models\cdr;
use app\models\Logged_in_users;
use app\models\abandon_calls;
use app\models\call_center_user;
use app\models\Dnd_records;
use app\models\Call_records;
use app\models\Extension_queue;
use app\models\Ext_queue_intermediate;
use app\models\Voicemail_extension;
use app\models\Queue_ivr_number;
use app\models\Call_channel;
use app\models\Extqueue_callchannel_intermediate;
use app\models\Audiotransferroutinelog;
use yii\helpers\Json;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReportsController
 *
 * @since 28/07/2017
 * @author Prabath
 */
class ReportsController extends Controller {

    /**
     * <b>Overrides parent class controller constructor</b>
     * 
     * @param type $id
     * @param type $module
     * @param type $config
     * 
     * @author Sandun
     * @since 2017-09-25
     */
    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
        set_time_limit(300);
    }

    /**
     * <b>Render the Reports</b>
     * 
     * @return render the performance overview
     * 
     * @author Vikum
     * @since 2017-07-07
     * 
     */
    public function actionPerformanceoverview() {
        $user_id = Yii::$app->session->get('user_id');
        $agent_ex = Yii::$app->session->get('voip');

        $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueue(Yii::$app->session->get("extQueueId"));

        $dailyBreakTime = Agent_requests::getDailyBreakTime($user_id);
        $dailyWorkTime = Logged_in_users::getDailyWorkedTime($user_id);

        $monthlyBreakTime = Agent_requests::getThisMonthBreakTime($user_id); // Total breaks taken by the agent this month
        $monthlyWorkTime = Logged_in_users::getThisMonthWorkedTime($user_id); // Total working hours by an agent of the current month

        $breaksDailyData = [$dailyBreakTime, $dailyWorkTime];
//        $breaksFullData = [0.5, 2.5];
        $breaksFullData = [$monthlyBreakTime, $monthlyWorkTime]; // Report data for the current month word graph
        /**
         * @author supun
         * @since 2017/08/16
         */
//        $answered_calls = cdr::getAnsweredCallsListByUser($agent_ex);
        $answered_calls_data = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-01"), date("Y-m-d"), "00:00:00", "23:59:59", $agent_ex, NULL, "Data", $extensionsInQueue, NULL);
//        $answered_calls  = Call_records::getAllAgentCallRecords($agent_id);
        $answered_calls = array();
        foreach ($answered_calls_data as $key) {
//            $date1 = date_create($key['call_date']);
//            $date2 = date_create($key['call_end_time']);
//            $dteStart = new DateTime($key['call_date']);
//            $dteEnd = new DateTime($key['call_end_time']);
//            $dteDiff  = $dteStart->diff($dteEnd); 

            $temp = array(
                'start' => $key['start'],
                'src' => $key['src'],
                'answer' => $key['answer'],
                'duration' => $key['duration'],
//            date_diff($date1, $date2),
                'end' => $key['end'],
                'voip' => $agent_ex
            );
            array_push($answered_calls, $temp);
        }

        $agent_answered_calls_count = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), "00:00:00", "23:59:59", $agent_ex, NULL, "Count", $extensionsInQueue, NULL); // All Calls Answered by the agent in current day
        $all_answered_calls_count = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), "00:00:00", "23:59:59", 0, NULL, "Count", $extensionsInQueue, NULL); // All Calls answered in the current day

        $all_agent_answered_calls_count_thismonth = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-01"), date("Y-m-d"), "00:00:00", "23:59:59", $agent_ex, NULL, "Count", $extensionsInQueue, NULL);
        $all_answered_calls_count_thisMonth = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-01"), date("Y-m-d"), "00:00:00", "23:59:59", 0, NULL, "Count", $extensionsInQueue, NULL); // All the calls answered by the call center current month
        $monthlyCallsData = [$all_agent_answered_calls_count_thismonth, $all_answered_calls_count_thisMonth];

        $outgoing_calls = cdr::getOutgoingCallsListByDate($agent_ex, date("Y-m-01"), date("Y-m-d"), "00:00:00", "23:59:59", NULL); //cdr::getOutgoingCallsListByUser($agent_ex);

        $agent_abandoned_calls = cdr::getAbandonedCallsOfAnAgentBetweenDates(date("Y-m-01"), date("Y-m-d"), "00:00:00", "23:59:59", $agent_ex, NULL, "Data", $extensionsInQueue);

//        $agent_answered_calls_count = count($answered_calls);
//        $all_answered_calls_count = cdr::getAnsweredCallsCount();
        $logged_in_records = Logged_in_users::getLoginRecordsByDate($user_id, date("Y-m-01"), date("Y-m-d"), "00:00:00", "23:59:59", NULL); //Logged_in_users::getLoginRecordsByUser($user_id);

        return $this->render('performanceOverview', ['breakFullData' => $breaksFullData, 'breakDailyData' => $breaksDailyData, 'answered_calls' => $answered_calls, 'outgoing_calls' => $outgoing_calls, 'agent_answered_calls_count' => $agent_answered_calls_count, 'all_answered_calls_count' => $all_answered_calls_count, 'agent_abandoned_calls' => $agent_abandoned_calls, 'login_records' => $logged_in_records, 'monthlyCallsData' => $monthlyCallsData]);
    }

    /**
     * <b>Render the Reports</b>
     * 
     * @return render a specific agent performance overview page
     * 
     * @author Prabath
     * @since 03-08-2017
     * 
     */
    public function actionPerformanceoverviewbyagent() {
        $agent_id = $_POST['agent_id'];
        $agent_ex = $_POST['agent_ex'];
        $agent_name = $_POST['agent_name'];
        $extensionsInQueue = (Yii::$app->session->get("user_role") == '1' ? NULL : Ext_queue_intermediate::getAllExtensionsOfTheQueue(Yii::$app->session->get("extQueueId")));

        if ($extensionsInQueue == NULL && $agent_id != 0) {
            // individual agent listing,
            $queueId = Ext_queue_intermediate::getExtensionQueueIdOfVOIPExtension($agent_ex);
            $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueue($queueId);
        }

        $dailyBreakTime = Agent_requests::getDailyBreakTime($agent_id); // Sum of break times taken by the agent of the current day, Also done with all user
        $dailyWorkTime = Logged_in_users::getDailyWorkedTime($agent_id); // Sum of working hours of the agent on Current Day, Done for All users

        $monthlyBreakTime = Agent_requests::getThisMonthBreakTime($agent_id); // Total breaks taken by the agent this month, Done for all users
        $monthlyWorkTime = Logged_in_users::getThisMonthWorkedTime($agent_id); // Total working hours by an agent of the current month, Done for all users

        $breaksDailyData = [$dailyBreakTime, $dailyWorkTime]; // Report data for the current work time graph
        $breaksFullData = [$monthlyBreakTime, $monthlyWorkTime]; // Report data for the current month word graph
//========================================================================================================================================
//        $answered_calls = cdr::getAnsweredCallsListByUser($agent_ex); // This need to be changed to get data from CallRecords table *** NOT MINE
        $answered_calls_data = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-01"), date("Y-m-d"), $agent_ex, NULL, "Data", $extensionsInQueue);
//        $answered_calls  = Call_records::getAllAgentCallRecords($agent_id);
        $answered_calls = array();
        if ($agent_ex == 0) {
            $agentNames = call_center_user::getAllAgentNamesAndExtensionsInAnArray();
        } else {
            $agentNames = NULL;
        }
        for ($i = 0; $i < count($answered_calls_data); $i++) {

            $agentName = NULL;
            if ($agentNames != NULL && count($agentNames) > 0) {
                // has registered agents and loading all agent reports
                foreach ($agentNames as $key) {
                    if ($key->voip_extension == $answered_calls_data[$i]['dst']) {
                        // match found
                        $agentName = $key->name;
                        break;
                    }
                }
            }

            $temp = array(
                'id' => $answered_calls_data[$i]['AcctId'],
                'start' => $answered_calls_data[$i]['start'],
                'src' => $answered_calls_data[$i]['src'],
                'answer' => $answered_calls_data[$i]['answer'],
                'duration' => $answered_calls_data[$i]['duration'],
                'name' => ($agentName != NULL ? $agentName : $answered_calls_data[$i]['dst']),
                'end' => $answered_calls_data[$i]['end'],
                'voip' => $answered_calls_data[$i]['dst']
            );
            array_push($answered_calls, $temp);
        }

        $agent_answered_calls_count = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), $agent_ex, NULL, "Count", $extensionsInQueue); // All Calls Answered by the agent in current day
        $all_answered_calls_count = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), 0, NULL, "Count", $extensionsInQueue); // All Calls answered in the current day

        if ($agent_ex == 0) {
            $missedCalls = $this->getAllMissedCallsBetweenDates(NULL, NULL, NULL, NULL);
            $todayMissedCallsCount = cdr::getAllMissedCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), NULL, "Count", NULL);
        } else {
            $missedCalls = null;
            $todayMissedCallsCount = null;
        }

        $all_agent_answered_calls_count_thismonth = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-01"), date("Y-m-d"), $agent_ex, NULL, "Count", $extensionsInQueue);
        $all_answered_calls_count_thisMonth = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-01"), date("Y-m-d"), 0, NULL, "Count", $extensionsInQueue); // All the calls answered by the call center current month
        $monthlyCallsData = [$all_agent_answered_calls_count_thismonth, $all_answered_calls_count_thisMonth];
        //
//========================================================================================================================================        

        $outgoing_calls = cdr::getOutgoingCallsListByUser($agent_ex); // This takes all outgoing calls taken by the agent
        if ($agent_ex == 0) {
            for ($i = 0; $i < count($outgoing_calls); $i++) {
                $name = $this->getExtensionFromCdrChannel($outgoing_calls[$i]['channel']);
                if ($name) {
                    $outgoing_calls[$i]['name'] = $name;
                } else {
                    $outgoing_calls[$i]['name'] = '';
                }
                $outgoing_calls[$i]['voip'] = $this->getExtensionNumberFromOutgoingCdrChannel($outgoing_calls[$i]['channel']);
            }
        }


        $agent_abandoned_calls = cdr::getAbandonedCallsOfAnAgentBetweenDates(date("Y-m-01"), date("Y-m-d"), $agent_ex, NULL, "Data", $extensionsInQueue);

        //$agent_abandoned_calls = array();
//        $agent_answered_calls_count = count($answered_calls);
//        $all_answered_calls_count = cdr::getAnsweredCallsCount(); // This need to be changed to get all answered calls from CallRecords

        $logged_in_records = Logged_in_users::getLoginRecordsByUser($agent_id);

        $agent_dnd_records = Dnd_records::getDndRecordsByUser($agent_id);

        $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';

        return $this->render('supervisorReportsPage', ['breakFullData' => $breaksFullData, 'breakDailyData' => $breaksDailyData, 'answered_calls' => $answered_calls, 'outgoing_calls' => $outgoing_calls, 'agent_answered_calls_count' => $agent_answered_calls_count, 'all_answered_calls_count' => $all_answered_calls_count, 'agent_answered_calls_count' => $agent_answered_calls_count, 'agent_abandoned_calls' => $agent_abandoned_calls, 'login_records' => $logged_in_records, 'agent_extension' => $agent_ex, 'agent_name' => $agent_name, 'agent_dnd_reocrds' => $agent_dnd_records, 'allCallsInTheMonth' => $all_answered_calls_count_thisMonth, 'allCallsForAgentThisMonth' => $all_agent_answered_calls_count_thismonth, 'monthlyCallsData' => $monthlyCallsData, 'ftpParams' => $ftpParams, 'agent_id' => $agent_id, 'missed_calls' => $missedCalls, 'todayMissedCallsCount' => $todayMissedCallsCount]);
    }

    public function actionPerformanceoverviewbydate() {
        if (Yii::$app->session->has('user_id') == false) {
            // has no active session
            $this->redirect('index.php?r=user/login_view');
        } else if (Yii::$app->session->get('user_role') == '2' || Yii::$app->session->get('user_role') == '4') {
            //logged in user is an agent or a super agent
            $this->redirect('index.php?r=user/login_view');
        }

        $agent_id = $_POST['agent_id'];
        $agent_ex = $_POST['agentex'];
        $agent_name = $_POST['agentname'];
        $searchContact = ($_POST['contactNum'] == "" || $_POST['contactNum'] == NULL ? NULL : $this->removeFirstZeroFilterWhitespaceIfExistsInContact($_POST['contactNum']));
        $fromDate = trim($_POST['fromdate'], " ");
        $toDate = trim($_POST['todate'], " ");

        $fromTime = (Yii::$app->request->post("fromTime") != NULL ? Yii::$app->request->post("fromTime") : "00:00:00");
        $toTime = (Yii::$app->request->post("toTime") != NULL ? Yii::$app->request->post("toTime") : "23:59:59");

        $selectedChannelsArray = (Yii::$app->request->post("channels") != "0" ? explode(",", Yii::$app->request->post("channels")) : NULL);

        $extensionsInQueue = (Yii::$app->session->get("user_role") == '1' || Yii::$app->session->get("user_role") == '5' ? NULL : Ext_queue_intermediate::getAllExtensionsOfTheQueue(Yii::$app->session->get("extQueueId")));
        $agentIdsInQueue = (Yii::$app->session->get("user_role") == '1' || Yii::$app->session->get("user_role") == '5' ? NULL : call_center_user::getAgentIdsFromQueueId(Yii::$app->session->get("extQueueId")));

        if (Yii::$app->session->get('user_role') == '5') {
            //user is executive 
            $queuesIds = Extension_queue::getAllAvailableQueuesIdOfSelectedExecutive(Yii::$app->session->get('user_id'));
            $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueueArray($queuesIds);
            $agentIdsInQueue = call_center_user::getAgentsOfSelectedQueues($queuesIds);
        }


        if ($extensionsInQueue == NULL && $agent_id != 0) {
            // individual agent listing,
            $queueId = Ext_queue_intermediate::getExtensionQueueIdOfVOIPExtension($agent_ex);
            $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueue($queueId);
            $agentIdsInQueue = call_center_user::getAgentIdsFromQueueId($queueId);
        }

        $dateRange = array($fromDate, $toDate);
        $dailyBreakTime = Agent_requests::getDailyBreakTime($agent_id); // Sum of break times taken by the agent of the current day, Done for all users
        $dailyWorkTime = Logged_in_users::getDailyWorkedTime($agent_id); // Sum of working hours of the agent on Current Day, Done for all users


        $dailyBreakTimeReal = $this->convertToHoursMins($dailyBreakTime, '%02d hours %02d minutes');
        $dailyWorkTimeReal = $this->convertToHoursMins($dailyWorkTime, '%02d hours %02d minutes');
        $breaksDailyData = [$dailyBreakTime, $dailyWorkTime, "$dailyBreakTimeReal", "$dailyWorkTimeReal"];


        $totalBreakTimeOfTheTimePeriod = Agent_requests::getBreakTimeByDate($agent_id, $fromDate, $toDate, $fromTime, $toTime); // Total break requests of the given time, Done for all users
        $totalWorkTimeOfTheTimePeriod = Logged_in_users::getDailyWorkedTimeInGivenRange($agent_id, $fromDate, $toDate, $fromTime, $toTime); // Total work time of the given time period, Done for all users
        $totalBreakTimeOfTheTimePeriodRealTime = $this->convertToHoursMins($totalBreakTimeOfTheTimePeriod, '%02d hours %02d minutes');
        $totalWorkTimeOfTheTimePeriodRealTime = $this->convertToHoursMins($totalWorkTimeOfTheTimePeriod, '%02d hours %02d minutes');
        $breaksFullData = [$totalBreakTimeOfTheTimePeriod, $totalWorkTimeOfTheTimePeriod, $totalBreakTimeOfTheTimePeriodRealTime, $totalWorkTimeOfTheTimePeriodRealTime]; // This is where full break set will come
        $answered_calls = array();
        $answered_calls_set = cdr::getAgentAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agent_ex, $searchContact, "Data", $extensionsInQueue, $selectedChannelsArray);

        if ($agent_ex == 0) {
            $agentNames = call_center_user::getAllAgentNamesAndExtensionsInAnArray();
        } else {
            $agentNames = NULL;
        }

        for ($i = 0; $i < count($answered_calls_set); $i++) {

            $agentName = NULL;
            if ($agentNames != NULL && count($agentNames) > 0) {
                // has registered agents and loading all agent reports
                foreach ($agentNames as $key) {
                    if ($key->voip_extension == $answered_calls_set[$i]['dst']) {
                        // match found
                        $agentName = $key->fullname;
                        break;
                    }
                }
            }

            $temp = array(
                'id' => $answered_calls_set[$i]['AcctId'],
                'start' => $answered_calls_set[$i]['start'],
                'src' => $answered_calls_set[$i]['src'],
                'answer' => $answered_calls_set[$i]['answer'],
                'duration' => $answered_calls_set[$i]['duration'],
                'name' => ($agentName != NULL ? $agentName : $answered_calls_set[$i]['dst']),
//            date_diff($date1, $date2),
                'end' => $answered_calls_set[$i]['end'],
                'voip' => $answered_calls_set[$i]['dst']
            );
            array_push($answered_calls, $temp);
        }
        $agent_answered_calls_count = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), '00:00:00', '23:59:59', $agent_ex, NULL, "Count", $extensionsInQueue, $selectedChannelsArray); // All Calls Answered by the agent in current day

        $all_answered_calls_count = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), '00:00:00', '23:59:59', 0, NULL, "Count", $extensionsInQueue, $selectedChannelsArray); // All Calls answered in the current day

        $allCallsAnsweredByCountInGivenPeriod = cdr::getAgentAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, 0, $searchContact, "Count", $extensionsInQueue, $selectedChannelsArray); //Call_records::allCallsAnsweredInGivenPeriod('0', $fromDate, $toDate);
        if ($agent_ex != "0") {
            // single agent
            $allCallsAnsweredByAgentCountInGivenPeriod = cdr::getAgentAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agent_ex, $searchContact, "Count", $extensionsInQueue, $selectedChannelsArray); //Call_records::allCallsAnsweredInGivenPeriod($agent_id, $fromDate, $toDate);
            $totalCallsAnsweredSet = array($allCallsAnsweredByAgentCountInGivenPeriod, $allCallsAnsweredByCountInGivenPeriod); // [agent, all]
        } else {
            $totalMissedCallsInfo = cdr::getAllMissedCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $searchContact, "Count", $extensionsInQueue);
            $totalCallsAnsweredSet = array($totalMissedCallsInfo, $allCallsAnsweredByCountInGivenPeriod); // [agent, all]
        }

        if ($searchContact == NULL || $searchContact == '') {
            $outgoing_calls = cdr::getOutgoingCallsListByDate($agent_ex, $fromDate, $toDate, $fromTime, $toTime, $extensionsInQueue);
        } else {
            $outgoing_calls = cdr::getOutgoingCallsListByDateAndContactNumber($agent_ex, $fromDate, $toDate, $fromTime, $toTime, $searchContact, $extensionsInQueue);
        }
        if ($agent_ex == 0) {
            for ($i = 0; $i < count($outgoing_calls); $i++) {
                $name = $this->getExtensionFromCdrChannel($outgoing_calls[$i]['channel']);
                if ($name) {
                    $outgoing_calls[$i]['name'] = $name;
                } else {
                    $outgoing_calls[$i]['name'] = '';
                }
                $outgoing_calls[$i]['voip'] = $this->getExtensionNumberFromOutgoingCdrChannel($outgoing_calls[$i]['channel']);
            }
        } else {
            for ($i = 0; $i < count($outgoing_calls); $i++) {
                $outgoing_calls[$i]['voip'] = $agent_ex;
            }
        }

        //$agent_abandoned_calls = abandon_calls::getAbandonedCallsByAgent($agent_ex);
        $agent_abandoned_calls = cdr::getAbandonedCallsOfAnAgentBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agent_ex, $searchContact, "Data", $extensionsInQueue);
        $missedCalls = $this->getAllMissedCallsBetweenDates($fromDate, $toDate, $fromTime, $toTime, $searchContact, $extensionsInQueue);
        $todayMissedCallsCount = cdr::getAllMissedCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), '00:00:00', '23:59:59', NULL, "Count", $extensionsInQueue);
//        $agent_answered_calls_count = count($answered_calls);
//        $all_answered_calls_count = cdr::getAnsweredCallsCount();
        $logged_in_records = Logged_in_users::getLoginRecordsByDate($agent_id, $fromDate, $toDate, $fromTime, $toTime, $agentIdsInQueue);

        $agent_dnd_records = Dnd_records::getDndRecordsByDate($agent_id, $fromDate, $toDate, $fromTime, $toTime, $agentIdsInQueue);

        $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';

        $voiceMailsData = null;
        $ivrCallsData = null;
        if ($agent_ex == 0) {
            // all agents, show voice mail numbers and ivr numbers calls
            $queueId = (Yii::$app->session->get('user_role') == '1' ? NULL : Yii::$app->session->get('extQueueId'));
            $voiceMailExtensions = Voicemail_extension::getVoicemailNumbersOfQueue($queueId);
            if ($voiceMailExtensions != false) {
                $voiceMailsData = cdr::getVoiceMailMessagesFromCustomers($fromDate, $toDate, $fromTime, $toTime, $searchContact, "Data", $voiceMailExtensions);
            }

            $ivrNumbers = Queue_ivr_number::getIvrNumbersOfQueue($queueId);
            if ($ivrNumbers != false) {
                $ivrCallsData = cdr::getIvrAnsweredCallsFromCustomers($fromDate, $toDate, $fromTime, $toTime, $searchContact, "Data", $ivrNumbers);
            }
        }


        return $this->render('supervisorReportsByDate', [
                    'breakFullData' => $breaksFullData,
                    'breakDailyData' => $breaksDailyData,
                    'answered_calls' => $answered_calls,
                    'outgoing_calls' => $outgoing_calls,
                    'agent_answered_calls_count' => $agent_answered_calls_count,
                    'all_answered_calls_count' => $all_answered_calls_count,
                    'agent_answered_calls_count' => $agent_answered_calls_count,
                    'agent_abandoned_calls' => $agent_abandoned_calls,
                    'login_records' => $logged_in_records,
                    'agent_extension' => $agent_ex,
                    'agent_name' => $agent_name,
                    'agent_dnd_reocrds' => $agent_dnd_records,
                    'dateRange' => $dateRange,
                    'callAnswerCounts' => $totalCallsAnsweredSet,
                    'ftpParams' => $ftpParams,
                    'agent_id' => $agent_id,
                    'missed_calls' => $missedCalls,
                    'todayMissedCallsCount' => $todayMissedCallsCount,
                    'contactNumber' => $searchContact,
                    'vmData' => $voiceMailsData,
                    'ivrCallsData' => $ivrCallsData
        ]);
    }

    /**
     * <b>Render the Reports</b>
     * 
     * @return render a supervisor reports page
     * 
     * @author Prabath
     * @since 03-08-2017
     * 
     */
    public function actionSupervisorreports() {
        if (Yii::$app->session->has('user_id') == false) {
            // has no active session
            $this->redirect('index.php?r=user/login_view');
        } else if (Yii::$app->session->get('user_role') == '2' || Yii::$app->session->get('user_role') == '4') {
            //logged in user is an agent or a super agent
            $this->redirect('index.php?r=user/login_view');
        }

        // Assign queue lists for executive user
        if (Yii::$app->session->get('user_role') == '5') {
            $extenstionQueues = Extension_queue::getAllAvailableQueuesDataOfSelectedExecutive(Yii::$app->session->get('user_id'));
            $agentsList = call_center_user::getAgentsOfSelectedQueues($extenstionQueues);
        }else{
            $extenstionQueues = Extension_queue::getAllAvailableQueuesData();            
            $agentsList = call_center_user::getUserData(Yii::$app->session->get("user_role"));
        }

        if (Yii::$app->session->get('user_role') == '3') {
            // Agent is a supervisor
            $channelsInfo = $this->getChannelsInfoOfQueue(Yii::$app->session->get("extQueueId"));
        } else {
            $channelsInfo = NULL;
        }
        return $this->render('supervisorReports', ['agents' => $agentsList, "queues" => $extenstionQueues, "channelsInfo" => $channelsInfo]);
    }


    /**
     * <b>Render the Reports</b>
     * 
     * @return render a Agent reports page
     * 
     * @author Vinothan
     * @since 17-04-2019
     * 
     */
    public function actionAgentreports() {
        if (Yii::$app->session->has('user_id') == false) {
            $this->redirect('index.php?r=user/login_view');
        }else{
            $agentData = call_center_user::getUser(Yii::$app->session->get("user_id"));
        return $this->render('agentReports',["channelsInfo" => [],'agents' => $agentData]);
        }
    }    

    private function convertToHoursMins($time, $format = '%02d:%02d') {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    /**
     * 
     * @return type
     * @since 2017-10-30
     * @author Sandun
     */
    public function actionSummery() {

        $agent_id = $_GET['agent_id'];
        $agent_ex = $_GET['agentex'];
        $agent_name = $_GET['agentname'];
        $searchContact = $_GET['contactNum'];
//        $searchContact = '';
        $fromDate = $_GET['fromdate'];
        $toDate = $_GET['todate'];
        $queueId = (Yii::$app->request->get("queue_id") == NULL || Yii::$app->request->get("queue_id") == "" ? NULL : Yii::$app->request->get("queue_id"));

        $fromTime = (Yii::$app->request->get("fromTime") != NULL ? Yii::$app->request->get("fromTime") : "00:00:00");
        $toTime = (Yii::$app->request->get("toTime") != NULL ? Yii::$app->request->get("toTime") : "23:59:59");

        $totalBreakTimeOfTheTimePeriod = Agent_requests::getBreakTimeByDate($agent_id, $fromDate, $toDate, $fromTime, $toTime); // Total break requests of the given time, Done for all users
        $totalWorkTimeOfTheTimePeriod = Logged_in_users::getDailyWorkedTimeInGivenRange($agent_id, $fromDate, $toDate, $fromTime, $toTime); // Total work time of the given time period, Done for all users
        $totalBreakTimeOfTheTimePeriodRealTime = $this->convertToHoursMins($totalBreakTimeOfTheTimePeriod, '%02d hours %02d minutes');
        $totalWorkTimeOfTheTimePeriodRealTime = $this->convertToHoursMins($totalWorkTimeOfTheTimePeriod, '%02d hours %02d minutes');
        $dateRange = array($fromDate, $toDate);

        $selectedChannelsArray = (Yii::$app->request->get("channels") != "0" ? explode(",", Yii::$app->request->get("channels")) : NULL);

        $extensionsInQueue = (Yii::$app->session->get("user_role") == '1' || Yii::$app->session->get("user_role") == '5' ? NULL : Ext_queue_intermediate::getAllExtensionsOfTheQueue(Yii::$app->session->get("extQueueId")));
        $agentIdsInQueue = (Yii::$app->session->get("user_role") == '1' || Yii::$app->session->get("user_role") == '5' ? NULL : call_center_user::getAgentIdsFromQueueId(Yii::$app->session->get("extQueueId")));

        if (Yii::$app->session->get('user_role') == '5') {
            //user is executive
            $queuesIds = Extension_queue::getAllAvailableQueuesIdOfSelectedExecutive(Yii::$app->session->get('user_id'));
            $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueueArray($queuesIds);
            $agentIdsInQueue = call_center_user::getAgentsOfSelectedQueues($queuesIds);
        }

        $agentName = "";
//        $totalCallsMissed = 0;
        $totalCallsAbandoned = 0;
//        $totalCallsReceived = 0;//cdr::getTotalCallsReceivedCountByDate($fromDate, $toDate);
        $totalOutgoingCalls = 0;
        $totalCallsMissed = cdr::getAllMissedCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, NULL, "Count", $extensionsInQueue); //$totalCallsReceived - $totalCallsAnswered;
        //cdr::getOutgoingCallsCountByUser(0);
        // $totalCallsAnswered = cdr::getAllAnsweredCallsCountBetweenDates($fromDate, $toDate, $extensionsInQueue);
        $noOfAgents = call_center_user::getRegisteredAgentsCountBetweenDates($fromDate, $toDate);
        $agentDndOnTimes = 0;
        if ($agent_id == 0) {
            // all agents
            $agentName = "All agents";
//            $totalCallsAnswered = Call_records::answeredCallsCountBetweenDates('0', $fromDate, $toDate);
            $totalOutgoingCalls = count(cdr::getOutgoingCallsListByDate('0', $fromDate, $toDate, $fromTime, $toTime, $extensionsInQueue));
            $totalCallsAnswered = cdr::getAgentAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, 0, NULL, "Count", $extensionsInQueue, $selectedChannelsArray);
            $totalCallsReceived = $totalCallsMissed + $totalCallsAnswered;
        } else {
            // single agent
            $agentName = $agent_name . "'s";
            $totalCallsAbandoned = cdr::getAbandonedCallsOfAnAgentBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agent_ex, NULL, "Count", $extensionsInQueue); //count(abandon_calls::getAbandonedCallsByAgentByDate($agent_ex, $agent_id, $fromDate, $toDate));
            $totalOutgoingCalls = count(cdr::getOutgoingCallsListByDate($agent_ex, $fromDate, $toDate, $fromTime, $toTime, $extensionsInQueue));
            $totalCallsAnswered = cdr::getAgentAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agent_ex, NULL, "Count", $extensionsInQueue, $selectedChannelsArray);
            $agentDndOnTimes = Dnd_records::noOfTimesPutDndOnBetweenDates($agent_id, $fromDate, $toDate);
            $totalCallsReceived = $totalCallsAnswered + $totalCallsAbandoned;
        }



        $agentInformation = array(
            "agentName" => $agentName,
            "agentExt" => $agent_ex,
            "noOfAgents" => $noOfAgents,
        );
        $summeryFieldsCalls = array(
            'totalCallsReceived' => $totalCallsReceived,
            'totalCallsAnswered' => $totalCallsAnswered,
            'totalCallsUnanswered' => 0,
            'totalCallsAbandoned' => $totalCallsAbandoned,
            'totalCallsMissed' => $totalCallsMissed,
            'totalCallsOutgoing' => $totalOutgoingCalls
        );
        $summeryFieldsWorkTime = array(
            'totalTimeWorked' => $totalWorkTimeOfTheTimePeriodRealTime,
            'totalBreakTime' => $totalBreakTimeOfTheTimePeriodRealTime,
            'dndOnCount' => $agentDndOnTimes
        );
        return $this->render("supervisorSummeryReport", ["calls" => $summeryFieldsCalls, "work" => $summeryFieldsWorkTime, "agentInfo" => $agentInformation]);
    }

    /**
     * <b>Extract caller extension from channel</b>
     * <p>This function will extract agent extension from passed channel data and return user name of the agent</p>
     * 
     * @author Vikum
     * @since 27-10-2017
     * 
     */
    private function getExtensionFromCdrChannel($channel) {
        preg_match("'SIP/(.*?)-'si", $channel, $match);
        if ($match) {
            if ($match[1] != '' && strlen($match[1]) < 5) {
                $userName = call_center_user::getUserInfoFromVOIPExtension($match[1]);
                return $userName['fullname'];
//                return implode(" ",$userName);
//                return $match[1];
            } else {
                return $match[1];
            }
        } else {
            return 0;
        }
    }

    private function getExtensionNumberFromOutgoingCdrChannel($channel) {
        preg_match("'SIP/(.*?)-'si", $channel, $match);
        if ($match) {
            if ($match[1] != '' && strlen($match[1]) < 5) {
                return $match[1];
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * 
     * @param type $from
     * @param type $to
     * @param type $contactNumber
     * @param type $extensionsInQueue
     * @return array
     * 
     * @modified Sandun 2018-01-23
     * @param type $fromTime
     * @param type $toTime
     */
    private function getAllMissedCallsBetweenDates($from, $to, $fromTime, $toTime, $contactNumber, $extensionsInQueue) {
        $result = array();
        $missedCalls = array();
        if ($from && $to) {
            // return data for the period
            if ($contactNumber == NULL) {
                $missedCalls = cdr::getAllMissedCallsDataBetweenDates($from, $to, $fromTime, $toTime, NULL, "Data", $extensionsInQueue);
            } else {
                $missedCalls = cdr::getAllMissedCallsDataBetweenDates($from, $to, $fromTime, $toTime, $contactNumber, "Data", $extensionsInQueue);
            }
        } else {
            // return data for the month
            $missedCalls = cdr::getAllMissedCallsDataBetweenDates(date("Y-m-01"), date("Y-m-d"), '00:00:00', '23:59:59', NULL, "Data", $extensionsInQueue);
        }

        if ($missedCalls != NULL && count($missedCalls) > 0) {
            // have missed calls
            for ($i = 0; $i < count($missedCalls); $i++) {
                $callDateExplode = explode(" ", $missedCalls[$i]['end']);
                $missedCallInfo = array(
                    "date" => $callDateExplode[0],
                    "time" => $callDateExplode[1],
                    "caller_num" => $missedCalls[$i]['src']
                );

                array_push($result, $missedCallInfo);
                $missedCallInfo = array();
            }

            return $result;
        } else {
            // no missed calls
            return null;
        }
    }

    /**
     * 
     * @return type
     * 
     * @author Sandun
     * @since 2017-12-20
     */
    public function actionPerformanceoverviewbyqueue() {
        if (Yii::$app->session->has('user_id') == false) {
            // has no active session
            $this->redirect('index.php?r=user/login_view');
        } else if (Yii::$app->session->get('user_role') == '2' || Yii::$app->session->get('user_role') == '4') {
            //logged in user is an agent or a super agent
            $this->redirect('index.php?r=user/login_view');
        }

        if (Yii::$app->session->get('user_role') == "1") {
            // user is admin
            $agent_id = (Yii::$app->request->post("agent_id") == NULL || Yii::$app->request->post("agent_id") == "" ? 0 : Yii::$app->request->post("agent_id"));
            $agent_ex = (Yii::$app->request->post("agentex") == NULL || Yii::$app->request->post("agentex") == "" ? 0 : Yii::$app->request->post("agentex"));
            $agent_name = $_POST['agentname'];
            $searchContact = (Yii::$app->request->post("contactNum") == NULL || Yii::$app->request->post("contactNum") == "" ? NULL : $this->removeFirstZeroFilterWhitespaceIfExistsInContact(Yii::$app->request->post("contactNum")));
            $queue_id = (Yii::$app->request->post("queue_id") == "" ? NULL : Yii::$app->request->post("queue_id"));
            if (Yii::$app->request->post("fromdate") == NULL || Yii::$app->request->post("fromdate") == "") {
                $fromDate = date("Y-m-01");
            } else {
                $fromDate = trim($_POST['fromdate'], " ");
            }

            if (Yii::$app->request->post("todate") == NULL || Yii::$app->request->post("todate") == "") {
                $toDate = date("Y-m-d");
            } else {
                $toDate = trim($_POST['todate'], " ");
            }
        } else if (Yii::$app->session->get('user_role') == "3") {
            // user is supervisor
            $agent_id = 0;
            $agent_ex = 0;
            $agent_name = "";
            $searchContact = NULL;
            $queue_id = Yii::$app->session->get('extQueueId');
            $fromDate = date("Y-m-01");
            $toDate = date("Y-m-d");
        } else if (Yii::$app->session->get('user_role') == "5") {
            // user is executive
            $agent_id = (Yii::$app->request->post("agent_id") == NULL || Yii::$app->request->post("agent_id") == "" ? 0 : Yii::$app->request->post("agent_id"));
            $agent_ex = (Yii::$app->request->post("agentex") == NULL || Yii::$app->request->post("agentex") == "" ? 0 : Yii::$app->request->post("agentex"));
            $agent_name = $_POST['agentname'];
            $searchContact = (Yii::$app->request->post("contactNum") == NULL || Yii::$app->request->post("contactNum") == "" ? NULL : $this->removeFirstZeroFilterWhitespaceIfExistsInContact(Yii::$app->request->post("contactNum")));
            $queue_id = (Yii::$app->request->post("queue_id") == "" ? NULL : Yii::$app->request->post("queue_id"));
            if (Yii::$app->request->post("fromdate") == NULL || Yii::$app->request->post("fromdate") == "") {
                $fromDate = date("Y-m-01");
            } else {
                $fromDate = trim($_POST['fromdate'], " ");
            }

            if (Yii::$app->request->post("todate") == NULL || Yii::$app->request->post("todate") == "") {
                $toDate = date("Y-m-d");
            } else {
                $toDate = trim($_POST['todate'], " ");
            }
        }

        $fromTime = (Yii::$app->request->post("fromTime") != NULL ? Yii::$app->request->post("fromTime") : "00:00:00");
        $toTime = (Yii::$app->request->post("toTime") != NULL ? Yii::$app->request->post("toTime") : "23:59:59");

        $selectedChannelsArray = (Yii::$app->request->post("channels") != "0" ? explode(",", Yii::$app->request->post("channels")) : NULL);

        $agentIdsInQueue = call_center_user::getAgentIdsFromQueueId($queue_id);
        $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueue($queue_id);

        $dateRange = array($fromDate, $toDate);
        $dailyBreakTime = Agent_requests::getDailyBreakTimesByQueue($agentIdsInQueue); // Sum of break times taken by the agent of the current day, Done for all users
        $dailyWorkTime = Logged_in_users::getDailyWorkedTimeByQueue($agentIdsInQueue); // Sum of working hours of the agent on Current Day, Done for all users


        $dailyBreakTimeReal = $this->convertToHoursMins($dailyBreakTime, '%02d hours %02d minutes');
        $dailyWorkTimeReal = $this->convertToHoursMins($dailyWorkTime, '%02d hours %02d minutes');
        $breaksDailyData = [$dailyBreakTime, $dailyWorkTime, "$dailyBreakTimeReal", "$dailyWorkTimeReal"];


        $totalBreakTimeOfTheTimePeriod = Agent_requests::getBreakTimeByQueueBetweenDates($agentIdsInQueue, $fromDate, $toDate, $fromTime, $toTime); // Total break requests of the given time, Done for all users
        $totalWorkTimeOfTheTimePeriod = Logged_in_users::getWorkedTimeByQueueBetweenDates($agentIdsInQueue, $fromDate, $toDate, $fromTime, $toTime); // Total work time of the given time period, Done for all users
        $totalBreakTimeOfTheTimePeriodRealTime = $this->convertToHoursMins($totalBreakTimeOfTheTimePeriod, '%02d hours %02d minutes');
        $totalWorkTimeOfTheTimePeriodRealTime = $this->convertToHoursMins($totalWorkTimeOfTheTimePeriod, '%02d hours %02d minutes');
        $breaksFullData = [$totalBreakTimeOfTheTimePeriod, $totalWorkTimeOfTheTimePeriod, $totalBreakTimeOfTheTimePeriodRealTime, $totalWorkTimeOfTheTimePeriodRealTime]; // This is where full break set will come
        $answered_calls = array();
        $answered_calls_set = cdr::getAgentAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agent_ex, $searchContact, "Data", $extensionsInQueue, $selectedChannelsArray);

        if ($agent_ex == 0) {
            $agentNames = call_center_user::getAllAgentNamesAndExtensionsInAnArray();
        } else {
            $agentNames = NULL;
        }

        for ($i = 0; $i < count($answered_calls_set); $i++) {

            $agentName = NULL;
            if ($agentNames != NULL && count($agentNames) > 0) {
                // has registered agents and loading all agent reports
                foreach ($agentNames as $key) {
                    if ($key->voip_extension == $answered_calls_set[$i]['dst']) {
                        // match found
                        $agentName = $key->fullname;
                        break;
                    }
                }
            }

            $temp = array(
                'id' => $answered_calls_set[$i]['AcctId'],
                'start' => $answered_calls_set[$i]['start'],
                'src' => $answered_calls_set[$i]['src'],
                'answer' => $answered_calls_set[$i]['answer'],
                'duration' => $answered_calls_set[$i]['duration'],
                'name' => ($agentName != NULL ? $agentName : $answered_calls_set[$i]['dst']),
//            date_diff($date1, $date2),
                'end' => $answered_calls_set[$i]['end'],
                'voip' => $answered_calls_set[$i]['dst']
            );
            array_push($answered_calls, $temp);
        }
        $agent_answered_calls_count = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), '00:00:00', '23:59:59', $agent_ex, NULL, "Count", $extensionsInQueue, $selectedChannelsArray); // All Calls Answered by the agent in current day

        $all_answered_calls_count = cdr::getAgentAnsweredCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), '00:00:00', '23:59:59', 0, NULL, "Count", $extensionsInQueue, $selectedChannelsArray); // All Calls answered in the current day

        $allCallsAnsweredByCountInGivenPeriod = cdr::getAgentAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, 0, $searchContact, "Count", $extensionsInQueue, $selectedChannelsArray); //Call_records::allCallsAnsweredInGivenPeriod('0', $fromDate, $toDate);
        if ($agent_ex != "0") {
            // single agent
            $allCallsAnsweredByAgentCountInGivenPeriod = cdr::getAgentAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agent_ex, $searchContact, "Count", $extensionsInQueue, $selectedChannelsArray); //Call_records::allCallsAnsweredInGivenPeriod($agent_id, $fromDate, $toDate);
            $totalCallsAnsweredSet = array($allCallsAnsweredByAgentCountInGivenPeriod, $allCallsAnsweredByCountInGivenPeriod); // [agent, all]
        } else {
            $totalMissedCallsInfo = cdr::getAllMissedCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $searchContact, "Count", $extensionsInQueue);
            $totalCallsAnsweredSet = array($totalMissedCallsInfo, $allCallsAnsweredByCountInGivenPeriod); // [agent, all]
        }

        if ($searchContact == NULL || $searchContact == '') {
            $outgoing_calls = cdr::getOutgoingCallsListByDate($agent_ex, $fromDate, $toDate, $fromTime, $toTime, $extensionsInQueue);
        } else {
            $outgoing_calls = cdr::getOutgoingCallsListByDateAndContactNumber($agent_ex, $fromDate, $toDate, $fromTime, $toTime, $searchContact, $extensionsInQueue);
        }
        if ($agent_ex == 0) {
            for ($i = 0; $i < count($outgoing_calls); $i++) {
                $name = $this->getExtensionFromCdrChannel($outgoing_calls[$i]['channel']);
                if ($name) {
                    $outgoing_calls[$i]['name'] = $name;
                } else {
                    $outgoing_calls[$i]['name'] = '';
                }
                $outgoing_calls[$i]['voip'] = $this->getExtensionNumberFromOutgoingCdrChannel($outgoing_calls[$i]['channel']);
            }
        }

        //$agent_abandoned_calls = abandon_calls::getAbandonedCallsByAgent($agent_ex);
        $agent_abandoned_calls = cdr::getAbandonedCallsOfAnAgentBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agent_ex, $searchContact, "Data", $extensionsInQueue);
        $missedCalls = $this->getAllMissedCallsBetweenDates($fromDate, $toDate, $fromTime, $toTime, $searchContact, $extensionsInQueue);
        $todayMissedCallsCount = cdr::getAllMissedCallsDataBetweenDates(date("Y-m-d"), date("Y-m-d"), '00:00:00', '23:59:59', NULL, "Count", $extensionsInQueue);
//        $agent_answered_calls_count = count($answered_calls);
//        $all_answered_calls_count = cdr::getAnsweredCallsCount();
        $logged_in_records = Logged_in_users::getLoginRecordsByDate($agent_id, $fromDate, $toDate, $fromTime, $toTime, $agentIdsInQueue);

        $agent_dnd_records = Dnd_records::getDndRecordsByDate($agent_id, $fromDate, $toDate, $fromTime, $toTime, $agentIdsInQueue);

        $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';

        $voiceMailsData = null;
        $ivrCallsData = null;
        if ($agent_ex == 0) {
            // all agents, show voice mail numbers and ivr numbers
            $voiceMailExtensions = Voicemail_extension::getVoicemailNumbersOfQueue($queue_id);
            if ($voiceMailExtensions != false) {
                $voiceMailsData = cdr::getVoiceMailMessagesFromCustomers($fromDate, $toDate, $fromTime, $toTime, $searchContact, 'Data', $voiceMailExtensions);
            }

            $ivrNumbers = Queue_ivr_number::getIvrNumbersOfQueue($queue_id);
            if ($ivrNumbers != false) {
                $ivrCallsData = cdr::getIvrAnsweredCallsFromCustomers($fromDate, $toDate, $fromTime, $toTime, $searchContact, "Data", $ivrNumbers);
            }
        }


        return $this->render('supervisorReportsByDate', [
                    'breakFullData' => $breaksFullData,
                    'breakDailyData' => $breaksDailyData,
                    'answered_calls' => $answered_calls,
                    'outgoing_calls' => $outgoing_calls,
                    'agent_answered_calls_count' => $agent_answered_calls_count,
                    'all_answered_calls_count' => $all_answered_calls_count,
                    'agent_answered_calls_count' => $agent_answered_calls_count,
                    'agent_abandoned_calls' => $agent_abandoned_calls,
                    'login_records' => $logged_in_records,
                    'agent_extension' => $agent_ex,
                    'agent_name' => $agent_name,
                    'agent_dnd_reocrds' => $agent_dnd_records,
                    'dateRange' => $dateRange,
                    'callAnswerCounts' => $totalCallsAnsweredSet,
                    'ftpParams' => $ftpParams,
                    'agent_id' => $agent_id,
                    'missed_calls' => $missedCalls,
                    'todayMissedCallsCount' => $todayMissedCallsCount,
                    'contactNumber' => $searchContact,
                    'vmData' => $voiceMailsData,
                    'ivrCallsData' => $ivrCallsData
        ]);
    }

    public function actionSummerybyqueue() {
        $agent_id = (Yii::$app->request->get("agent_id") == NULL || Yii::$app->request->get("agent_id") == "" ? 0 : Yii::$app->request->get("agent_id"));
        $agent_ex = (Yii::$app->request->get("agentex") == NULL || Yii::$app->request->get("agentex") == "" ? 0 : Yii::$app->request->get("agentex"));
        $agent_name = $_GET['agentname'];
        $searchContact = (Yii::$app->request->get("contactNum") == NULL || Yii::$app->request->get("contactNum") == "" ? NULL : Yii::$app->request->get("contactNum"));
        $queue_id = (Yii::$app->request->get("queue_id") == "" ? NULL : Yii::$app->request->get("queue_id"));
        if (Yii::$app->request->get("fromdate") == NULL || Yii::$app->request->get("fromdate") == "") {
            $fromDate = date("Y-m-01");
        } else {
            $fromDate = trim($_GET['fromdate'], " ");
        }

        if (Yii::$app->request->get("todate") == NULL || Yii::$app->request->get("todate") == "") {
            $toDate = date("Y-m-d");
        } else {
            $toDate = trim($_GET['todate'], " ");
        }

        $fromTime = (Yii::$app->request->get("fromTime") != NULL ? Yii::$app->request->get("fromTime") : "00:00:00");
        $toTime = (Yii::$app->request->get("toTime") != NULL ? Yii::$app->request->get("toTime") : "23:59:59");

        $selectedChannelsArray = (Yii::$app->request->get("channels") != "0" ? explode(",", Yii::$app->request->get("channels")) : NULL);

        $agentIdsInQueue = call_center_user::getAgentIdsFromQueueId($queue_id);
        $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueue($queue_id);


        $totalBreakTimeOfTheTimePeriod = Agent_requests::getBreakTimeByQueueBetweenDates($agentIdsInQueue, $fromDate, $toDate, $fromTime, $toTime); // Total break requests of the given time, Done for all users
        $totalWorkTimeOfTheTimePeriod = Logged_in_users::getWorkedTimeByQueueBetweenDates($agentIdsInQueue, $fromDate, $toDate, $fromTime, $toTime); // Total work time of the given time period, Done for all users
        $totalBreakTimeOfTheTimePeriodRealTime = $this->convertToHoursMins($totalBreakTimeOfTheTimePeriod, '%02d hours %02d minutes');
        $totalWorkTimeOfTheTimePeriodRealTime = $this->convertToHoursMins($totalWorkTimeOfTheTimePeriod, '%02d hours %02d minutes');
        $dateRange = array($fromDate, $toDate);


        $agentName = "";
//        $totalCallsMissed = 0;
        $totalCallsAbandoned = 0;
//        $totalCallsReceived = 0;//cdr::getTotalCallsReceivedCountByDate($fromDate, $toDate);
        $totalOutgoingCalls = 0;
        $totalCallsMissed = cdr::getAllMissedCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, NULL, "Count", $extensionsInQueue); //$totalCallsReceived - $totalCallsAnswered;
        //cdr::getOutgoingCallsCountByUser(0);
        // $totalCallsAnswered = cdr::getAllAnsweredCallsCountBetweenDates($fromDate, $toDate, $extensionsInQueue);
        $noOfAgents = call_center_user::getRegisteredAgentsCountBetweenDates($fromDate, $toDate);
        $agentDndOnTimes = 0;
        if ($agent_id == 0 && $queue_id == NULL) {
            // all agents of all queues
            $agentName = "All agents";
//            $totalCallsAnswered = Call_records::answeredCallsCountBetweenDates('0', $fromDate, $toDate);
            $totalOutgoingCalls = count(cdr::getOutgoingCallsListByDate('0', $fromDate, $toDate, $fromTime, $toTime, NULL));
            $totalCallsAnswered = cdr::getAgentAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, '0', NULL, "Count", NULL, $selectedChannelsArray);
            $totalCallsReceived = $totalCallsMissed + $totalCallsAnswered;
        } else if ($agent_id == 0 && $queue_id != NULL) {
            // all agents of the queue
            $agentName = $agent_name . " queue ";
//            $totalCallsAnswered = Call_records::answeredCallsCountBetweenDates('0', $fromDate, $toDate);
            $totalOutgoingCalls = count(cdr::getOutgoingCallsListByDate('0', $fromDate, $toDate, $fromTime, $toTime, $extensionsInQueue));
            $totalCallsAnswered = cdr::getAgentAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, '0', NULL, "Count", $extensionsInQueue, $selectedChannelsArray);
            $totalCallsReceived = $totalCallsMissed + $totalCallsAnswered;
        } else {
            // single agent
            $agentName = $agent_name . "'s";
            $totalCallsAbandoned = cdr::getAbandonedCallsOfAnAgentBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agent_ex, NULL, "Count", NULL); //count(abandon_calls::getAbandonedCallsByAgentByDate($agent_ex, $agent_id, $fromDate, $toDate));
            $totalOutgoingCalls = count(cdr::getOutgoingCallsListByDate($agent_ex, $fromDate, $toDate, $fromTime, $toTime, NULL));
            // $totalCallsAnswered = cdr::getAgentAnsweredCallsCountBetweenDates($fromDate, $toDate, $agent_ex);
            $totalCallsAnswered = cdr::getAgentAnsweredCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agent_ex, NULL, "Count", NULL, $selectedChannelsArray);
            $agentDndOnTimes = Dnd_records::noOfTimesPutDndOnBetweenDates($agent_id, $fromDate, $toDate);
            $totalCallsReceived = $totalCallsAnswered + $totalCallsAbandoned;
        }



        $agentInformation = array(
            "agentName" => $agentName,
            "agentExt" => $agent_ex,
            "noOfAgents" => $noOfAgents,
        );
        $summeryFieldsCalls = array(
            'totalCallsReceived' => $totalCallsReceived,
            'totalCallsAnswered' => $totalCallsAnswered,
            'totalCallsUnanswered' => 0,
            'totalCallsAbandoned' => $totalCallsAbandoned,
            'totalCallsMissed' => $totalCallsMissed,
            'totalCallsOutgoing' => $totalOutgoingCalls
        );
        $summeryFieldsWorkTime = array(
            'totalTimeWorked' => $totalWorkTimeOfTheTimePeriodRealTime,
            'totalBreakTime' => $totalBreakTimeOfTheTimePeriodRealTime,
            'dndOnCount' => $agentDndOnTimes
        );
        return $this->render("supervisorSummeryReport", [
                    "calls" => $summeryFieldsCalls,
                    "work" => $summeryFieldsWorkTime,
                    "agentInfo" => $agentInformation
        ]);
    }

    /**
     *
     * @since 2018-1-25
     * @author Sandun
     */
    public function actionAgentcallcenterinformation() {
        if (!Yii::$app->session->has('user_id')) {
            // not an logged in user
            return $this->redirect('index.php?r=user/login_view');
        }

        $agent_id = Yii::$app->session->get('user_id');
        $agent_ex = Yii::$app->session->get('voip');
        $extQueueId = Yii::$app->session->get("extQueueId");

        // $agentIdsInQueue = call_center_user::getAgentIdsFromQueueId($extQueueId);
        $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueue($extQueueId);
        $missedCalls = $this->getAllMissedCallsBetweenDates(date('Y-m-d', strtotime("-7 day")), date("Y-m-d"), "00:00:00", "23:59:59", NULL, $extensionsInQueue);
        $voiceMailExtensions = Voicemail_extension::getVoicemailNumbersOfQueue($extQueueId);
        if ($voiceMailExtensions != false) {
            $voiceMailsData = cdr::getVoiceMailMessagesFromCustomers(date('Y-m-d', strtotime("-7 day")), date("Y-m-d"), "00:00:00", "23:59:59", NULL, 'Data', $voiceMailExtensions);
        } else {
            $voiceMailsData = NULL;
        }

        $ivrNumbers = Queue_ivr_number::getIvrNumbersOfQueue($extQueueId);
        if ($ivrNumbers != false) {
            $ivrCallsData = cdr::getIvrAnsweredCallsFromCustomers(date('Y-m-d', strtotime("-7 day")), date("Y-m-d"), "00:00:00", "23:59:59", NULL, "Data", $ivrNumbers);
        } else {
            $ivrCallsData = NULL;
        }
        return $this->render("agentCallCenterReports", [
                    'missed_calls' => $missedCalls,
                    'vmData' => $voiceMailsData,
                    'ivrCallsData' => $ivrCallsData
        ]);
    }

    /**
     * <b>Find the call channels for the queue</b>
     * <p></p>
     */
    public function actionFindchannelsforqueue() {
        $queueId = Yii::$app->request->get("queueId");
        $channelIdsOfQueue = $this->getChannelsInfoOfQueue($queueId);
        if ($channelIdsOfQueue != false) {
            echo Json::encode($channelIdsOfQueue);
        } else {
            echo "0";
        }
    }

    private function getChannelsInfoOfQueue($queueId) {
        $channelIdsOfQueue = Extqueue_callchannel_intermediate::getChannelIdsofTheQueue($queueId);
        if (count($channelIdsOfQueue) > 0) {
            $channelsInfoArray = array();
            foreach ($channelIdsOfQueue as $key) {
                $channelInformation = Call_channel::getChannelInfoFomId($key->channel_id);
                array_push($channelsInfoArray, $channelInformation);
            }
            return $channelsInfoArray;
        } else {
            return false;
        }
    }

    /*
     *
     * @modified Sandun 2018-3-29
     * @description Added a regex to replace any whitespace in the string if theres any in addition to the previous 1st character 0 filter
     */

    private function removeFirstZeroFilterWhitespaceIfExistsInContact($contactNumber) {
        if ($contactNumber[0] == "0") {
            // first number is zero
            $contactNumber = substr($contactNumber, 1);
        }

        return preg_replace('/\s+/', '', $contactNumber);
    }

    /**
     * One time functions
     */
    public function actionOutputcsv() {

        $dateFrom = "2018-09-01";
        $timeFrom = "00:00:00";
        $dateTo = "2018-09-30";
        $timeTo = "23:59:59";
        $this->outputAnsweredCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, NULL);
        $this->outputOutboundCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, NULL);
        $this->outputMissedCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, NULL);
        echo "<br> CSV writing completed";
    }

    /**
     * 
     * @return type
     * 
     * @author Nuwan
     * @since 26-02-2019
     * @description if the queue number is not empty then generate csv file according to the queue number.
     */
    public function actionSavefilecsvbyqueue() {

        if (Yii::$app->session->has('user_id') == false) {
            // has no active session
            $this->redirect('index.php?r=user/login_view');
        } else if (Yii::$app->session->get('user_role') == '2' || Yii::$app->session->get('user_role') == '4') {
            //logged in user is an agent or a super agent
            $this->redirect('index.php?r=user/login_view');
        }

        $incoming = yii::$app->request->get("incoming");

        if (Yii::$app->session->get('user_role') == "1") {
            // user is admin
            $agentId = (Yii::$app->request->get("agentId") == NULL || Yii::$app->request->get("agentId") == "" ? 0 : Yii::$app->request->get("agentId"));
            $agentEx = (Yii::$app->request->get("agentEx") == NULL || Yii::$app->request->get("agentEx") == "" ? 0 : Yii::$app->request->get("agentEx"));
            $queueId = (Yii::$app->request->get("queueId") == "" ? NULL : Yii::$app->request->get("queueId"));
            if (Yii::$app->request->get("fromDate") == NULL || Yii::$app->request->get("fromDate") == "") {
                $dateFrom = date("Y-m-01");
            } else {
                $dateFrom = trim($_GET['fromDate'], " ");
            }

            if (Yii::$app->request->get("toDate") == NULL || Yii::$app->request->get("toDate") == "") {
                $dateTo = date("Y-m-d");
            } else {
                $dateTo = trim($_GET['toDate'], " ");
            }
        } else if (Yii::$app->session->get('user_role') == "3") {
            // user is supervisor
            $agentId = 0;
            $agentEx = 0;
            $queueId = Yii::$app->session->get('extQueueId');
            $dateFrom = date("Y-m-01");
            $dateTo = date("Y-m-d");
        } else if (Yii::$app->session->get('user_role') == "5") {
            // user is executive
            $agentId = (Yii::$app->request->get("agentId") == NULL || Yii::$app->request->get("agentId") == "" ? 0 : Yii::$app->request->get("agentId"));
            $agentEx = (Yii::$app->request->get("agentEx") == NULL || Yii::$app->request->get("agentEx") == "" ? 0 : Yii::$app->request->get("agentEx"));
            $queueId = (Yii::$app->request->get("queueId") == "" ? NULL : Yii::$app->request->get("queueId"));
            if (Yii::$app->request->get("fromDate") == NULL || Yii::$app->request->get("fromDate") == "") {
                $dateFrom = date("Y-m-01");
            } else {
                $dateFrom = trim($_GET['fromDate'], " ");
            }

            if (Yii::$app->request->get("toDate") == NULL || Yii::$app->request->get("toDate") == "") {
                $dateTo = date("Y-m-d");
            } else {
                $dateTo = trim($_GET['toDate'], " ");
            }
        }

        $timeFrom = (Yii::$app->request->get("fromTime") != NULL ? Yii::$app->request->get("fromTime") : "00:00:00");
        $timeTo = (Yii::$app->request->get("toTime") != NULL ? Yii::$app->request->get("toTime") : "23:59:59");

        $selectedChannelsArray = (Yii::$app->request->get("selectedChannels") != "0" ? explode(",", Yii::$app->request->get("selectedChannels")) : NULL);

        $agentIdsInQueue = call_center_user::getAgentIdsFromQueueId($queueId);

        $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueue($queueId);


        //Check the type of the request and sends the params to the relavant function
        if ($incoming == "incoming") {
            //Agent incoming calls
            $this->outputAnsweredCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $selectedChannelsArray, $extensionsInQueue);
        } elseif ($incoming == "outgoing") {
            //Agent outgoing calls
            $this->outputOutboundCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $extensionsInQueue);
        } elseif ($incoming == "missed") {
            //Agent missed calls
            $this->outputMissedCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $extensionsInQueue);
        } elseif ($incoming == "ivrReceived") {
            //Agent IVR calls  
            $this->outputIvrReceivedCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $extensionsInQueue);
        } elseif ($incoming == "voiceMail") {
            //Agent voice mail calls
            $this->outputVoiceMailCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $extensionsInQueue);
        } elseif ($incoming == "abondondCalls") {
            //Agent abondond calls
            $this->outputAbondondCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $extensionsInQueue);
        } elseif ($incoming == "logged") {
            //Agent looged in calls
            $this->outputLoggedCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $agentId, $extensionsInQueue, $agentIdsInQueue);
        } else {
            //Agnt DND records are here
            $this->outputDndCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $agentId, $extensionsInQueue, $agentIdsInQueue);
        }
    }

//Save relavant csv file in excel format
    public function actionSavefilecsv() {

        if (Yii::$app->session->has('user_id') == false) {
            // has no active session
            $this->redirect('index.php?r=user/login_view');
        } else if (Yii::$app->session->get('user_role') == '2' || Yii::$app->session->get('user_role') == '4') {
            //logged in user is an agent or a super agent
            $this->redirect('index.php?r=user/login_view');
        }

        $dateFrom = trim($_GET['fromDate'], " ");
        $dateTo = trim($_GET['toDate'], " ");
        $incoming = yii::$app->request->get("incoming");
        $agentEx = yii::$app->request->get("agentEx");
        $agentId = yii::$app->request->get("agentId");
        $timeFrom = (Yii::$app->request->get("fromTime") != NULL ? Yii::$app->request->get("fromTime") : "00:00:00");
        $timeTo = (Yii::$app->request->get("toTime") != NULL ? Yii::$app->request->get("toTime") : "23:59:59");
        $queueId = yii::$app->request->get("queueId");


        $selectedChannelsArray = (Yii::$app->request->get("selectedChannels") != "0" ? explode(",", Yii::$app->request->get("selectedChannels")) : NULL);

        $extensionsInQueue = (Yii::$app->session->get("user_role") == '1' || Yii::$app->session->get("user_role") == '5' ? NULL : Ext_queue_intermediate::getAllExtensionsOfTheQueue(Yii::$app->session->get("extQueueId")));
        $agentIdsInQueue = (Yii::$app->session->get("user_role") == '1' || Yii::$app->session->get("user_role") == '5' ? NULL : call_center_user::getAgentIdsFromQueueId(Yii::$app->session->get("extQueueId")));

        if (Yii::$app->session->get('user_role') == '5') {
            //user is executive 
            $queuesIds = Extension_queue::getAllAvailableQueuesIdOfSelectedExecutive(Yii::$app->session->get('user_id'));
            $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueueArray($queuesIds);
            $agentIdsInQueue = call_center_user::getAgentsOfSelectedQueues($queuesIds);
        }

        if ($extensionsInQueue == NULL && $agentId != 0) {
            // individual agent listing,
            $queueId = Ext_queue_intermediate::getExtensionQueueIdOfVOIPExtension($agentEx);
            $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueue($queueId);
            $agentIdsInQueue = call_center_user::getAgentIdsFromQueueId($queueId);
        }

//Check the type of the request and sends the params to the relavant function
        if ($incoming == "incoming") {
            //Agent incoming calls
            $this->outputAnsweredCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $selectedChannelsArray, $extensionsInQueue);
        } elseif ($incoming == "outgoing") {
            //Agent outgoing calls
            $this->outputOutboundCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $extensionsInQueue);
        } elseif ($incoming == "missed") {
            //Agent missed calls
            $this->outputMissedCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $extensionsInQueue);
        } elseif ($incoming == "ivrReceived") {
            //Agent IVR calls  
            $this->outputIvrReceivedCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $extensionsInQueue);
        } elseif ($incoming == "voiceMail") {
            //Agent voice mail calls
            $this->outputVoiceMailCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $extensionsInQueue);
        } elseif ($incoming == "abondondCalls") {
            //Agent abondond calls
            $this->outputAbondondCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $extensionsInQueue);
        } elseif ($incoming == "logged") {
            //Agent looged in calls
            $this->outputLoggedCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $agentId, $extensionsInQueue);
        } else {
            //Agnt DND records are here
            $this->outputDndCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queueId, $agentEx, $agentId, $extensionsInQueue);
        }
    }

    /**
     * 
     * @param type $callType
     * @param type $path
     * @description common function for all types of calls to generate the csv file.
     * author Nuwan.
     * Since 25/03/2019.
     */
    public function readCsvFile($callType, $path) {
      
        header('Content-Type: text/csv');
        if ($callType == "incoming") {
            header("Content-Disposition: attachment; filename=Answered_Calls.csv");
        } elseif ($callType == "outBound") {
            header("Content-Disposition: attachment; filename=outbound_Calls.csv");
        } elseif ($callType == "missed") {
            header("Content-Disposition: attachment; filename=missed_Calls.csv");
        } elseif ($callType == "ivr") {
            header("Content-Disposition: attachment; filename=Ivr_Calls.csv");
        } elseif ($callType == "voiceMail") {
            header("Content-Disposition: attachment; filename=voice_mail_Calls.csv");
        } elseif ($callType == "abondond") {
            header("Content-Disposition: attachment; filename=Abondond_Calls.csv");
        } elseif ($callType == "logged") {
            header("Content-Disposition: attachment; filename=Loggedin_Calls.csv");
        } else {
            header("Content-Disposition: attachment; filename=Dnd_Calls.csv");
        }
        header("Pragma: no-cache");

        readfile($path);
    }

    /**
     * 
     * @return type
     * 
     * @author Nuwan
     * @since 26-02-2019
     */
//Save answered calls records to a CSV file
    private function outputAnsweredCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queue_id, $agentEx, $selectedChannelsArray, $extensionsInQueue) {

        $id = yii::$app->session->get("user_id");
        $currentDateFrom = $dateFrom;
        $currentTimeFrom = $timeFrom;
        $currentDateTime = $currentDateFrom . " " . $currentTimeFrom;
        $currentDateTimeObj = strtotime($currentDateTime);
        $endDateTime = $dateTo . " " . $timeTo;
        $endDateTimeObj = strtotime($endDateTime);


        $directoryName = "fileoutputs\csv\\" . $id . "\\"; //set the directory path
        //check whether the directory is available if not then create the directory by using the user id
        if (!is_dir($directoryName)) {
            mkdir($directoryName, 0755, true);
        }
        $path = "fileoutputs\csv\\" . $id . "\answered_calls.csv";
        $linkAddress = "fileoutputs/csv/" . $id . "/answered_calls.csv";
        $file = fopen($path, "w");
        $tableLineArray = array();

        $headerArray = array(
            "Caller Number",
            "Answered Extension",
            "Started Time",
            "Ended Time",
            "Duration (Seconds)"
        );
        fputcsv($file, $headerArray);

        while ($currentDateTimeObj <= $endDateTimeObj) {

            $answered_calls_set = cdr::getAgentAnsweredCallsDataBetweenDates($currentDateFrom, $currentDateFrom, $currentTimeFrom, $timeTo, $agentEx, NULL, "Data", $extensionsInQueue, $selectedChannelsArray);

            $answered_calls_final = array();

            if (count($answered_calls_set) > 0) {
                foreach ($answered_calls_set as $line) {
                    $temp = array(
                        "src" => $line->src,
                        "dst" => $line->dst,
                        "start" => $line->start,
                        "end" => $line->end,
                        "duration" => $line->duration,
                    );
                    array_push($answered_calls_final, $temp);
                }
            }

            usort($answered_calls_final, function($a, $b) {
                return ($a['end'] < $b['end']) ? -1 : 1;
            });


            if (count($answered_calls_final) > 0) {
                for ($i = 0; $i < count($answered_calls_final); $i++) {
                    $tableLineArray = array();
                    array_push($tableLineArray, $answered_calls_final[$i]['src']);
                    array_push($tableLineArray, $answered_calls_final[$i]['dst']);
                    array_push($tableLineArray, date("d/m/Y H:i", strtotime($answered_calls_final[$i]['start'])));
                    array_push($tableLineArray, date("d/m/Y H:i", strtotime($answered_calls_final[$i]['end'])));
                    array_push($tableLineArray, $answered_calls_final[$i]['duration']);
                    fputcsv($file, $tableLineArray);
                }
            }

            $nextDateTimeFromObj = strtotime($currentDateTime . " + 1 days");
            $nextDateFrom = date("Y-m-d", $nextDateTimeFromObj);
            $nextTimeFrom = date("H:i:s", $nextDateTimeFromObj);
            $currentDateTimeObj = $nextDateTimeFromObj;
            $currentDateFrom = $nextDateFrom;
            $currentTimeFrom = $nextTimeFrom;
            $currentDateTime = $nextDateFrom . " " . $nextTimeFrom;
        }

        $this->readCsvFile("incoming", $path);
    }

//Save outbound calls to a CSV file
    private function outputOutboundCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queue_id, $agentEx, $extensionsInQueue) {

        $id = yii::$app->session->get("user_id");
        $currentDateFrom = $dateFrom;
        $currentTimeFrom = $timeFrom;
        $currentDateTime = $currentDateFrom . " " . $currentTimeFrom;
        $currentDateTimeObj = strtotime($currentDateTime);
        $endDateTime = $dateTo . " " . $timeTo;
        $endDateTimeObj = strtotime($endDateTime);

        $directoryName = "fileoutputs\csv\\" . $id . "\\"; //set the directory path
        //check whether the directory is available if not then create the directory by using the user id
        if (!is_dir($directoryName)) {
            mkdir($directoryName, 0755, true);
        }
        $path = "fileoutputs\csv\\" . $id . "\outbound_calls.csv";
        $linkAddress = "fileoutputs/csv/" . $id . "/outbound_calls.csv";
        $file = fopen($path, "w");

        $tableLineArray = array();
        $headerArray = array(
            "Source Extension",
            "Desination Number",
            "Started Time",
            "Answered Time",
            "Ended Time",
            "Duration (Seconds)"
        );
        fputcsv($file, $headerArray);
        while ($currentDateTimeObj <= $endDateTimeObj) {

            $outgoing_calls = cdr::getOutgoingCallsListByDate($agentEx, $currentDateFrom, $currentDateFrom, $currentTimeFrom, $timeTo, $extensionsInQueue);


            usort($outgoing_calls, function($a, $b) {
                return ($a['end'] < $b['end']) ? -1 : 1;
            });


            if (count($outgoing_calls) > 0) {
//            print_r($outgoing_calls);

                for ($i = 0; $i < count($outgoing_calls); $i++) {
                    $tableLineArray = array();
                    array_push($tableLineArray, $this->getExtensionNumberFromOutgoingCdrChannel($outgoing_calls[$i]['channel']));
                    array_push($tableLineArray, $outgoing_calls[$i]['dst']);
                    array_push($tableLineArray, date("d/m/Y H:i", strtotime($outgoing_calls[$i]['start'])));
                    array_push($tableLineArray, ($outgoing_calls[$i]['answer'] == NULL ? "" : date("d/m/Y H:i", strtotime($outgoing_calls[$i]['answer']))));
                    array_push($tableLineArray, date("d/m/Y H:i", strtotime($outgoing_calls[$i]['end'])));
                    array_push($tableLineArray, $outgoing_calls[$i]['duration']);
                    fputcsv($file, $tableLineArray);
                }
            }

            $nextDateTimeFromObj = strtotime($currentDateTime . " + 1 days");
            $nextDateFrom = date("Y-m-d", $nextDateTimeFromObj);
            $nextTimeFrom = date("H:i:s", $nextDateTimeFromObj);
            $currentDateTimeObj = $nextDateTimeFromObj;
            $currentDateFrom = $nextDateFrom;
            $currentTimeFrom = $nextTimeFrom;
            $currentDateTime = $nextDateFrom . " " . $nextTimeFrom;
        }

        $this->readCsvFile("outBound", $path);
    }

//Save missed calls to a CSV file
    private function outputMissedCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queue_id, $agentEx, $extensionsInQueue) {

        $id = yii::$app->session->get("user_id");
        $currentDateFrom = $dateFrom;
        $currentTimeFrom = $timeFrom;
        $currentDateTime = $currentDateFrom . " " . $currentTimeFrom;
        $currentDateTimeObj = strtotime($currentDateTime);
        $endDateTime = $dateTo . " " . $timeTo;
        $endDateTimeObj = strtotime($endDateTime);

        $directoryName = "fileoutputs\csv\\" . $id . "\\"; //set the directory path
        //check whether the directory is available if not then create the directory by using the user id
        if (!is_dir($directoryName)) {
            mkdir($directoryName, 0755, true);
        }
        $path = "fileoutputs\csv\\" . $id . "\missed_calls.csv";

        $linkAddress = "fileoutputs/csv/" . $id . "/missed_calls.csv";


        $file = fopen($path, "w");

        $tableLineArray = array();
        $headerArray = array(
            "Datetime",
            "Caller Number"
        );
        fputcsv($file, $headerArray);
        while ($currentDateTimeObj <= $endDateTimeObj) {

            $missedCalls = cdr::getAllMissedCallsDataBetweenDates($currentDateFrom, $currentDateFrom, $currentTimeFrom, $timeTo, NULL, "Data", $extensionsInQueue);

            $missedCalls_final = array();

            if (count($missedCalls) > 0) {
                foreach ($missedCalls as $line) {
                    $temp = array(
                        "end" => $line->end,
                        "src" => $line->src,
                    );
                    array_push($missedCalls_final, $temp);
                }
            }

            usort($missedCalls_final, function($a, $b) {
                return ($a['end'] < $b['end']) ? -1 : 1;
            });

            if (count($missedCalls_final) > 0) {
                for ($i = 0; $i < count($missedCalls_final); $i++) {
                    $tableLineArray = array();
                    array_push($tableLineArray, date("d/m/Y H:i", strtotime($missedCalls_final[$i]['end'])));
                    array_push($tableLineArray, $missedCalls_final[$i]['src']);
                    fputcsv($file, $tableLineArray);
                }
            }


            $nextDateTimeFromObj = strtotime($currentDateTime . " + 1 days");
            $nextDateFrom = date("Y-m-d", $nextDateTimeFromObj);
            $nextTimeFrom = date("H:i:s", $nextDateTimeFromObj);
            $currentDateTimeObj = $nextDateTimeFromObj;
            $currentDateFrom = $nextDateFrom;
            $currentTimeFrom = $nextTimeFrom;
            $currentDateTime = $nextDateFrom . " " . $nextTimeFrom;
        }

        $this->readCsvFile("missed", $path);
    }

//Sava a csv file of IVR Received calls
    public function outputIvrReceivedCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queue_id, $agentEx, $extensionsInQueue) {

        $id = yii::$app->session->get("user_id");
        $currentDateFrom = $dateFrom;
        $currentTimeFrom = $timeFrom;
        $currentDateTime = $currentDateFrom . " " . $currentTimeFrom;
        $currentDateTimeObj = strtotime($currentDateTime);
        $endDateTime = $dateTo . " " . $timeTo;
        $endDateTimeObj = strtotime($endDateTime);

        $directoryName = "fileoutputs\csv\\" . $id . "\\"; //set the directory path
        //check whether the directory is available if not then create the directory by using the user id
        if (!is_dir($directoryName)) {
            mkdir($directoryName, 0755, true);
        }
        $path = "fileoutputs\csv\\" . $id . "\ivr_calls.csv";
        $linkAddress = "fileoutputs/csv/" . $id . "/ivr_calls.csv";
        $file = fopen($path, "w");

        $tableLineArray = array();

        $headerArray = array(
            "Caller Number",
            "Answered Extension",
            "Started Time",
            "Ended Time",
            "Duration (Seconds)"
        );
        fputcsv($file, $headerArray);
        $ivr_calls_set = null;

        while ($currentDateTimeObj <= $endDateTimeObj) {

            $ivrNumbers = Queue_ivr_number::getIvrNumbersOfQueue($queue_id);

            if ($ivrNumbers != false) {

                $ivr_calls_set = cdr:: getIvrAnsweredCallsFromCustomers($currentDateFrom, $currentDateFrom, $currentTimeFrom, $timeTo, NULL, "Data", $ivrNumbers);
            }
            $ivr_calls_final = array();

            if (count($ivr_calls_set) > 0) {
                foreach ($ivr_calls_set as $line) {
                    $temp = array(
                        "src" => $line->src,
                        "dst" => $line->dst,
                        "start" => $line->start,
                        "end" => $line->end,
                        "duration" => $line->duration,
                    );
                    array_push($ivr_calls_final, $temp);
                }
            }

            usort($ivr_calls_final, function($a, $b) {
                return ($a['end'] < $b['end']) ? -1 : 1;
            });


            if (count($ivr_calls_final) > 0) {
                for ($i = 0; $i < count($ivr_calls_final); $i++) {
                    $tableLineArray = array();
                    array_push($tableLineArray, $ivr_calls_final[$i]['src']);
                    array_push($tableLineArray, $ivr_calls_final[$i]['dst']);
                    array_push($tableLineArray, date("d/m/Y H:i", strtotime($ivr_calls_final[$i]['start'])));
                    array_push($tableLineArray, date("d/m/Y H:i", strtotime($ivr_calls_final[$i]['end'])));
                    array_push($tableLineArray, $ivr_calls_final[$i]['duration']);
                    fputcsv($file, $tableLineArray);
                }
            }

            $nextDateTimeFromObj = strtotime($currentDateTime . " + 1 days");
            $nextDateFrom = date("Y-m-d", $nextDateTimeFromObj);
            $nextTimeFrom = date("H:i:s", $nextDateTimeFromObj);
            $currentDateTimeObj = $nextDateTimeFromObj;
            $currentDateFrom = $nextDateFrom;
            $currentTimeFrom = $nextTimeFrom;
            $currentDateTime = $nextDateFrom . " " . $nextTimeFrom;
        }

        $this->readCsvFile("ivr", $path);
    }

//Save voice mail calls to a CSV file    
    public function outputVoiceMailCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queue_id, $agentEx, $extensionsInQueue) {

        $id = yii::$app->session->get("user_id");
        $currentDateFrom = $dateFrom;
        $currentTimeFrom = $timeFrom;
        $currentDateTime = $currentDateFrom . " " . $currentTimeFrom;
        $currentDateTimeObj = strtotime($currentDateTime);
        $endDateTime = $dateTo . " " . $timeTo;
        $endDateTimeObj = strtotime($endDateTime);

        $directoryName = "fileoutputs\csv\\" . $id . "\\"; //set directory path
        //check whether the directory is available if not then create the directory by using the user id
        if (!is_dir($directoryName)) {
            mkdir($directoryName, 0755, true);
        }
        $path = "fileoutputs\csv\\" . $id . "\Voicemail_calls.csv";
        $linkAddress = "fileoutputs/csv/" . $id . "/Voicemail_calls.csv";

        $file = fopen($path, "w");

        $tableLineArray = array();

        $headerArray = array(
            "Caller Number",
            "Answered Extension",
            "Started Time",
            "Ended Time",
            "Duration (Seconds)"
        );
        fputcsv($file, $headerArray);


        $voiceMailCalls_set = null;

        while ($currentDateTimeObj <= $endDateTimeObj) {
//         
            $voiceMailExtensions = Voicemail_extension::getVoicemailNumbersOfQueue($queue_id);

//            print_r( $voiceMailExtensions);

            if ($voiceMailExtensions != false) {

                $voiceMailCalls_set = cdr::getVoiceMailMessagesFromCustomers($currentDateFrom, $currentDateFrom, $currentTimeFrom, $timeTo, NULL, "Data", $voiceMailExtensions); //sends the same currentDateFrom to the fromDate and the toDate for a special reason(refer line number 1521)
            }
            $voiceMailCalls_final = array();

            if (count($voiceMailCalls_set) > 0) {
                foreach ($voiceMailCalls_set as $line) {
                    $temp = array(
                        "src" => $line->src,
                        "dst" => $line->dst,
                        "start" => $line->start,
                        "end" => $line->end,
                        "duration" => $line->duration,
                    );
                    array_push($voiceMailCalls_final, $temp);
                }
            }

            usort($voiceMailCalls_final, function($a, $b) {
                return ($a['end'] < $b['end']) ? -1 : 1;
            });


            if (count($voiceMailCalls_final) > 0) {
                for ($i = 0; $i < count($voiceMailCalls_final); $i++) {
                    $tableLineArray = array();
                    array_push($tableLineArray, $voiceMailCalls_final[$i]['src']);
                    array_push($tableLineArray, $voiceMailCalls_final[$i]['dst']);
                    array_push($tableLineArray, date("d/m/Y H:i", strtotime($voiceMailCalls_final[$i]['start'])));
                    array_push($tableLineArray, date("d/m/Y H:i", strtotime($voiceMailCalls_final[$i]['end'])));
                    array_push($tableLineArray, $voiceMailCalls_final[$i]['duration']);
                    fputcsv($file, $tableLineArray);
                }
            }

            $nextDateTimeFromObj = strtotime($currentDateTime . " + 1 days");
            $nextDateFrom = date("Y-m-d", $nextDateTimeFromObj);
            $nextTimeFrom = date("H:i:s", $nextDateTimeFromObj);
            $currentDateTimeObj = $nextDateTimeFromObj;
            $currentDateFrom = $nextDateFrom;
            $currentTimeFrom = $nextTimeFrom;
            $currentDateTime = $nextDateFrom . " " . $nextTimeFrom;
        }


        $this->readCsvFile("voiceMail", $path);
    }

//Create a CSV file to abondond calls 
    public function outputAbondondCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queue_id, $agentEx, $extensionsInQueue) {

        $id = yii::$app->session->get("user_id");
        $currentDateFrom = $dateFrom;
        $currentTimeFrom = $timeFrom;
        $currentDateTime = $currentDateFrom . " " . $currentTimeFrom;
        $currentDateTimeObj = strtotime($currentDateTime);
        $endDateTime = $dateTo . " " . $timeTo;
        $endDateTimeObj = strtotime($endDateTime);

        $directoryName = "fileoutputs\csv\\" . $id . "\\"; //Set the directory path
        //check whether the directory is available if not then create the directory by using the user id
        if (!is_dir($directoryName)) {
            mkdir($directoryName, 0755, true);
        }
        $path = "fileoutputs\csv\\" . $id . "\abondond_calls.csv";
        $linkAddress = "fileoutputs/csv/" . $id . "/abondond_calls.csv";

        $file = fopen($path, "w");



        $tableLineArray = array();
        $headerArray = array(
            "Caller Number",
            "Date and Time"
        );
        fputcsv($file, $headerArray);
        while ($currentDateTimeObj <= $endDateTimeObj) {

            $abondondCalls = cdr::getAbandonedCallsOfAnAgentBetweenDates($currentDateFrom, $currentDateFrom, $currentTimeFrom, $timeTo, $agentEx, NULL, "Data", $extensionsInQueue); //sends the same currentDateFrom to the fromDate and the toDate for a special reason(refer line number 1596)

            $abondondCalls_final = array();

            if (count($abondondCalls) > 0) {
                foreach ($abondondCalls as $line) {
                    $temp = array(
                        "src" => $line->src,
                        "dateTime" => $line->timestamp,
                    );
                    array_push($abondondCalls_final, $temp);
                }
            }

            usort($abondondCalls_final, function($a, $b) {
                return ($a['end'] < $b['end']) ? -1 : 1;
            });

            if (count($abondondCalls_final) > 0) {
                for ($i = 0; $i < count($abondondCalls_final); $i++) {
                    $tableLineArray = array();
                    array_push($tableLineArray, $abondondCalls_final[$i]['src']);
                    array_push($tableLineArray, date("d/m/Y H:i", strtotime($abondondCalls_final[$i]['dateTime'])));
                    fputcsv($file, $tableLineArray);
                }
            }


            $nextDateTimeFromObj = strtotime($currentDateTime . " + 1 days");
            $nextDateFrom = date("Y-m-d", $nextDateTimeFromObj);
            $nextTimeFrom = date("H:i:s", $nextDateTimeFromObj);
            $currentDateTimeObj = $nextDateTimeFromObj;
            $currentDateFrom = $nextDateFrom;
            $currentTimeFrom = $nextTimeFrom;
            $currentDateTime = $nextDateFrom . " " . $nextTimeFrom;
        }


        $this->readCsvFile("abondond", $path);
    }

//Create a CSV file to logged in calls 
    public function outputLoggedCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queue_id, $agentEx, $agentId, $extensionsInQueue, $agentIdsInQueue) {

        $id = yii::$app->session->get("user_id");
        $currentDateFrom = $dateFrom;
        $currentTimeFrom = $timeFrom;
        $currentDateTime = $currentDateFrom . " " . $currentTimeFrom;
        $currentDateTimeObj = strtotime($currentDateTime);
        $endDateTime = $dateTo . " " . $timeTo;
        $endDateTimeObj = strtotime($endDateTime);

        $directoryName = "fileoutputs\csv\\" . $id . "\\"; //Set the directory path
        //check whether the directory is available if not then create the directory by using the user id
        if (!is_dir($directoryName)) {
            mkdir($directoryName, 0755, true);
        }
        $path = "fileoutputs\csv\\" . $id . "\logged_calls.csv";
        $linkAddress = "fileoutputs/csv/" . $id . "/logged_calls.csv";

        $file = fopen($path, "w");



        $tableLineArray = array();
        $headerArray = array(
            "Caller Number",
            "Date and Time"
        );
        fputcsv($file, $headerArray);
        while ($currentDateTimeObj <= $endDateTimeObj) {

            $loggedCalls = Logged_in_users::getLoginRecordsByDate($agentId, $currentDateFrom, $currentDateFrom, $currentTimeFrom, $timeTo, $agentIdsInQueue); //sends the same currentDateFrom to the fromDate and the toDate for a special reason(refer line number 1671)

            $loggedCalls_final = array();

            if (count($loggedCalls) > 0) {
                foreach ($loggedCalls as $line) {
                    $temp = array(
                        "loggedInTime" => $line->logged_in_time,
                        "timeSignature" => $line->time_signature,
                    );
                    array_push($loggedCalls_final, $temp);
                }
            }

            usort($loggedCalls_final, function($a, $b) {
                return ($a['end'] < $b['end']) ? -1 : 1;
            });

            if (count($loggedCalls_final) > 0) {
                for ($i = 0; $i < count($loggedCalls_final); $i++) {
                    $tableLineArray = array();
                    array_push($tableLineArray, date("d/m/Y H:i", strtotime($loggedCalls_final[$i]['logedInTime'])));
                    array_push($tableLineArray, date("d/m/Y H:i", strtotime($loggedCalls_final[$i]['timeSignature'])));
                    fputcsv($file, $tableLineArray);
                }
            }


            $nextDateTimeFromObj = strtotime($currentDateTime . " + 1 days");
            $nextDateFrom = date("Y-m-d", $nextDateTimeFromObj);
            $nextTimeFrom = date("H:i:s", $nextDateTimeFromObj);
            $currentDateTimeObj = $nextDateTimeFromObj;
            $currentDateFrom = $nextDateFrom;
            $currentTimeFrom = $nextTimeFrom;
            $currentDateTime = $nextDateFrom . " " . $nextTimeFrom;
        }

        $this->readCsvFile("logged", $path);
    }

//Create a CSV file to Agent DND records   
    public function outputDndCallsToFile($dateFrom, $timeFrom, $dateTo, $timeTo, $queue_id, $agentEx, $agentId, $extensionsInQueue, $agentIdsInQueue) {

        $id = yii::$app->session->get("user_id");
        $currentDateFrom = $dateFrom;
        $currentTimeFrom = $timeFrom;
        $currentDateTime = $currentDateFrom . " " . $currentTimeFrom;
        $currentDateTimeObj = strtotime($currentDateTime);
        $endDateTime = $dateTo . " " . $timeTo;
        $endDateTimeObj = strtotime($endDateTime);

        $directoryName = "fileoutputs\csv\\" . $id . "\\"; //Set the directory path
        //check whether the directory is available if not then create the directory by using the user id 
        if (!is_dir($directoryName)) {
            mkdir($directoryName, 0755, true);
        }
        $path = "fileoutputs\csv\\" . $id . "\dnd_calls.csv";
        $linkAddress = "fileoutputs/csv/" . $id . "/dnd_calls.csv";

        $file = fopen($path, "w");

        $tableLineArray = array();
        $headerArray = array(
            "DND Mode",
            "Date and Time"
        );
        fputcsv($file, $headerArray);
        while ($currentDateTimeObj <= $endDateTimeObj) {

            $dndCalls = Dnd_records::getDndRecordsByDate($agentId, $currentDateFrom, $currentDateFrom, $currentTimeFrom, $timeTo, $agentIdsInQueue); //sends the same currentDateFrom to the fromDate and the toDate for a special reason(refer line number 1746)

            $dndCalls_final = array();

            if (count($dndCalls) > 0) {
                foreach ($dndCalls as $line) {
                    $temp = array(
                        "dndMode" => $line->dnd_mode,
                        "timeStamp" => $line->timestamp,
                    );
                    array_push($dndCalls_final, $temp);
                }
            }

            usort($dndCalls_final, function($a, $b) {
                return ($a['end'] < $b['end']) ? -1 : 1;
            });

            if (count($dndCalls_final) > 0) {
                for ($i = 0; $i < count($dndCalls_final); $i++) {
                    $tableLineArray = array();
                    array_push($tableLineArray, $dndCalls_final[$i]['dndMode']);
                    array_push($tableLineArray, date("d/m/Y H:i", strtotime($dndCalls_final[$i]['timeStamp'])));
                    fputcsv($file, $tableLineArray);
                }
            }


            $nextDateTimeFromObj = strtotime($currentDateTime . " + 1 days");
            $nextDateFrom = date("Y-m-d", $nextDateTimeFromObj);
            $nextTimeFrom = date("H:i:s", $nextDateTimeFromObj);
            $currentDateTimeObj = $nextDateTimeFromObj;
            $currentDateFrom = $nextDateFrom;
            $currentTimeFrom = $nextTimeFrom;
            $currentDateTime = $nextDateFrom . " " . $nextTimeFrom;
        }

        $this->readCsvFile("dnd", $path);
    }

    /**
     * End of One time functions
     */
    public function actionSearchmissingaudio() {

        if (Yii::$app->session->has('user_id') == false) {
            $this->redirect('index.php?r=user/login_view');
        } else if (Yii::$app->session->get('user_role') != '1') {
            $this->redirect('index.php?r=user/login_view');
        }
        $agentsList = call_center_user::getUserData(Yii::$app->session->get("user_role"));
        $extenstionQueues = Extension_queue::getAllAvailableQueuesData();
        // Assign queue lists for executive user
        if (Yii::$app->session->get('user_role') == '5') {
            $extenstionQueues = Extension_queue::getAllAvailableQueuesDataOfSelectedExecutive(Yii::$app->session->get('user_id'));
            $agentsList = call_center_user::getAgentsOfSelectedQueues($extenstionQueues);
        }
        if (Yii::$app->session->get('user_role') == '3') {
            // Agent is a supervisor
            $channelsInfo = $this->getChannelsInfoOfQueue(Yii::$app->session->get("extQueueId"));
        } else {
            $channelsInfo = NULL;
        }
        return $this->render('viewMissingFiles', ['agents' => $agentsList, "queues" => $extenstionQueues, "channelsInfo" => $channelsInfo]);
    }

    public function actionPerformanceoverviewmissingaudio() {
        if (Yii::$app->session->has('user_id') == false) {
            $this->redirect('index.php?r=user/login_view');
        } else if (Yii::$app->session->get('user_role') == '2' || Yii::$app->session->get('user_role') == '4') {
            $this->redirect('index.php?r=user/login_view');
        }

        $params = $_REQUEST;

        if (Yii::$app->request->post('tableType') == "Answered") {
            $columns = array(
                0 => 'src',
                1 => 'start',
                2 => 'answer',
                3 => 'dst',
                4 => 'end',
                5 => 'duration',
            );
        } else if (Yii::$app->request->post('tableType') == "Outgoing") {
            $columns = array(
                0 => 'dst',
                1 => 'channel',
                2 => 'start',
                3 => 'answer',
                4 => 'end',
                5 => 'duration'
            );
        }

        if (Yii::$app->request->post('outputType') == "byDate") {

            $agent_id = Yii::$app->request->post('agent_id');
            $agent_ex = Yii::$app->request->post('agentex');
            $agent_name = Yii::$app->request->post('agentname');
            $searchContact = (Yii::$app->request->post('contactNum') == "" || Yii::$app->request->post('contactNum') == NULL ? NULL : $this->removeFirstZeroFilterWhitespaceIfExistsInContact(Yii::$app->request->post('contactNum')));
            $fromDate = trim(Yii::$app->request->post('fromdate'), " ");
            $toDate = trim(Yii::$app->request->post('todate'), " ");

            $fromTime = (Yii::$app->request->post("fromTime") != NULL ? Yii::$app->request->post("fromTime") : "00:00:00");
            $toTime = (Yii::$app->request->post("toTime") != NULL ? Yii::$app->request->post("toTime") : "23:59:59");

            $toDateTimeaccess = Audiotransferroutinelog::getRoutineLastExecutedTime();

            if (strtotime($toDateTimeaccess) < strtotime($toDate . ' ' . $toTime)) {
                $toTime = substr($toDateTimeaccess, 11, 8);
            }

            $selectedChannelsArray = (Yii::$app->request->post("channels") != "0" ? explode(",", Yii::$app->request->post("channels")) : NULL);

            $extensionsInQueue = (Yii::$app->session->get("user_role") == '1' || Yii::$app->session->get("user_role") == '5' ? NULL : Ext_queue_intermediate::getAllExtensionsOfTheQueue(Yii::$app->session->get("extQueueId")));
            $agentIdsInQueue = (Yii::$app->session->get("user_role") == '1' || Yii::$app->session->get("user_role") == '5' ? NULL : call_center_user::getAgentIdsFromQueueId(Yii::$app->session->get("extQueueId")));

            if (Yii::$app->session->get('user_role') == '5') {
                //user is executive 
                $queuesIds = Extension_queue::getAllAvailableQueuesIdOfSelectedExecutive(Yii::$app->session->get('user_id'));
                $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueueArray($queuesIds);
                $agentIdsInQueue = call_center_user::getAgentsOfSelectedQueues($queuesIds);
            }


            if ($extensionsInQueue == NULL && $agent_id != 0) {
                // individual agent listing,
                $queueId = Ext_queue_intermediate::getExtensionQueueIdOfVOIPExtension($agent_ex);
                $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueue($queueId);
                $agentIdsInQueue = call_center_user::getAgentIdsFromQueueId($queueId);
            }
        } else if (Yii::$app->request->post('outputType') == "byQueue") {

            if (Yii::$app->session->get('user_role') == "1") {
                // user is admin
                $agent_id = (Yii::$app->request->post("agent_id") == NULL || Yii::$app->request->post("agent_id") == "" ? 0 : Yii::$app->request->post("agent_id"));
                $agent_ex = (Yii::$app->request->post("agentex") == NULL || Yii::$app->request->post("agentex") == "" ? 0 : Yii::$app->request->post("agentex"));
                $agent_name = Yii::$app->request->post('agentname');
                $searchContact = (Yii::$app->request->post("contactNum") == NULL || Yii::$app->request->post("contactNum") == "" ? NULL : $this->removeFirstZeroFilterWhitespaceIfExistsInContact(Yii::$app->request->post("contactNum")));
                $queue_id = (Yii::$app->request->post("queue_id") == "" ? NULL : Yii::$app->request->post("queue_id"));
                if (Yii::$app->request->post("fromdate") == NULL || Yii::$app->request->post("fromdate") == "") {
                    $fromDate = date("Y-m-01");
                } else {
                    $fromDate = trim(Yii::$app->request->post('fromdate'), " ");
                }

                if (Yii::$app->request->post("todate") == NULL || Yii::$app->request->post("todate") == "") {
                    $toDate = date("Y-m-d");
                } else {
                    $toDate = trim(Yii::$app->request->post('todate'), " ");
                }
            } else if (Yii::$app->session->get('user_role') == "3") {
                // user is supervisor
                $agent_id = 0;
                $agent_ex = 0;
                $agent_name = "";
                $searchContact = NULL;
                $queue_id = Yii::$app->session->get('extQueueId');
                $fromDate = date("Y-m-01");
                $toDate = date("Y-m-d");
            } else if (Yii::$app->session->get('user_role') == "5") {
                // user is executive
                $agent_id = (Yii::$app->request->post("agent_id") == NULL || Yii::$app->request->post("agent_id") == "" ? 0 : Yii::$app->request->post("agent_id"));
                $agent_ex = (Yii::$app->request->post("agentex") == NULL || Yii::$app->request->post("agentex") == "" ? 0 : Yii::$app->request->post("agentex"));
                $agent_name = Yii::$app->request->post('agentname');
                $searchContact = (Yii::$app->request->post("contactNum") == NULL || Yii::$app->request->post("contactNum") == "" ? NULL : $this->removeFirstZeroFilterWhitespaceIfExistsInContact(Yii::$app->request->post("contactNum")));
                $queue_id = (Yii::$app->request->post("queue_id") == "" ? NULL : Yii::$app->request->post("queue_id"));
                if (Yii::$app->request->post("fromdate") == NULL || Yii::$app->request->post("fromdate") == "") {
                    $fromDate = date("Y-m-01");
                } else {
                    $fromDate = trim(Yii::$app->request->post('fromdate'), " ");
                }

                if (Yii::$app->request->post("todate") == NULL || Yii::$app->request->post("todate") == "") {
                    $toDate = date("Y-m-d");
                } else {
                    $toDate = trim(Yii::$app->request->post('todate'), " ");
                }
            }

            $fromTime = (Yii::$app->request->post("fromTime") != NULL ? Yii::$app->request->post("fromTime") : "00:00:00");
            $toTime = (Yii::$app->request->post("toTime") != NULL ? Yii::$app->request->post("toTime") : "23:59:59");

            $toDateTimeaccess = Audiotransferroutinelog::getRoutineLastExecutedTime();

            if (strtotime($toDateTimeaccess) < strtotime($toDate . ' ' . $toTime)) {
                $toTime = substr($toDateTimeaccess, 11, 8);
            }

            $selectedChannelsArray = (Yii::$app->request->post("channels") != "0" ? explode(",", Yii::$app->request->post("channels")) : NULL);

            $agentIdsInQueue = call_center_user::getAgentIdsFromQueueId($queue_id);
            $extensionsInQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueue($queue_id);
        }


        if (Yii::$app->request->post('tableType') == "Answered") {

            $answered_calls_set = cdr::getAgentAnsweredMissingAudioCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agent_ex, $searchContact, "Data", $extensionsInQueue, $selectedChannelsArray, $params['length'], $params['start'], $columns[$params['order'][0]['column']], $params['order'][0]['dir'], 1);

            $countAnsweredData = cdr::getAgentAnsweredMissingAudioCallsDataBetweenDates($fromDate, $toDate, $fromTime, $toTime, $agent_ex, $searchContact, "Count", $extensionsInQueue, $selectedChannelsArray, null, null, null, null, 1);

            $data = $this->getAnsweredCallsForDatatableJson($agent_ex, $answered_calls_set, $countAnsweredData, $agent_name);
            echo $data;
        } else if (Yii::$app->request->post('tableType') == "Outgoing") {

            $outgoing_calls = cdr::getOutgoingMissingAudioCallsListByDate($agent_ex, $fromDate, $toDate, $fromTime, $toTime, $searchContact, $extensionsInQueue, "Data", $params['length'], $params['start'], $columns[$params['order'][0]['column']], $params['order'][0]['dir'], 1);

            $countOutgoingCallsData = cdr::getOutgoingMissingAudioCallsListByDate($agent_ex, $fromDate, $toDate, $fromTime, $toTime, $searchContact, $extensionsInQueue, "Count", null, null, null, null, null);


            $data = $this->getOutgoingCallsForDatatableJson($agent_ex, $outgoing_calls, $countOutgoingCallsData, $agent_name);
            echo $data;
        }
    }

    private function getAnsweredCallsForDatatableJson($agent_ex, $answered_calls_set, $countAnsweredData, $agent_name) {

        $answered_calls = array();
        if ($agent_ex == 0) {
            $agentNames = call_center_user::getAllAgentNamesAndExtensionsInAnArray();
        } else {
            $agentNames = NULL;
        }
        for ($i = 0; $i < count($answered_calls_set); $i++) {

            $agentName = NULL;
            if ($agentNames != NULL && count($agentNames) > 0) {
                foreach ($agentNames as $key) {
                    if ($key->voip_extension == $answered_calls_set[$i]['dst']) {
                        $agentName = $key->fullname;
                        break;
                    }
                }
            }
            $temp = array(
                'id' => $answered_calls_set[$i]['AcctId'],
                'start' => $answered_calls_set[$i]['start'],
                'src' => $answered_calls_set[$i]['src'],
                'answer' => $answered_calls_set[$i]['answer'],
                'duration' => $answered_calls_set[$i]['duration'],
                'name' => ($agentName != NULL ? $agentName : $answered_calls_set[$i]['dst']),
                'end' => $answered_calls_set[$i]['end'],
                'voip' => $answered_calls_set[$i]['dst']
            );
            array_push($answered_calls, $temp);
        }
        $answeredCallsFinal = array();
        for ($i = 0; $i < count($answered_calls); $i++) {

            if ($agent_ex == 0) {
                $outputName = $answered_calls[$i]['name'];
            } else {
                $outputName = $agent_name;
            }
            $temp = array(
                $answered_calls[$i]['src'],
                $answered_calls[$i]['start'],
                $answered_calls[$i]['answer'],
                ($outputName != $answered_calls[$i]['voip'] ? $outputName . ':' . $answered_calls[$i]['voip'] : $outputName),
                $answered_calls[$i]['end'],
                $answered_calls[$i]['duration'] . " Sec");
            array_push($answeredCallsFinal, $temp);
        }
        $result = [
            "draw" => Yii::$app->request->post("draw"),
            "recordsTotal" => $countAnsweredData,
            "recordsFiltered" => $countAnsweredData,
            "data" => $answeredCallsFinal
        ];

        return json_encode($result);
    }

    private function getOutgoingCallsForDatatableJson($agent_ex, $outgoing_calls, $countOutgoingCallsData, $agent_name) {
        if ($agent_ex == 0) {
            for ($i = 0; $i < count($outgoing_calls); $i++) {
                $name = $this->getExtensionFromCdrChannel($outgoing_calls[$i]['channel']);
                if ($name) {
                    $outgoing_calls[$i]['name'] = $name;
                } else {
                    $outgoing_calls[$i]['name'] = '';
                }
                $outgoing_calls[$i]['voip'] = $this->getExtensionNumberFromOutgoingCdrChannel($outgoing_calls[$i]['channel']);
            }
        } else {
            for ($i = 0; $i < count($outgoing_calls); $i++) {
                $outgoing_calls[$i]['voip'] = $agent_ex;
            }
        }
        $outgoing_calls_array = array();

        for ($i = 0; $i < count($outgoing_calls); $i++) {

            if ($agent_ex == 0) {
                $outputName = $outgoing_calls[$i]['name'];
            } else {
                $outputName = $agent_name;
            }

            $temp = array(
                $outgoing_calls[$i]['dst'],
                ($outputName != $outgoing_calls[$i]['voip'] ? $outputName . ':' . $outgoing_calls[$i]['voip'] : $outputName),
                $outgoing_calls[$i]['start'],
                $outgoing_calls[$i]['answer'],
                $outgoing_calls[$i]['end'],
                $outgoing_calls[$i]['duration'] . " Sec"
            );
            array_push($outgoing_calls_array, $temp);
        }
        $result = [
            "draw" => Yii::$app->request->post("draw"),
            "recordsTotal" => $countOutgoingCallsData,
            "recordsFiltered" => $countOutgoingCallsData,
            "data" => $outgoing_calls_array
        ];
        echo json_encode($result);
    }

}
