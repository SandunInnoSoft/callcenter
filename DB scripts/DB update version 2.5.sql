# This table keep contact list details

CREATE TABLE `contact_list` (
`id`  int NOT NULL AUTO_INCREMENT ,
`contact_number`  int NULL ,
`contact_name`  varchar(255) NULL ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `contact_list`
ADD COLUMN `created_date`  datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP AFTER `contact_name`;

