#This table will save and manage user requests 
# author: Vikum

CREATE TABLE `agent_requests` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `request_type` ENUM('lunch', 'washroom', 'sick', 'other') NULL,
  `requested_time` DATETIME NULL,
  `responded_time` DATETIME NULL,
  `approved_time_period` FLOAT NULL COMMENT 'Only for a sick break and other break. Value will be on minutes',
  `request_status` ENUM('approved', 'pending', 'denied', 'closed', 'taken') NULL COMMENT '\'Approved after supervisor approve the request, pending after agent requested the request, closed after closing the request\'. Denied after supervisor denied the request, taken once agent take the request',
  `taken_time` DATETIME NULL,
  `closed_time` DATETIME NULL,
  `created_date` DATETIME NULL,
  `active` INT NULL,
  PRIMARY KEY (`id`));
  
ALTER TABLE `agent_requests` 
ADD COLUMN `agent_id` INT NULL AFTER `closed_time`,
ADD COLUMN `supervisor_id` INT NULL AFTER `agent_id`;

ALTER TABLE `agent_requests` 
ADD COLUMN `consumed` INT NULL COMMENT 'This column has a flag value to make sure record fetch by server events' AFTER `active`;

