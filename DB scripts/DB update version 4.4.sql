# HNBGI Call Center System - DB Update Version 4.4
# @author Sandun
# @since 2018-07-26

-- Inserts new user role name "Executive" to roles table

INSERT INTO `roles` (`name`) VALUES ('Executive');

-- Creates new table to record 
CREATE TABLE `allowed_exec_extqueues` (
`id`  int NOT NULL AUTO_INCREMENT ,
`user_id`  int NULL ,
`allowed_ext_queue_id`  int NULL ,
`granted_by`  int NULL ,
PRIMARY KEY (`id`)
)
COMMENT='This table maintains the list of extensions queues which is allowed by the admin to a user to view reports'
;

-- ---------- END OF SCRIPT ----------------------------------