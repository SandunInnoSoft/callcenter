/**
* HNB call center database update version 4.7
* @author Sandun
* @since 2019-01-30
* @target main DB
*/

-- creates a new table to record missing calls notification email subscribers

CREATE TABLE `missed_calls_email_subscribers` (
`id`  int NOT NULL AUTO_INCREMENT,
`subsciber_name`  varchar(255) NULL ,
`email_address`  varchar(255) NOT NULL ,
`created_date`  datetime NULL ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`id`)
)
;

