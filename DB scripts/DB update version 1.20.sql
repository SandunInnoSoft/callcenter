# added timestamp column to record call time stamp
ALTER TABLE `call_records` 
ADD COLUMN `timestamp` VARCHAR(50) NULL AFTER `consumed`;
