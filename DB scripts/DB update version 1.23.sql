# This table records email log for the emails send every 15 minutes for misscalls
CREATE TABLE `miss_calls_email_log` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `numbers` INT NULL COMMENT 'Count number of missed calls recorded in the given time.',
  `timestamp` VARCHAR(50) NULL,
  `created_date_time` DATETIME NULL,
  `active` INT NULL DEFAULT 1,
  PRIMARY KEY (`id`));
