# HNB call center system DB update version 1.16
# @author Sandun
# @since 2017-07-28

# This adds 4 new columns to the call_center_user table

-- ------------- BEGIN SCRIPT ------------------------------------------
ALTER TABLE `call_center_user` 
ADD COLUMN `cli_number` INT NULL COMMENT 'This keeps the phone number of the call made from' AFTER `voip_extension`,
ADD COLUMN `policy_number` INT NULL COMMENT 'This keeps the phone number that is actually associated with the customer data in the customer database' AFTER `cli_number`,
ADD COLUMN `comment` VARCHAR(500) NULL COMMENT 'This keeps the typed comment of the call record' AFTER `policy_number`,
ADD COLUMN `cdr_id` INT NULL COMMENT 'This is the PBX db cdr row id associated with this call record' AFTER `comment`;
-- ----------------- END SCRIPT -------------------------------------------