# changed Call_record table end time default value to null
ALTER TABLE `call_records` CHANGE `call_end_time` `call_end_time` DATETIME NULL DEFAULT NULL;