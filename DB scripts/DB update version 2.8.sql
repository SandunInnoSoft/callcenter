/*
HNB call center DB update version 2.8
@since 2017-09-13
@author Sandun
*/

# This creates the tables with the struncture same as in the call events database

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for abandon_calls
-- ----------------------------
DROP TABLE IF EXISTS `abandon_calls`;
CREATE TABLE `abandon_calls` (
  `agent_id` varchar(45) DEFAULT NULL,
  `caller_number` varchar(45) DEFAULT NULL,
  `timestamp` varchar(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table ';

-- ----------------------------
-- Table structure for call_forwards
-- ----------------------------
DROP TABLE IF EXISTS `call_forwards`;
CREATE TABLE `call_forwards` (
  `caller_number` int(11) NOT NULL COMMENT 'caller cli number',
  `agent_extension` int(11) DEFAULT NULL COMMENT 'agent extension number',
  `time_stamp` varchar(100) NOT NULL DEFAULT '0' COMMENT 'Unix timestamp ',
  `created_date` datetime DEFAULT NULL COMMENT 'Current date time',
  `state` int(11) DEFAULT '1',
  PRIMARY KEY (`caller_number`,`time_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for call_queue
-- ----------------------------
DROP TABLE IF EXISTS `call_queue`;
CREATE TABLE `call_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for web_presence
-- ----------------------------
DROP TABLE IF EXISTS `web_presence`;
CREATE TABLE `web_presence` (
  `ext` varchar(32) NOT NULL,
  `state` varchar(16) NOT NULL,
  `cidnum` varchar(64) DEFAULT NULL,
  `cidname` varchar(64) DEFAULT NULL,
  `inorout` varchar(1) DEFAULT NULL,
  `callstart` int(11) DEFAULT NULL,
  PRIMARY KEY (`ext`),
  KEY `state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `web_presence` (`ext`, `state`) VALUES ('800', '0');
INSERT INTO `web_presence` (`ext`, `state`) VALUES ('801', '0');
INSERT INTO `web_presence` (`ext`, `state`) VALUES ('802', '0');
INSERT INTO `web_presence` (`ext`, `state`) VALUES ('803', '0');
INSERT INTO `web_presence` (`ext`, `state`) VALUES ('804', '0');
INSERT INTO `web_presence` (`ext`, `state`) VALUES ('805', '0');
INSERT INTO `web_presence` (`ext`, `state`) VALUES ('806', '0');
INSERT INTO `web_presence` (`ext`, `state`) VALUES ('807', '0');
INSERT INTO `web_presence` (`ext`, `state`) VALUES ('808', '0');
INSERT INTO `web_presence` (`ext`, `state`) VALUES ('809', '0');