#this query will add meeting/training request type
ALTER TABLE `agent_requests` 
CHANGE COLUMN `request_type` `request_type` ENUM('lunch', 'short', 'sick', 'other', 'meeting') NULL DEFAULT NULL ;
