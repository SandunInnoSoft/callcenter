#Prabath
#This sql script will be truncate `logged_in_users` table and alter the data type of `logged_in_time` column to varchar.

Truncate table `logged_in_users`;

ALTER TABLE `logged_in_users` 
CHANGE COLUMN `logged_in_time` `logged_in_time` VARCHAR(50) NULL DEFAULT NULL ;
