/**
* HNB General DB update version 4.3
* @author Sandun
* @target pbxreplicalocaldb
* @since 2018-4-20
* 
* This script will add an index to the start field to efficently retrieve datetime data faster
* Please note this script should be executed in root user
**/

ALTER TABLE `cdr`
ADD INDEX `start_datetime_index` (`start`) USING BTREE COMMENT 'This index is used to get data beween a date range in start field';

