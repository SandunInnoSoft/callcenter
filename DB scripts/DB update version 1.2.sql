#this remove customer_id from call_event_data and add it in call_records

ALTER TABLE `call_event_data`
DROP FOREIGN KEY `event_customer_key`;

ALTER TABLE `call_event_data` 
DROP COLUMN `customer_id`;

ALTER TABLE `call_records` 
ADD COLUMN `customer_id` INT NULL AFTER `case_id`;

ALTER TABLE `call_records`
ADD FOREIGN KEY (customer_id) REFERENCES customer(id);





