# HNB Call center DB update version 2.1
# @author Sandun
# @since 2017-08-13

-- This script creates a new table to keep DND mode on / off records in the database
CREATE TABLE `dnd_records` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NULL,
  `dnd_mode` ENUM('On', 'Off') NULL,
  `timestamp` DATETIME NULL,
  PRIMARY KEY (`id`))
COMMENT = 'This table keeps all the dnd on off records of all users for reportring and monitoring purposes';
