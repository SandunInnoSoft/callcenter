# HNB call center DB update 1.11
# @author Sandun
# @since 2017-07-20

# This script adds a new column `voip_extension` to the `call_center_user` table to keep the voip extension number of
# the agent / supervisor

ALTER TABLE `call_center_user` 
ADD COLUMN `voip_extension` VARCHAR(45) NULL AFTER `contact_number`;
