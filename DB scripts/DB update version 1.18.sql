# HNB Call center DB update V1.18
# @author Sandun
# @since 2017-07-31

# This script adds a new column to the DB table logged_in_users to keep active and inactive user logins
ALTER TABLE `logged_in_users` 
ADD COLUMN `active` TINYINT(1) NULL AFTER `time_signature`;
