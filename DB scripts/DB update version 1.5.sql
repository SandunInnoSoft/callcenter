# HNB call center DB update version 1.5
# @author Sandun
# @since 2017-06-30

# This creates a new table to keep the online users

create table logged_in_users(
id int(11) auto_increment primary key, 
user_id int(11) ,
user_logged_ip_address varchar(60) ,
logged_in_time datetime ,
time_signature varchar(50)
);