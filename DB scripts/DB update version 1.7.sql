# HNB call center app DB update version 1.7
# @author Sandun
# @since 2017-07-03

# This script adds a new column to keep the phone number of the user

ALTER TABLE `call_center_user` 
ADD COLUMN `contact_number` INT(10) NULL AFTER `created_date`;
