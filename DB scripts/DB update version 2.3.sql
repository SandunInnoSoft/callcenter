/*
Navicat MySQL Data Transfer

Source Server         : LOCALHOST
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : hnb

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2017-08-21 14:37:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for call_summary
-- ----------------------------
DROP TABLE IF EXISTS `call_summary`;
CREATE TABLE `call_summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `total_calls` int(11) NOT NULL,
  `answered_calls` int(11) NOT NULL,
  `unanswered_calls` int(11) NOT NULL,
  `average_waiting_time` float NOT NULL,
  `maximum_waiting_time` int(11) NOT NULL,
  `last_picked_call` float DEFAULT NULL,
  `created_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
