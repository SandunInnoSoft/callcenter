/**
* HNB call center database update version 4.6
* @author Sandun
* @since 2019-01-28
* @target logs DB
*/

-- alters the audiotransferroutinelog table to add a new column to track missing audio files count
ALTER TABLE `audiotransferroutinelog`
ADD COLUMN `missingAudioFilesFound`  int NULL AFTER `executionEndedTime`;

